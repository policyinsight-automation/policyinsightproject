package com.automation.pi.library;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotInteractableException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.Reporter;

import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;

public class WebActionUtils {

	WebDriver driver;
	ITestResult result;

	public WebActionUtils(WebDriver driver) {
		this.driver = driver;
	}

	public void extentTestName(String testName) {
		try {
			Reporter.log(testName, true);
			BaseTest.extentTest = BaseTest.extentReports.createTest(testName);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void TestCaseinfo(String description) {
		try {
			Reporter.log(description, true);
			BaseTest.extentTest.log(Status.INFO, MarkupHelper.createLabel("  INFO :" + description, ExtentColor.PINK));
		} catch (Exception e) {
			e.printStackTrace();		}
		
	
	}

	public void info(String description) {
		try {
			Reporter.log(description, true);
			BaseTest.extentTest.log(Status.INFO, MarkupHelper.createLabel("  INFO :" + description, ExtentColor.TEAL));
		} catch (Exception e) {
			e.printStackTrace();		}
		
	
	}

	public void pass(String description) {
		
		try {
			Reporter.log(description, true);
			BaseTest.extentTest.log(Status.PASS, MarkupHelper.createLabel("  PASSED :" + description, ExtentColor.GREEN));	
		} catch (Exception e) {
			e.printStackTrace();		}
		
	
	}

	public void fail(String description) {
		Reporter.log(description, true);

		try {
			BaseTest.extentTest.fail(description + " :STEP FAILED");

			String screenShotpath = new BaseTest().captureScreenShot(driver);
			BaseTest.extentTest.addScreenCaptureFromPath(screenShotpath);

			if (this.result != null) {
				BaseTest.extentTest.fail(this.result.getThrowable().getMessage(),
						MediaEntityBuilder.createScreenCaptureFromPath(screenShotpath).build());
				this.result.getThrowable();

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// to check element is displayed or not
	public Boolean boolean1(WebElement element) {
		if (element == null) {
			return false;
		}
		return true;
	}

	// to clear the text present in the textbox
	public void clearText(WebElement element) {
		element.clear();
	}

	/******************************
	 * Page Utilization Methods
	 *************************************/

	public void clickElement(WebElement element, String text) {

		try {
			info("Clicking on Element " + text);
			element.click();
		} catch (Exception e) {
			fail("unable to click on " + element);
			// Assert.fail();
		}
	}

	public void doubleClick(WebElement element) {
		Actions actions = new Actions(driver);
		actions.moveToElement(element).doubleClick().perform();
	}

	public Alert getAlertMethods() {
		Alert alert = driver.switchTo().alert();
		return alert;
	}

	// to find the element Text
	public void getallElementText(List<WebElement> elements) {
		System.out.println("Number of elements:" + elements.size());

		for (WebElement ele : elements) {
			ele.getText();
		}

	}

	public List<WebElement> getAllSelectOptions(WebElement element) {
		Select select = new Select(element);
		return select.getOptions();

	}

	///////////////////////////////////////////////////////////////////////////////////////////////

	// to get the Current Date And Time
	public String getCurrentDateAndTime() {
		Date date = new Date();
		String strDateFormat = "EEEE, MMMM dd, hh-mm-ss ";
		DateFormat dateFormat = new SimpleDateFormat(strDateFormat);
		String formattedDate = dateFormat.format(date);
		Reporter.log("Current time of the day using Date - 12 hour format: " + formattedDate, true);
		return formattedDate;
	}

	// to get the Current time
	public String getCurrentTime() {
		String timeformat = "HH:mm:ss a";
		SimpleDateFormat formatter1 = new SimpleDateFormat(timeformat);
		Date time = new Date();
		System.out.println(formatter1.format(time));
		return timeformat;
	}

	// to get the Current Date
	public String getCustomisedDate(int incdecDays) {
		DateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		// declare as DateFormat
		// Calendar today = Calendar.getInstance();
		Calendar y = Calendar.getInstance();
		y.add(Calendar.DATE, incdecDays);
		Date d = y.getTime(); // get a Date object
		String yesDate = sdf.format(d);
		// System.out.println(yesDate);
		return yesDate;
	}

	public WebDriver getdriver() {

		return driver;
	}

	// to find the element Text
	public String getElementText(WebElement element) {
		return element.getText();
	}

	// to get the Number from the string
	public String getNumber(By byLocator) {
		String v = driver.findElement(byLocator).getText();
		String numberOnly = v.replaceAll("[^0-9]", "");
		return numberOnly;
	}

	public String getPageTitle() {
		return driver.getTitle();
	}

	public String getPageUrl() {
		return driver.getCurrentUrl();
	}

	public String getTodaysDate(String dateformat) {
		// String dateformat = "MM/dd/yyyy";
		SimpleDateFormat formatter1 = new SimpleDateFormat(dateformat);
		Date date = new Date();
		System.out.println(formatter1.format(date));
		return formatter1.format(date);
	}

	public void hoverAndClick(WebElement mainElement, WebElement subElement) {
		Actions actions = new Actions(driver);
		actions.moveToElement(mainElement).moveToElement(subElement).click().build().perform();
	}

	public Boolean invisibilityOfElementLocated(By locator) {
		Wait<WebDriver> wait = new WebDriverWait(driver, Duration.ofSeconds(20));

		return wait.until(ExpectedConditions.invisibilityOfElementLocated(locator));
	}

	public Boolean isElementDisplayed(WebElement element) {
		return element.isEnabled();
	}

	public void isElementDisplayed1(final WebElement elementId, int timeoutInSeconds) {
		Wait<WebDriver> wait = new WebDriverWait(driver, Duration.ofSeconds(20));

		try {
			ExpectedCondition<Boolean> condition = new ExpectedCondition<Boolean>() {
				@Override
				public Boolean apply(final WebDriver driver) {

					return elementId.isDisplayed();
				}
			};
			wait.until(condition);
		} catch (Exception ex) {
			while (elementId.isDisplayed()) {
				isElementDisplayed1(elementId, 5);
				break;
			}
		}
	}

	// to check radio button.. is enabled or not
	public Boolean isElementEnabled(WebElement element) {
		return element.isEnabled();
	}

	public boolean isElementNotDisplayed(final WebElement elementId, final int timeoutInSeconds) {
		try {
			ExpectedCondition<Boolean> condition = new ExpectedCondition<Boolean>() {
				@Override
				public Boolean apply(final WebDriver driver) {

					return !elementId.isDisplayed();
				}
			};
			Wait<WebDriver> wait = new WebDriverWait(driver, Duration.ofSeconds(20));

			wait.until(condition);
		} catch (Exception ex) {
			Assert.fail("Element is Displayed");
		}
		return true;
	}

	// to check checkbox button.. is enabled or not
	public Boolean isElementSelected(WebElement element) {
		return element.isSelected();
	}

	// ================================================================================
	// End of page general methods 1st set
	// ================================================================================

	// =================================================================================
	// Wait methods Start
	// =================================================================================

	public void jsclickElement(WebElement element, String string) {

		try {
			Actions builder = new Actions(driver);
			builder.moveToElement(element).click(element);
			builder.perform();
		} catch (NullPointerException e) {
			JavascriptExecutor ex = (JavascriptExecutor) driver;
			ex.executeScript("arguments[0].click()", element);
		} catch (ElementNotInteractableException e1) {
			element.click();
		}

	}

	public void jsexecutor(String exer) {

		try {
			JavascriptExecutor ex = (JavascriptExecutor) driver;
			ex.executeScript(exer);
		} catch (Exception e) {

		}
	}

	public void mouseHover(WebElement element) {
		Actions action = new Actions(driver);
		action.moveToElement(element).perform();

	}

	public void navigateBack() {
		driver.navigate().back();
	}

	public void navigateForward() {
		driver.navigate().forward();
	}

	public void refreshPage() {
		driver.navigate().refresh();
		waitSleep(3);
	}

	public void scrollBy(By byLocator) {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		WebElement element = driver.findElement(byLocator);
		js.executeScript("arguments[0].scrollIntoView(true);", element);
		System.out.println(element.getText());

	}

	public void scrollBy(WebElement element) {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].scrollIntoView(true);", element);

		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void scrolltoEnd() {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(0,document.body.scrollHeight)");

	}

	public void selectByIndex(WebElement element, int index) {
		Select select = new Select(element);
		select.selectByIndex(index);
	}

	public void selectByValue(WebElement element, String value) {
		Select select = new Select(element);
		select.selectByValue(value);
	}

	public void selectByVisibleText(WebElement element, String option) {
		Select select = new Select(element);
		select.selectByVisibleText(option);

	}

	public void sendKeysARROW_DOWN(WebElement element) {
		element.sendKeys(Keys.ARROW_DOWN);

	}

	public void setText(WebElement element, String text) {
//		info("Entering value " + text);
//		element.sendKeys(text);
		try {
			info("Entering value " + text);
			element.sendKeys(text);
		} catch (Exception e) {
			fail("unable to send text to " + element);
			Assert.fail();
		}
	}

	public void setTextPassword(WebElement element, String text) {
		try {
			info("Entering password : ************");
			element.sendKeys(text);
		} catch (Exception e) {
			fail("Unable to set Password to " + element);
			Assert.fail();
		}
	}

	public By toByVal(WebElement we) {
		// By format = "[foundFrom] -> locator: term"
		// see RemoteWebElement toString() implementation
		String[] data = we.toString().split(" -> ")[1].replace("]", "").split(": ");
		String locator = data[0];
		String term = data[1];

		switch (locator) {
		case "xpath":
			return By.xpath(term);
		case "css selector":
			return By.cssSelector(term);
		case "id":
			return By.id(term);
		case "tag name":
			return By.tagName(term);
		case "name":
			return By.name(term);
		case "link text":
			return By.linkText(term);
		case "class name":
			return By.className(term);
		}
		return (By) we;
	}

	public void verifyElement(WebElement element, String expectedText) {
		info("Verifying Actual and Expected Texts ");
		String actualText = element.getText().toString().trim();
		System.out.println("expected test");
		System.out.println(" log test : " + expectedText);
		System.out.println(" log test : " + actualText);
		try {

			info("Actual text   : " + actualText);
			info("Expected text : " + expectedText);
			Assert.assertEquals(actualText, expectedText);
			pass("Text verification is PASSED");

		} catch (AssertionError e) {
			fail("Text verification is FAILED ");
			fail("Actual text   : " + actualText);
			fail("Expected text : " + expectedText);
			Assert.fail();
		}
	}

	public void verifyElementContainsText(WebElement element, String expectedText) {
		info("Verifying Actual and Expected Texts ");
		waitUntilLoadedAndVisibilityOfElementLocated(element);
		String actualText = element.getText().toString().trim();

		System.out.println("expected test");
		System.out.println(" log test : " + expectedText);
		System.out.println(" log test : " + actualText);
		try {

			if (actualText.contains(expectedText)) {
				info("Actual text   : " + actualText);
				info("Expected text : " + expectedText);
				Assert.assertEquals(actualText, expectedText, "fAILD");
			}
			pass("Text verification is PASSED");

		} catch (AssertionError e) {
			fail("Text verification is FAILED ");
			fail("Actual text   : " + actualText);
			fail("Expected text : " + expectedText);
			Assert.fail();
		}
	}

	public void verifyElementContains(WebElement element, String expectedText) {
		info("Verifying element Containing expected Text");
		waitUntilLoadedAndVisibilityOfElementLocated(element);
		String actualText = element.getText().toString().trim();
		try {

			if (actualText.contains(expectedText)) {
				info("Actual text   : " + actualText);
				info("Expected text : " + expectedText);
			}
			pass("Text verification is PASSED");

		} catch (Exception e) {
			fail("Text verification is FAILED ");
			fail("Actual text   : " + actualText);
			fail("Expected text : " + expectedText);
			e.printStackTrace();
		}
	}

	// ================================================================================
	// END of wait methods
	// ================================================================================

	// ================================================================================
	// Start of page action methods
	// ================================================================================

	public void verifyElementIsPresent(WebElement element) {

		Wait<WebDriver> wait = new WebDriverWait(driver, Duration.ofSeconds(20));

		try {
			wait.until(ExpectedConditions.visibilityOf(element));
			pass("Element is Present and Visible");
		} catch (Exception e) {
			fail("Element is not Present");
		}
	}

	public Boolean verifyElementText(WebElement element, String expectedText) {
		Boolean bool = false;

		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(60));
		// wait.until(ExpectedConditions.stalenessOf(element));
		wait.until(ExpectedConditions.visibilityOf(element));
//    	wait.until(ExpectedConditions.textToBePresentInElementValue(element, expectedText));
		try {
			info("Verifying Actual and Expected Texts ");
			String actualText = element.getText().toString().trim();
			if (actualText.equals(expectedText)) {
				info("Actual :" + actualText);
				info("Expected :" + expectedText);
				pass("Text verification is PASSED");
				bool = true;
				Assert.assertEquals(actualText, expectedText);
			} else {
				info("Actual :" + actualText);
				info("Expected :" + expectedText);
				fail("Text verification is Failed");
				bool = false;
			}
		} catch (AssertionError e) {
			fail("Text verification is Failed");
			e.printStackTrace();
			Assert.fail();
		}
		return bool;
	}

	public void verifyText(String actualText, String expectedText) {
		info("Verifying Actual and Expected Texts ");

		try {
			Assert.assertEquals(actualText, expectedText, "Failed");
//			info("Actual text   : " + actualText);
//			info("Expected text : " + expectedText);
			pass("Text verification is PASSED");

		} catch (AssertionError e) {

			fail("Actual text   : " + actualText);
			fail("Expected text : " + expectedText);
			fail("Text verification is FAILED ");
			Assert.fail();
		}
	}

	// ================================================================================
	// Start of page general methods 2nd set
	// ================================================================================
	public void verifyTitle(String etitle) {

		try {
			Thread.sleep(10000);
			waitForPageLoad();
			String actual = driver.getTitle();
			System.out.println("Actual Title:" + actual);
			System.out.println("Expected Title:" + etitle);
			Assert.assertEquals(actual, etitle);
			pass("Title is Verified");
		} catch (Exception e) {
			fail("Title is not Verified");
		}
	}

	public Boolean waitAndGetTitle(String expectedTitle) {
		Wait<WebDriver> wait = new WebDriverWait(driver, Duration.ofSeconds(180));
		waitForPageLoad();
		Boolean bool = wait.until(ExpectedConditions.titleIs(expectedTitle));
		return bool;
	}

	public void waitForPageLoad() {

		Wait<WebDriver> wait = new WebDriverWait(driver, Duration.ofSeconds(60));
		try {
			wait.until(new ExpectedCondition<Boolean>() {
				@Override
				public Boolean apply(WebDriver wdriver) {
					return ((JavascriptExecutor) driver).executeScript("return document.readyState").equals("complete");
				}
			});
		} catch (Throwable error) {
			info("Timeout waiting for Page Load Request to complete.");
		}
	}

	public void waitForjsPageLoad() {

		Wait<WebDriver> wait = new WebDriverWait(driver, Duration.ofSeconds(60));
		try {
			wait.until(driver -> ((JavascriptExecutor) driver).executeScript("return document.readyState")
					.equals("complete"));

		} catch (Throwable error) {
			info("Timeout waiting for Page Load Request to complete.");
		}
	}

	// Wait for the ready state to be "complete"

	// ================================================================================
	// END of page action methods
	// ================================================================================

	public void waitInVisibilityOfElementLocated(WebElement element) {
		Wait<WebDriver> wait = new WebDriverWait(driver, Duration.ofSeconds(20));

		wait.until(ExpectedConditions.invisibilityOf(element));
	}

	public void waitSleep(int sec) {
		try {
			Thread.sleep(sec * 1000);
		} catch (InterruptedException e) {

		}
	}

	public void waitSleep1(int sec) {
		try {
			Thread.sleep(sec * 1000);
		} catch (InterruptedException e) {

		}
	}

	public void waitUntilLoadedAndElementClickable(By locator) {
		Wait<WebDriver> wait = new WebDriverWait(driver, Duration.ofSeconds(20));

		wait.until(ExpectedConditions.elementToBeClickable(locator));
	}

	public void waitUntilLoadedAndElementClickable(WebElement element) {
		Wait<WebDriver> wait = new WebDriverWait(driver, Duration.ofSeconds(20));

		wait.until(ExpectedConditions.elementToBeClickable(element));
	}

	public void waitUntilLoadedAndPresenceOfAllElementsLocated(By locator) {
		Wait<WebDriver> wait = new WebDriverWait(driver, Duration.ofSeconds(20));

		wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(locator));
	}

	public void waitUntilLoadedAndPresenceOfElementLocated(By locator) {
		Wait<WebDriver> wait = new WebDriverWait(driver, Duration.ofSeconds(20));

		wait.until(ExpectedConditions.presenceOfElementLocated(locator));
	}

	// ================================================================================
	// End of page general methods 2nd set
	// ================================================================================

	// extras added methods

	public void waitUntilLoadedAndPresenceOfElementLocated(WebElement element) {
		Wait<WebDriver> wait = new WebDriverWait(driver, Duration.ofSeconds(20));

		wait.until(ExpectedConditions.presenceOfElementLocated(toByVal(element)));
	}

	public void waitUntilLoadedAndStalenessOffElement(By locator) {
		Wait<WebDriver> wait = new WebDriverWait(driver, Duration.ofSeconds(20));

		wait.until(ExpectedConditions.stalenessOf(driver.findElement(locator)));
	}

	public void waitUntilLoadedAndStalenessOffElement(WebElement element) {
		Wait<WebDriver> wait = new WebDriverWait(driver, Duration.ofSeconds(20));

		wait.until(ExpectedConditions.stalenessOf(element));
	}

	public void waitUntilLoadedAndTextToBePresentInElementLocated(By locator, String text) {
		Wait<WebDriver> wait = new WebDriverWait(driver, Duration.ofSeconds(20));

		wait.until(ExpectedConditions.textToBePresentInElementLocated(locator, text));
	}

	public void waitUntilLoadedAndTextToBePresentInElementLocated(WebElement element, String text) {
		Wait<WebDriver> wait = new WebDriverWait(driver, Duration.ofSeconds(60));

		wait.until(ExpectedConditions.textToBePresentInElement(element, text));
	}

	public void waitUntilLoadedAndVisibilityOfAllElements(List<WebElement> elements) {
		Wait<WebDriver> wait = new WebDriverWait(driver, Duration.ofSeconds(20));

		wait.until(ExpectedConditions.visibilityOfAllElements(elements));
	}

	public void waitUntilLoadedAndVisibilityOfElementLocated(By element) {
		WebElement ele = driver.findElement(element);
		Wait<WebDriver> wait = new WebDriverWait(driver, Duration.ofSeconds(20));

		wait.until(ExpectedConditions.visibilityOf(ele));
	}

	public void waitUntilLoadedAndVisibilityOfElementLocated(WebElement element) {
		Wait<WebDriver> wait = new WebDriverWait(driver, Duration.ofSeconds(100));

		wait.until(ExpectedConditions.visibilityOf(element));
	}
}