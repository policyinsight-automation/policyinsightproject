package com.automation.pi.library;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class BasePage {

	public WebDriver driver;
	public WebActionUtils webActionUtils;

	/**
	 * @author Shobhan
	 * @description Constructor to initialize POM Pages
	 * @param driver
	 * @param webActionUtils
	 *
	 */
	public BasePage(WebDriver driver, WebActionUtils webActionUtils) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
		this.webActionUtils = webActionUtils;
	}
}