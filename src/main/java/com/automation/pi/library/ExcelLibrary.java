package com.automation.pi.library;


import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.testng.Assert;

public class ExcelLibrary {

	static boolean labelFoundInExcel = false;
	public static ArrayList<Integer> findCellIndexOfLabelInExcel(String xlPath, String sheetName, String content)
			throws EncryptedDocumentException, InvalidFormatException, IOException {
		ArrayList<Integer> labelIndexInExcel = new ArrayList<Integer>();

		FileInputStream fis = new FileInputStream(xlPath);
		Workbook wb = WorkbookFactory.create(fis);
		Sheet sh = wb.getSheet(sheetName);

		for (int i = 0; i < sh.getLastRowNum(); i++) {
			Row row = sh.getRow(i);
			try {
				int lastCellCount = row.getLastCellNum();
				for (int j = 0; j < lastCellCount; j++) {

					try {
						Cell cellValue = row.getCell(j);
						if (cellValue.toString().equals(content)) {
							labelIndexInExcel.add(i);
							labelIndexInExcel.add(j);
							labelFoundInExcel = true;
							break;
						}
					} catch (Exception e) {

					}
				}

			} catch (NullPointerException e) {

			}
		}

		/*
		 * if(labelFoundInExcel==false) {
		 * 
		 * // Assert.fail("Given Label Is Not Found In the Excel Sheet");
		 * System.out.println(("Given Label Is Not Found In the Excel Sheet")); }
		 */
		return labelIndexInExcel;
	}

	public static ArrayList<String> getAllValuesOfSelectBoxFromExcel(String xlPath, String sheetName, String content) {

		ArrayList<String> valueOfLabelInExcel = new ArrayList<String>();
		ArrayList<Integer> cellIndexOfLabelInExcel = new ArrayList<Integer>();
		try {
			cellIndexOfLabelInExcel = findCellIndexOfLabelInExcel(xlPath, sheetName, content);

			FileInputStream fis = new FileInputStream(xlPath);
			Workbook w1 = WorkbookFactory.create(fis);
			Sheet s1 = w1.getSheet(sheetName);
			if (cellIndexOfLabelInExcel.size() == 0) {
				// System.out.println("Label Not Found In Excel");
				Assert.fail("Label Not Found In Excel");
			} else {
				for (int i = cellIndexOfLabelInExcel.get(0) + 1; i <= s1.getLastRowNum(); i++) {
					Row r1 = s1.getRow(i);
					try {
						Cell c1 = r1.getCell(cellIndexOfLabelInExcel.get(1));
						if (c1 != null) {
							valueOfLabelInExcel.add(c1.getStringCellValue());
						}
					} catch (NullPointerException e) {
						break;
					}
				}
			}

		}

		catch (Exception e) {
			e.printStackTrace();
		}

		return valueOfLabelInExcel;
	}

	public static String getCellValueFromExcel(String xlPath, String sheetName, String content, int indexvalue) {

		ArrayList<String> valueOfLabelInExcel = new ArrayList<String>();
		ArrayList<Integer> cellIndexOfLabelInExcel = new ArrayList<Integer>();
		try {
			cellIndexOfLabelInExcel = findCellIndexOfLabelInExcel(xlPath, sheetName, content);

			FileInputStream fis = new FileInputStream(xlPath);
			Workbook w1 = WorkbookFactory.create(fis);
			Sheet s1 = w1.getSheet(sheetName);
			if (cellIndexOfLabelInExcel.size() == 0) {
				// System.out.println("Label Not Found In Excel");
				Assert.fail("Label Not Found In Excel");
			} else {
				for (int i = cellIndexOfLabelInExcel.get(0) + 1; i <= s1.getLastRowNum(); i++) {
					Row r1 = s1.getRow(i);
					try {
						Cell c1 = r1.getCell(cellIndexOfLabelInExcel.get(1));
						if (c1 != null) {
							valueOfLabelInExcel.add(c1.getStringCellValue());
						}
					} catch (NullPointerException e) {
						break;
					}
				}
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return valueOfLabelInExcel.get((indexvalue - 1));
	}

	/**
	 * @author Shobhan
	 * @description To Read the data from the cell
	 * @param excelPath
	 * @param sheetName
	 * @param rowNo
	 * @param cellNo
	 * @return
	 */
	public String getExcelData(String excelPath, String sheetName, int rowNo, int cellNo) {

		String data = "";

		try {
	
			FileInputStream fis = new FileInputStream(excelPath);
			Workbook wb = WorkbookFactory.create(fis);
			Sheet sht = wb.getSheet(sheetName);
			data = sht.getRow(rowNo).getCell(cellNo).toString();
			//System.out.println("excelPath:" + excelPath);
			//System.out.println("SheetName:" + sheetName + " " + "rowNo:" + rowNo + " " + "cellNo:" + cellNo);
			//System.out.println(data);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return data;
	}
}
