package com.automation.pi.library;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;
import com.aventstack.extentreports.reporter.configuration.Protocol;
import com.aventstack.extentreports.reporter.configuration.Theme;

public class BaseTest implements IAutoConst {

	public WebDriver driver;
	public static ExtentSparkReporter spark;
	public static ExtentReports extentReports;
	public static ExtentTest extentTest;
	static ITestResult result;

	public WebActionUtils webActionUtils = new WebActionUtils(driver);
	public ExcelLibrary excelLibrary = new ExcelLibrary();

	static String userDir = System.getProperty("user.dir");
	static String fileSeperator = System.getProperty("file.separator");
	static String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
	static String reportFileName = "Policy Insight Test Automation Report" + timeStamp + ".html";
	static String reportFilepath = userDir + fileSeperator + "ExecutionReports";
	static String reportFileLocation = reportFilepath + fileSeperator + reportFileName;

	static String customDownloadFolder = userDir + fileSeperator + "DownloadedFiles" + fileSeperator;

	static final String TASKLIST = "tasklist";
	static final String KILL = "taskkill /F /IM /T";
	String pid = null;

	@BeforeSuite
	public void beforeSuiteM() {

		String fileName = getReportPath(reportFilepath);
		extentReports = new ExtentReports();
		spark = new ExtentSparkReporter(fileName);
		extentReports.attachReporter(spark);

		extentReports.setSystemInfo("Application Name", "Policy Insights Redesign");
		extentReports.setSystemInfo("Platform", "Windows");
		extentReports.setSystemInfo("Enviornment", "QA");

		spark.config().setTheme(Theme.STANDARD);
		spark.config().setDocumentTitle("Policy Insight Test-Automation-Report");
		spark.config().setEncoding("utf-8");
		spark.config().setReportName(reportFileName);
		spark.config().setProtocol(Protocol.HTTPS);
	}

	@BeforeMethod
	public void Start() {
		try {
			//System.setProperty("webdriver.chrome.driver",userDir + fileSeperator +"chromedriver//chromedriver.exe");

			//System.setProperty("webdriver.chrome.driver","\"C:\\Users\\shobhan_shiva\\Downloads\\chromedriver-win64\\chromedriver-win64\\chromedriver.exe\"");

			
		//	String processName = ManagementFactory.getRuntimeMXBean().getName();
//			pid = processName.split("@")[0];
//			System.out.println("PID: " + pid);

			System.out.println("Launching the browser");
			File downloadFolder = new File(customDownloadFolder);
			downloadFolder.mkdir();

			Map<String, Object> chromePrefs = new HashMap<String, Object>();
			chromePrefs.put("download.default_directory", customDownloadFolder);
			chromePrefs.put("profile.default_content_setting_values.notifications", 2);
			chromePrefs.put("excludeSwitches", Arrays.asList("disable-popup-blocking"));

			ChromeOptions options = new ChromeOptions();

			options.addArguments("--headless", "--no-sandbox", "--disable-dev-shm-usage");
			options.addArguments("--headless=new");

			// options.setExperimentalOption("cdpVersion", "127");

			options.setExperimentalOption("prefs", chromePrefs);
			options.setExperimentalOption("excludeSwitches", new String[] { "enable-automation" });

			driver = new ChromeDriver(options);
			//WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));

			driver.manage().deleteAllCookies();
			driver.manage().window().maximize();
			driver.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(180));
			driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(180));

			Reporter.log("Automation Script      : Started ", true);
			Reporter.log("Launching the  Browser : Chrome ", true);
			Reporter.log("Entering the URL       : " + APP_URL, true);
			driver.get(APP_URL);
			webActionUtils = new WebActionUtils(driver);
			Thread.sleep(2000);

		} catch (Exception e) {
			Start();
		}
	}

	@AfterMethod
	public synchronized void afterMethod(ITestResult result) throws IOException {
		switch (result.getStatus()) {
		case ITestResult.SUCCESS:

			extentTest.log(Status.PASS, MarkupHelper.createLabel(result.getName() + "  PASSED ", ExtentColor.GREEN));
			extentReports.flush();
			closeBrowser();
			break;

		case ITestResult.FAILURE:

			extentTest.log(Status.FAIL, MarkupHelper.createLabel(result.getName() + "  FAILED ", ExtentColor.RED));
			extentTest.fail(result.getThrowable());
			extentTest.addScreenCaptureFromPath(captureScreenShot(driver));
			extentReports.flush();
			closeBrowser();
			break;
		}
	}

	@AfterSuite
	public void afterSuite() {

		try {
			Reporter.log("Automation Script : Stopped", true);
			if (driver != null) {

				driver.quit();
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			{
				//killProcessByPID(pid);

			}
		}
	}

	@AfterTest
	public void Enddriver() {
		// }
	}

	private String getReportPath(String path) {

		File testDirectory = new File(path);
		if (!testDirectory.exists()) {
			if (testDirectory.mkdir()) {
				Reporter.log("Directory: " + path + " is created!", true);
				return reportFileLocation;
			} else {
				Reporter.log("Failed to create directory: " + path, true);
				return System.getProperty("user.dir");
			}
		} else {
			Reporter.log("Directory already exists: " + path, true);
		}
		return reportFileLocation;
	}

	public String captureScreenShot(WebDriver driver) {
		String path = "";

		File src = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		try {

			path = System.getProperty("user.dir") + "/ScreenShots/" + "Captured_Image_" + System.currentTimeMillis()
					+ "_" + RandomStringUtils.randomAlphanumeric(10) + ".png";
			FileUtils.copyFile(src, new File(path));
			return path;
		} catch (IOException e) {
			Reporter.log(e.getMessage(), true);
		}
		return path;
	}

	public void closeBrowser() throws IOException {
		Reporter.log("Closing the Browser", true);
		try {
			if (driver != null) {
				driver.quit();
				//killProcessBy();
			}

		} catch (Exception e) {
			System.out.println("Driver is null");

		}

	}

	public void killProcessByPID(String pid) {
		try {

			String os = System.getProperty("os.name").toLowerCase();
			if (os.contains("win")) {
				Runtime.getRuntime().exec("taskkill /F /PID " + pid);
			} else {
				Runtime.getRuntime().exec("kill -9 " + pid);
			}
			System.out.println("Process with PID " + pid + " has been terminated.");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void killProcessBy() {
		System.out.println("Driver process is Killing started");

		String browserName = "Chrome";
		try {
			String os = System.getProperty("os.name").toLowerCase();
			if (os.contains("win")) {
				Runtime.getRuntime().exec("taskkill /F /IM " + browserName + ".exe");
			} else {
				Runtime.getRuntime().exec("pkill " + browserName);
			}
			System.out.println("Remaining " + browserName + " processes have been terminated.");
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@SuppressWarnings(value = { "unused" })
	public void killProcess(String... serviceName) throws Exception {
		int len = serviceName.length;
		for (int i = 0; i < len; i++) {
			if (System.getProperty("os.name").toLowerCase().indexOf("windows") > -1) {
				Process p = Runtime.getRuntime().exec("cmd /c taskkill  /F /IM " + serviceName[i]);
			} else {
				Process p = Runtime.getRuntime().exec("kill -9 " + serviceName[i]);
			}

		}
	}

}
