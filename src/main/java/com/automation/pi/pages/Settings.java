package com.automation.pi.pages;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import com.automation.pi.library.BasePage;
import com.automation.pi.library.WebActionUtils;

public class Settings extends BasePage {

	@FindBy(xpath = "//a[text()='Settings']")
	private WebElement SettingsTab;

	// 1
	@FindBy(xpath = "(//li[@class='dropdown-submenu'])[1]")
	private WebElement DepartmentSettingsSubMenu;

	@FindBy(xpath = "//a[text()='Department Setting']")
	private WebElement DepartmentSettingOption;

	@FindBy(xpath = "//a[text()='Department Mapping']")
	private WebElement DepartmentMappingOption;

	@FindBy(xpath = "//a[text()='Parameter Setting']")
	private WebElement ParameterSettingOption;

	@FindBy(xpath = "//a[text()='Department Segmentation']")
	private WebElement DepartmentSegmentationOption;

	@FindBy(xpath = "//em[text()='  Department Setting  ']")
	private WebElement DeptEleTxt;

	@FindBy(xpath = "//em[normalize-space()='Department Mapping']")
	private WebElement DepartmentMappingEleTxt;
	
	@FindBy(xpath = "//em[normalize-space()='Create Parameter Setting']")
	private WebElement ParameterSettingEleTxt;
	
	@FindBy(xpath = "//em[normalize-space()='Create Department Segmentation Setting']")
	private WebElement DepartmentSegmentationEleTxt;

	@FindBy(xpath = "//em[normalize-space()='Configure AM Section']")
	private WebElement ConfigureAMSectionEleTxt;
	
//	@FindBy(xpath = "//em[normalize-space()='Template Default Value Setting']")
//	private WebElement TemplateDefaultValueSettingEleTxt;
//	
	// 2
	@FindBy(xpath = "(//li[@class='dropdown-submenu'])[2]")
	private WebElement ChecklistTemplateSettingsSubMenu;

	@FindBy(xpath = "//a[text()='LOB Setting']")
	private WebElement LOBSettingOption;

	@FindBy(xpath = "//a[text()='Checkpoint Setting']")
	private WebElement CheckpointSettingOption;

	@FindBy(xpath = "//a[text()='AM Customized Checkpoint']")
	private WebElement AMCustomizedCheckpointOption;

	@FindBy(xpath = "//a[text()='Section LOB Setting']")
	private WebElement SectionLOBSettingOption;

	@FindBy(xpath = "//a[text()='Source Setting']")
	private WebElement SourceSettingOption;

	@FindBy(xpath = "//a[text()='Section Setting']")
	private WebElement SectionSettingOption;

	@FindBy(xpath = "//a[text()='Template Manager']")
	private WebElement TemplateManagerOption;

	@FindBy(xpath = "//a[contains(.,'Configure AM Section')]")
	private WebElement ConfigureAMSectionOption;
	
	@FindBy(xpath = "//a[text()='Form Section Setting']")
	private WebElement FormSectionSettingOption;

	@FindBy(xpath = "//a[text()='Form Checkpoint Setting']")
	private WebElement FormCheckpointSettingOption;

	@FindBy(xpath = "//a[text()='Form Setting on Upload File Page']")
	private WebElement FormSettingonUploadFilePageOption;
	


	// 3
	@FindBy(xpath = "(//li[@class='dropdown-submenu'])[3]")
	private WebElement AdvancedSettingsSubMenu;

	@FindBy(xpath = "//a[text()='Configure Discrepancy Available']")
	private WebElement ConfigureDiscrepancyAvailableOption;

	@FindBy(xpath = "//a[text()='Advanced Search']")
	private WebElement AdvancedSearchOption;

	// 4
	@FindBy(xpath = "(//li[@class='dropdown-submenu'])[4]")
	private WebElement AccountManagerSettingSubMenu;

	// 5
	@FindBy(xpath = "(//li[@class='dropdown-submenu'])[5]")
	private WebElement AdhocPIRequestSettingSubMenu;

	// 6
	@FindBy(xpath = "(//li[@class='dropdown-submenu'])[6]")
	private WebElement DAISySettingSubMenu;

	// 8
	@FindBy(xpath = "(//li[@class='dropdown-submenu'])[8]")
	private WebElement AMPSettingsSubMenu;

	// 7
	// @FindBy(xpath = "(//li[@class='dropdown-submenu'])[7]")
	// private WebElement ArchiveSubMenu;

	// 9
	@FindBy(xpath = "(//li[@class='dropdown-submenu'])[9]")
	private WebElement RefreshSubMenu;

	// 10
	@FindBy(xpath = "(//li[@class='dropdown-submenu'])[10]")
	private WebElement IRPackageStatusSubMenu;

	@FindBy(xpath = "//a[text()='Checklists Setting']")
	private WebElement ChecklistsSettingOption;

	@FindBy(xpath = "(//a[text()='Account Manager Setting'])[2]")
	private WebElement AccountManagerSettingOption;

	@FindBy(xpath = "//a[text()='Checklist Header Setting']")
	private WebElement ChecklistHeaderSettingOption;

	@FindBy(xpath = "//a[text()='Template Default Value Setting']")
	private WebElement TemplateDefaultValueSettingOption;

	@FindBy(xpath = "//a[text()='Client Estimated Time']")
	private WebElement ClientEstimatedTimeOption;

	@FindBy(xpath = "//a[text()='Default Template Sections']")
	private WebElement DefaultTemplateSectionsOption;

	@FindBy(xpath = "//a[text()='Ignore Form Setting']")
	private WebElement IgnoreFormSettingOption;

	@FindBy(xpath = "//a[text()='Alternate Wordlist Setting']")
	private WebElement AlternateWordListSettingOption;

	@FindBy(xpath = "//a[text()='Form Library Setting']")
	private WebElement FormLibrarySettingOption;
	
	@FindBy(xpath = "//a[text()='Rush Setting']")
	private WebElement RushSettingOption;
	
	@FindBy(xpath = "//a[text()='Note and Missing Info']")
	private WebElement NoteAndMissingInfoOption;
	

	@FindBy(xpath = "//a[text()='Source Alternative Name Setting']")
	private WebElement SourceAlternativeNameSettingOption;

	@FindBy(xpath = "//a[text()='Account Management System Setting']")
	private WebElement AccountManagementSystemSettingOption;

	@FindBy(xpath = "//a[text()='Auto Push Management']")
	private WebElement AutoPushManagementOption;

	@FindBy(xpath = "//a[text()='Discrepancy Word Management']")
	private WebElement DiscrepancyWordManagementOption;

	@FindBy(xpath = "//a[text()='Communication Setting']")
	private WebElement CommunicationSettingOption;

	@FindBy(xpath = "//a[text()='Coverage Section Settings']")
	private WebElement CoverageSectionSettingsOption;

	@FindBy(xpath = "//a[text()='AM Alternative Name Setting']")
	private WebElement AMAlternativeNameSettingOption;

	@FindBy(xpath = "//a[text()='Department/Office Code Setting']")
	private WebElement DepartmentOfficeCodeSettingOption;

	@FindBy(xpath = "//a[text()='Upload File Alternative Setting']")
	private WebElement UploadFileAlternativeSettingOption;

	@FindBy(xpath = "//a[text()='Configure New Client Actions For AMP']")
	private WebElement ConfigureNewClientActionsForAMPOption;

	
	@FindBy(xpath = "//a[text()='AMP Severity Indicator']")
	private WebElement AMPSeverityIndicatorOption;
	
	@FindBy(xpath = "(//a[text()='Refresh'])[2]")
	private WebElement RefreshOption;

	@FindBy(xpath = "(//a[text()='IR Package Status'])[2]")
	private WebElement IRPackageStatusOption;

	@FindBy(xpath = "//i[normalize-space()='Checklists Setting']")
	private WebElement ChecklistsEleTxt;

	@FindBy(xpath = "(//div[@class='groupTitle'])[1]")
	private WebElement StandardLOBEleTxt;

	@FindBy(xpath = "//em[normalize-space()='LOB Setting']")
	private WebElement LOBSettingHeaderTxt;

	@FindBy(xpath = "(//div[@class='groupTitle'])[2]")
	private WebElement AccountSpecifiedLOBEleTxt;

	@FindBy(xpath = "(//div[@class='groupTitle'])[3]")
	private WebElement policyStatusTable;

	@FindBy(xpath = "//em[normalize-space()='Account Manager Setting']")
	private WebElement AccountManagerEleTxt;

	@FindBy(xpath = "//em[normalize-space()='Checkpoint Setting']")
	private WebElement CheckpointEleTxt;

	@FindBy(xpath = "//em[normalize-space()='AM Customized Checkpoint']")
	private WebElement AMCustomaisedCheckpointEleTxt;

	@FindBy(xpath = "//em[normalize-space()='Source Setting']")
	private WebElement SourceSettingEleTxt;

	@FindBy(xpath = "//em[normalize-space()='Section Setting']")
	private WebElement SectionSettingEleTxt;

	@FindBy(xpath = "//em[normalize-space()='Template Manager Setting']")
	private WebElement TemplateManagerEleTxt;

	@FindBy(xpath = "//em[normalize-space()='Section LOB Setting']")
	private WebElement SectionLOBSettingEleTxt;

	@FindBy(xpath = "//em[normalize-space()='Form Section Setting']")
	private WebElement FormSectionSettingEleTxt;

	@FindBy(xpath = "//em[normalize-space()='Form Checkpoint Setting']")
	private WebElement FormCheckpointSettingEleTxt;

	@FindBy(xpath = "//em[normalize-space()='Checklist Header Setting']")
	private WebElement ChecklistHeaderSettingEleTxt;

	@FindBy(xpath = "//em[normalize-space()='Form Setting on Upload File Page']")
	private WebElement FormSettingUploadFilePageEleTxt;

	@FindBy(xpath = "//em[normalize-space()='Template Default Value Setting']")
	private WebElement TemplateDefaultValueSettingEleTxt;

	@FindBy(xpath = "//em[normalize-space()='Task Assignment Setting']")
	private WebElement TaskAssignmentSettingEleTxt;

	@FindBy(xpath = "//em[normalize-space()='Default Template Section Setting']")
	private WebElement DefaultTemplateSectionSettingEleTxt;

	@FindBy(xpath = "//em[normalize-space()='Non-Discrepancy Form Library Setting']")
	private WebElement NonDiscrepancyFormLibrarySettingEleTxt;

	@FindBy(xpath = "//em[normalize-space()='Alternate Wordlist Setting']")
	private WebElement AlternateWordListSettingEleTxt;

	@FindBy(xpath = "//em[normalize-space()='Advanced Search Checking Point Setting']")
	private WebElement AdvancedSearchCheckingPointSettingEleTxt;

	@FindBy(xpath = "//em[normalize-space()='Configure Discrepancy Available']")
	private WebElement ConfigureDiscrepancyAvailableEleTxt;

	@FindBy(xpath = "//em[normalize-space()='Form Library Setting']")
	private WebElement FormLibrarySettingEleTxt;
	
	@FindBy(xpath = "//em[normalize-space()='Rush Setting']")
	private WebElement RushSettingEleTxt;	
	
	@FindBy(xpath = "//em[normalize-space()='Note and Missing Info']")
	private WebElement NoteAndMissingInfoEleTxt;
	
	@FindBy(xpath = "//em[normalize-space()='Communication Report']")
	private WebElement CommunicationReportEleTxt;
	
	@FindBy(xpath = "//em[normalize-space()='Rush Report']")
	private WebElement RushReportEleTxt;
	
	@FindBy(xpath = "//h4[normalize-space()='Update PIS Owners']")
	private WebElement UpdatePISOwnersEleTxt;
	
	@FindBy(xpath = "//em[normalize-space()='AMP Severity Indicator']")
	private WebElement AMPSeverityIndicatorsEleTxt;
	
	@FindBy(xpath = "//em[normalize-space()='Source Alternative Name Setting']")
	private WebElement SourceAlternativeNameSettingEleTxt;

	@FindBy(xpath = "//em[normalize-space()='Account Management System Setting']")
	private WebElement AccountManagementSystemSettingEleTxt;

	@FindBy(xpath = "//em[normalize-space()='Auto Push Management']")
	private WebElement AutoPushManagementHeader;

	@FindBy(xpath = "//h4[normalize-space()='Discrepancy Word Management']")
	private WebElement DiscrepancyWordManagementHeader;

	@FindBy(xpath = "//h4[normalize-space()='Coverage Section Settings']")
	private WebElement CoverageSectionSettingstHeader;

	@FindBy(xpath = "//select[@id='accountID']")
	private WebElement AutoPushManagement1AccountEleTxt;

	@FindBy(xpath = "//select[@id='accountDepartmentID']")
	private WebElement AutoPushManagement2departmentEleTxt;

	@FindBy(xpath = "//input[@id='stDateCaptureDatepicker']")
	private WebElement fromDatepicker;

	@FindBy(xpath = "//input[@id='endtoDateCaptureDatepicker']")
	private WebElement toDatepicker;

	@FindBy(xpath = "//button[@id='btnSearch']")
	private WebElement SearchBtn;

	@FindBy(xpath = "//em[@id='178713']/../..//td[text()='178713']")
	private WebElement AutoPushManagementEleTxt;

	@FindBy(xpath = "//select[@id='ListBoxCap']")
	private WebElement ManageDiscrepancy1EleTxt;

	@FindBy(xpath = "//select[@id='ListBoxSmall']")
	private WebElement ManageDiscrepancy2EleTxt;

	@FindBy(xpath = "//em[normalize-space()='Communication Setting']")
	private WebElement CommunicationSettingEleTxt;

	@FindBy(xpath = "//h4[normalize-space()='Coverage Section Settings']")
	private WebElement CoverageSectionSettings1EleTxt;

	@FindBy(xpath = "//select[@id='ListBoxFPkg']")
	private WebElement CoverageSectionSettings2EleTxt;

	@FindBy(xpath = "//select[@id='ListBoxEPkg']")
	private WebElement CoverageSectionSettings3EleTxt;

	@FindBy(xpath = "//em[normalize-space()='AM Alternative Name Setting']")
	private WebElement AMAlternativeNameSettingEleTxt;

	@FindBy(xpath = "//em[normalize-space()='Manage Department/Office Code Setting']")
	private WebElement ManageDepartmentOfficeCodeSettingEleTxt;

	@FindBy(xpath = "//table[@id='tbMgr']")
	private WebElement UploadFileAlternativeSettingEleTxt;

	@FindBy(xpath = "//td[text()='Ignore Action Path : Ignore']")
	private WebElement ConfigureNewClientActionsforAMPSettingEleTxt;

	@FindBy(xpath = "//button[@id='btnClearCacheAMP']")
	private WebElement RefreshAMPBtn;

	@FindBy(xpath = "//button[@id='btnClearCacheRefresh']")
	private WebElement RefreshAMPDemoBtn;

	@FindBy(xpath = "//button[@id='btnClearCache']")
	private WebElement PIPlatformRefreshBtn;

	@FindBy(xpath = "//span[@id='spanMessageContent']")
	private WebElement Errormessage;

	@FindBy(xpath = "//h2[text()='  Refresh  ']")
	private WebElement RefreshTitle;

	@FindBy(xpath = "//a[@class='ui-state-default ui-state-highlight ui-state-hover']")
	private WebElement DailyPackageStatusCheckEleTxt;

	@FindBy(xpath = "//li[@class='dropdown-submenu']")
	private WebElement AdhocPIRequestEleTxt;
	// "//a[text()='Ad-hoc PI Request']")

	@FindBy(xpath = "//a[text()='Change Date to Client & Policy Status']")
	private WebElement ChangeDatetoClientPolicyStatusOpn;

	@FindBy(xpath = "//a[text()='Add FBE Employee']")
	private WebElement AddFBEEmployeeOpn;

	@FindBy(xpath = "//a[text()='Change Checklist Title']")
	private WebElement ChangeChecklistTitleOpn;

	@FindBy(xpath = "(//a[text()='AMP Reports'])")
	private WebElement AMPReportsOpn;

	@FindBy(xpath = "//a[text()='Policy List']")
	private WebElement PolicyListOpn;

	@FindBy(xpath = "(//a[text()='Assign Employee Location'])")
	private WebElement AssignEmployeeLocationOpn;


	@FindBy(xpath = "(//a[text()='Form Discrepancy Report'])")
	private WebElement FormDiscrepancyReportOpn;
	
	@FindBy(xpath = "(//a[text()='Communication Report'])")
	private WebElement CommunicationReportOpn;
	

	@FindBy(xpath = "(//a[text()='Rush Report'])")
	private WebElement RushReportOpn;
	
	@FindBy(xpath = "(//a[text()='Update PIS Owners'])")
	private WebElement UpdatePISOwnersOpn;

	@FindBy(xpath = "(//a[text()='DAISy Setting'])")
	private WebElement DAISySettingOpn;

	@FindBy(xpath = "//a[text()='Configure Account For DAISy']")
	private WebElement ConfigureAccountForDAISyOpn;

	@FindBy(xpath = "//em[text()='Change Date to Client & Policy Status  ']")
	private WebElement ChangeDatetoClientPolicyStatusEleTxt;

	@FindBy(xpath = "//em[text()='Add FBE Employee  ']")
	private WebElement AddFBEEmployeeEleTxt;

	@FindBy(xpath = "//em[text()='Change Checklist Title  ']")
	private WebElement ChangeChecklistTitleEleTxt;

	//
	@FindBy(xpath = "//em[text()=' AMP Reports ']")
	private WebElement AMPReportsEleTxt;

	@FindBy(xpath = "//em[text()=' Policy List']")
	private WebElement PolicyListEleTxt;

	@FindBy(xpath = "//em[text()='Assign Employee Location  ']")
	private WebElement AssignEmployeeLocationEleTxt;

	@FindBy(xpath = "//em[text()=' Form Discrepancy Report']")
	private WebElement FormDiscrepancyReportEleTxt;

	@FindBy(xpath = "//em[text()='  Configure Account For DAISy  ']")
	private WebElement ConfigureAccountForDAISyEleTxt;

	@FindBy(xpath = "//em[text()='IR Package Status Check  ']")
	private WebElement IRPackageStatusCheckEleTxt;

	@FindBy(id = "btnViewPackage")
	private WebElement IRCheckFileTreeBTN;

	@FindBy(xpath = "//input[@id='packId']")
	private WebElement IRFileCheckPackageID;

	@FindBy(id = "Search")
	private WebElement IRFileCheckPackageIDSearch;

	@FindBy(xpath = "//div[@id='tree']")
	private WebElement TreeEle;

	@FindBy(xpath = "//a[@class='btn btn-success btn-success-new']")
	private WebElement AddIcon;

	@FindBy(id = "ddlAccount")
	private WebElement account;

	@FindBy(id = "selAccount")
	private WebElement account1;

	@FindBy(id = "ddlDepartment")
	private WebElement deparment;

	@FindBy(id = "selDept")
	private WebElement deparment1;

	@FindBy(id = "chkIsAccessEnabled")
	private WebElement deparmentChkBx;

	@FindBy(xpath = "//button[@type='submit']")
	private WebElement submitBTN;

	@FindBy(id = "ddlSource")
	private WebElement Source;

	@FindBy(id = "txtAlternativeFileUploadName")
	private WebElement UploadFileName;

	@FindBy(id = "txtCode")
	private WebElement codeTxtBx;

	@FindBy(id = "txtMgr")
	private WebElement manager;

	@FindBy(id = "btnReDirectConfirmYes")
	private WebElement OKBTN;

	@FindBy(id = "txtEmail")
	private WebElement email1;

	@FindBy(id = "txtCheckingPoint")
	private WebElement CheckingpointTxt;

	@FindBy(id = "txtAlternativeExpression")
	private WebElement AlternativeExpressionTxt;

	@FindBy(id = "txtIncludedForm")
	private WebElement IncludedinwhichFormTxt;

	@FindBy(id = "txtCheckingPoint")
	private WebElement CheckingpointsTxt2;

	@FindBy(id = "btnSearch")
	private WebElement SearchBtn2;

	@FindBy(xpath = "//td[2]")
	private WebElement newRecordCheckpoint;

	@FindBy(id = "txtCommunication")
	private WebElement communicationTxt;

	@FindBy(id = "txtCommunicationDescription")
	private WebElement CommunicationDescriptionTxt;

	@FindBy(id = "txtSearchCommunication")
	private WebElement communicationTxt2;

	@FindBy(xpath = "//td[2]")
	private WebElement newRecordCommunication;


	@FindBy(xpath = "//select[@id='AccountList']")
	private WebElement parameterAccountList;
	
	@FindBy(id="Parameter")
	private WebElement Parameter;
	
	@FindBy(id="AddToOfficeOrLocation")
	private WebElement AddToOfficeOrLocationBTN;
	
	@FindBy(id="AddtoSegmentation")
	private WebElement AddtoSegmentationBTN;
	
	@FindBy(id="AddtoCheckingType")
	private WebElement AddtoCheckingTypeBTN;
	
	@FindBy(xpath = "//button[@class='btn btn-primary space_button']/em")
	private WebElement parameterSave;
	
	@FindBy(id="spanMessageContent")
	private WebElement parameterMessage;

	
	@FindBy(xpath = "//div/label[contains(.,'Department/Program')]/../..//label[contains(.,'Service Type')]")
	private WebElement serviceTypeTxt1;
	
	@FindBy(xpath = "//div//label[contains(.,'Service Type')]")
	private WebElement serviceTypeTxt2;
	
	@FindBy(xpath = "//a[@class='btn btn-success btn-success-new']")
	private WebElement addBTN;
	
	@FindBy(id="ServiceType")
	private WebElement selectServiceType;
	
	
	
	
	
	
	
	
	public Settings(WebDriver driver, WebActionUtils webActionUtils) {
		super(driver, webActionUtils);
	}

	public void addNewRecordINAccountManager() {
		webActionUtils.info("add new records to the Account Manager Setting");
		webActionUtils.waitUntilLoadedAndVisibilityOfElementLocated(account1);
		webActionUtils.selectByVisibleText(account1, "CBIZ");
		webActionUtils.waitUntilLoadedAndVisibilityOfElementLocated(deparment1);
		webActionUtils.selectByVisibleText(deparment1, "Alpharetta, GA");
		String addedrecord = "testmail1234" + RandomStringUtils.randomAlphabetic(3);
		String addedrecordemail = addedrecord + RandomStringUtils.randomAlphabetic(19) + "@outlook.com";
		webActionUtils.setText(manager, addedrecord);
		webActionUtils.setText(email1, addedrecordemail);
		webActionUtils.clickElement(submitBTN, "Submit Button");
		webActionUtils.info("New records is created in the Manage Department Office Code Setting page" + addedrecord);
	}

	public void addNewRecordINAdvancedSearchCheckingPointSetting() {
		webActionUtils.info("add new records to the Advanced Search Checking Point Setting ");
		String text = RandomStringUtils.randomAlphabetic(12) + "Z";
		webActionUtils.setText(CheckingpointTxt, text);
		webActionUtils.setText(AlternativeExpressionTxt, "Z" + RandomStringUtils.randomAlphabetic(3));
		webActionUtils.setText(IncludedinwhichFormTxt, "Z" + RandomStringUtils.randomAlphabetic(3));
		webActionUtils.clickElement(submitBTN, "Submit Button");
		webActionUtils.waitSleep1(3);
		webActionUtils.setText(CheckingpointsTxt2, text);
		webActionUtils.waitSleep1(3);
		webActionUtils.clickElement(SearchBtn2, "Search button");
		webActionUtils.waitUntilLoadedAndVisibilityOfElementLocated(newRecordCheckpoint);
		webActionUtils.waitSleep1(6);
		webActionUtils.verifyElementText(newRecordCheckpoint, text);
		webActionUtils.pass("Added new records to the Advanced Search Checking Point Setting ");

	}

	public void addNewRecordINCommunicationSetting() {
		webActionUtils.info("add new records to the Advanced Search Checking Point Setting ");
		String text = RandomStringUtils.randomAlphabetic(5) + "Z";
		webActionUtils.setText(communicationTxt, text);
		String text2 = RandomStringUtils.randomAlphabetic(13) + "Z";
		webActionUtils.setText(CommunicationDescriptionTxt, text2);
		webActionUtils.clickElement(submitBTN, "Submit Button");
		webActionUtils.waitSleep(4);
		webActionUtils.setText(communicationTxt2, text);
		webActionUtils.waitSleep(3);
		webActionUtils.clickElement(SearchBtn2, "Search button");
		webActionUtils.waitUntilLoadedAndVisibilityOfElementLocated(newRecordCommunication);
		webActionUtils.waitSleep(3);
		webActionUtils.verifyElementText(newRecordCommunication, text);
		webActionUtils.pass("Added new records to the Communication Point Setting ");
	}

	public void addNewRecordINDaisy() {
		webActionUtils.info("add new records to the Configure Account For DAISy page");
		webActionUtils.waitUntilLoadedAndVisibilityOfElementLocated(account);
		webActionUtils.clickElement(account, "account");
		webActionUtils.selectByVisibleText(account, "CBIZ");
		webActionUtils.clickElement(deparment, "deparment");
		webActionUtils.waitUntilLoadedAndVisibilityOfElementLocated(deparment);
		webActionUtils.selectByVisibleText(deparment, "Alpharetta, GA");
		// webActionUtils.clickElement(deparmentChkBx, "Select All Departments
		// Checkbox");
		webActionUtils.clickElement(submitBTN, "Submit Button");
		webActionUtils.waitUntilLoadedAndVisibilityOfElementLocated(Errormessage);
		webActionUtils.verifyElementText(Errormessage, "Message: This is a duplicate account for DAISy, please check.");
		webActionUtils.pass("New records  are created to the Configure Account For DAISy page");
	}

	public void addNewRecordinDiscrepancyAvailable() {
		webActionUtils.info("add new records to the Discrepancy Available ");
		webActionUtils.waitUntilLoadedAndVisibilityOfElementLocated(account);
		webActionUtils.selectByVisibleText(account, "CBIZ");
		webActionUtils.waitUntilLoadedAndVisibilityOfElementLocated(deparment);
		webActionUtils.selectByVisibleText(deparment, "Alpharetta, GA");
		webActionUtils.clickElement(submitBTN, "Submit Button");
		webActionUtils.waitUntilLoadedAndVisibilityOfElementLocated(Errormessage);
		webActionUtils.verifyElementText(Errormessage,
				"Message: This is a duplicate account for Discrepancy Available, please check.");

	}

	public void addNewRecordINManageDepartment_OfficeCodeSetting() {
		webActionUtils.info("add new records to the Manage Department Office Code Setting");
		webActionUtils.waitUntilLoadedAndVisibilityOfElementLocated(account);
		webActionUtils.selectByVisibleText(account, "CBIZ");
		webActionUtils.waitUntilLoadedAndVisibilityOfElementLocated(deparment);
		webActionUtils.selectByVisibleText(deparment, "Alpharetta, GA");
		String addedrecord = "test" + RandomStringUtils.randomAlphabetic(3);
		webActionUtils.setText(codeTxtBx, addedrecord);

		webActionUtils.clickElement(submitBTN, "Submit Button");
		webActionUtils.pass("Added record  :" + addedrecord);
		webActionUtils.pass("New records is created in the Manage Department Office Code Setting page");
	}

	public void addNewRecordInUploadFileAlternativeSetting() {
		webActionUtils.info("add new records to the Upload File Alternative Setting");
		webActionUtils.waitUntilLoadedAndVisibilityOfElementLocated(Source);
		webActionUtils.selectByVisibleText(Source, "Others");
		webActionUtils.setText(UploadFileName, "test" + RandomStringUtils.randomAlphabetic(3));
		// webActionUtils.clickElement(deparmentChkBx, "Select All Departments
		// Checkbox");
		webActionUtils.clickElement(submitBTN, "Submit Button");
		webActionUtils.waitUntilLoadedAndVisibilityOfElementLocated(Errormessage);
		webActionUtils.verifyElementText(Errormessage, "Message: This is a duplicate source, please check.");
		webActionUtils.pass("Message is Verified");

	}

	public void AssignEmployeeLocationOption() {
		webActionUtils.info("Clicking on the Assign Employee Location Option ");
		webActionUtils.clickElement(AssignEmployeeLocationOpn, "Assign Employee Location Option");

	}

	public void clickAccountManagementSystemSettingOption() {
		webActionUtils.info("Clicking on the Account Management System Setting Option ");
		webActionUtils.clickElement(AccountManagementSystemSettingOption, "Account Management System Setting Option ");
	}

	public void clickAccountManagerSettingOption() {
		webActionUtils.info("Clicking on the Account Manager Setting Option");
		webActionUtils.clickElement(AccountManagerSettingOption, " Account Manager Setting Option");
	}

	public void clickAddFBEEmployeeOption() {
		webActionUtils.info("Clicking Add FBE Employee Option");
		// webActionUtils.waitInVisibilityOfElementLocated(ChangeDatetoClientPolicyStatusOpn);
		// webActionUtils.scrolltoEnd();
		// webActionUtils.hoverAndClick(AdhocPIRequestEleTxt,
		// ChangeDatetoClientPolicyStatusOpn);
		// webActionUtils.mouseHover(ChangeDatetoClientPolicyStatusOpn);
		webActionUtils.clickElement(AddFBEEmployeeOpn, " Add FBE Employee Option");
	}

	public void clickAdhocPIRequestOption() {
		webActionUtils.info("Clicking Ad-hoc PI Request Option");
		webActionUtils.scrolltoEnd();
		webActionUtils.scrolltoEnd();
		webActionUtils.clickElement(AdhocPIRequestEleTxt, " Ad-hoc PI Request Option");
		webActionUtils.mouseHover(AdhocPIRequestEleTxt);
		// webActionUtils.waitSleep(3);
		webActionUtils.clickElement(AdhocPIRequestEleTxt, " Ad-hoc PI Request Option");
		webActionUtils.scrolltoEnd();
		webActionUtils.scrolltoEnd();

	}

	public void clickAdvancedSearchOption() {
		webActionUtils.info("Clicking on the AdvancedSearchOption");
		webActionUtils.clickElement(AdvancedSearchOption, "AdvancedSearchOption ");
	}

	public void clickAlternateWordListSettingOption() {
		webActionUtils.info("Clicking on the Alternate Word List Setting Option ");
		webActionUtils.clickElement(AlternateWordListSettingOption, " Alternate Word List Setting Option");
	}

	public void clickAMAlternativeNameSettingOption() {
		webActionUtils.info("Clicking on the AM Alternative Name Setting Option ");
		webActionUtils.clickElement(AMAlternativeNameSettingOption, "AM Alternative Name Setting Option ");
	}

	public void clickAMCustomizedCheckpointOption() {
		webActionUtils.info("Clicking on the AM Customized Check point Setting Option");
		webActionUtils.clickElement(AMCustomizedCheckpointOption, "AM Customized Check point Setting Option");
	}

	public void clickAMPReportsOption() {
		webActionUtils.info("Clicking on the AMP Reports Option ");
		webActionUtils.clickElement(AMPReportsOpn, "AMP Reports Option");
	}

	public void clickAutoPushManagementOption() {
		webActionUtils.info("Clicking on the Auto Push Management Option ");
		webActionUtils.clickElement(AutoPushManagementOption, "Auto Push Management Option ");
	}

	public void clickChangeChecklistTitleOption() {
		webActionUtils.info("Clicking Change Checklist Title Option");
		// webActionUtils.waitInVisibilityOfElementLocated(ChangeChecklistTitleOpn);
		// webActionUtils.scrolltoEnd();
		// webActionUtils.mouseHover(ChangeDatetoClientPolicyStatusOpn);
		// webActionUtils.hoverAndClick(AdhocPIRequestEleTxt,
		// ChangeChecklistTitleOpn);
		webActionUtils.clickElement(ChangeChecklistTitleOpn, " Change Checklist Title Option");
	}

	public void clickChangeDatetoClientPolicyStatusOption() {
		webActionUtils.info("Clicking Change Date to Client & Policy Status Option");
		// webActionUtils.scrolltoEnd();
		// webActionUtils.waitUntilLoadedAndElementClickable(ChangeDatetoClientPolicyStatusOpn);
		// webActionUtils.hoverAndClick(AdhocPIRequestEleTxt,
		// ChangeDatetoClientPolicyStatusOpn);
		// webActionUtils.mouseHover(ChangeDatetoClientPolicyStatusOpn);
		webActionUtils.clickElement(ChangeDatetoClientPolicyStatusOpn, " Change Date to Client & Policy Status Option");
	}

	public void clickChecklistHeaderSettingOption() {
		webActionUtils.info("Clicking on the Checklist Header Setting Option ");
		webActionUtils.clickElement(ChecklistHeaderSettingOption, "Checklist Header Setting Option ");
	}

	public void clickChecklistsSettingOption() {
		webActionUtils.info("Clicking on the Check lists Setting  Option");
		webActionUtils.clickElement(ChecklistsSettingOption, " Checklists Setting  Option");
	}

	public void clickCheckpointSettingOption() {
		webActionUtils.info("Clicking on the Check point Setting Option");
		webActionUtils.clickElement(CheckpointSettingOption, " Check point Setting Option");
	}

	public void clickClientEstimatedTimeOption() {
		webActionUtils.info("Clicking on the Client Estimated Time Option ");
		webActionUtils.clickElement(ClientEstimatedTimeOption, " Client Estimated Time Option");
	}

	public void clickCommunicationSettingOption() {
		webActionUtils.info("Clicking on the Communication Setting Option ");
		webActionUtils.clickElement(CommunicationSettingOption, "Communication Setting Option ");
	}

	public void clickConfigureAccountForDAISySettingOption() {
		webActionUtils.info("Clicking on the Configure Account For DAISy Option ");
		webActionUtils.clickElement(ConfigureAccountForDAISyOpn, "  Configure Account For DAISy Option");

	}

	public void clickConfigureDiscrepancyAvailableOption() {
		webActionUtils.info("Clicking on the    Configure Discrepancy Available");
		webActionUtils.clickElement(ConfigureDiscrepancyAvailableOption, "   Configure Discrepancy Available ");
	}

	public void clickConfigureNewClientActionsForAMPOption() {
		webActionUtils.info("Clicking on the Configure New Client Actions For AMP Option ");
		webActionUtils.clickElement(ConfigureNewClientActionsForAMPOption,
				"Configure New Client  Actions For AMP Option ");
	}

	public void clickCoverageSectionSettingsOption() {
		webActionUtils.info("Clicking on the Coverage Section Settings Option ");
		webActionUtils.clickElement(CoverageSectionSettingsOption, "Coverage Section Settings Option ");
	}

	public void clickDefaultTemplateSectionsOption() {
		webActionUtils.info("Clicking on the Default Template Sections Option ");
		webActionUtils.clickElement(DefaultTemplateSectionsOption, "Default Template Sections Option ");
	}

	public void clickDepartmentMappingOption() {
		webActionUtils.info("Clicking on the Department Mapping Option ");
		webActionUtils.clickElement(DepartmentMappingOption, " Department Mapping Option");
	}

	public void clickDepartmentOfficeCodeSettingOption() {
		webActionUtils.info("Clicking on the Department Office Code Setting Option ");
		webActionUtils.clickElement(DepartmentOfficeCodeSettingOption, "Department Office Code Setting Option ");
	}

	public void clickDepartmentSettingOption() {
		webActionUtils.info("Clicking on the  Department Setting Option");
		webActionUtils.waitUntilLoadedAndElementClickable(DepartmentSettingOption);
		webActionUtils.clickElement(DepartmentSettingOption, "Department Setting Option ");
	}

	public void clickDiscrepancyWordManagementOption() {
		webActionUtils.info("Clicking on the Discrepancy Word Management  ption ");
		webActionUtils.clickElement(DiscrepancyWordManagementOption, "Discrepancy Word Management Option ");
	}

	public void clickFormCheckpointSettingOption() {
		webActionUtils.info("Clicking on the Form Check point Setting Option ");
		webActionUtils.clickElement(FormCheckpointSettingOption, " Form Checkpoint Setting Option");
	}

	public void clickFormDiscrepancyReportOption() {
		webActionUtils.info("Clicking on the Form Discrepancy Report Option ");
		webActionUtils.clickElement(FormDiscrepancyReportOpn, " Form Discrepancy Report Option");
	}

	public void clickFormLibrarySettingOption() {
		webActionUtils.info("Clicking on the Form Library Setting Option ");
		webActionUtils.clickElement(FormLibrarySettingOption, "Form Library Setting Option");
	}

	public void clickFormSectionSettingOption() {
		webActionUtils.info("Clicking on the Form Section Setting  Option ");
		webActionUtils.clickElement(FormSectionSettingOption, " Form Section Setting Option");
	}

	public void clickFormSettingonUploadFilePageOption() {
		webActionUtils.info("Clicking on the Form Setting on Upload File Page Option ");
		webActionUtils.clickElement(FormSettingonUploadFilePageOption, "Form Setting on Upload File Page Option");
	}

	public void clickIgnoreFormSettingOption() {
		webActionUtils.info("Clicking on the Ignore Form Setting Option ");
		webActionUtils.clickElement(IgnoreFormSettingOption, " Ignore Form Setting Option");
	}

	public void clickIRPackageStatusOption() {
		webActionUtils.info("Clicking on the IRPackage Status Option");
		webActionUtils.clickElement(IRPackageStatusOption, " IR Package Status Option");
	}

	public void clickLOBSettingOption() {
		webActionUtils.info("Clicking on the LOB Setting  Option");
		webActionUtils.waitSleep(5);
		webActionUtils.clickElement(LOBSettingOption, " LOB Setting  Option");
		webActionUtils.waitForPageLoad();
	}

	public void clickOKonAMPopup() {
		webActionUtils.info("Click OK on the Account Manager Setting Saving Pop up");
		webActionUtils.waitSleep(3);
		webActionUtils.waitUntilLoadedAndElementClickable(OKBTN);
		webActionUtils.clickElement(OKBTN, "OK button");
		webActionUtils.pass("Record Created Sucessfully");
	}

	public void clickOnAddbutton() {
		webActionUtils.info(" Clicking on Add Button ");
		webActionUtils.clickElement(AddIcon, "Add Icon");
		webActionUtils.info("User is navigated to Add page");
	}

	public void clickonAMPDemoRefreshButton() {
		webActionUtils.info("Clicking on AMP Demo Refresh Button");
		webActionUtils.clickElement(RefreshAMPDemoBtn, "Refresh AMP DEMO Button");
	}

	public void clickonAMPRefreshButton() {
		webActionUtils.info("Clicking on AMP Refresh Button");
		webActionUtils.clickElement(RefreshAMPBtn, "Refresh AMP Button");
	}

	public void clickOnCheckfileButton() {

		webActionUtils.info("clicking On Check file Button");
		webActionUtils.clickElement(IRCheckFileTreeBTN, "Check file Tree Button");
	}

	public void clickonDepartmentMappingSettingsOption() {
		webActionUtils.info("Clicking  Department Mapping Settings options");
		webActionUtils.clickElement(DepartmentMappingOption, "Department Mapping");
	}

	public void clickonDepartmentParameterSettingOption() {
		webActionUtils.info("Clicking Parameter Setting options");
		webActionUtils.clickElement(ParameterSettingOption, "Parameter Setting");
	}
	
	public void clickonDepartmentDepartmentSegmentationOption() {
		webActionUtils.info("Clicking Department Segmentation options");
		webActionUtils.clickElement(DepartmentSegmentationOption, "Parameter Setting");
	}
	
	
	public void clickonDepartmentSettingsOption() {
		webActionUtils.info("Clicking  Department Settings options");
		webActionUtils.clickElement(DepartmentSettingOption, "Department Setting");

	}

	public void clickonPIPlatformRefreshButton() {
		webActionUtils.info("Clicking on PI Platform Refresh Button");
		webActionUtils.clickElement(PIPlatformRefreshBtn, "PI Platform Refresh Button");

	}

	public void clickPolicyListOption() {
		webActionUtils.info("Clicking on the Policy List Option ");
		webActionUtils.clickElement(PolicyListOpn, "Policy List Option");

	}

	public void clickRefreshOption() {
		webActionUtils.info("Clicking on the  Refresh Option");
		webActionUtils.clickElement(RefreshOption, "Refresh Option");
	}

	public void clickSearchButtonAutoPushManagement() {
		webActionUtils.info("Clicking on Auto Push Management - Search button");
		webActionUtils.scrollBy(SearchBtn);
		//webActionUtils.waitSleep(2);
		webActionUtils.setText(fromDatepicker, webActionUtils.getCustomisedDate(-10));
		webActionUtils.setText(toDatepicker, webActionUtils.getCustomisedDate(+10));
		webActionUtils.clickElement(SearchBtn, "search Button");
	}

	public void clickSectionLOBSettingOption() {
		webActionUtils.info("Clicking on the Section LOB Setting Option");
		webActionUtils.clickElement(SectionLOBSettingOption, "Section LOB Setting Option ");
	}

	public void clickSectionSettingOption() {
		webActionUtils.info("Clicking on the Section Setting Option");
		webActionUtils.clickElement(SectionSettingOption, "Section Setting Option");
	}

	public void clickSettingOption() {
		webActionUtils.info("Clicking on the Setting Tab");
		webActionUtils.waitUntilLoadedAndElementClickable(SettingsTab);
		webActionUtils.waitForPageLoad();
		webActionUtils.waitSleep1(5);
		webActionUtils.clickElement(SettingsTab, " Setting Tab ");
		webActionUtils.waitForPageLoad();


	}

	public void clickSourceAlternativeNameSettingOption() {
		webActionUtils.info("Clicking on the Source Alternative Name Setting Option ");
		webActionUtils.clickElement(SourceAlternativeNameSettingOption, " Source Alternative Name Setting Option");
	}

	public void clickSourceSettingOption() {
		webActionUtils.info("Clicking on the  Source Setting Option ");
		webActionUtils.clickElement(SourceSettingOption, " Source Setting Option");
	}

	public void clickTemplateDefaultValueSettingOption() {
		webActionUtils.info("Clicking on the Template Default Value Setting Option ");
		webActionUtils.clickElement(TemplateDefaultValueSettingOption, "Template Default Value Setting Option ");
	}

	public void clickTemplateManagerOption() {
		webActionUtils.info("Clicking on the Template Manager Option ");
		webActionUtils.clickElement(TemplateManagerOption, "Template Manager Option ");
	}

	public void clickUploadFileAlternativeSettingOption() {
		webActionUtils.info("Clicking on the Upload File Alternative Setting Option ");
		webActionUtils.clickElement(UploadFileAlternativeSettingOption, " Upload File Alternative Setting Option");
	}

	public void enterAndSearchPackageFileTree(String text) {
		webActionUtils.info("enter the File tree Package id");
		webActionUtils.setText(IRFileCheckPackageID, text);
		webActionUtils.info("clicking On Search Button");
		webActionUtils.clickElement(IRFileCheckPackageIDSearch, "SearchButton");
	}

	// 4
	public void hoverOnAccountManagerSettingSubMenu() {
		webActionUtils.info("Hover on Submenu  : Account Manager Setting SubMenu ");
		webActionUtils.mouseHover(AccountManagerSettingSubMenu);

	}

	// 5
	public void hoverOnAdhocPIRequestSettingSubMenu() {
		webActionUtils.info("Hover on Submenu  : Ad-hoc PI Request Setting SubMenu ");
		webActionUtils.mouseHover(AdhocPIRequestSettingSubMenu);

	}

	// 3
	public void hoverOnAdvancedSettingsSubMenu() {
		webActionUtils.info("Hover on Submenu : Advanced Settings SubMenu ");
		webActionUtils.mouseHover(AdvancedSettingsSubMenu);

	}

	// 8
	public void hoverOnAMPSettingsSubMenu() {
		webActionUtils.info("Hover on Submenu  : AMP Settings SubMenu");
		webActionUtils.mouseHover(AMPSettingsSubMenu);

	}

	// 2
	public void hoverOnChecklistTemplateSettingsSubMenu() {
		try {
			webActionUtils.info("Hover on Submenu  : Checklist Template Settings SubMenu");
			webActionUtils.mouseHover(ChecklistTemplateSettingsSubMenu);
		} catch (Exception e) {
e.printStackTrace();		}
		

	}

	// 6
	public void hoverOnDAISySettingSubMenu() {
		webActionUtils.info("Hover on Submenu  : DAISy Setting SubMenu");
		webActionUtils.mouseHover(DAISySettingSubMenu);

	}

	// 1
	public void hoverOnDepartmentSettingsSubMenu() {
		webActionUtils.info("Hover on Submenu  : Department Settings SubMenu ");
		webActionUtils.mouseHover(DepartmentSettingsSubMenu);
	}

	// 10
	public void hoverOnIRPackageStatusSubMenu() {
		webActionUtils.info("Hover on Submenu  : IR Package Status SubMenu");
		webActionUtils.mouseHover(IRPackageStatusSubMenu);

	}

	// 9
	public void hoverOnRefreshSubMenu() {
		webActionUtils.info("Hover on Submenu  : Refresh SubMenu");
		webActionUtils.mouseHover(RefreshSubMenu);

	}

	public void verifyAccountManagementSystemSettingPage(String title) {
		webActionUtils.info("Verifying Account anagement System Setting Page Title");
		webActionUtils.verifyTitle(title);

		// webActionUtils.pass("Page Title Verified " + title);
	}

	public void verifyAccountManagementSystemSettingPageElement() {
		webActionUtils.info("Verifying Account Management System Setting Page Element");
		webActionUtils.scrollBy(AccountManagementSystemSettingEleTxt);
		//webActionUtils.waitSleep(2);
		webActionUtils.verifyElementIsPresent(AccountManagementSystemSettingEleTxt);
		webActionUtils.info("Account Management System Setting Page Element Verified");
	}

	public void verifyAccountManagerSettingPage(String title) {
		webActionUtils.info("Verifying  Account Manager Setting Page Title");
		System.out.println(webActionUtils.getPageTitle());
		webActionUtils.verifyTitle(title);

		// webActionUtils.pass("Page Title Verified " + title);
	}

	public void verifyAccountManagerSettingPageElement() {
		webActionUtils.info("Verifying Account Manager Setting Page Element");
		webActionUtils.scrollBy(AccountManagerEleTxt);
		//webActionUtils.waitSleep(2);
		webActionUtils.verifyElementIsPresent(AccountManagerEleTxt);
		webActionUtils.info("Account Manager Page Element Verified");
	}

	public void verifyAccountManagerSettingsOptionsList() {
		webActionUtils.info("Verifying Account Manager Settings options");
		webActionUtils.verifyElementText(AccountManagerSettingOption, "Account Manager Setting");

	}

	public void verifyAccountSpecifiedLOBPageElement() {
		webActionUtils.info("Verifying Account Specified LOB Element");
		webActionUtils.waitForPageLoad();
		webActionUtils.scrollBy(AccountSpecifiedLOBEleTxt);
		//webActionUtils.waitSleep(2);
		webActionUtils.verifyElementIsPresent(AccountSpecifiedLOBEleTxt);
		webActionUtils.info("Account Specified LOB Element Verified");
	}

	public void verifyAddFBEEmployeePageElement() {
		webActionUtils.info("Verifying Add FBE Employee Setting Page Element");
		webActionUtils.scrollBy(AddFBEEmployeeEleTxt);
		//webActionUtils.waitSleep(2);
		webActionUtils.verifyElementIsPresent(AddFBEEmployeeEleTxt);
		webActionUtils.info("Add FBE Employee Page Element Verified");

	}

	public void verifyAddFBEEmployeeSettingPage(String title) {
		webActionUtils.info("Verifying Add FBE Employee Setting Page Title");
		webActionUtils.verifyTitle(title);

	}

	public void verifyAdhocPIRequestSettingsOptionsList() {
		webActionUtils.info("Verifying Ad-hoc PI Request Settings options");
		webActionUtils.verifyElementText(ChangeDatetoClientPolicyStatusOpn, "Change Date to Client & Policy Status");
		webActionUtils.verifyElementText(AddFBEEmployeeOpn, "Add FBE Employee");
		webActionUtils.verifyElementText(ChangeChecklistTitleOpn, "Change Checklist Title");
		webActionUtils.verifyElementText(AMPReportsOpn, "AMP Reports");
		webActionUtils.verifyElementText(PolicyListOpn, "Policy List");
		webActionUtils.verifyElementText(AssignEmployeeLocationOpn, "Assign Employee Location");
		webActionUtils.verifyElementText(FormDiscrepancyReportOpn, "Form Discrepancy Report");
		webActionUtils.verifyElementText(CommunicationReportOpn, "Communication Report");
		webActionUtils.verifyElementText(RushReportOpn, "Rush Report");
		webActionUtils.verifyElementText(UpdatePISOwnersOpn, "Update PIS Owners");
		webActionUtils.pass("All Options are available");

	}

	public void verifyAdvancedSearchCheckingPointSettingPageElement() {
		webActionUtils.info("Verifying Advanced Search Checking Point Setting Page Element");
		webActionUtils.scrollBy(AdvancedSearchCheckingPointSettingEleTxt);
		//webActionUtils.waitSleep(2);
		webActionUtils.verifyElementIsPresent(AdvancedSearchCheckingPointSettingEleTxt);
		webActionUtils.info("Advanced Search Checking Point Setting Page Element Verified");
	}

	public void verifyAdvancedSearchSettingPage(String title) {
		webActionUtils.info("Verifying Advanced Search Page Title");
		webActionUtils.verifyTitle(title);

		// webActionUtils.pass("Page Title Verified " + title);
	}

	public void verifyAdvancedSettingsOptionsList() {
		webActionUtils.info("Verifying Advanced Settings options");
		
		webActionUtils.waitUntilLoadedAndTextToBePresentInElementLocated(ConfigureDiscrepancyAvailableOption, "Configure Discrepancy Available");
		webActionUtils.verifyElementText(ConfigureDiscrepancyAvailableOption, "Configure Discrepancy Available");
		
		webActionUtils.waitUntilLoadedAndTextToBePresentInElementLocated(AdvancedSearchOption, "Advanced Search");
		webActionUtils.verifyElementText(AdvancedSearchOption, "Advanced Search");
		
		webActionUtils.waitUntilLoadedAndTextToBePresentInElementLocated(ClientEstimatedTimeOption, "Client Estimated Time");
		webActionUtils.verifyElementText(ClientEstimatedTimeOption, "Client Estimated Time");
		
		webActionUtils.waitUntilLoadedAndTextToBePresentInElementLocated(CommunicationSettingOption, "Communication Setting");
		webActionUtils.verifyElementText(CommunicationSettingOption, "Communication Setting");
		
		webActionUtils.waitUntilLoadedAndTextToBePresentInElementLocated(CoverageSectionSettingsOption, "Coverage Section Settings");
		webActionUtils.verifyElementText(CoverageSectionSettingsOption, "Coverage Section Settings");
		
		webActionUtils.waitUntilLoadedAndTextToBePresentInElementLocated(DepartmentOfficeCodeSettingOption, "Department/Office Code Setting");
		webActionUtils.verifyElementText(DepartmentOfficeCodeSettingOption, "Department/Office Code Setting");
		
		webActionUtils.waitUntilLoadedAndTextToBePresentInElementLocated(UploadFileAlternativeSettingOption, "Upload File Alternative Setting");
		webActionUtils.verifyElementText(UploadFileAlternativeSettingOption, "Upload File Alternative Setting");
		
		webActionUtils.waitUntilLoadedAndTextToBePresentInElementLocated(FormLibrarySettingOption, "Form Library Setting");
		webActionUtils.verifyElementText(FormLibrarySettingOption, "Form Library Setting");
		
		webActionUtils.waitUntilLoadedAndTextToBePresentInElementLocated(RushSettingOption, "Rush Setting");
		webActionUtils.verifyElementText(RushSettingOption, "Rush Setting");
		
		webActionUtils.waitUntilLoadedAndTextToBePresentInElementLocated(NoteAndMissingInfoOption, "Note and Missing Info");
		webActionUtils.verifyElementText(NoteAndMissingInfoOption, "Note and Missing Info");
		
	
		webActionUtils.pass("All Options are available");
	}

	public void verifyAlternateWordListSettingPage(String title) {
		webActionUtils.info("Verifying Alternate Word List Setting Page Title");
		webActionUtils.verifyTitle(title);


		// webActionUtils.pass("Page Title Verified " + title);
	}

	// public void verifyAutoPushManagement() {
	// webActionUtils.info("Verifying Auto Push Management Page Element");
	// webActionUtils.verifyElementIsPresent(AutoPushManagementEleTxt);
	// webActionUtils.scrollBy();
	// //webActionUtils.waitSleep(2);
	// webActionUtils.info(" Page Element Verified");
	// }

	public void verifyAlternateWordListSettingPageElement() {
		webActionUtils.info("Verifying Alternate WordList Setting Page Element");
		webActionUtils.scrollBy(AlternateWordListSettingEleTxt);
		//webActionUtils.waitSleep(2);
		webActionUtils.verifyElementIsPresent(AlternateWordListSettingEleTxt);
		webActionUtils.info("Alternate WordList Setting Page Element Verified");
	}

	public void verifyAMAlternativeNameSettingPage(String title) {
		webActionUtils.info("Verifying AM Alternative Name Setting Page Title");
		webActionUtils.verifyTitle(title);

		// webActionUtils.pass("Page Title Verified " + title);
	}

	public void verifyAMAlternativeNameSettingPageElement() {
		webActionUtils.info("Verifying AM Alternative Name Setting Page Element");
		webActionUtils.scrollBy(AMAlternativeNameSettingEleTxt);
		//webActionUtils.waitSleep(2);
		webActionUtils.verifyElementIsPresent(AMAlternativeNameSettingEleTxt);
		webActionUtils.info("AM Alternative Name Setting Page Element Verified");
	}

	public void verifyAMCustomizedCheckpointSettingPage(String title) {
		webActionUtils.info("Verifying AM Customized Check point Setting Page Title");
		webActionUtils.verifyTitle(title);

		webActionUtils.pass("Page Title Verified " + title);
	}

	public void verifyAMCustomizedCheckpointSettingPageElement() {
		webActionUtils.info("Verifying AM customized Check point Setting Page Element");
		webActionUtils.scrollBy(AMCustomaisedCheckpointEleTxt);
		//webActionUtils.waitSleep(2);
		webActionUtils.verifyElementIsPresent(AMCustomaisedCheckpointEleTxt);
		webActionUtils.info("AM customized Check point Setting Page Element Verified");
	}

	public void verifyAMPDemoRefreshErrorMessage(String expectedText) {
		webActionUtils.info("verifying AMP Demo Refresh Error Message  ");
		webActionUtils.waitUntilLoadedAndTextToBePresentInElementLocated(Errormessage, expectedText);
		webActionUtils.verifyElementText(Errormessage, expectedText);
		webActionUtils.waitSleep(5);

	}

	public void verifyAMPRefreshErrorMessage(String expectedText1, String expectedText2, String expectedText3) {
		webActionUtils.info("Verifying AMP Refresh Error Message");
		webActionUtils.waitSleep(5);
		String str = Errormessage.getText();

		if (expectedText1.equalsIgnoreCase(str)) {
			webActionUtils.pass("AMP Refresh Message : Verified " + str);

		} else if (expectedText2.equalsIgnoreCase(str)) {
			webActionUtils.pass("AMP Refresh Message : Verified " + str);

		}
		else if (expectedText3.equalsIgnoreCase(str)) {
			webActionUtils.pass("AMP Refresh Message : Verified " + str);

		}

		webActionUtils.pass("AMP Refresh Message : Verified " + str);

//		try {
//			if (str.equalsIgnoreCase(expectedText1)) {
//				webActionUtils.info(" AMP Refresh Message : Verified " + expectedText1);
//			}
//		} catch (Exception e) {
//
//			if (str.equalsIgnoreCase(expectedText2)) {
//				webActionUtils.info(" AMP Refresh Message : Verified " + expectedText2);
//			}
//		}

		webActionUtils.waitSleep(5);
	}

	public void verifyAMPReportsSettingPage(String title) {
		webActionUtils.info("Verifying AMP Reports Setting Page");
		System.out.println(webActionUtils.getPageTitle());
		webActionUtils.verifyTitle(title);

		webActionUtils.pass("Page Title Verified " + title);

	}

	public void verifyAMPReportsSettingPageElement() {
		webActionUtils.info("Verifying  Setting Page Element");
		webActionUtils.scrollBy(AMPReportsEleTxt);
		//webActionUtils.waitSleep(2);
		webActionUtils.verifyElementIsPresent(AMPReportsEleTxt);
		webActionUtils.info("Page Element Verified");
	}

	public void verifyAMPSettingsOptionsList() {
		webActionUtils.info("Verifying AMP Settings options");
		
		webActionUtils.waitUntilLoadedAndTextToBePresentInElementLocated(ConfigureNewClientActionsForAMPOption, "Configure New Client Actions For AMP");
		webActionUtils.verifyElementText(ConfigureNewClientActionsForAMPOption, "Configure New Client Actions For AMP");
		
		webActionUtils.waitUntilLoadedAndTextToBePresentInElementLocated(AMPSeverityIndicatorOption, "AMP Severity Indicator");
		webActionUtils.verifyElementText(AMPSeverityIndicatorOption, "AMP Severity Indicator");
		
		webActionUtils.pass("All Options are available");
	}

	public void verifyArchiveSettingsOptionsList() {
		webActionUtils.info("Verifying Archive Settings options");
		
		webActionUtils.waitUntilLoadedAndTextToBePresentInElementLocated(IgnoreFormSettingOption, "Ignore Form Setting");
		webActionUtils.verifyElementText(IgnoreFormSettingOption, "Ignore Form Setting");

		webActionUtils.waitUntilLoadedAndTextToBePresentInElementLocated(AlternateWordListSettingOption, "Alternate Wordlist Setting");
		webActionUtils.verifyElementText(AlternateWordListSettingOption, "Alternate Wordlist Setting");
		
		webActionUtils.waitUntilLoadedAndTextToBePresentInElementLocated(AccountManagementSystemSettingOption, "Account Management System Setting");
		webActionUtils.verifyElementText(AccountManagementSystemSettingOption, "Account Management System Setting");
		
		webActionUtils.waitUntilLoadedAndTextToBePresentInElementLocated(AutoPushManagementOption, "Auto Push Management");
		webActionUtils.verifyElementText(AutoPushManagementOption, "Auto Push Management");
		
		webActionUtils.waitUntilLoadedAndTextToBePresentInElementLocated(DiscrepancyWordManagementOption, "Discrepancy Word Management");
		webActionUtils.verifyElementText(DiscrepancyWordManagementOption, "Discrepancy Word Management");
		webActionUtils.pass("All Options are available");
	}

	public void verifyAssignEmployeeLocationSettingPage(String title) {
		webActionUtils.info("Verifying Assign Employee Location Setting Page");
		webActionUtils.verifyTitle(title);

		webActionUtils.pass("Page Title Verified " + title);

	}

	public void verifyAssignEmployeeLocationSettingPageElement() {
		webActionUtils.info("Verifying  Setting Page Element");
		webActionUtils.scrollBy(AssignEmployeeLocationEleTxt);
		//webActionUtils.waitSleep(2);
		webActionUtils.verifyElementIsPresent(AssignEmployeeLocationEleTxt);
		webActionUtils.info(" Page Element Verified");

	}

	public void verifyAutoPushManagement1AccountPageElement() {
		webActionUtils.info("Verifying Auto Push Management Account Page Element");
		webActionUtils.scrollBy(AutoPushManagement1AccountEleTxt);
		//webActionUtils.waitSleep(2);
		webActionUtils.verifyElementIsPresent(AutoPushManagement1AccountEleTxt);
		webActionUtils.info("Auto Push Management Account- Page Element Verified");
	}

	public void verifyAutoPushManagement2DepartmentPageElement() {
		webActionUtils.info("Verifying Auto Push Management Department Page Element");
		webActionUtils.scrollBy(AutoPushManagement2departmentEleTxt);
		//webActionUtils.waitSleep(2);
		webActionUtils.verifyElementIsPresent(AutoPushManagement2departmentEleTxt);
		webActionUtils.info("Auto Push Management Department- Page Element Verified");
	}

	public void verifyAutoPushManagementSettingPage(String title) {
		webActionUtils.info("Verifying Auto Push Management Page Title");
		webActionUtils.verifyTitle(title);

		// webActionUtils.pass("Page Title Verified " + title);
	}

	public void verifyAutoPushManagementSettingPageElement() {
		webActionUtils.info("Verifying Auto Push Management Setting Page Element");
		webActionUtils.verifyElementIsPresent(AutoPushManagementHeader);
		webActionUtils.info("Auto Push Management verified");
	}

	public void verifyChangeChecklistTitleSettingPage(String title) {
		webActionUtils.info("Verifying Change Checklist Title Setting Page Title");
		webActionUtils.verifyTitle(title);

	}

	public void verifyChangeChecklistTitleSettingPageElement() {
		webActionUtils.info("Verifying Change Checklist Title Setting Page Element");
		webActionUtils.scrollBy(ChangeChecklistTitleEleTxt);
		//webActionUtils.waitSleep(2);
		webActionUtils.verifyElementIsPresent(ChangeChecklistTitleEleTxt);
		webActionUtils.info("Change Check list Title Page Element Verified");
	}

	public void verifyChangeDatetoClientPolicyStatusPageElement() {
		webActionUtils.info("Verifying Change Date to Client And Policy Status Setting Page Element");
		webActionUtils.scrollBy(ChangeDatetoClientPolicyStatusEleTxt);
		//webActionUtils.waitSleep(2);
		webActionUtils.verifyElementIsPresent(ChangeDatetoClientPolicyStatusEleTxt);
		webActionUtils.info("Change Date to Client And Policy Status  Page Element Verified");
	}

	public void verifyChangeDatetoClientPolicyStatusSettingPage(String title) {
		webActionUtils.info("Verifying Change Date to Client And Policy Status Title");
		webActionUtils.verifyTitle(title);

	}

	public void verifyChecklistHeaderSettingPage(String title) {
		webActionUtils.info("Verifying Checklist Header Setting Page Title");
		webActionUtils.verifyTitle(title);

		// webActionUtils.pass("Page Title Verified " + title);
	}

	public void verifyChecklistHeaderSettingPageElement() {
		webActionUtils.info("Verifying Checklist Header Setting Page Element");
		webActionUtils.scrollBy(ChecklistHeaderSettingEleTxt);
		//webActionUtils.waitSleep(2);
		webActionUtils.verifyElementIsPresent(ChecklistHeaderSettingEleTxt);
		webActionUtils.info("Checklist Header Setting Page Element Verified");
	}

	public void verifyChecklistsSettingPage(String title) {
		webActionUtils.info("Verifying Check lists Setting Page Title");
		webActionUtils.verifyTitle(title);

		// webActionUtils.pass("Page Title Verified " + title);
	}

	public void verifyChecklistsSettingPageElement() {
		webActionUtils.info("Verifying Checklists Setting Page Element");
		webActionUtils.verifyElementIsPresent(ChecklistsEleTxt);
		webActionUtils.info("Checklists Setting Page Element Verified");
	}

	public void verifyChecklistTemplateSettingsOptionsList() {
		webActionUtils.info("Verifying Checklist Template Settings options");
		
		webActionUtils.waitUntilLoadedAndTextToBePresentInElementLocated(LOBSettingOption, "LOB Setting");
		webActionUtils.verifyElementText(LOBSettingOption, "LOB Setting");
		
		webActionUtils.waitUntilLoadedAndTextToBePresentInElementLocated(CheckpointSettingOption, "Checkpoint Setting");
		webActionUtils.verifyElementText(CheckpointSettingOption, "Checkpoint Setting");
		
		webActionUtils.waitUntilLoadedAndTextToBePresentInElementLocated(AMCustomizedCheckpointOption, "AM Customized Checkpoint");
		webActionUtils.verifyElementText(AMCustomizedCheckpointOption, "AM Customized Checkpoint");
		
		webActionUtils.waitUntilLoadedAndTextToBePresentInElementLocated(SectionLOBSettingOption, "Section LOB Setting");
		webActionUtils.verifyElementText(SectionLOBSettingOption, "Section LOB Setting");
		
		webActionUtils.waitUntilLoadedAndTextToBePresentInElementLocated(SourceSettingOption, "Source Setting");
		webActionUtils.verifyElementText(SourceSettingOption, "Source Setting");

		webActionUtils.waitUntilLoadedAndTextToBePresentInElementLocated(SectionSettingOption, "Section Setting");
		webActionUtils.verifyElementText(SectionSettingOption, "Section Setting");

		webActionUtils.waitUntilLoadedAndTextToBePresentInElementLocated(TemplateManagerOption, "Template Manager");
		webActionUtils.verifyElementText(TemplateManagerOption, "Template Manager");

		webActionUtils.waitUntilLoadedAndTextToBePresentInElementLocated(ConfigureAMSectionOption, "Configure AM Section");
		webActionUtils.verifyElementText(ConfigureAMSectionOption, "Configure AM Section");

		webActionUtils.waitUntilLoadedAndTextToBePresentInElementLocated(FormSectionSettingOption, "Form Section Setting");
		webActionUtils.verifyElementText(FormSectionSettingOption, "Form Section Setting");

		webActionUtils.waitUntilLoadedAndTextToBePresentInElementLocated(FormCheckpointSettingOption, "Form Checkpoint Setting");
		webActionUtils.verifyElementText(FormCheckpointSettingOption, "Form Checkpoint Setting");

		webActionUtils.waitUntilLoadedAndTextToBePresentInElementLocated(FormSettingonUploadFilePageOption, "Form Setting on Upload File Page");
		webActionUtils.verifyElementText(FormSettingonUploadFilePageOption, "Form Setting on Upload File Page");

		webActionUtils.waitUntilLoadedAndTextToBePresentInElementLocated(SourceAlternativeNameSettingOption, "Source Alternative Name Setting");
		webActionUtils.verifyElementText(SourceAlternativeNameSettingOption, "Source Alternative Name Setting");

		webActionUtils.waitUntilLoadedAndTextToBePresentInElementLocated(DefaultTemplateSectionsOption, "Default Template Sections");
		webActionUtils.verifyElementText(DefaultTemplateSectionsOption, "Default Template Sections");

		webActionUtils.waitUntilLoadedAndTextToBePresentInElementLocated(TemplateDefaultValueSettingOption, "Template Default Value Setting");
		webActionUtils.verifyElementText(TemplateDefaultValueSettingOption, "Template Default Value Setting");

		webActionUtils.waitUntilLoadedAndTextToBePresentInElementLocated(AMAlternativeNameSettingOption, "AM Alternative Name Setting");
		webActionUtils.verifyElementText(AMAlternativeNameSettingOption, "AM Alternative Name Setting");

		webActionUtils.waitUntilLoadedAndTextToBePresentInElementLocated(ChecklistHeaderSettingOption, "Checklist Header Setting");
		webActionUtils.verifyElementText(ChecklistHeaderSettingOption, "Checklist Header Setting");

		webActionUtils.pass("All Options are available");
	}

	public void verifyCheckpointSettingPage(String title) {
		webActionUtils.info("Verifying Check point Setting Page Title");
		webActionUtils.verifyTitle(title);
		// webActionUtils.pass("Page Title Verified " + title);
	}

	public void verifyCheckpointSettingPageElement() {
		webActionUtils.info("Verifying Check point Setting Page Element");
		webActionUtils.scrollBy(CheckpointEleTxt);
		//webActionUtils.waitSleep(2);
		webActionUtils.verifyElementIsPresent(CheckpointEleTxt);
		webActionUtils.info("Check point Setting Page Element Verified");
	}

	public void verifyClientEstimatedTime_TaskAssignmentSettingPageElement() {
		webActionUtils.info("Verifying Client Estimated -Task Assignment Setting Page Element");
		webActionUtils.scrollBy(TaskAssignmentSettingEleTxt);
		//webActionUtils.waitSleep(2);
		webActionUtils.verifyElementIsPresent(TaskAssignmentSettingEleTxt);
		webActionUtils.info("Client Estimated -Task Assignment Setting Page Element Verified");
	}

	public void verifyClientEstimatedTimeSettingPage(String title) {
		webActionUtils.info("Verifying  Client Estimated  Time Page Title");
		webActionUtils.verifyTitle(title);

		// webActionUtils.pass("Page Title Verified " + title);
	}

	public void verifyCommunicationSettingPage(String title) {
		webActionUtils.info("Verifying Communication Setting Page Title");
		webActionUtils.verifyTitle(title);

		// webActionUtils.pass("Page Title Verified " + title);
	}

	public void verifyCommunicationSettingPageElement() {
		webActionUtils.info("Verifying Communication Setting  Page  Element");
		webActionUtils.scrollBy(CommunicationSettingEleTxt);
		//webActionUtils.waitSleep(2);
		webActionUtils.verifyElementIsPresent(CommunicationSettingEleTxt);
		webActionUtils.info("Communication Setting Page Element Verified");
	}

	public void verifyConfigureAccountForDAISySettingPage(String title) {
		webActionUtils.info("Verifying Configure Account For DAISy Setting Page");
		webActionUtils.verifyTitle(title);

		webActionUtils.pass("Page Title Verified " + title);

	}

	public void verifyConfigureAccountForDAISySettingPageElement() {
		webActionUtils.info("Verifying ConfigureAccountForDAISySetting Page Element");
		webActionUtils.scrollBy(ConfigureAccountForDAISyEleTxt);
		//webActionUtils.waitSleep(2);
		webActionUtils.verifyElementIsPresent(ConfigureAccountForDAISyEleTxt);
		webActionUtils.info(" Page Element Verified");
	}

	public void verifyConfigureDiscrepancyAvailablePageElement() {
		webActionUtils.info("Verifying Configure Discrepancy Available Setting Page Element");
		webActionUtils.scrollBy(ConfigureDiscrepancyAvailableEleTxt);
		////webActionUtils.waitSleep(2);
		webActionUtils.verifyElementIsPresent(ConfigureDiscrepancyAvailableEleTxt);
		webActionUtils.info("Configure Discrepancy Available Page Element Verified");
	}

	public void verifyConfigureDiscrepancyAvailableSettingPage(String title) {
		webActionUtils.info("Verifying Configure Discrepancy Available Page Title");
		webActionUtils.verifyTitle(title);

		webActionUtils.pass("Page Title Verified " + title);
	}

	public void verifyConfigureNewClientActionsForAMPSettingPage(String title) {
		webActionUtils.info("Verifying Configure New Client Actions For AMP Page Title");
		webActionUtils.verifyTitle(title);

		webActionUtils.pass("Page Title Verified " + title);
	}

	public void verifyConfigureNewClientActionsforAMPSettingPageElement() {
		webActionUtils.info("Verifying Configure New Client Actions  for AMP Setting  Element");
		webActionUtils.scrollBy(ConfigureNewClientActionsforAMPSettingEleTxt);
		////webActionUtils.waitSleep(2);
		webActionUtils.verifyElementIsPresent(ConfigureNewClientActionsforAMPSettingEleTxt);
		webActionUtils.info(" Configure New Client Actions for AMP Setting Page Element Verified");
	}

	public void verifyCoverageSectionSettingPage(String title) {
		webActionUtils.info("Verifying  Coverage Section Settings Page Title");
		webActionUtils.verifyTitle(title);

		// webActionUtils.pass("Page Title Verified " + title);
	}

	public void verifyCoverageSectionSettings1PageElement() {
		webActionUtils.info("Verifying Coverage Section Settings-PKG Page  Element");
		webActionUtils.scrollBy(CoverageSectionSettings1EleTxt);
		////webActionUtils.waitSleep(2);
		webActionUtils.verifyElementIsPresent(CoverageSectionSettings1EleTxt);
		webActionUtils.info("Coverage Section Settings-PKG Page  Element Verified");
	}

	// 7
	// public void hoverOnArchiveSubMenu() {
	// webActionUtils.info("Hover on Submenu : Archive SubMenu");
	// webActionUtils.mouseHover(ArchiveSubMenu);
	//
	// }

	public void verifyCoverageSectionSettings2PageElement() {
		webActionUtils.info("Verifying Coverage Section Settings -FPKG Page  Element");
		webActionUtils.scrollBy(CoverageSectionSettings2EleTxt);
		////webActionUtils.waitSleep(2);
		webActionUtils.verifyElementIsPresent(CoverageSectionSettings2EleTxt);
		webActionUtils.info("Coverage Section Settings-FPKG Page  ElementVerified");
	}

	public void verifyCoverageSectionSettings3PageElement() {
		webActionUtils.info("Verifying Coverage Section Settings -EPKG Page  Element");
		webActionUtils.scrollBy(CoverageSectionSettings3EleTxt);
		////webActionUtils.waitSleep(2);
		webActionUtils.verifyElementIsPresent(CoverageSectionSettings3EleTxt);
		webActionUtils.info("Coverage Section Settings -EPKG Page Element Verified");
	}

	public void verifyCoverageSectionSettingsPageElement() {
		webActionUtils.info("Verifying Coverage Section Settings Page Element");
		webActionUtils.verifyElementIsPresent(CoverageSectionSettingstHeader);
		webActionUtils.info("Coverage Section Settings verified");

	}

	public void verifyDailyPackageStatusPageElement() {
		// webActionUtils.info("Verifying Daily Package Status Page Element");
		// webActionUtils.verifyElementIsPresent();
		// webActionUtils.scrollBy();
		// //webActionUtils.waitSleep(2);
		// webActionUtils.info(" Page Element Verified");
	}

	public void verifyDAISySettingsOptionsList() {
		webActionUtils.info("Verifying DAISy Settings options");
		webActionUtils.waitUntilLoadedAndTextToBePresentInElementLocated(DAISySettingOpn, "DAISy Setting");
		webActionUtils.verifyElementText(DAISySettingOpn, "DAISy Setting");
		webActionUtils.pass("All Options are available");
	}

	public void verifyDefaultTemplateSectionSettingPageElement() {
		webActionUtils.info("Verifying Default Template Section Setting Page Element");
		webActionUtils.scrollBy(DefaultTemplateSectionSettingEleTxt);
		////webActionUtils.waitSleep(2);
		webActionUtils.verifyElementIsPresent(DefaultTemplateSectionSettingEleTxt);
		webActionUtils.info("Default Template Section Setting Page Element Verified");
	}

	public void verifyDefaultTemplateSectionsSettingPage(String title) {
		webActionUtils.info("Verifying Default Template Sections Page Title");
		webActionUtils.verifyTitle(title);

		// webActionUtils.pass("Page Title Verified " + title);
	}

	public void verifyDepartmentMappingSettingPage(String title) {
		webActionUtils.info("Verifying Department Mapping Page Title");
		webActionUtils.verifyTitle(title);

		// webActionUtils.pass("Page Title Verified " + title);
	}

	public void verifyDepartmentMappingSettingPageElement() {
		webActionUtils.info("Verifying Department Mapping Setting Page Element");
		// webActionUtils.scrollBy(DepartmentMappingEleTxt);
		// //webActionUtils.waitSleep(2);
		webActionUtils.verifyElementIsPresent(DepartmentMappingEleTxt);
		webActionUtils.info("Department Mapping Setting Page Element Verified");
	}
	

	public void verifyDepartmentOfficeCodeSettingPage(String title) {
		webActionUtils.info("Verifying Department Office Code Setting Page Title");
		webActionUtils.verifyTitle(title);

		// webActionUtils.pass("Page Title Verified " + title);
	}

	public void verifyDepartmentOfficeCodeSettingPageElement() {
		webActionUtils.info("Verifying Manage Department Office Code Setting Page Element");
		webActionUtils.scrollBy(ManageDepartmentOfficeCodeSettingEleTxt);
		////webActionUtils.waitSleep(2);
		webActionUtils.verifyElementIsPresent(ManageDepartmentOfficeCodeSettingEleTxt);
		webActionUtils.info("Manage Department Office Code Setting Page Element Verified");
	}

	public void verifyDepartmentSettingPage(String expectedTitle) {
		try {
			webActionUtils.info("Verifying Department Settings Page Title");
			Boolean isTitleCorrect = webActionUtils.waitAndGetTitle(expectedTitle);
			if (isTitleCorrect) {
				String actualTitle = webActionUtils.getdriver().getTitle();
				webActionUtils.pass("Title is Verified");
				System.out.println("Title is correct: " +actualTitle);
				Assert.assertEquals(actualTitle, expectedTitle);

			} else {
				System.out.println("Department Setting Page Title did not match the expected value.");
			}
		} catch (Exception e) {
			webActionUtils.info("Department Setting Page Title is not Verified");
			 e.printStackTrace();
			Assert.fail();
		}
	}

	public void verifyDepartmentSettingPageElement() {
		webActionUtils.info("Verifying Department Setting Page Element");
		webActionUtils.scrollBy(DeptEleTxt);
		////webActionUtils.waitSleep(2);
		webActionUtils.verifyElementIsPresent(DeptEleTxt);
		webActionUtils.info("Department Setting Page Element Verified");
	}

	public void clickOnParameterSettingOption() {
		webActionUtils.info("Click On Parameter Setting Option");
		webActionUtils.clickElement(ParameterSettingOption, "Parameter Setting");
		//webActionUtils.waitSleep(5);
	}
	
	public void selectrequiredAccount(String account) {
		try {
			webActionUtils.info("select required Account "+account);
			//webActionUtils.waitSleep(3);
			webActionUtils.selectByVisibleText(parameterAccountList, account);
			webActionUtils.waitSleep(3);
		} catch (Exception e) {
e.printStackTrace();		}
		
	

	}
	
	public void enterParameter() {
		
		
		webActionUtils.info("enter required Parameter name");
		webActionUtils.setText(Parameter, RandomStringUtils.randomAlphanumeric(10));
		webActionUtils.waitSleep(3);
	}
	
	
	public void checkSemicolonisNotAllowedParameter() {

		webActionUtils.info("Enter SemiColon into Parameter name");
		Parameter.click();
		Parameter.sendKeys(Keys.SEMICOLON);
		webActionUtils.waitSleep(3);
		webActionUtils.info("Entered Chareter is ;");

		if (Parameter.getAttribute("value").isBlank()||Parameter.getAttribute("value").isEmpty()) {
			webActionUtils.info( "Chareter taken"+Parameter.getAttribute("value"));
			webActionUtils.pass(" SemiColon is not present Parameter name Field");
	
		}else {
			webActionUtils.fail(" SemiColon is present Parameter name Field");

		}
	}
	
	public void checkAllowedSplCharParameter() {

		webActionUtils.info("Enter Special Charter into Parameter name");
		Parameter.click();
		String str="!@#$%^&*()_+{}|\\\\':\\\",./<>,??/";
		webActionUtils.info("Entered Charter is "+str);
		Parameter.sendKeys(str);
		webActionUtils.waitSleep(3);
		
		if (Parameter.getAttribute("value").equals(str)) {
			webActionUtils.info("Chareter taken"+Parameter.getAttribute("value"));
			webActionUtils.pass("SPL Char is present Parameter name Field");
	
		}else {
			webActionUtils.fail(" SPL Char is not present Parameter name Field");

		}
	}
	
//	public void selectParameterSaveButton() {
//		webActionUtils.info("Click On Parameter :AddtoCheckingTypeBTN button");
//		webActionUtils.clickElement(AddtoCheckingTypeBTN, "AddtoCheckingType button");
//		webActionUtils.waitSleep(5);
//	}
	
	public void clickOnAddToOfficeOrLocationButton() {
		webActionUtils.info("Click On Parameter :AddToOfficeOrLocation button");
		webActionUtils.clickElement(AddToOfficeOrLocationBTN, "AddToOfficeOrLocation button");
	//	//webActionUtils.waitSleep(2);
	}
	
	public void clickOnAddtoSegmentationButton() {
		webActionUtils.info("Click On Parameter :AddtoSegmentationBTN button");
		webActionUtils.clickElement(AddtoSegmentationBTN, "AddtoSegmentation button");
	//	//webActionUtils.waitSleep(2);
	}
	
	
	public void clickOnAddtoCheckingTypeButton() {
		webActionUtils.info("Click On Parameter :AddtoCheckingTypeBTN button");
		webActionUtils.clickElement(AddtoCheckingTypeBTN, "AddtoCheckingType button");
		////webActionUtils.waitSleep(2);
	}
	
	public void clickOnParameterSaveButton() {
		webActionUtils.info("Click On Parameter : Save button");
		webActionUtils.clickElement(parameterSave, "parameter Save button");
		//webActionUtils.waitSleep(3);
	}
	
	public void verifyParameterMessage() {
		webActionUtils.info("verify Parameter Message");
		webActionUtils.verifyElementContainsText(parameterMessage, "Message: Data is saved successfully.");
	}
	
	
	
	
	
	
	
	
	public void verifyDepartmentSettingsOptionsList() {
		webActionUtils.info("Verifying  Department Settings options");
		
		webActionUtils.waitUntilLoadedAndTextToBePresentInElementLocated(DepartmentSettingOption, "Department Setting");
		webActionUtils.verifyElementText(DepartmentSettingOption, "Department Setting");
		
		webActionUtils.waitUntilLoadedAndTextToBePresentInElementLocated(DepartmentMappingOption, "Department Mapping");		
		webActionUtils.verifyElementText(DepartmentMappingOption, "Department Mapping");
		
		webActionUtils.waitUntilLoadedAndTextToBePresentInElementLocated(ParameterSettingOption, "Parameter Setting");
		webActionUtils.verifyElementText(ParameterSettingOption, "Parameter Setting");
		
		webActionUtils.waitUntilLoadedAndTextToBePresentInElementLocated(DepartmentSegmentationOption, "Department Segmentation");
		webActionUtils.verifyElementText(DepartmentSegmentationOption, "Department Segmentation");
		
		webActionUtils.pass("All Options are available");
	}
	
	

	public void verifyDiscrepancyWordManagement1SettingPageElement() {
		webActionUtils.info("Verifying Manage Discrepancy Settings Page Element");
		webActionUtils.scrollBy(ManageDiscrepancy1EleTxt);
		////webActionUtils.waitSleep(2);
		webActionUtils.verifyElementIsPresent(ManageDiscrepancy1EleTxt);
		webActionUtils.info("Manage Discrepancy Word Details Selected Capital Words: Page Element Verified");
	}

	public void verifyDiscrepancyWordManagement2SettingPageElement() {
		webActionUtils.info("Verifying Manage Discrepancy Settings Page  Element");
		webActionUtils.scrollBy(ManageDiscrepancy2EleTxt);
	//	//webActionUtils.waitSleep(2);
		webActionUtils.verifyElementIsPresent(ManageDiscrepancy2EleTxt);
		webActionUtils.info("Manage Discrepancy Word Details Selected Small Words: Page Element Verified");
	}

	public void verifyDiscrepancyWordManagementPageElement() {
		webActionUtils.info("Verifying Discrepancy Word Management Page Element");
		webActionUtils.verifyElementIsPresent(DiscrepancyWordManagementHeader);
		webActionUtils.info("Discrepancy Word Management verified");
	}

	public void verifyDiscrepancyWordManagementSettingPage(String title) {
		webActionUtils.info("Verifying Discrepancy Word Management Page Title");
		webActionUtils.verifyTitle(title);

		// webActionUtils.pass("Page Title Verified " + title);
	}

	public void verifyFormCheckpointSettingPage(String title) {
		webActionUtils.info("Verifying Form Checkpoint Setting Page Title");
		webActionUtils.verifyTitle(title);

		// webActionUtils.pass("Page Title Verified " + title);
	}

	public void verifyFormCheckpointSettingPageElement() {
		webActionUtils.info("Verifying Form Checkpoint Setting Page Element");
		webActionUtils.scrollBy(FormCheckpointSettingEleTxt);
		//webActionUtils.waitSleep(2);
		webActionUtils.verifyElementIsPresent(FormCheckpointSettingEleTxt);
		webActionUtils.info("Form Checkpoint Setting Page Element Verified");
	}

	public void verifyFormDiscrepancyReportSettingPage(String title) {
		webActionUtils.info("Verifying  Form Discrepancy Report Setting Page");
		webActionUtils.verifyTitle(title);

		webActionUtils.pass("Page Title Verified " + title);

	}

	public void verifyFormDiscrepancyReportSettingPageElement() {
		webActionUtils.info("Verifying Form Discrepancy Report  Setting Page Element");
		webActionUtils.scrollBy(FormDiscrepancyReportEleTxt);
		//webActionUtils.waitSleep(2);
		webActionUtils.verifyElementIsPresent(FormDiscrepancyReportEleTxt);
		webActionUtils.info(" Page Element Verified");
	}

	public void verifyFormLibrarySettingPage(String title) {
		webActionUtils.info("Verifying Form Library Setting Page  Title");
		webActionUtils.verifyTitle(title);

		// webActionUtils.pass("Page Title Verified " + title);
	}

	public void verifyFormLibrarySettingPageElement() {
		webActionUtils.info("Verifying Form Library Setting Page Element");
		webActionUtils.scrollBy(FormLibrarySettingEleTxt);
		//webActionUtils.waitSleep(2);
		webActionUtils.verifyElementIsPresent(FormLibrarySettingEleTxt);
		webActionUtils.info("Form Library Setting Page Element Verified");
	}

	public void verifyFormSectionSettingPage(String title) {
		webActionUtils.info("Verifying Form Section Setting Page Title");
		webActionUtils.verifyTitle(title);

		// webActionUtils.pass("Page Title Verified " + title);
	}

	public void verifyFormSectionSettingPageElement() {
		webActionUtils.info("Verifying Form Section Setting Page Element");
		webActionUtils.scrollBy(FormSectionSettingEleTxt);
		//webActionUtils.waitSleep(2);
		webActionUtils.verifyElementIsPresent(FormSectionSettingEleTxt);
		webActionUtils.info("Form Section Setting Page Element Verified");
	}

	public void verifyFormSettingUploadFileSettingPage(String title) {
		webActionUtils.info("Verifying Form Setting on Upload File Page Title");
		webActionUtils.verifyTitle(title);

		// webActionUtils.pass("Page Title Verified " + title);
	}

	public void verifyFormSettingUploadFileSettingPageElement() {
		webActionUtils.info("Verifying Checklist Header Setting Page Element");
		webActionUtils.scrollBy(FormSettingUploadFilePageEleTxt);
		//webActionUtils.waitSleep(2);
		webActionUtils.verifyElementIsPresent(FormSettingUploadFilePageEleTxt);
		webActionUtils.info("Checklist Header Setting Page Element Verified");
	}

	public void verifyIgnoreFormSetting_NonDiscrepancyFormLibrarySettingPageElement() {
		webActionUtils.info("Verifying Ignore Form Setting/ Non Discrepancy Form Library Setting Page Element");
		webActionUtils.scrollBy(NonDiscrepancyFormLibrarySettingEleTxt);
		//webActionUtils.waitSleep(2);
		webActionUtils.verifyElementIsPresent(NonDiscrepancyFormLibrarySettingEleTxt);
		webActionUtils.info("Ignore Form Setting/ Non Discrepancy Form Library Setting Page Element Verified");
	}

	public void verifyIgnoreFormSettingPage(String title) {
		webActionUtils.info("Verifying Ignore Form Setting Page Title");
		webActionUtils.verifyTitle(title);

		// webActionUtils.pass("Page Title Verified " + title);
	}

	public void verifyIRPackageStatusSettingPage(String title) {
		webActionUtils.info("Verifying IR Package Status Page Title");
		webActionUtils.verifyTitle(title);

		webActionUtils.pass("Page Title Verified " + title);
	}

	public void verifyIRPackageStatusSettingPageElement() {
		webActionUtils.info("Verifying Package Status Setting Page Element");
		webActionUtils.scrollBy(IRPackageStatusCheckEleTxt);
		//webActionUtils.waitSleep(2);
		webActionUtils.verifyElementIsPresent(IRPackageStatusCheckEleTxt);
		webActionUtils.pass(" Page Element Verified");

	}

	public void verifyIRPackageStatusSettingsOptionsList() {
		webActionUtils.info("Verifying IR Package status Settings options");
		webActionUtils.verifyElementText(IRPackageStatusOption, "IR Package Status");
		webActionUtils.pass("All Options are available");
	}

	public void verifyLOBSettingPage(String title) {
		webActionUtils.info("Verifying LOB Setting Page Title");
		webActionUtils.verifyTitle(title);
		// webActionUtils.pass("Page Title Verified " + title);
	}

	public void verifyLOBSettingPageElement() {
		webActionUtils.info("Verifying LOB Setting Page Element");
		webActionUtils.verifyElementIsPresent(LOBSettingHeaderTxt);
		webActionUtils.info("LOB Setting Page Element Verified");
	}

	public void verifyPIPlatformRefreshErrorMessage(String expectedText) {
		webActionUtils.info(" verifying PI Platform Refresh Error Message");
		webActionUtils.waitUntilLoadedAndTextToBePresentInElementLocated(Errormessage, expectedText);
		webActionUtils.verifyElementText(Errormessage, expectedText);
		webActionUtils.waitSleep(5);

	}

	public void verifyPolicyListSettingPage(String title) {
		webActionUtils.info("Verifying Policy list Setting Page");
		webActionUtils.verifyTitle(title);

		webActionUtils.pass("Page Title Verified " + title);
	}

	public void verifyPolicyListSettingPageElement() {
		webActionUtils.info("Verifying  Setting Page Element");
		webActionUtils.scrollBy(PolicyListEleTxt);
		//webActionUtils.waitSleep(2);
		webActionUtils.verifyElementIsPresent(PolicyListEleTxt);
		webActionUtils.info(" Page Element Verified");

	}

	public void verifyPolicyStatusTablePageElement() {
		webActionUtils.info("Verifying Policy Status Table LOB Element");
		webActionUtils.waitForPageLoad();
		webActionUtils.scrollBy(policyStatusTable);
		//webActionUtils.waitSleep(2);
		webActionUtils.verifyElementIsPresent(policyStatusTable);
		webActionUtils.info("Policy Status Table Element Verified");
	}

	public void verifyRefreshSettingPage(String title) {
		webActionUtils.info("Verifying Refresh AMP Page Title");
		webActionUtils.verifyTitle(title);

		webActionUtils.pass("Page Title Verified " + title);
		webActionUtils.waitSleep(5);

	}

	public void verifyRefreshSettingPageElement() {
		webActionUtils.info("Verifying Refresh Page Element");
		webActionUtils.verifyElementIsPresent(RefreshTitle);
		webActionUtils.info("Page Element Verified");
	}

	public void verifyRefreshSettingsOptionsList() {
		webActionUtils.info("Verifying Refresh Settings options");
		webActionUtils.verifyElementText(RefreshOption, "Refresh");
		webActionUtils.pass("All Options are available");
	}

	public void verifySectionLOBSettingPage(String title) {
		webActionUtils.info("Verifying  Section LOB Setting Page Title");
		webActionUtils.verifyTitle(title);

		// webActionUtils.pass("Page Title Verified " + title);
	}

	public void verifySectionLOBSettingPageElement() {
		webActionUtils.info("Verifying Section LOB Setting Page Element");
		webActionUtils.scrollBy(SectionLOBSettingEleTxt);
		//webActionUtils.waitSleep(2);
		webActionUtils.verifyElementIsPresent(SectionLOBSettingEleTxt);
		webActionUtils.info("Section LOB Page Element Verified");
	}

	public void verifySectionSettingPage(String title) {
		webActionUtils.info("Verifying Section Setting Page Title");
		webActionUtils.verifyTitle(title);

		// webActionUtils.pass("Page Title Verified " + title);
	}

	public void verifySectionSettingPageElement() {
		webActionUtils.info("Verifying Section Setting Page Element");
		webActionUtils.scrollBy(SectionSettingEleTxt);
		//webActionUtils.waitSleep(2);
		webActionUtils.verifyElementIsPresent(SectionSettingEleTxt);
		webActionUtils.info("Section Setting Page Element Verified");
	}

	public void verifySettingPageListinGivenOrder(String departmentSettings, String checklistTemplateSettings,
			String advancedSettings, String accountManagerSetting, String adhocPIRequestSetting, String dAISySettings,
			String aMPSettings, String refresh, String iRPackageStatus) {
		webActionUtils.info("verifying Setting Page List in Given Order");
		List<String> arrList1 = Arrays.asList(
				new String[] { departmentSettings, checklistTemplateSettings, advancedSettings, accountManagerSetting,
						adhocPIRequestSetting, dAISySettings, aMPSettings, refresh, iRPackageStatus });

		List<String> arrList2 = Arrays
				.asList(new String[] { DepartmentSettingsSubMenu.getText(), ChecklistTemplateSettingsSubMenu.getText(),
						AdvancedSettingsSubMenu.getText(), AccountManagerSettingSubMenu.getText(),
						AdhocPIRequestSettingSubMenu.getText(), DAISySettingSubMenu.getText(),
						AMPSettingsSubMenu.getText(), RefreshSubMenu.getText(), IRPackageStatusSubMenu.getText() });

		System.out.println("Are List 1 and List2 equal? " + arrList1.equals(arrList2));
		webActionUtils.pass("Setting Page: Lists are in the Given Order");
	}

	
	public void verifySettingsAllSubMenu() {
		webActionUtils.info("Checking all sub menu under settings");
		webActionUtils.verifyElementIsPresent(DepartmentSettingsSubMenu);
		webActionUtils.verifyElementIsPresent(ChecklistTemplateSettingsSubMenu);
		webActionUtils.verifyElementIsPresent(AdvancedSettingsSubMenu);
		webActionUtils.verifyElementIsPresent(AccountManagerSettingSubMenu);
		webActionUtils.verifyElementIsPresent(AdhocPIRequestSettingSubMenu);
		webActionUtils.verifyElementIsPresent(DAISySettingSubMenu);
		// webActionUtils.verifyElementIsPresent(ArchiveSubMenu);
		webActionUtils.verifyElementIsPresent(AMPSettingsSubMenu);
		webActionUtils.verifyElementIsPresent(RefreshSubMenu);
		webActionUtils.verifyElementIsPresent(IRPackageStatusSubMenu);
		webActionUtils.pass("All sub menu are displayed");

	}

	public void verifySourceAlternativeNameSettingPage(String title) {
		webActionUtils.info("Verifying Source Alternative Name Setting Page Title");
		webActionUtils.verifyTitle(title);

		// webActionUtils.pass("Page Title Verified " + title);
	}

	public void verifySourceAlternativeNameSettingPageElement() {
		webActionUtils.info("Verifying Source Alternative Name Setting Page Element");
		webActionUtils.scrollBy(SourceAlternativeNameSettingEleTxt);
		//webActionUtils.waitSleep(2);
		webActionUtils.verifyElementIsPresent(SourceAlternativeNameSettingEleTxt);
		webActionUtils.info("Source Alternative Name Setting Page Element Verified");
	}

	public void verifySourceSettingPage(String title) {
		webActionUtils.info("Verifying  Source Setting Page Title");
		webActionUtils.waitForPageLoad();
		webActionUtils.verifyTitle(title);

		// webActionUtils.pass("Page Title Verified " + title);
	}

	public void verifySourceSettingPageElement() {
		webActionUtils.info("Verifying Source Setting Page Element");
		webActionUtils.scrollBy(SourceSettingEleTxt);
		//webActionUtils.waitSleep(2);
		webActionUtils.verifyElementIsPresent(SourceSettingEleTxt);
		webActionUtils.info("Source Setting Page Element Verified");
	}

	public void verifyStandardLOBPageElement() {
		webActionUtils.info("Verifying Standard LOB Element");
		webActionUtils.waitForPageLoad();
		webActionUtils.scrollBy(StandardLOBEleTxt);
		//webActionUtils.waitSleep(2);
		webActionUtils.verifyElementIsPresent(StandardLOBEleTxt);
		webActionUtils.info("Standard LOB Element Verified");
	}

	public void verifyTemplateDefaultValueSettingPage(String title) {
		webActionUtils.info("Verifying Template Default Value Setting Page Title");
		webActionUtils.verifyTitle(title);

		// webActionUtils.pass("Page Title Verified " + title);
	}

	public void verifyTemplateDefaultValueSettingPageElement() {
		webActionUtils.info("Verifying Template Default Value Setting Page Element");
		webActionUtils.scrollBy(TemplateDefaultValueSettingEleTxt);
		//webActionUtils.waitSleep(2);
		webActionUtils.verifyElementIsPresent(TemplateDefaultValueSettingEleTxt);
		webActionUtils.info("Template Default Value Setting Page Element Verified");
	}

	public void verifyTemplateManagerSettingPage(String title) {
		webActionUtils.info("Verifying Template Manager Page Title");
		webActionUtils.verifyTitle(title);

		// webActionUtils.pass("Page Title Verified " + title);
	}

	public void verifyTemplateManagerSettingPageElement() {
		webActionUtils.info("Verifying Template Manager Setting Page Element");
		webActionUtils.scrollBy(TemplateManagerEleTxt);
		//webActionUtils.waitSleep(2);
		webActionUtils.verifyElementIsPresent(TemplateManagerEleTxt);
		webActionUtils.info("Template Manager Setting Page Element Verified");
	}

	public void verifyTreeIsPresent() {
		webActionUtils.info("verifying Tree ");
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		webActionUtils.verifyElementIsPresent(TreeEle);
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		webActionUtils.pass("Tree is present");
	}

	public void verifyUploadFileAlternativeSettingPage(String title) {
		webActionUtils.info("Verifying Upload File Alternative Setting Page Title");
		webActionUtils.verifyTitle(title);

		// webActionUtils.pass("Page Title Verified " + title);
	}

	public void verifyUploadFileAlternativeSettingPageElement() {
		webActionUtils.info("Verifying Upload File Alternative Setting Page  Element");
		webActionUtils.scrollBy(UploadFileAlternativeSettingEleTxt);
		//webActionUtils.waitSleep(2);
		webActionUtils.verifyElementIsPresent(UploadFileAlternativeSettingEleTxt);
		webActionUtils.info("Upload File Alternative Setting Page Element Verified");
	}

	
	
	public void clickOnUpdatePISOwnersOption() {
		webActionUtils.info("Click On 'Update PIS Owners Option");
		webActionUtils.clickElement(UpdatePISOwnersOpn, "'Update PIS Owners Option");
		webActionUtils.waitSleep(5);
	}
	
	
	public void verifyUpdatePISOwners(String title) {
		webActionUtils.info("Verifying Update PIS Owners Page Title");
		webActionUtils.verifyTitle(title);
		webActionUtils.pass("Page Title Verified " + title);
	}

	public void verfiyUpdatePISOwnersOptionAvailableAdhoc() {
		webActionUtils.info("Verifying Update PIS Owners Option is avaiable in Ad-hoc PI Request Settings options");	
		webActionUtils.verifyElementText(UpdatePISOwnersOpn, "Update PIS Owners");
	webActionUtils.pass("Update PIS Owners Option is Available");		
	}

	
	@FindBy(id="spanAccountDDL")
	private WebElement accountDD;
	
	
	@FindBy(id="spanSelecteDepartments")
	private WebElement DepartmentDD;
	
	
	
	@FindBy(xpath = "//input[@type='checkbox' and @name='selectAllAccountDDL']")
	private WebElement selectAllAccountDDL;
	
	
	
	@FindBy(xpath = "//input[@type='checkbox' and @name='selectAllSelecteDepartments']")
	private WebElement selectAllSelecteDepartmentsDD;
	
	@FindBy(xpath = "//tr/td[3]")
	private List<WebElement> listOfRecords;
	
	public void CheckUpdatePISOwnersRecordDisplayed() {
		webActionUtils.info(" Check records dashboard  is created  below the 'Save' and 'Clear' button of Update PIS owner");	
		webActionUtils.waitUntilLoadedAndPresenceOfElementLocated(accountDD);
		webActionUtils.clickElement(accountDD, "Account Dropdown");
		webActionUtils.waitUntilLoadedAndElementClickable(selectAllAccountDDL);
		webActionUtils.clickElement(selectAllAccountDDL, "select All Account Dropdown option");
		//webActionUtils.waitSleep(2);
		webActionUtils.waitUntilLoadedAndPresenceOfElementLocated(DepartmentDD);
		webActionUtils.clickElement(DepartmentDD, "Department Dropdown");
		webActionUtils.waitUntilLoadedAndElementClickable(selectAllSelecteDepartmentsDD);
		webActionUtils.clickElement(selectAllSelecteDepartmentsDD, "select All Department Dropdown option");
		webActionUtils.clickElement(SearchBtn, "Search Button");
		//webActionUtils.waitSleep(3);
		webActionUtils.waitUntilLoadedAndVisibilityOfAllElements(listOfRecords);
			try {
				if (listOfRecords.size() != 0) {
					System.out.println(listOfRecords.size());
					for (WebElement Records : listOfRecords) {
						webActionUtils.info("PIS Owners:Details" + Records.getText() + "  Records Displayed TC passed");
					}
				} else {
					webActionUtils.info("No records ");
				}
			} catch (Exception e) {
				webActionUtils.info("No Records");
			}
		}

	public void verifyParameterSettingPage(String parameterSetting) {
		webActionUtils.info("Verifying parameter Setting Page Title");
		webActionUtils.verifyTitle(parameterSetting);		
	}

	public void verifyDepartmentSegmentationPage(String departmentSegmentation) {
		webActionUtils.info("Verifying departmentSegmentation Page Title");
		webActionUtils.verifyTitle(departmentSegmentation);		
	}

	public void verifyParameterSettingPageElement() {
		webActionUtils.verifyElementIsPresent(ParameterSettingEleTxt);
		webActionUtils.info("Department Mapping Setting Page Element Verified");		
	}

	public void verifyDepartmentSegmentationSettingPageElement() {
		webActionUtils.verifyElementIsPresent(DepartmentSegmentationEleTxt);
		webActionUtils.info("Department Mapping Setting Page Element Verified");		
	}

	public void clickConfigureAMSectionOption() {
		webActionUtils.info("Clicking on the Configure AM Section Option ");
		webActionUtils.clickElement(ConfigureAMSectionOption, "Configure AM Section Option ");		
	}

	public void verifyConfigureAMSectionSettingPage(String configureAMSection) {
		webActionUtils.info("Verifying configure AM Section Page Title");
		webActionUtils.verifyTitle(configureAMSection);	
	}

	public void verifyConfigureAMSectionSettingPageElement() {
		webActionUtils.verifyElementIsPresent(ConfigureAMSectionEleTxt);
		webActionUtils.info("Configure AM Section Setting Page Element Verified");					
	}

	public void clickTemplateDefaultValuesOption() {
		webActionUtils.info("Clicking on the Configure AM Section Option ");
		webActionUtils.clickElement(TemplateDefaultValueSettingOption, "Configure AM Section Option ");			
	}

	public void verifyTemplateDefaultValuesSettingPage(String templateDefaultValues) {
		webActionUtils.info("Verifying template Default Values Page Title");
		webActionUtils.verifyTitle(templateDefaultValues);			
	}

	public void verifyTemplateDefaultValuesSettingPageElement() {
		webActionUtils.verifyElementIsPresent(TemplateDefaultValueSettingEleTxt);
		webActionUtils.info("Configure Template Default Value Setting Element Verified");				
	}

	public void clickRushSettingOption() {
		webActionUtils.info("Clicking on the Rush Setting Option ");
		webActionUtils.clickElement(RushSettingOption, "RushSetting Option");		
	}

	public void verifyRushSettingPage(String rushSetting) {
		webActionUtils.info("Verifying Rush Setting Page  Title");
		webActionUtils.verifyTitle(rushSetting);		
	}

	public void verifyRushSettingPageElement() {
		webActionUtils.info("Verifying Rush Setting Page Element");
		webActionUtils.scrollBy(RushSettingEleTxt);
		webActionUtils.verifyElementIsPresent(RushSettingEleTxt);
		webActionUtils.info("Rush Setting Page Element Verified");		
	}

	public void clickNoteAndMissingInfoOption() {
		webActionUtils.info("Clicking on the Note And Missing Info Option ");
		webActionUtils.clickElement(NoteAndMissingInfoOption, "Note And Missing Info Option");		
	}

	public void verifyNoteAndMissingInfoPage(String noteAndMissingInfo) {
		webActionUtils.info("Verifying Note And Missing Info Page  Title");
		webActionUtils.verifyTitle(noteAndMissingInfo);		
	}

	public void verifyNoteAndMissingInfoPageElement() {
		webActionUtils.info("Verifying Note And Missing Info Element");
		webActionUtils.scrollBy(NoteAndMissingInfoEleTxt);
		webActionUtils.verifyElementIsPresent(NoteAndMissingInfoEleTxt);
		webActionUtils.pass("Note And Missing Info Page Element Verified");		
	}

	public void clickCommunicationReportOption() {
		webActionUtils.info("Clicking on the Communication Report Option ");
		webActionUtils.clickElement(CommunicationReportOpn, " Form Discrepancy Report Option");		
	}

	public void verifyCommunicationReportPage(String CommunicationReport) {
		webActionUtils.info("Verifying Communication Report Page Title");
		webActionUtils.verifyTitle(CommunicationReport);				
	}

	public void verifyCommunicationReportPageElement() {
		webActionUtils.info("Verifying Communication Report Element");
		webActionUtils.scrollBy(CommunicationReportEleTxt);
		webActionUtils.verifyElementIsPresent(CommunicationReportEleTxt);
		webActionUtils.pass("Communication Report Page Element Verified");			
	}

	public void clickRushReportOption() {
		webActionUtils.info("Clicking on the Rush Report Option ");
		webActionUtils.clickElement(RushReportOpn, "Rush Report Option");			
	}

	public void verifyRushReportgPage(String RushReport) {
		webActionUtils.info("Verifying Rush Report Page Title");
		webActionUtils.verifyTitle(RushReport);				
	}

	public void verifyRushReportPageElement() {
		webActionUtils.info("Verifying Rush Report Element");
		webActionUtils.scrollBy(RushReportEleTxt);
		webActionUtils.verifyElementIsPresent(RushReportEleTxt);
		webActionUtils.pass("Rush Report Page Element Verified");			
	}

	public void clickUpdatePISOwnersOption() {
		webActionUtils.info("Clicking on the Update PIS Owners Option ");
		webActionUtils.clickElement(UpdatePISOwnersOpn, "Update PIS Owners Option");			
	}		
	

	public void verifyUpdatePISOwnersPage(String UpdatePISOwners) {
		webActionUtils.info("Verifying Update PIS Owners Page Title");
		webActionUtils.verifyTitle(UpdatePISOwners);						
	}

	public void verifyUpdatePISOwnersPageElement() {
		webActionUtils.info("Verifying Update PIS Owners Element");
		webActionUtils.scrollBy(UpdatePISOwnersEleTxt);
		webActionUtils.verifyElementIsPresent(UpdatePISOwnersEleTxt);
		webActionUtils.pass("Update PIS Owners Page Element Verified");					
	}

	public void clickAMPSeverityIndicatorOption() {
		webActionUtils.info("Clicking on the AMP Severity Indicator Option ");
		webActionUtils.clickElement(AMPSeverityIndicatorOption, "AMP Severity Indicator Option");				
	}

	public void verifyAMPSeverityIndicatorPage(String AMPSeverityIndicator) {
		webActionUtils.info("Verifying AMP Severity Indicator Page Title");
		webActionUtils.verifyTitle(AMPSeverityIndicator);				
	}

	public void verifyAMPSeverityIndicatorPageElement() {
		webActionUtils.info("Verifying AMP Severity Indicator Element");
		webActionUtils.scrollBy(AMPSeverityIndicatorsEleTxt);
		webActionUtils.verifyElementIsPresent(AMPSeverityIndicatorsEleTxt);
		webActionUtils.pass("AMP Severity Indicator Page Element Verified");				
	}
	
	public void clickOnAddbuttonDepartmentSettingsPage() {
		webActionUtils.info("Click on the Add button in Department Settings Page");
		try {
			webActionUtils.clickElement(addBTN, "Add Button");

		} catch (Exception e) {
			webActionUtils.fail(" Service Type Field Is not Available in Department Settings Page");
		}
		
		
	}

	public void verifyServiceTypeFieldIsAvailbleinDepartmentSettingsPage() {
		webActionUtils.info("Verifying Service Type Field Is Available in Department Settings Page");
		try {
			webActionUtils.verifyElementContainsText(serviceTypeTxt1, "Service Type*");
			webActionUtils.pass(" Service Type Field Is Available in Department Settings Page");

		} catch (Exception e) {
			webActionUtils.fail(" Service Type Field Is not Available in Department Settings Page");
		}
		
		
	}
	
	public void verifyServiceTypeFieldIsAvailbleinDepartmentSegmentationPage() {
		webActionUtils.info("Verifying Service Type Field Is Available in Department Segmentation Settings Page");
		try {
			webActionUtils.verifyElementContainsText(serviceTypeTxt2, "Service Type*");
			webActionUtils.pass(" Service Type Field Is Available in Department Segmentation Page");

		} catch (Exception e) {
			webActionUtils.fail(" Service Type Field Is not Available in Department Segmentation Page");
		}
			}

	public void verifyServiceTypeFieldIsAvailableBelowDepartmentField() {
		webActionUtils.info("Verifying Service Type Field Is Available Below 'Department/Program' Field in the Department Settings Page");
		try {
			webActionUtils.verifyElementContainsText(serviceTypeTxt1, "Service Type*");
			webActionUtils.pass(" Service Type Field Is Available Below  'Department/Program' Field in Department Settings Page");

		} catch (Exception e) {
			webActionUtils.fail(" Service Type Field Is not Available in Department Settings Page");
		}
		
		
	}

	public void verifyServicetypeOptionsAvailable(String pIAdvanceCheck, String pIExpressCheck) {

		webActionUtils.info("Verify All Service type Options Available in the Department Settings Page");

		List<WebElement> list1 = webActionUtils.getAllSelectOptions(selectServiceType);

		for (WebElement webElement : list1) {

			if (webElement.getText().equals(pIAdvanceCheck)) {
				webActionUtils.info(pIAdvanceCheck + " option is present");

			} else if (webElement.getText().equals(pIExpressCheck)) {
				webActionUtils.info(pIExpressCheck + " option is present");
			}
		}
	}

}
