package com.automation.pi.pages;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.automation.pi.library.BasePage;
import com.automation.pi.library.WebActionUtils;

public class LoginPage extends BasePage {

	WebDriver driver;
	@FindBy(id = "username")
	private WebElement usernameTB;

	@FindBy(id = "password")
	private WebElement passwordTB;

	@FindBy(xpath = "//button[@class='btn btn-primary btn-login btn-large']")
	private WebElement signInBTN;

	@FindBy(id = "spanMessageContent")
	private WebElement errorMsg;

	public LoginPage(WebDriver driver, WebActionUtils webActionUtils) {
		super(driver, webActionUtils);
	}

	public void enterUserName(String text) {
		try {
			webActionUtils.info("Entering Username");
			usernameTB.clear();
			webActionUtils.setText(usernameTB, text);
		} catch (Exception e) {
			webActionUtils.fail("Entering Username: Failed");
			e.printStackTrace();
			Assert.fail();

		}
	}

	public void enterPassword(String text) {
		try {
			webActionUtils.info("Entering Password");
			passwordTB.clear();
			webActionUtils.setTextPassword(passwordTB, text);
		} catch (Exception e) {
			webActionUtils.fail("Entering Password: Failed");
			e.printStackTrace();
			Assert.fail();

		}
	}

	public void clickOnSignin() {
		try {
			webActionUtils.info("Signin into Policy Application");
			webActionUtils.clickElement(signInBTN, "Signin Button");
		} catch (Exception e) {
			webActionUtils.fail("Unable to Login: Failed");
			e.printStackTrace();
			Assert.fail();
		}

	}

	public void verifyLoginPageTitle(String expectedTitle) {
		try {
			webActionUtils.info("Verifying Login Page Title");
			Boolean isTitleCorrect = webActionUtils.waitAndGetTitle(expectedTitle);
			if (isTitleCorrect) {
				String actualTitle = webActionUtils.getdriver().getTitle();
				webActionUtils.pass("Title is Verified");
				System.out.println("Title is correct: " +actualTitle);
				Assert.assertEquals(actualTitle, expectedTitle);

			} else {
				System.out.println("Title did not match the expected value.");
			}
		} catch (Exception e) {
			webActionUtils.info("Title is not Verified");
			 e.printStackTrace();
			Assert.fail();
		}
	}
	
	public void verifyErrorMessage(String expectedText) {
		try {
			webActionUtils.info("Verifying Login Error Message");
			webActionUtils.waitUntilLoadedAndVisibilityOfElementLocated(errorMsg);
			String actualText = errorMsg.getText();

			
			if (actualText.equals(expectedText)) {
				webActionUtils.pass("Error Message is Verified");
				System.out.println("Error Message is correct: " +actualText);
				Assert.assertEquals(actualText, expectedText);

			} else {
				System.out.println("Error Message did not match the expected value.");
			}
		} catch (Exception e) {
			webActionUtils.info("Error Message is not Verified");
			e.printStackTrace();
			Assert.fail();
		}
	}
	
//	
//	public void verifyErrorMessage(String expectedText) {
//		webActionUtils.waitSleep(5);
//		webActionUtils.info("Verifying Login Page Title");
//		webActionUtils.waitUntilLoadedAndVisibilityOfElementLocated(errorMsg);
//		String actualText = errorMsg.getText();
//		webActionUtils.verifyText(actualText, expectedText);
//		webActionUtils.pass("Passed");
//	}
	

}