package com.automation.pi.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import com.automation.pi.library.BasePage;
import com.automation.pi.library.WebActionUtils;

public class HomePage extends BasePage {

	@FindBy(xpath = "//img[@alt='Sign Out']")
	private WebElement signOutICN;

	@FindBy(id = "btnConfirmYes")
	private WebElement yesBTN;

	@FindBy(xpath = "//a[text()='Upload File']")
	private WebElement uploadFileTab;

	@FindBy(xpath = "//a[text()='Manage Page']")
	private WebElement managePageTab;

	@FindBy(xpath = "//a[text()='Communications']")
	private WebElement communicationPageTab;

	@FindBy(xpath = "//a[text()='Settings']")
	private WebElement settingsTab;

	@FindBy(xpath = "//a[text()='Reports']")
	private WebElement reportsTab;

	@FindBy(xpath = "//span[@id='spanddlSelectOffice']")
	private WebElement SelectOffice;

	public HomePage(WebDriver driver, WebActionUtils webActionUtils) {
		super(driver, webActionUtils);
	}

	public void clickOnSignOutIcon() {
		try {
			webActionUtils.info("Clicking On Sign Out Icon");
			webActionUtils.waitUntilLoadedAndVisibilityOfElementLocated(signOutICN);
			webActionUtils.clickElement(signOutICN, "Sign Out Icon");
			webActionUtils.pass("Clicked On Sign Out Icon successfully");

		} catch (Exception e) {
			webActionUtils.fail("Unable to Click on Sign Out Icon : Failed");
			e.printStackTrace();
		}
	}

	public void verifyTitle(String etitle) {
		webActionUtils.verifyTitle(etitle);
	}

	public void verifyHomePageTitle(String eTitle) {
		try {
			webActionUtils.info("Verifying Home Page Title");
			Boolean isTitleCorrect = webActionUtils.waitAndGetTitle(eTitle);
			if (isTitleCorrect) {
				String actualTitle = webActionUtils.getdriver().getTitle();
				webActionUtils.pass("Title is Verified");
				System.out.println("Title is correct: " + actualTitle);
				Assert.assertEquals(actualTitle, eTitle);

			} else {
				System.out.println("Title did not match the expected value.");
			}
		} catch (Exception e) {
			webActionUtils.info("Title is not Verified");
			e.printStackTrace();
			Assert.fail();

		}
	}

	public void verifyDTUserTabs(String uploadFileTabText, String managePageTabText, String reportsTabText) {
		Boolean bool1 = false, bool2 = false, bool3 = false;
		try {
			webActionUtils.info("Verifying DT User Tabs");
			bool1 = webActionUtils.verifyElementText(uploadFileTab, uploadFileTabText);
			bool2 = webActionUtils.verifyElementText(managePageTab, managePageTabText);
			bool3 = webActionUtils.verifyElementText(reportsTab, reportsTabText);

			if (bool1 && bool2 && bool3) {
				webActionUtils.pass("Verification of DT User Tabs : Passed");
			} else {
				webActionUtils.fail("Verification of DT User Tabs : Failed");

			}

		} catch (Exception e) {
			webActionUtils.fail("Verification failed : DT User Tabs");
			e.printStackTrace();
			Assert.fail();
		}
	}

	public void verifyPIUserTabs(String managePageTabText, String reportsTabText) {
		Boolean bool1 = false, bool2 = false;
		try {
			webActionUtils.info("Verifying PI User Tabs");
			bool1 = webActionUtils.verifyElementText(managePageTab, managePageTabText);
			bool2 = webActionUtils.verifyElementText(reportsTab, reportsTabText);

			if (bool1 && bool2) {
				webActionUtils.pass("Verification of PI User Tabs : Passed");
			} else {
				webActionUtils.fail("Verification of PI User Tabs : Failed");
			}
		} catch (Exception e) {
			webActionUtils.fail("Verification failed : PI User Tabs");
			e.printStackTrace();
			Assert.fail();
		}

	}

	public void verifyFBEUserTabs(String uploadFileTabText, String managePageTabText, String communicationTabText,
			String reportsTabText) {
		Boolean bool1 = false, bool2 = false, bool3 = false, bool4 = false;
		try {
			webActionUtils.info("Verifying FBE User Tabs");
			bool1 = webActionUtils.verifyElementText(uploadFileTab, uploadFileTabText);
			bool2 = webActionUtils.verifyElementText(managePageTab, managePageTabText);
			bool3 = webActionUtils.verifyElementText(communicationPageTab, communicationTabText);
			bool4 = webActionUtils.verifyElementText(reportsTab, reportsTabText);

			if (bool1 && bool2 && bool3 && bool4) {
				webActionUtils.pass("Verification of FBE User Tabs : Passed");
			} else {
				webActionUtils.fail("Verification of FBE User Tabs : Failed");

			}

		} catch (Exception e) {
			webActionUtils.fail("Verification failed : FBE User Tabs");
			e.printStackTrace();
			Assert.fail();
		}
	}

	public void verifyAdminUserTabs(String uploadFileTabText, String managePageTabText, String communicationTabText,
			String reportsTabText, String settingsTabText) {
		Boolean bool1 = false, bool2 = false, bool3 = false, bool4 = false, bool5 = false;
		try {
			webActionUtils.info("Verifying Admin User Tabs");
			bool1 = webActionUtils.verifyElementText(uploadFileTab, uploadFileTabText);
			bool2 = webActionUtils.verifyElementText(managePageTab, managePageTabText);
			bool3 = webActionUtils.verifyElementText(communicationPageTab, communicationTabText);
			bool4 = webActionUtils.verifyElementText(reportsTab, reportsTabText);
			bool5 = webActionUtils.verifyElementText(settingsTab, settingsTabText);

			if (bool1 && bool2 && bool3 && bool4 && bool5) {
				webActionUtils.pass("Verification of Admin User Tabs : Passed");
			} else {
				webActionUtils.fail("Verification of Admin User Tabs : Failed");

			}

		} catch (Exception e) {
			webActionUtils.fail("Verification failed : Admin User Tabs");
			e.printStackTrace();
			Assert.fail();
		}
	}

	public void clickOnYes() {
		try {
			webActionUtils.info("Clicking On Yes button on the Sign out Popup ");
			webActionUtils.clickElement(yesBTN, "Yes Button");
			webActionUtils.pass("Clicked On Yes Button Successfully");
		} catch (Exception e) {
			webActionUtils.fail("Unable to Click on Yes button: Failed");
			e.printStackTrace();
			Assert.fail();
		}
	}

}