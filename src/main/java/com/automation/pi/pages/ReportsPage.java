package com.automation.pi.pages;

import static org.testng.Assert.assertEquals;

import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.automation.pi.library.BasePage;
import com.automation.pi.library.WebActionUtils;

public class ReportsPage extends BasePage {

	@FindBy(xpath = "//a[text()='Reports']")
	private WebElement reportsTab;

	@FindBy(xpath = "//a[text()='PIS Time Sheet']")
	private WebElement pISTimeSheetDrpDwn;

	@FindBy(xpath = "//a[text()='Policy Insights Work Summary']")
	private WebElement policyInsightWorkSummaryDrpDwn;

	@FindBy(xpath = "//a[text()='Policy Insights Reports']")
	private WebElement policyInsightsReportsDrpDwn;

	@FindBy(xpath = "//a[text()='Policy Insights Coverage sections Reports']")
	private WebElement policyInsightsCoverageSectionsReportsDrpDwn;

	@FindBy(xpath = "//a[text()='Policy Insights Data Capture BLR Team Reports']")
	private WebElement policyInsightsDataCaptureBLRTeamReportsDrpDwn;

	@FindBy(xpath = "//a[text()='Policy Status Report']")
	private WebElement policyStatusReportDrpDwn;

	@FindBy(xpath = "//a[text()='Discrepancy Report']")
	private WebElement discrepancyReportDrpDwn;

	@FindBy(xpath = "//a[text()='Exception Report']")
	private WebElement exceptionReportDrpDwn;

	@FindBy(xpath = "// button[contains(text(),'Generate')]")
	private WebElement GenerateBTN;

	@FindBy(xpath = "//span[text()='Policy Checking Timesheet']")
	private WebElement poicyCheckingTimesheetTXT;

	@FindBy(xpath = "//span[text()='Policy Checking Timesheet']/../..//div[2]/span")
	private List<WebElement> poicyCheckingTimesheetdatetime;

	@FindBy(xpath = "(//tbody)[6]//td")
	private List<WebElement> poicyCheckingTimesheetTabelDataTXT;

	@FindBy(xpath = "//span[text()='Policy Plus Work Summary']")
	private WebElement policyPlusWorkSummaryTXT;

	@FindBy(xpath = "//span[text()='Policy Plus Work Summary']/../..//div[3]/span")
	private List<WebElement> policyPlusWorkSummarydatetime;

	@FindBy(xpath = "(//tbody)[13]//td")
	private List<WebElement> poicyPlusWorkSummaryTabelDataTXT;

	@FindBy(xpath = "//div[text()='Serial #']/../../..//tr//td")
	private List<WebElement> coverageSectionsReportsTabelDataTXT;

	@FindBy(id = "accounts")
	private WebElement reportsAccounts;

	public ReportsPage(WebDriver driver, WebActionUtils webActionUtils) {
		super(driver, webActionUtils);
	}

	public void clickDiscrepancyReportOption() {
		webActionUtils.info("Clicking on the Discrepancy Report Option ");
		webActionUtils.clickElement(discrepancyReportDrpDwn, "Discrepancy Report DropDown Option");
	}

	public void clickExceptionReportOption() {
		webActionUtils.info("Clicking on the Exception Report Option");
		webActionUtils.clickElement(exceptionReportDrpDwn, "Exception Report DropDown option");
	}

	public void clickOnGenerateButton() {
		webActionUtils.info("Click On the PI Time sheet Generate Button");
		webActionUtils.clickElement(GenerateBTN, "Generate Button");
		webActionUtils.waitForPageLoad();
		// webActionUtils.waitSleep(5);
	}

	public void clickOnPIWorkSummaryGenerateButton() {
		webActionUtils.info("Click On the PI Work Summary Generate Button");
		webActionUtils.clickElement(GenerateBTN, "Generate Button");
		webActionUtils.waitForPageLoad();
		// webActionUtils.waitSleep(5);
	}

	public void clickPISTimeSheetOptions() {
		webActionUtils.info("Clicking on the PIS TimeSheet DropDown Options");
		webActionUtils.clickElement(pISTimeSheetDrpDwn, "PIS TimeSheet DropDown Options");
	}

	public void clickPolicyInsightsCoverageSectionsReportsOption() {
		webActionUtils.info("Clicking on the Policy Insights Coverage Sections Reports Option");
		webActionUtils.clickElement(policyInsightsCoverageSectionsReportsDrpDwn,
				"Policy Insights Coverage Sections Reports DropDown Option");
	}

	public void clickPolicyInsightsDataCaptureBLRTeamReportsOption() {
		webActionUtils.info("Clicking on the Policy Insights Data Capture BLR Team Reports  DropDown Option ");
		webActionUtils.clickElement(policyInsightsDataCaptureBLRTeamReportsDrpDwn,
				"Policy Insights Data Capture BLR Team Reports  DropDown Option");
	}

	public void clickPolicyInsightsReportsOption() {
		webActionUtils.info("Clicking on the Policy Insights Reports Option");
		webActionUtils.clickElement(policyInsightsReportsDrpDwn, "Policy Insights Reports DropDown Option");
	}

	public void clickPolicyInsightWorkSummaryOption() {
		webActionUtils.info("Clicking on the Policy Insight Work Summary DropDown Option");
		webActionUtils.clickElement(policyInsightWorkSummaryDrpDwn, "Policy Insight Work Summary DropDown Option");
	}

	public void clickPolicyStatusReportOption() {
		webActionUtils.info("Clicking on the Policy Status Report Option");
		webActionUtils.clickElement(policyStatusReportDrpDwn, "Policy Status Report DropDown Option");
	}

	public void clickReportsTab() {
		webActionUtils.info("Clicking on the Reports Tab");
		webActionUtils.waitUntilLoadedAndElementClickable(reportsTab);
		webActionUtils.clickElement(reportsTab, "Reports Tab");
	}

	public void displayCoverageSectionsReportsTabel() {
	try {
		webActionUtils.info("Display Coverage Sections Reports Tabel");
		for (WebElement tabeldata : coverageSectionsReportsTabelDataTXT) {

			if (tabeldata.getText().isEmpty()) {

			} else {
				webActionUtils.pass("The table values are " + tabeldata.getText());
			}

		}
	} catch (Exception e) {
		webActionUtils.info("The table values are displayed");
	}

	}

	public void verifyDiscrepancyReportPage(String title) {
		webActionUtils.info("Verifying Discrepancy Report Title");
		webActionUtils.verifyTitle(title);
		webActionUtils.waitForPageLoad();
		webActionUtils.info(driver.getTitle());
		webActionUtils.pass("Page Verified " + title);
	}

	public void verifyExceptionReportPage(String title) {
		webActionUtils.info("Verifying Exception Report Title");
		webActionUtils.verifyTitle(title);
		webActionUtils.waitForPageLoad();
		webActionUtils.info(driver.getTitle());
		webActionUtils.pass("Page Verified " + title);
	}

	public void verifyPISTimeSheetPage(String title) {
		webActionUtils.info("Verifying Exception Report Title");
		webActionUtils.verifyTitle(title);
		webActionUtils.waitForPageLoad();
		webActionUtils.info(driver.getTitle());
		webActionUtils.pass("Page Verified " + title);
	}

	public void verifyPolicyCheckingTimesheetTabel(String PoicyCheckingTimesheetTXT) {
		try {

			webActionUtils.info("verify Policy Checking Timesheet Details");
			webActionUtils.waitUntilLoadedAndVisibilityOfElementLocated(poicyCheckingTimesheetTXT);
			webActionUtils.verifyElementText(poicyCheckingTimesheetTXT, PoicyCheckingTimesheetTXT);
			webActionUtils.waitUntilLoadedAndVisibilityOfAllElements(poicyCheckingTimesheetdatetime);

			String Sout = "";
			for (WebElement datetime : poicyCheckingTimesheetdatetime) {

				Sout = Sout + datetime.getText() + "";
			}
			webActionUtils.info("The Date range is " + Sout);
			webActionUtils.info("The Policy Checking Timesheet details are");

			String str = "", str1 = " ";

			webActionUtils.waitUntilLoadedAndVisibilityOfAllElements(poicyCheckingTimesheetTabelDataTXT);

			for (WebElement tabeldata : poicyCheckingTimesheetTabelDataTXT) {
				if (tabeldata.getText().equals(null) || tabeldata.getText().isEmpty()
						|| tabeldata.getText().toString().equals("") || tabeldata.getText().toString().equals(str)
						|| tabeldata.getText().toString().equals(str1)) {

				} else {
					webActionUtils.pass("The table values are " + tabeldata.getText());
				}

			}
			
		} catch (Exception e) {
			webActionUtils.info("The table values are not displayed");
		}
	}

	public void verifyPolicyInsightsCoverageSectionsReportsPage(String title) {
		webActionUtils.info("Verifying Policy Insights Coverage Sections Reports Title");
		webActionUtils.verifyTitle(title);
		webActionUtils.waitForPageLoad();
		webActionUtils.info(driver.getTitle());
		webActionUtils.pass("Page Verified " + title);
	}

	public void verifyPolicyInsightsDataCaptureBLRTeamReportsPage(String title) {
		webActionUtils.info("Verifying Policy Insights Data Capture BLR Team Reports Title");
		webActionUtils.verifyTitle(title);
		webActionUtils.waitForPageLoad();
		webActionUtils.info(driver.getTitle());
		webActionUtils.pass("Page Verified " + title);
	}

	public void verifyPolicyInsightsReportsPage(String title) {
		webActionUtils.info("Verifying Policy Insights Reports Title");
		webActionUtils.verifyTitle(title);
		webActionUtils.waitForPageLoad();
		webActionUtils.info(driver.getTitle());
		webActionUtils.pass("Page Verified " + title);
	}

	public void verifyPolicyInsightWorkSummaryPage(String title) {
		webActionUtils.info("Verifying Policy Insight Work Summary Title");
		webActionUtils.verifyTitle(title);
		webActionUtils.waitForPageLoad();
		webActionUtils.info(driver.getTitle());
		webActionUtils.pass("Page Verified " + title);
	}

	public void verifyPolicyStatusReportPage(String title) {
		webActionUtils.info("Verifying Policy Status Report Title");
		webActionUtils.verifyTitle(title);
		webActionUtils.waitForPageLoad();
		webActionUtils.info(driver.getTitle());
		webActionUtils.pass("Page Verified " + title);
	}

	public void verifyPolicyWorkSummaryTabel(String PolicyPlusWorkSummaryTXT) {
		try {
			webActionUtils.info("verify Policy Work Summary Details");
			webActionUtils.waitUntilLoadedAndVisibilityOfElementLocated(policyPlusWorkSummaryTXT);
			webActionUtils.verifyElementText(policyPlusWorkSummaryTXT, PolicyPlusWorkSummaryTXT);
			String Sout = "";
			webActionUtils.waitUntilLoadedAndVisibilityOfAllElements(policyPlusWorkSummarydatetime);

			for (WebElement datetime : policyPlusWorkSummarydatetime) {

				Sout = Sout + datetime.getText() + "";
			}
			webActionUtils.info("The Date range is " + Sout);

			webActionUtils.info("The Policy details are");

			webActionUtils.waitUntilLoadedAndVisibilityOfAllElements(poicyCheckingTimesheetTabelDataTXT);

			for (WebElement tabeldata : poicyCheckingTimesheetTabelDataTXT) {
				if (tabeldata.getText().isEmpty()) {

				} else {
					webActionUtils.pass("The table values are " + tabeldata.getText());
				}

			}

			webActionUtils.info("The Policy plus work summary details are");
			webActionUtils.waitUntilLoadedAndVisibilityOfAllElements(poicyPlusWorkSummaryTabelDataTXT);

			for (WebElement tabeldata : poicyPlusWorkSummaryTabelDataTXT) {
				if (tabeldata.getText().isEmpty()) {

				} else {
					webActionUtils.pass("The table values are " + tabeldata.getText());
				}

			}
		} catch (Exception e) {
			webActionUtils.info("The table values are not displayed");

		}
	}

	public void verifyReportsOptions() {
		webActionUtils.info("Verfiying all the Reports options");
		webActionUtils.verifyElementIsPresent(pISTimeSheetDrpDwn);
		webActionUtils.verifyElementIsPresent(policyInsightWorkSummaryDrpDwn);
		webActionUtils.verifyElementIsPresent(policyInsightsReportsDrpDwn);
		webActionUtils.verifyElementIsPresent(policyInsightsCoverageSectionsReportsDrpDwn);
		webActionUtils.verifyElementIsPresent(policyStatusReportDrpDwn);
		webActionUtils.verifyElementIsPresent(discrepancyReportDrpDwn);
		webActionUtils.verifyElementIsPresent(exceptionReportDrpDwn);
		webActionUtils.pass("All sub menu are displayed");

	}

	public void verifyReportsPageListinGivenOrder(String PISTimeSheet, String PolicyInsightsWorkSummaryReport,
			String PolicyInsightsReport, String PolicyInsightsGetCovergeSectionsReport,
			String PolicyInsightsDataCaptureBLRTeamReport, String PolicyStatusReport, String DiscrepancyReport,
			String ExceptionReport) {

		webActionUtils.info("verifying Reports Page List in Given Order");
		List<String> arrList1 = Arrays.asList(new String[] { PISTimeSheet, PolicyInsightsWorkSummaryReport,
				PolicyInsightsReport, PolicyInsightsGetCovergeSectionsReport, PolicyInsightsDataCaptureBLRTeamReport,
				PolicyStatusReport, DiscrepancyReport, ExceptionReport });

		List<String> arrList2 = Arrays
				.asList(new String[] { pISTimeSheetDrpDwn.getText(), policyInsightWorkSummaryDrpDwn.getText(),
						policyInsightsReportsDrpDwn.getText(), policyInsightsCoverageSectionsReportsDrpDwn.getText(),
						policyInsightsDataCaptureBLRTeamReportsDrpDwn.getText(), policyStatusReportDrpDwn.getText(),
						discrepancyReportDrpDwn.getText(), exceptionReportDrpDwn.getText() });
		webActionUtils.info("Checking List 1 and List2 equal? " + arrList1.equals(arrList2));
		assertEquals(arrList1, arrList2, "TC failed.. Please Check");
		webActionUtils.pass("Reports Page: Lists are present in the Given Order");
	}

	public void selectRequiredAccount() {
		webActionUtils.info("Select Required Account");
		webActionUtils.selectByValue(reportsAccounts, "CBIZ");
	}

}