package com.automation.pi.pages;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.Reporter;

import com.automation.pi.library.BasePage;
import com.automation.pi.library.WebActionUtils;

public class UploadFilePage extends BasePage {

	@FindBy(id = "pi")
	private WebElement piRadioBTN;

	@FindBy(id = "pilite")
	private WebElement piLiteRadioBTN;

	@FindBy(id = "Carrier")
	private WebElement carrierTxtBx;

	@FindBy(xpath = "//body/ul/li/div[@class='ui-menu-item-wrapper' and text()='CBIC (Contractors Bonding Insurance Company)'] ")
	private WebElement carrierOpn;

	@FindBy(xpath = "//a[text()='Upload File']")
	private WebElement uploadFileMenuLNK;

	@FindBy(id = "AccountID")
	private WebElement selectAccountDPDN;

	@FindBy(id = "DepartmentID")
	private WebElement selectDepartmentDPDN;

	@FindBy(xpath = "//em[@class='icon-refresh']")
	private WebElement formatNameInsured;

	@FindBy(id = "NameInsured")
	private WebElement nameInsuredTXT;

	@FindBy(id = "LOB")
	private WebElement lobTXT;

	@FindBy(id = "PolicyNo")
	private WebElement policyNoTXT;

	@FindBy(id = "EffectiveDate")
	private WebElement effectiveDateTXT;

	@FindBy(id = "DateFromClient")
	private WebElement dateFromClientTXT;

	@FindBy(id = "ClientDueDate")
	private WebElement clientDueDateTXT;

	@FindBy(id = "InternalDueDate")
	private WebElement internalDueDate;

	@FindBy(xpath = "//label[text() = 'Upload Files']/../..//input[@id='RushEnable']")
	private WebElement rushCheckBox;

	@FindBy(id = "txtRush")
	private WebElement rushTextAreaBox;

	@FindBy(xpath = "//label[contains(.,'Rush Fix Wording')]")
	private WebElement RushFixWordingBox;

	@FindBy(xpath = "//label[contains(.,'Rush Fix Wording')]/../select")
	private WebElement RushFixWordingBoxDropdown;

	@FindBy(xpath = "//td[@class=' ui-datepicker-week-end ui-datepicker-days-cell-over  ui-datepicker-today']|//div[@id='ui-datepicker-div']//td[@class=' ui-datepicker-days-cell-over  ui-datepicker-today']")
	private WebElement highlightedDate;

	@FindBy(id = "AccountManager")
	private WebElement accountManagerTXT;

	@FindBy(xpath = "//body/ul[1]/li/div[@class='ui-menu-item-wrapper']")
	private WebElement accountManagerOpn;

	// "//body/ul[1]/li[@role='presentation']//a")

	@FindBy(id = "PackageTypeId")
	private WebElement policyStatusDPDN;

	@FindBy(xpath = "//input[@type='file']")
	private WebElement uploadfilesBTN;

	@FindBy(xpath = "(//button[@type='button'])[3]")
	private WebElement submitBTN;

	@FindBy(xpath = "/html//span[@id='spanMessageContent']")
	private WebElement alertMsg;
	
	@FindBy(xpath = "//button[@id='btnConfirmYes']")
	List<WebElement> confirmYesBTN;

	@FindBy(xpath = "//button[@id='btnConfirmYes']")
	private WebElement confirmYesBTN1;

	@FindBy(id = "OptionalField1")
	private WebElement AdditionalFieldTxt;

	@FindBy(xpath = "//td[@class=' ui-datepicker-days-cell-over  ui-datepicker-today']|//td[@class=' ui-datepicker-week-end ui-datepicker-days-cell-over  ui-datepicker-today']")
	private WebElement effDate;

	@FindBy(xpath = "//*[@id='divLoading']")
	private WebElement uploadingTxt2;

	@FindBy(xpath = "//label[text()='Upload Files']")
	private WebElement uploadfiles;

	@FindBy(xpath = "//input[@type='checkbox' and @id='chkIsEnabled']")
	private WebElement chkIsEnabled;

	By uploadingTxt = By.xpath("//*[@id='divLoading']");

	@FindBy(id = "pMessage")
	private WebElement duplicatePopUp;

	@FindBy(id = "pMessage")
	private WebElement popUpText;

	@FindBy(xpath = "//button[@onclick='RemoveCofirmYes()']")
	private WebElement confirmNoBtn1;

	@FindBy(xpath = "//button[@class='btn']")
	List<WebElement> confirmNoBtn;

	@FindBy(xpath = "//div[@class='ui-menu-item-wrapper' and text()='Other']")
	private WebElement assistantAccountManagerOpn;
	
	@FindBy(xpath = "(//div[@class='ui-menu-item-wrapper' and text()='Other'])[2]")
	private WebElement assistantAccountManagerOpn1;
	

	@FindBy(id = "NewAccountManager")
	private WebElement newAccountManager;

	@FindBy(id = "AccountManagerEmail")
	private WebElement accountManagerEmail;

	@FindBy(id = "NewAssistantAccountManager")
	private WebElement newAssistantAccountManager;

	@FindBy(id = "AssistantAccountManagerEmail")
	private WebElement assistantAccountManagerEmail;

	@FindBy(id = "AssistantManager")
	private WebElement assistantManager;

	@FindBy(xpath = "//li[@class='ui-menu-item']")
	private WebElement lobOption;

	@FindBy(id = "NewLOB")
	private WebElement newLOBTextField;

	@FindBy(xpath = "//p[text()='The Internal Due Date is later than the Client Due Date, are you sure you want to submit ?']")
	private WebElement internalPopUpTXT;

	@FindBy(xpath = "//*[@id='InternalDueDate']")
	private WebElement internalDueDateTxt;

	@FindBy(xpath = "//td[text()='Policy']/../..//tr[5]/td[1]/input")
	private WebElement formSectionCheckBox;

	// @FindBy(xpath= "//td[text()='Policy']/../..//tr[5]/td[1]/input")
	// private WebElement formSectionCheckBox;

	@FindBy(xpath = "//div[@id='divExpressIX']")
	private WebElement ExpressIXAccount;

	@FindBy(xpath = "//input[@id='txtOfficeCode']")
	private WebElement DepartmentOfficeCode;

	@FindBy(xpath = "//label[text()='Department/Office Code']")
	private WebElement DepartmentOfficeCodeLabel;

	@FindBy(xpath = "//table[@id='tbFormSections']")
	private WebElement FormsectionTable;

	@FindBy(xpath = "//body/ul[1]/li/div[@class='ui-menu-item-wrapper']")
	private WebElement AAaccountManagerOpn;

	@FindBy(xpath = "//td[text()='Exp Pol.pdf']/../td[3]/select")
	private WebElement sourceDrpDnTXT;

	@FindBy(xpath = "//td[text()='5-Policy.pdf']/../td[3]/select")
	private WebElement sourceDrpDnTXT1;

	@FindBy(xpath = "//*[@id=\"docselectedsource_dropdown_tr_2\"]/select/option[2]")
	private WebElement sourceDrpDnOptn;

	@FindBy(xpath = "//*[@id=\"docselectedsource_dropdown_tr_2\"]/select/option[3]")
	private WebElement sourceDrpDnOptn1;

	@FindBy(xpath = "//*[@id=\"docselectedsource_dropdown_tr_2\"]/select/option[8]")
	private WebElement sourceDrpDnOptn2;

	@FindBy(xpath = "//*[@class='ui-menu-item']")
	private WebElement accountManagerOpn1;

	@FindBy(xpath = "//*[@id=\"InternalDueDate\"]")
	private WebElement internalDueDateTXT;

	@FindBy(xpath = "//div[@class='controls row'] /../div[7]/div/button[2]")
	private WebElement ClearBtn;

	@FindBy(xpath = "//input[@id='LOB']")
	private WebElement lobOptn;

	@FindBy(id = "ParentLOB")
	private WebElement StandardLOBTXT;

	@FindBy(id = "PremiumRange")
	private WebElement PremiumRangeTXT;

	public UploadFilePage(WebDriver driver, WebActionUtils webActionUtils) {
		super(driver, webActionUtils);
	}

	public boolean checkConfirmButton() {
		boolean confirmYesbool = false;
		try {
			if (confirmYesBTN.size() > 0 && confirmYesBTN.get(0) != null && confirmYesBTN1.isDisplayed()) {
				confirmYesbool = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
			e.getMessage();
		}
		return confirmYesbool;
	}

	public void checkInternalAndClientDuedate(String internalDate, String dateFromClient, String policyStatusOpn) {
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
			Date date = new Date();
			sdf.setLenient(false);
			String currentDate = sdf.format(date);

			webActionUtils.info("Enter Date From Client ");
			dateFromClientTXT.clear();
			dateFromClientTXT.sendKeys(dateFromClient);
			webActionUtils.info("Entered Date From Client : " + dateFromClient);
			webActionUtils.waitSleep(5);

			webActionUtils.info("Selecting policy Status ");
			webActionUtils.selectByVisibleText(policyStatusDPDN, policyStatusOpn);
			webActionUtils.info("Selected Policy Status : " + policyStatusOpn);
			webActionUtils.waitSleep(20);

			String afterInternalDate = internalDueDate.getAttribute("value");
			String clientDate = clientDueDateTXT.getAttribute("value");
			
			System.out.println(afterInternalDate+" afterInternalDate");
			System.out.println(clientDate+" clientDate");


			try {
				if (internalDate.equals(clientDate) && internalDate.equals(currentDate)) {
					webActionUtils.info(
							"Verification of Internal Due Date when Internal Due date is Equal to Client Due Date and Internal Due Date is Equal to Current Date");

					webActionUtils.info("Internal Due : " + internalDate);
					webActionUtils.info("Client Due Date : " + clientDate);
					webActionUtils.info("Current Date : " + currentDate);
					webActionUtils.info("Internal Due Date after Verification : " + afterInternalDate);

					if (afterInternalDate.equals(internalDate)) {
						webActionUtils.pass(
								"Verification of Un-change of Internal Due Date when Internal Due date is Equal to Client Due Date and Internal Due Date is Equal to Current Date : Passed ");
					} else {
						webActionUtils.fail(
								"Verification of Un-change of Internal Due Date when Internal Due date is Equal to Client Due Date and Internal Due Date is less than Current Date : Failed ");
					}
				}

				if (internalDate.compareTo(clientDate) == 0 && internalDate.compareTo(currentDate) < 0) {
					webActionUtils.info(
							"Verification of Internal Due Date when Internal Due Date is Equal to Client Due Date and Internal Due Date is Less than Current Date");

					webActionUtils.info("Internal Due Date : " + internalDate);
					webActionUtils.info("Client Due Date : " + clientDate);
					webActionUtils.info("Current Date : " + currentDate);
					webActionUtils.info("Internal Due Date after Verification : " + afterInternalDate);

					if (afterInternalDate.equals(internalDate)) {
						webActionUtils.pass(
								"Verification of Un-change of Internal Due Date when Internal Due date is Equal to Client Due Date and Internal Due Date is Less than Current Date : Passed ");
					} else {
						webActionUtils.fail(
								"Verification of Un-change of Internal Due Date when Internal Due date is Equal to Client Due Date and Internal Due Date is Less than Current Date : Failed ");
					}
				}

				if (internalDate.compareTo(clientDate) == 0 && internalDate.compareTo(currentDate) > 0) {
					webActionUtils.info(
							"Verification of Internal Due Date when Internal Due date is Equal to Client Due Date and Internal Due Date is Greater than Current Date");

					Calendar calendar = Calendar.getInstance();
					Date date1 = new SimpleDateFormat("MM/dd/yyyy").parse(internalDate);
					calendar.setTime(date1);
					calendar.add(Calendar.DAY_OF_MONTH, -1);
					Date date2 = calendar.getTime();
					String internalDueDates = sdf.format(date2);

					webActionUtils.info("Internal Due Date : " + internalDate);
					webActionUtils.info("Client Due Date : " + clientDate);
					webActionUtils.info("Current Date : " + currentDate);
					webActionUtils.info("Internal Due Date after Verification : " + internalDueDates);

					if (afterInternalDate.compareTo(internalDueDates) == 0) {
						webActionUtils.pass(
								"Verification of Internal Due Date changing to previous date when Internal Due date is Equal to Client Due Date and Internal Due Date is Greater than Current Date : Passed  ");
					} else {
						webActionUtils.fail(
								"Verification of Internal Due Date changing to previous date when Internal Due date is Equal to Client Due Date and Internal Due Date is Greater than Current Date : Failed  ");
					}
				}

			} catch (Exception e) {
				Reporter.log(e.getMessage(), true);
				webActionUtils.fail("" + e.getMessage());
			}
		} catch (Exception e) {
			Reporter.log(e.getMessage(), true);
			webActionUtils.fail("" + e.getMessage());
		}
	}

	public void checkInternalDateAndClientDueDate(String internalDate, String dateFromClient, String policyStatusOpn) {
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
			Date date = new Date();
			sdf.setLenient(false);
			String currentDate = sdf.format(date);

			webActionUtils.info("Enter Date From Client ");
			dateFromClientTXT.clear();
			dateFromClientTXT.sendKeys(dateFromClient);
			webActionUtils.info("Entered Date From Client : " + dateFromClient);
			webActionUtils.waitSleep(5);

			webActionUtils.info("Selecting policy Status ");
			webActionUtils.selectByVisibleText(policyStatusDPDN, policyStatusOpn);
			webActionUtils.info("Selected Policy Status : " + policyStatusOpn);
			webActionUtils.waitSleep(30);

			webActionUtils.waitUntilLoadedAndPresenceOfElementLocated(internalDueDate);
			webActionUtils.waitUntilLoadedAndPresenceOfElementLocated(clientDueDateTXT);
			webActionUtils.waitSleep(10);
			String afterInternalDate = internalDueDate.getAttribute("value");
			webActionUtils.waitSleep(10);
			String clientDate = clientDueDateTXT.getAttribute("value");

			try {
				if (internalDate.equals(clientDate) && internalDate.equals(currentDate)) {
					webActionUtils.info(
							"Verification of Internal Due Date when Internal Due date is Equal to Client Due Date and Internal Due Date is Equal to Current Date");

					webActionUtils.info("Internal Due : " + internalDate);
					webActionUtils.info("Client Due Date : " + clientDate);
					webActionUtils.info("Current Date : " + currentDate);
					webActionUtils.info("Internal Due Date after Verification : " + afterInternalDate);

					if (afterInternalDate.equals(internalDate)) {
						webActionUtils.pass(
								"Verification of Un-change of Internal Due Date when Internal Due date is Equal to Client Due Date and Internal Due Date is Equal to Current Date : Passed ");
					} else {
						webActionUtils.fail(
								"Verification of Un-change of Internal Due Date when Internal Due date is Equal to Client Due Date and Internal Due Date is less than Current Date : Failed ");
					}
				}

				if (internalDate.compareTo(clientDate) == 0 && internalDate.compareTo(currentDate) < 0) {
					webActionUtils.info(
							"Verification of Internal Due Date when Internal Due Date is Equal to Client Due Date and Internal Due Date is Less than Current Date");

					webActionUtils.info("Internal Due Date : " + internalDate);
					webActionUtils.info("Client Due Date : " + clientDate);
					webActionUtils.info("Current Date : " + currentDate);
					webActionUtils.info("Internal Due Date after Verification : " + afterInternalDate);

					if (afterInternalDate.equals(internalDate)) {
						webActionUtils.pass(
								"Verification of Un-change of Internal Due Date when Internal Due date is Equal to Client Due Date and Internal Due Date is Less than Current Date : Passed ");
					} else {
						webActionUtils.fail(
								"Verification of Un-change of Internal Due Date when Internal Due date is Equal to Client Due Date and Internal Due Date is Less than Current Date : Failed ");
					}
				}

				if (internalDate.compareTo(clientDate) == 0 && internalDate.compareTo(currentDate) > 0) {
					webActionUtils.info(
							"Verification of Internal Due Date when Internal Due date is Equal to Client Due Date and Internal Due Date is Greater than Current Date");

					Calendar calendar = Calendar.getInstance();
					Date date1 = new SimpleDateFormat("MM/dd/yyyy").parse(internalDate);
					calendar.setTime(date1);
					calendar.add(Calendar.DAY_OF_MONTH, -1);
					Date date2 = calendar.getTime();
					String internalDueDates = sdf.format(date2);

					webActionUtils.info("Internal Due Date : " + internalDate);
					webActionUtils.info("Client Due Date : " + clientDate);
					webActionUtils.info("Current Date : " + currentDate);
					webActionUtils.info("Internal Due Date after Verification : " + internalDueDates);

					if (afterInternalDate.compareTo(internalDueDates) == 0) {
						webActionUtils.pass(
								"Verification of Internal Due Date changing to previous date when Internal Due date is Equal to Client Due Date and Internal Due Date is Greater than Current Date : Passed  ");
					} else {
						webActionUtils.fail(
								"Verification of Internal Due Date changing to previous date when Internal Due date is Equal to Client Due Date and Internal Due Date is Greater than Current Date : Failed  ");
					}
				}

				if (internalDate.compareTo(clientDate) != 0) {
					webActionUtils.info(
							"Verification of Internal Due Date when Internal Due Date is Not Equal to Client Due Date");

					webActionUtils.info("Internal Due Date : " + internalDate);
					webActionUtils.info("Client Due Date : " + clientDate);
					webActionUtils.info("Internal Due Date after Verification : " + afterInternalDate);

					if (afterInternalDate.equals(internalDate)) {
						webActionUtils.pass(
								"Verification of Un-change of Internal Due Date when Internal Due date is Not Equal to Client Due Date : Passed ");
					} else {
						webActionUtils.fail(
								"Verification of Un-change of Internal Due Date when Internal Due date is Not Equal to Client Due Date  : Failed ");
					}
				}

			} catch (Exception e) {
				Reporter.log(e.getMessage(), true);
				webActionUtils.fail("" + e.getMessage());
			}
		} catch (Exception e) {
			Reporter.log(e.getMessage(), true);
			webActionUtils.fail("" + e.getMessage());
		}
	}

	/**
	 * Verification of Internal Due Date and Client Due Date changing to Current
	 * Date on Check of Rush Checkbox.
	 */

	public void checkRushCheckboxAndValidateDates() {
		try {
			webActionUtils.info(
					"Verification of Internal Due date and Client Due Date changing to current date On Check of Rush checkbox ");
			SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
			Date date = new Date();
			sdf.setLenient(false);
			String str = sdf.format(date);

			webActionUtils.info("Client Due Date and Internal Due Date before checking the Rush Check box : ");
			webActionUtils.waitSleep(5);
			String internalDate = internalDueDate.getAttribute("value");
			String clientDate = clientDueDateTXT.getAttribute("value");
			webActionUtils.info("Internal Due Date : " + internalDate);
			webActionUtils.info("Client Due Date : " + clientDate);

			webActionUtils.info("Checking the Rush check box");
			rushCheckBox.click();
			webActionUtils.waitSleep(5);

			webActionUtils.info("Client Due Date and Internal Due Date After checking the Rush Check box : ");
			String internalDateAftercheck = internalDueDate.getAttribute("value");
			String clientDateAfterCheck = clientDueDateTXT.getAttribute("value");
			webActionUtils.info("Internal Due Date : " + internalDateAftercheck);
			webActionUtils.info("Client Due Date : " + clientDateAfterCheck);

			try {
				if ((internalDateAftercheck.equals(str) && internalDueDate.isEnabled())
						&& (clientDateAfterCheck.equals(str) && clientDueDateTXT.isEnabled())) {
					webActionUtils.pass(
							"Verification of Internal Due date and Client Due Date Changing to Current date and are Editable On checking the Rush Check box : Passed ");
				}
			} catch (Exception e) {
				webActionUtils.fail(
						" Verification of Internal Due date and Client Due Date Changing to Current date and are Editable On checking the Rush Check box : Failed ");
				Reporter.log(e.getMessage(), true);
				webActionUtils.fail("" + e.getMessage());
			}
		} catch (Exception e) {
			Reporter.log(e.getMessage(), true);
			webActionUtils.fail("" + e.getMessage());
		}
	}

	public void clickClear() {
		webActionUtils.info("Clearing the field values");
		webActionUtils.waitSleep(5);
		ClearBtn.click();
	}

	public void clickConfirmYesAndCheckToast(String expectedText) {

		try {
			try {
				if (checkConfirmButton() == true) {
					webActionUtils.info("Verifing Confirm Button");
					// webActionUtils.jsclickElement(confirmYesBTN.get(0),
					// "confirmYesBTN");
					webActionUtils.clickElement(confirmYesBTN1, "Confirm button");
					webActionUtils.info("Confirm Button is present");
				}
			} catch (Exception e) {
				webActionUtils.info("Confirm Button not  is present");
				e.printStackTrace();
				e.getMessage();
			}
		} catch (Exception e) {
			webActionUtils.info("Checking again for Confirm button");

			try {
				if (checkConfirmButton() == true) {
					webActionUtils.info("Verifying Confirm Button");
					webActionUtils.jsclickElement(confirmYesBTN.get(0), "confirmYesBTN");

					webActionUtils.info("Confirm Button is present");
				}
			} catch (Exception e1) {
				webActionUtils.info("Confirm Button not  is present");
				e.printStackTrace();
				e.getMessage();
			}
		}

		verifyingToast(expectedText);
	}

	/**
	 * Chaithra
	 * 
	 * @param expectedText
	 */
	public void clickConfirmYesAndCheckToast1(String expectedText) {
		try {
			if (checkConfirmButton() == true) {
				webActionUtils.info("verifing Confirm Button");
				webActionUtils.jsclickElement(confirmYesBTN.get(0), "confirmYesBTN");
				webActionUtils.info("Confirm Button is present");
			}
		} catch (Exception e) {
			webActionUtils.info("Confirm Button not is present");
			e.printStackTrace();
			e.getMessage();
		}
		verifyingToast(expectedText);
	}

	public boolean clickNoForInternalduedate() {
		boolean confirmNobool = false;
		try {
			if (confirmNoBtn.size() > 0 && confirmNoBtn.get(0) != null && confirmNoBtn1.isDisplayed()) {
				confirmNobool = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
			e.getMessage();
		}
		return confirmNobool;
	}

	/**
	 * chaithra
	 * 
	 * @param expectedText
	 */
	public void clickNoForInternalDueDatePopup() {

		try {
			if (clickNoForInternalduedate() == true) {
				webActionUtils.info("Click on No Button");

				webActionUtils.clickElement(confirmNoBtn1, "Click on No button");
				webActionUtils.pass("Clicking on No button in the Internal Pop Up window");
			}
		} catch (Exception e) {
			webActionUtils.info("Confirm No Button is not present");
			e.printStackTrace();
			e.getMessage();
		}
	}

	public boolean clickNoForRenewal() {
		boolean confirmNobool = false;
		try {
			if (confirmNoBtn.size() > 0 && confirmNoBtn.get(0) != null && confirmNoBtn1.isDisplayed()) {
				confirmNobool = true;
			}
		} catch (Exception e) {
			e.getMessage();
		}

		return confirmNobool;

	}

	/**
	 * @author Chaithra
	 * @param renewalPopUpText
	 */
	public void clickNoForRenewalPopup() {
		try {

			if (clickNoForRenewal() == true) {
				webActionUtils.clickElement(confirmNoBtn1, "Click on NO button in the Renewal POP up");
			}
		} catch (Exception e) {
			webActionUtils.info("Unable to Click on NO button in the renewal pop up:" + e.getMessage());

		}
	}

	public void clickOnPolicyInsightsExpressCheckRadiobutton() {
		try {
			webActionUtils.info("Click on the Policy Insights - ExpressCheck Radio button ");

			webActionUtils.clickElement(piLiteRadioBTN, "Policy Insights - ExpressCheck Radio button Radio button");
		} catch (Exception e) {
			webActionUtils.fail("Failed to Click on the Policy Insights - ExpressCheck Radio button Radio button present in Upload file Page");
		}

	}

	public void clickOnPolicyInsightsAdvanceCheckbutton() {
		try {
			webActionUtils.info("Click on Policy Insights - AdvanceCheck Radio button present in Upload file page");
			webActionUtils.clickElement(piRadioBTN, "Policy Insights - AdvanceCheck  Radio button");
		} catch (Exception e) {
			webActionUtils.fail("Failed to Click on the Policy Insights - AdvanceCheck Radio button present in Upload file Page");
		}

	}

	public void clickOnRushCheckBox() {
		webActionUtils.info("Clicking On Rush CheckBox button");
		webActionUtils.clickElement(rushCheckBox, "rushCheckBox");
		webActionUtils.pass("Rush Checkbox is Checked");
	}

	public void clickOnUploadFile() {
		try {
			webActionUtils.waitSleep(3);
			webActionUtils.clickElement(uploadFileMenuLNK, "Upload File Menu bar");
			webActionUtils.waitForPageLoad();
			webActionUtils.info("Clicked On Upload File Menu Link");
			webActionUtils.waitSleep(3);
		} catch (Exception e) {
			webActionUtils.fail("Unable to Click on Upload File Menu bar");
		}
	}

	/**
	 * Chaithra
	 * 
	 * @param internalDate
	 * @param clientDate
	 */
	public void compareInternalAndClientDueDate(String internalDate, String clientDate) {
		webActionUtils.info("Comparing the Internal Due Date and Client Due Date");
		if (internalDate.compareTo(clientDate) > 0) {
			webActionUtils.pass("Entered Internal Due Date is greater than Client Due Date ");
		} else if (internalDate.compareTo(clientDate) < 0) {
			webActionUtils.info("Entered Internal Due Date is less than Client Due Date");
		} else {
			webActionUtils.info("Entered Internal Due Date is equal to Client Due Date");
		}
	}

	public void confirmYesOninternalPopUp(String expectedText) {
		try {
			if (InternalPopUpYesBtn() == true) {
				webActionUtils.info("Verifing Confirm Button");
				// webActionUtils.jsclickElement(confirmYesBTN.get(0),
				// "confirmYesBTN");
				webActionUtils.clickElement(confirmYesBTN1, "Confirm Yes button");
				webActionUtils.info("Confirm Yes Button is present");
			}
		} catch (Exception e) {
			webActionUtils.info("Confirm Button not is present");
			e.printStackTrace();
			e.getMessage();
		}
	}

	/*
	 * Chaithra
	 * 
	 * @param expectedText
	 */
	public void confirmYesOnRenewalPopUp(String expectedText) {
		try {
			if (renewalPopUpYesBtn() == true) {
				webActionUtils.info("Verifing Confirm Button");
				// webActionUtils.jsclickElement(confirmYesBTN.get(0),
				// "confirmYesBTN");
				webActionUtils.clickElement(confirmYesBTN1, "Confirm Yes button");
				webActionUtils.info("Confirm Yes Button is present");
			}
		} catch (Exception e) {
			webActionUtils.info("Confirm Button not is present");
			e.printStackTrace();
			e.getMessage();
		}
	}

	public String convertUppercaseEachWord(String message) {
		webActionUtils.info("Original String :- \n" + message);
		{
			message = message.toLowerCase();
			// stores each characters to a char array
			char[] charArray = message.toCharArray();

			boolean foundSpace = true;

			for (int i = 0; i < charArray.length; i++) {

				// if the array element is a letter
				if (Character.isLetter(charArray[i])) {

					// check space is present before the letter
					if (foundSpace) {

						// change the letter into uppercase
						charArray[i] = Character.toUpperCase(charArray[i]);
						foundSpace = false;
					}
				}

				else {
					// if the new character is not character
					foundSpace = true;
				}
			}

			// convert the char array to the string
			message = String.valueOf(charArray);
			System.out.println("Message after converstion : " + message);
		}
		return message;
	}

	public String enterFieldvalues(String accountName, String departmentName, String lobValue, String policyNumber,
			String dateformat, String accountManager, String policyStatusOpn, String uploadfilepath1,
			String expectedText) {

		String nameInsured = "";

		try {

			webActionUtils.info("Selecting Account field");
			webActionUtils.selectByVisibleText(selectAccountDPDN, accountName);
			webActionUtils.info("Selected Account Name : " + accountName);

			webActionUtils.info("Selecting Department / Program DropDown value");
			webActionUtils.selectByVisibleText(selectDepartmentDPDN, departmentName);
			webActionUtils.info("Selected Department Name : " + departmentName + "");

			webActionUtils.info("Entering Named Insured ");
			nameInsured = "2024 TEST_00_ " + RandomStringUtils.randomAlphanumeric(5);
			webActionUtils.setText(nameInsuredTXT, nameInsured);

			webActionUtils.info("Entering LOB");
			webActionUtils.setText(lobTXT, lobValue);
			// webActionUtils.waitSleep(5);

			webActionUtils.info("Entering Policy#");
			webActionUtils.setText(policyNoTXT, policyNumber + RandomStringUtils.randomAlphanumeric(5));
			// webActionUtils.waitSleep(5);

			webActionUtils.info("Entering Effective Date");
			// webActionUtils.clickElement(effectiveDateTXT, "Effective Date");
			// webActionUtils.clickElement(effDate, "Effective Date");
			effectiveDateTXT.sendKeys(dateformat);

			webActionUtils.info("Entering Date From Client");
			webActionUtils.clickElement(dateFromClientTXT, "date From Client TXT");
			webActionUtils.waitUntilLoadedAndElementClickable(highlightedDate);
			webActionUtils.verifyElementIsPresent(highlightedDate);
			webActionUtils.clickElement(highlightedDate, "high lighted Date");
			// webActionUtils.waitSleep(5);

			webActionUtils.info("Entering account Manager Name ");
			// webActionUtils.waitSleep(5);
			webActionUtils.clickElement(accountManagerTXT, accountManager);
			// webActionUtils.waitSleep(5);
			webActionUtils.setText(accountManagerTXT, accountManager);
			// webActionUtils.waitSleep(5);
			webActionUtils.clickElement(accountManagerOpn, "account Manager Opn");
			if (chkIsEnabled.isSelected()) {
				chkIsEnabled.click();
			}

			webActionUtils.info("Selecting policy Status ");
			webActionUtils.selectByVisibleText(policyStatusDPDN, policyStatusOpn);
			// webActionUtils.waitSleep(5);

			webActionUtils.info("Uploading a file ");
			webActionUtils.setText(uploadfilesBTN, uploadfilepath1);
			webActionUtils.waitSleep(5);
			
			webActionUtils.info("Clicking On Rush CheckBox button");
			webActionUtils.clickElement(rushCheckBox, "rushCheckBox");
			webActionUtils.pass("Rush Checkbox is Checked");
			

			webActionUtils.info("Submitting the Policy ");
			webActionUtils.invisibilityOfElementLocated(uploadingTxt);
			webActionUtils.scrolltoEnd();
			webActionUtils.waitSleep(10);
			webActionUtils.clickElement(submitBTN, "Submit  Button");
			// webActionUtils.waitSleep(10);
			clickConfirmYesAndCheckToast(expectedText);

		} catch (Exception e) {
			Reporter.log(e.getMessage(), true);
			webActionUtils.fail("Unable to submit the file - pls check" + e.getMessage());
			webActionUtils.fail(e.getMessage());
			e.printStackTrace();
		}
		return nameInsured;
	}

	public void enterFieldvalues2(String accountName, String departmentName, String lobValue, String policyNumber,
			String dateformat, String accountManager, String policyStatusOpn, String uploadfilepath1,
			String uploadfilepath2, String uploadfilepath3, String expectedText) {

		try {
			webActionUtils.info("Selecting Account field");
			webActionUtils.selectByVisibleText(selectAccountDPDN, accountName);
			webActionUtils.info("Selected Account Name : " + accountName);

			webActionUtils.info("Selecting Department / Program DropDown value");
			webActionUtils.selectByVisibleText(selectDepartmentDPDN, departmentName);
			webActionUtils.info("Selected Department Name : " + departmentName + "");

			// Shobhan
			// webActionUtils.info("Entering Named Insured ");
			// webActionUtils.setText(nameInsuredTXT, "Smoke Test" +
			// RandomStringUtils.randomAlphanumeric(3));
			// webActionUtils.waitSleep(5);

			webActionUtils.info("Entering Named Insured ");
			webActionUtils.setText(nameInsuredTXT, "2024 " + RandomStringUtils.randomAlphanumeric(5));
			webActionUtils.waitSleep(5);

			webActionUtils.info("Entering LOB");
			webActionUtils.setText(lobTXT, lobValue);
			// webActionUtils.waitSleep(5);

			webActionUtils.info("Entering Policy#");
			webActionUtils.setText(policyNoTXT, policyNumber + RandomStringUtils.randomAlphanumeric(10));

			webActionUtils.info("Entering Effective Date");
			webActionUtils.clickElement(effectiveDateTXT, "Effective Date");
			webActionUtils.clickElement(effDate, "Effective Date");

			webActionUtils.info("Entering Date From Client");
			webActionUtils.clickElement(dateFromClientTXT, "date From Client TXT");
			webActionUtils.waitUntilLoadedAndElementClickable(highlightedDate);
			webActionUtils.verifyElementIsPresent(highlightedDate);
			webActionUtils.clickElement(highlightedDate, "high lighted Date");
			// webActionUtils.waitSleep(5);

			webActionUtils.info("Entering account Manager Name ");
			webActionUtils.clickElement(accountManagerTXT, accountManager);
			// webActionUtils.waitSleep(5);
//			accountManagerTXT.sendKeys(Keys.CLEAR);;

			// webActionUtils.waitSleep(5);
			webActionUtils.setText(accountManagerTXT, accountManager);
			webActionUtils.clickElement(accountManagerOpn, "account Manager Opn");
			// webActionUtils.waitSleep(5);
			// webActionUtils.waitSleep(2);

			if (chkIsEnabled.isSelected()) {
				chkIsEnabled.click();
			} else {

			}

			webActionUtils.info("Selecting policy Status ");
			webActionUtils.selectByVisibleText(policyStatusDPDN, policyStatusOpn);
			// webActionUtils.waitSleep(20);
			webActionUtils.info("uploading a file ");
			// webActionUtils.clickElement(uploadfiles, "");
			webActionUtils.waitInVisibilityOfElementLocated(uploadfilesBTN);
			webActionUtils.setText(uploadfilesBTN, uploadfilepath1 + "\n " + uploadfilepath2 + "\n " + uploadfilepath3);
			// webActionUtils.setText(uploadfilesBTN, uploadfilepath1 );
			webActionUtils.waitSleep(5);
			webActionUtils.info("Submitting the Policy ");
			try {
				webActionUtils.waitInVisibilityOfElementLocated(uploadingTxt2);
			} catch (Exception e1) {
				e1.printStackTrace();
				System.out.println("uploadingTxt2 element issue ");
			}

			// webActionUtils.waitSleep(5);

			// webActionUtils.waitUntilLoadedAndVisibilityOfElementLocated(submitBTN);
			webActionUtils.scrolltoEnd();
			webActionUtils.clickElement(submitBTN, "Submit Button");
			// webActionUtils.waitSleep(3);
			clickConfirmYesAndCheckToast(expectedText);

		} catch (Exception e) {
			Reporter.log(e.getMessage(), true);
			e.printStackTrace();
			webActionUtils.fail("Package creation failed - Pls check :" + e.getMessage());
			webActionUtils.fail(e.getMessage());
			Assert.fail();

		}
		webActionUtils.pass("Package is created");
	}

	public void enterFieldvalues3(String nIns, String accountName, String departmentName, String lobValue,
			String policyNumber, String dateformat, String accountManager, String policyStatusOpn,
			String uploadfilepath1, String expectedText) {
		try {

			webActionUtils.info("Selecting Account field");
			webActionUtils.selectByVisibleText(selectAccountDPDN, accountName);
			webActionUtils.pass("Selected Account Name : " + accountName);

			webActionUtils.info("Selecting Department / Program DropDown value");
			webActionUtils.selectByVisibleText(selectDepartmentDPDN, departmentName);
			webActionUtils.pass("Selected Department Name : " + departmentName + "");
			// Shobhan
			// webActionUtils.info("Entering Named Insured ");
			// webActionUtils.setText(nameInsuredTXT, "Smoke Test" +
			// RandomStringUtils.randomAlphanumeric(3));
			// webActionUtils.waitSleep(5);

			webActionUtils.info("Entering Named Insured ");
			webActionUtils.setText(nameInsuredTXT, nIns);// " +
															// RandomStringUtils.randomAlphanumeric(3));
			webActionUtils.waitSleep(1);

			webActionUtils.info("Entering LOB");
			webActionUtils.setText(lobTXT, lobValue);
			webActionUtils.waitSleep(1);

			webActionUtils.info("Entering Policy#");
			webActionUtils.setText(policyNoTXT, policyNumber);

			webActionUtils.info("Entering Effective Date");
			// webActionUtils.clickElement(effectiveDateTXT, "Effective Date");
			// webActionUtils.clickElement(effDate, "Effective Date");
			effectiveDateTXT.sendKeys(dateformat);

			webActionUtils.info("Entering Date From Client");
			webActionUtils.clickElement(dateFromClientTXT, "date From Client TXT");
			webActionUtils.waitUntilLoadedAndElementClickable(highlightedDate);
			webActionUtils.verifyElementIsPresent(highlightedDate);
			webActionUtils.clickElement(highlightedDate, "high lighted Date");
			webActionUtils.waitSleep(3);

			// webActionUtils.info("Entering account Manager Name ");
			// webActionUtils.waitSleep(5);
			// webActionUtils.clickElement(accountManagerTXT, accountManager);
			// webActionUtils.waitSleep(5);
			// webActionUtils.setText(accountManagerTXT, accountManager);
			// webActionUtils.waitSleep(5);
			// webActionUtils.clickElement(accountManagerOpn, "account Manager
			// Opn");
			chkIsEnabled.click();
			webActionUtils.info("Selecting policy Status ");
			webActionUtils.selectByVisibleText(policyStatusDPDN, policyStatusOpn);
			webActionUtils.waitSleep(10);
			webActionUtils.info("uploading a file ");
			webActionUtils.clickElement(uploadfiles, "");
			webActionUtils.setText(uploadfilesBTN, uploadfilepath1);
			// webActionUtils.setText(uploadfilesBTN, uploadfilepath1 );

			webActionUtils.waitSleep(5);

			webActionUtils.info("Submitting the Policy ");
			try {
				webActionUtils.waitInVisibilityOfElementLocated(uploadingTxt2);
			} catch (Exception e1) {
				e1.printStackTrace();
				System.out.println("uploadingTxt2 element issue ");
			}

			webActionUtils.waitSleep(3);
			webActionUtils.waitUntilLoadedAndVisibilityOfElementLocated(submitBTN);
			webActionUtils.clickElement(submitBTN, "Submit Button");
			webActionUtils.waitSleep(5);
			try {
				webActionUtils.info("1st Time try");

				clickConfirmYesAndCheckToast(expectedText);
			} catch (Exception e) {
				webActionUtils.info("2nd Time try");
				clickConfirmYesAndCheckToast(expectedText);
			}

		} catch (Exception e) {
			Reporter.log(e.getMessage(), true);
			e.printStackTrace();
			webActionUtils.fail("Unable to Submit the file - Pls check" + e.getMessage());
			webActionUtils.fail(e.getMessage());
		}

	}
	
	
	public void enterFieldvalues4(String nIns, String accountName, String departmentName, String lobValue,
			String policyNumber, String dateformat, String accountManager, String policyStatusOpn,
			String uploadfilepath1, String expectedText) {
		try {

			webActionUtils.info("Selecting Account field");
			webActionUtils.selectByVisibleText(selectAccountDPDN, accountName);
			webActionUtils.pass("Selected Account Name : " + accountName);

			webActionUtils.info("Selecting Department / Program DropDown value");
			webActionUtils.selectByVisibleText(selectDepartmentDPDN, departmentName);
			webActionUtils.pass("Selected Department Name : " + departmentName + "");
			// Shobhan
			// webActionUtils.info("Entering Named Insured ");
			// webActionUtils.setText(nameInsuredTXT, "Smoke Test" +
			// RandomStringUtils.randomAlphanumeric(3));
			// webActionUtils.waitSleep(5);

			webActionUtils.info("Entering Named Insured ");
			webActionUtils.setText(nameInsuredTXT, nIns);// " +
															// RandomStringUtils.randomAlphanumeric(3));
			//webActionUtils.waitSleep(1);

			webActionUtils.info("Entering LOB");
			webActionUtils.setText(lobTXT, lobValue);
			//webActionUtils.waitSleep(1);

			webActionUtils.info("Entering Policy#");
			webActionUtils.setText(policyNoTXT, policyNumber);

			webActionUtils.info("Entering Effective Date");
			// webActionUtils.clickElement(effectiveDateTXT, "Effective Date");
			// webActionUtils.clickElement(effDate, "Effective Date");
			effectiveDateTXT.sendKeys(dateformat);

			webActionUtils.info("Entering Date From Client");
			webActionUtils.clickElement(dateFromClientTXT, "date From Client TXT");
			webActionUtils.waitUntilLoadedAndElementClickable(highlightedDate);
			webActionUtils.verifyElementIsPresent(highlightedDate);
			webActionUtils.clickElement(highlightedDate, "high lighted Date");
			//webActionUtils.waitSleep(3);

			 webActionUtils.info("Entering account Manager Name ");
			 //webActionUtils.waitSleep(5);
			 webActionUtils.clickElement(accountManagerTXT, accountManager);
			 //webActionUtils.waitSleep(5);
			 webActionUtils.setText(accountManagerTXT, accountManager);
			 //webActionUtils.waitSleep(5);
			 webActionUtils.clickElement(accountManagerOpn, "account ManagerOpn");

				if (chkIsEnabled.isSelected()) {
					chkIsEnabled.click();
				}			webActionUtils.info("Selecting policy Status ");
			webActionUtils.selectByVisibleText(policyStatusDPDN, policyStatusOpn);
			webActionUtils.waitSleep(10);
			webActionUtils.info("uploading a file ");
			webActionUtils.clickElement(uploadfiles, "");
			webActionUtils.setText(uploadfilesBTN, uploadfilepath1);
			// webActionUtils.setText(uploadfilesBTN, uploadfilepath1 );

			webActionUtils.waitSleep(5);

			webActionUtils.info("Submitting the Policy ");
			try {
				webActionUtils.waitInVisibilityOfElementLocated(uploadingTxt2);
			} catch (Exception e1) {
				e1.printStackTrace();
				System.out.println("uploadingTxt2 element issue ");
			}

			webActionUtils.waitSleep(3);
			webActionUtils.waitUntilLoadedAndVisibilityOfElementLocated(submitBTN);
			webActionUtils.clickElement(submitBTN, "Submit Button");
			webActionUtils.waitSleep(5);
			try {
				webActionUtils.info("1st Time try");

				clickConfirmYesAndCheckToast(expectedText);
			} catch (Exception e) {
				webActionUtils.info("2nd Time try");
				clickConfirmYesAndCheckToast(expectedText);
			}

		} catch (Exception e) {
			Reporter.log(e.getMessage(), true);
			e.printStackTrace();
			webActionUtils.fail("Unable to Submit the file - Pls check" + e.getMessage());
			webActionUtils.fail(e.getMessage());
		}

	}

	/**
	 * Chaithra
	 */
	public void enterFieldValuesInUpload(String nameInsured, String accountName, String departmentName, String lobValue,
			String policyNumber, String dateformat, String accountManager, String policyStatusOpn,
			String uploadfilepath1, String expectedText) {
		webActionUtils.info("Entering the required fields in the UploadFile Page");
		try {
			webActionUtils.waitUntilLoadedAndVisibilityOfElementLocated(selectAccountDPDN);
			webActionUtils.info("Selecting Account field");
			webActionUtils.selectByVisibleText(selectAccountDPDN, accountName);
			webActionUtils.info("Selected Account Name : " + accountName);

			webActionUtils.info("Selecting Department / Program DropDown value");
			webActionUtils.selectByVisibleText(selectDepartmentDPDN, departmentName);
			webActionUtils.info("Selected Department Name : " + departmentName);

			webActionUtils.info("Enter values to Named Insured textfield ");
			webActionUtils.setText(nameInsuredTXT, nameInsured);
			webActionUtils.info("Named Insured Name : " + nameInsured);

			webActionUtils.info("Selecting required LOB");
			// webActionUtils.waitSleep(1);
			webActionUtils.setText(lobTXT, lobValue);
			webActionUtils.info("Selected LOB Name :" + lobValue);

			webActionUtils.info("Enter values to policy Number # textfield");
			webActionUtils.setText(policyNoTXT, policyNumber);
			webActionUtils.info("Policy Number Name : " + policyNumber);

			webActionUtils.info("Enter values to  Effective Date");
			effectiveDateTXT.sendKeys(dateformat);
			webActionUtils.info("Effective Date : " + dateformat);

			webActionUtils.info("Selecting the Date From Client");
			webActionUtils.clickElement(dateFromClientTXT, "date From Client TXT");
			webActionUtils.waitUntilLoadedAndElementClickable(highlightedDate);
			webActionUtils.verifyElementIsPresent(highlightedDate);
			// webActionUtils.waitSleep(3);
			webActionUtils.clickElement(highlightedDate, "high lighted Date");
			// webActionUtils.waitSleep(3);

			webActionUtils.info("Entering Account Manager Name ");
			accountManagerTXT.clear();
			webActionUtils.clickElement(accountManagerTXT, accountManager);
			// webActionUtils.waitSleep(3);
			webActionUtils.setText(accountManagerTXT, accountManager);
			// webActionUtils.waitSleep(5);
			webActionUtils.clickElement(accountManagerOpn, "Account Manager Option");
			chkIsEnabled.click();

			if (chkIsEnabled.isSelected()) {
				chkIsEnabled.click();
			}

			webActionUtils.info("Select required Policy Status");
			webActionUtils.selectByVisibleText(policyStatusDPDN, policyStatusOpn);
			webActionUtils.info("Selected Policy Status  :" + policyStatusOpn);
			// webActionUtils.waitSleep(10);
			webActionUtils.info("uploading required files");
			webActionUtils.clickElement(uploadfiles, "");
			webActionUtils.setText(uploadfilesBTN, uploadfilepath1);
			webActionUtils.waitSleep(5);

			try {
				webActionUtils.waitInVisibilityOfElementLocated(uploadingTxt2);
				webActionUtils.waitSleep(5);
			} catch (Exception e1) {
				e1.printStackTrace();
				webActionUtils.info("Issue in the loading uploading element ");
				webActionUtils.waitSleep(5);
			}

		} catch (Exception e) {
			webActionUtils.fail("Unable to fill the data in the Text Fields - Pls check" + e.getMessage());
		}

	}

	/**
	 * @author Chaithra
	 * @param nameInsured
	 * @param accountName
	 * @param departmentName
	 * @param lobValue
	 * @param policyNumber
	 * @param dateformat
	 * @param accountManager
	 * @param policyStatusOpn
	 * @param uploadfilepath1
	 * @param expectedText
	 */
	public void enterFieldValuesInUpload1(String nameInsured, String accountName, String departmentName,
			String lobValue, String policyNumber, String dateformat, String accountManager, String policyStatusOpn,
			String uploadfilepath1, String expectedText) {
		webActionUtils.info("Entering the required fields in the UploadFile Page");
		try {

			webActionUtils.info("Selecting Account field");
			webActionUtils.selectByVisibleText(selectAccountDPDN, accountName);
			webActionUtils.info("Selected Account Name : " + accountName);

			webActionUtils.info("Selecting Department / Program DropDown value");
			webActionUtils.selectByVisibleText(selectDepartmentDPDN, departmentName);
			webActionUtils.info("Selected Department Name : " + departmentName);

			webActionUtils.info("Enter values to Named Insured textfield ");
			webActionUtils.setText(nameInsuredTXT, nameInsured);
			webActionUtils.info("Named Insured Name : " + nameInsured);

			webActionUtils.info("Selecting required LOB");
			// webActionUtils.waitSleep(1);
			webActionUtils.setText(lobTXT, lobValue);
			webActionUtils.info("Selected LOB Name :" + lobValue);

			webActionUtils.info("Enter values to policy Number # textfield");
			webActionUtils.setText(policyNoTXT, policyNumber);
			webActionUtils.info("Policy Number Name : " + policyNumber);

			webActionUtils.info("Enter values to  Effective Date");
			effectiveDateTXT.sendKeys(dateformat);
			webActionUtils.info("Effective Date : " + dateformat);

			webActionUtils.info("Selecting the Date From Client");
			webActionUtils.clickElement(dateFromClientTXT, "date From Client TXT");
			webActionUtils.waitUntilLoadedAndElementClickable(highlightedDate);
			webActionUtils.verifyElementIsPresent(highlightedDate);
			// webActionUtils.waitSleep(3);
			webActionUtils.clickElement(highlightedDate, "high lighted Date");
			// webActionUtils.waitSleep(3);

			webActionUtils.info("Entering Account Manager Name ");
			webActionUtils.clickElement(accountManagerTXT, accountManager);
			webActionUtils.waitSleep(1);
			webActionUtils.setText(accountManagerTXT, accountManager);
			webActionUtils.waitSleep(1);
			webActionUtils.waitUntilLoadedAndVisibilityOfElementLocated(accountManagerOpn);
			webActionUtils.clickElement(accountManagerOpn, "Account Manager Option");
			chkIsEnabled.click();

			if (chkIsEnabled.isSelected()) {
				chkIsEnabled.click();
			}

			webActionUtils.info("Select required Policy Status");
			webActionUtils.selectByVisibleText(policyStatusDPDN, policyStatusOpn);
			webActionUtils.info("Selected Policy Status  :" + policyStatusOpn);
			// webActionUtils.waitSleep(10);
			webActionUtils.info("uploading required files");
			webActionUtils.clickElement(uploadfiles, "");
			webActionUtils.setText(uploadfilesBTN, uploadfilepath1);
			webActionUtils.waitSleep(5);

			try {
				webActionUtils.waitInVisibilityOfElementLocated(uploadingTxt2);
				webActionUtils.waitSleep(5);
			} catch (Exception e1) {
				e1.printStackTrace();
				webActionUtils.info("Issue in the loading uploading element ");
			}

		} catch (Exception e) {
			webActionUtils.fail("Unable to fill the data in the Text Fields - Pls check" + e.getMessage());
		}

	}

	public String enterFieldvaluesKS(String accountName, String departmentName, String lobValue, String policyNumber,
			String dateformat, String accountManager, String policyStatusOpn, String uploadfilepath1,
			String expectedText) {

		String nameInsured = "";

		try {

			webActionUtils.info("Selecting Account field");
			webActionUtils.selectByVisibleText(selectAccountDPDN, accountName);
			webActionUtils.pass("Selected Account Name : " + accountName);

			webActionUtils.info("Selecting Department / Program DropDown value");
			webActionUtils.selectByVisibleText(selectDepartmentDPDN, departmentName);
			webActionUtils.pass("Selected Department Name : " + departmentName + "");

			webActionUtils.info("Entering Named Insured ");
			// nameInsured
			// ="TechTest0"+RandomStringUtils.randomNumeric(1)+"-update"+
			// RandomStringUtils.randomNumeric(1);
			nameInsured = "SHOBHAN_TEST_EXCELCHECKLIST_ " + RandomStringUtils.randomAlphanumeric(5);
			webActionUtils.setText(nameInsuredTXT, nameInsured);

			webActionUtils.info("Entering LOB");
			webActionUtils.setText(lobTXT, lobValue);
			webActionUtils.waitSleep(5);

			webActionUtils.info("Entering Policy#");
			webActionUtils.setText(policyNoTXT, policyNumber + RandomStringUtils.randomAlphanumeric(5));
			webActionUtils.waitSleep(5);

			webActionUtils.info("Entering Effective Date");
			// webActionUtils.clickElement(effectiveDateTXT, "Effective Date");
			// webActionUtils.clickElement(effDate, "Effective Date");
			effectiveDateTXT.sendKeys(dateformat);

			webActionUtils.info("Entering Date From Client");
			webActionUtils.clickElement(dateFromClientTXT, "date From Client TXT");
			webActionUtils.waitUntilLoadedAndElementClickable(highlightedDate);
			webActionUtils.verifyElementIsPresent(highlightedDate);
			webActionUtils.clickElement(highlightedDate, "high lighted Date");
			webActionUtils.waitSleep(5);

			webActionUtils.info("Entering account Manager Name ");
			webActionUtils.waitSleep(5);
			webActionUtils.clickElement(accountManagerTXT, accountManager);
			webActionUtils.waitSleep(5);
			webActionUtils.setText(accountManagerTXT, accountManager);
			webActionUtils.waitSleep(5);
			webActionUtils.clickElement(accountManagerOpn, "account Manager Opn");

			webActionUtils.info("Selecting policy Status ");
			webActionUtils.selectByVisibleText(policyStatusDPDN, policyStatusOpn);
			webActionUtils.waitSleep(5);

			webActionUtils.info("uploading a file ");
			webActionUtils.setText(uploadfilesBTN, uploadfilepath1);
			webActionUtils.waitSleep(20);

			webActionUtils.info("Submitting the Policy ");
			webActionUtils.invisibilityOfElementLocated(uploadingTxt);
			webActionUtils.waitSleep(10);
			webActionUtils.clickElement(submitBTN, "submit  Button");
			webActionUtils.waitSleep(10);
			webActionUtils.clickElement(confirmYesBTN1, "Confirm button");
			webActionUtils.waitSleep(10);
			clickConfirmYesAndCheckToast(expectedText);

		} catch (Exception e) {
			Reporter.log(e.getMessage(), true);
			webActionUtils.fail("Unable to submit the file - pls check" + e.getMessage());
			webActionUtils.fail(e.getMessage());
			e.printStackTrace();
		}
		return nameInsured;
	}

	public void enterFieldvaluesMia(String nIns, String accountName, String departmentName, String lobValue,
			String policyNumber, String dateformat, String policyStatusOpn, String p1, String expectedText) {
		try {

			webActionUtils.info("Selecting Account field");
			webActionUtils.selectByVisibleText(selectAccountDPDN, accountName);
			webActionUtils.pass("Selected Account Name : " + accountName);

			webActionUtils.info("Selecting Department / Program DropDown value");
			webActionUtils.selectByVisibleText(selectDepartmentDPDN, departmentName);
			webActionUtils.pass("Selected Department Name : " + departmentName + "");

			webActionUtils.info("Entering Named Insured ");
			webActionUtils.setText(nameInsuredTXT, nIns);

			webActionUtils.info("Entering LOB");
			webActionUtils.setText(lobTXT, lobValue);

			webActionUtils.info("Entering Policy#");
			webActionUtils.setText(policyNoTXT, policyNumber);

			webActionUtils.info("Entering Effective Date");
			effectiveDateTXT.sendKeys(dateformat);

			webActionUtils.info("Entering Date From Client");
			webActionUtils.clickElement(dateFromClientTXT, "date From Client TXT");
			webActionUtils.waitUntilLoadedAndElementClickable(highlightedDate);
			webActionUtils.verifyElementIsPresent(highlightedDate);
			webActionUtils.clickElement(highlightedDate, "high lighted Date");
			webActionUtils.waitSleep(3);

			webActionUtils.info("Selecting policy Status ");
			webActionUtils.selectByVisibleText(policyStatusDPDN, policyStatusOpn);
			if (chkIsEnabled.isSelected()) {
				chkIsEnabled.click();
				if (chkIsEnabled.isSelected() == true) {
					chkIsEnabled.click();
				}
			}

			webActionUtils.info("uPploading a file ");
			// webActionUtils.waitInVisibilityOfElementLocated(uploadfilesBTN);
			webActionUtils.setText(uploadfilesBTN, p1);

			webActionUtils.waitSleep(5);

			webActionUtils.info("Submitting the Policy ");
			try {
				webActionUtils.waitInVisibilityOfElementLocated(uploadingTxt2);
			} catch (Exception e1) {
				e1.printStackTrace();
				System.out.println("uploadingTxt2 element issue ");
			}

			webActionUtils.waitSleep(3);
			webActionUtils.waitUntilLoadedAndVisibilityOfElementLocated(submitBTN);
			webActionUtils.clickElement(submitBTN, "Submit Button");
			webActionUtils.waitSleep(5);
			try {
				webActionUtils.info("1st Time try");

				clickConfirmYesAndCheckToast(expectedText);
			} catch (Exception e) {
				webActionUtils.info("2nd Time try");
				clickConfirmYesAndCheckToast(expectedText);
			}

		} catch (Exception e) {
			Reporter.log(e.getMessage(), true);
			e.printStackTrace();
			webActionUtils.fail("Unable to Submit the file - Pls check" + e.getMessage());
			webActionUtils.fail(e.getMessage());
		}

	}

	/**
	 * Chaithra
	 * 
	 * @param internalDate
	 * @param clientDate
	 */
	public void enterInternalAndClientDueDate(CharSequence internalDate, CharSequence clientDate) {
		webActionUtils.info("Enter value to internal due Date field ");
		internalDueDateTxt.clear();
		internalDate=webActionUtils.getCustomisedDate(16);
		internalDueDateTxt.sendKeys(internalDate);
		webActionUtils.waitSleep(2);
		webActionUtils.info("Entered value to internal due Date field is : " + internalDate);
		webActionUtils.info("Enter value to Client due Date field");
		clientDate=webActionUtils.getCustomisedDate(0);
		clientDueDateTXT.clear();
		clientDueDateTXT.sendKeys(clientDate);
		webActionUtils.waitSleep(2);
		webActionUtils.info("Entered value to Client due Date field is : " + clientDate);
	}
	
	public void enterInternalAndClientDueDate1(CharSequence internalDate, CharSequence clientDate) {
		webActionUtils.info("Enter value to internal due Date field ");
		internalDueDateTxt.clear();
		internalDate=webActionUtils.getCustomisedDate(16);
		internalDueDateTxt.sendKeys(internalDate);
		webActionUtils.waitSleep(2);
		webActionUtils.info("Entered value to internal due Date field is : " + internalDate);
		webActionUtils.info("Enter value to Client due Date field");
		clientDate=webActionUtils.getCustomisedDate(16);
		clientDueDateTXT.clear();
		clientDueDateTXT.sendKeys(clientDate);
		webActionUtils.waitSleep(2);
		webActionUtils.info("Entered value to Client due Date field is : " + clientDate);
	}
	
	public void internalLaterThanClientDueDate(CharSequence internalDate, CharSequence clientDate) {
		webActionUtils.info("Entering internal due Date");
		internalDueDateTxt.clear();
		internalDate=webActionUtils.getCustomisedDate(16);
		internalDueDateTxt.sendKeys(internalDate);
		webActionUtils.waitSleep(5);

		webActionUtils.info("Entering Client due Date");
		clientDate=webActionUtils.getCustomisedDate(0);
		clientDueDateTXT.clear();
		clientDueDateTXT.sendKeys(clientDate);
		webActionUtils.waitSleep(5);
	}

	public void enterUploadfileFieldvalues(String accountName, String departmentName, String lobValue,
			String policyNumber, String dateformat, String accountManager, String policyStatusOpn, String nameInsured) {
		try {
			webActionUtils.info("Selecting Account field");
			webActionUtils.selectByVisibleText(selectAccountDPDN, accountName);
			webActionUtils.info("Selected Account Name : " + accountName);

			webActionUtils.info("Selecting Department / Program DropDown value");
			webActionUtils.selectByVisibleText(selectDepartmentDPDN, departmentName);
			webActionUtils.info("Selected Department Name : " + departmentName);

			webActionUtils.info("Enter values to Named Insured textfield ");
			webActionUtils.setText(nameInsuredTXT, nameInsured);
			webActionUtils.info("Named Insured Name : " + nameInsured);

			webActionUtils.info("Selecting required LOB");
			webActionUtils.setText(lobTXT, lobValue);
			webActionUtils.info("Selected LOB Name :" + lobValue);

			webActionUtils.info("Enter values to policy Number # textfield");
			webActionUtils.setText(policyNoTXT, policyNumber);
			webActionUtils.info("Policy Number Name : " + policyNumber);

			webActionUtils.info("Enter values to  Effective Date");
			effectiveDateTXT.sendKeys(dateformat);
			webActionUtils.info("Effective Date : " + dateformat);

			webActionUtils.info("Selecting the Date From Client");
			webActionUtils.clickElement(dateFromClientTXT, "date From Client TXT");
			webActionUtils.waitUntilLoadedAndElementClickable(highlightedDate);
			webActionUtils.verifyElementIsPresent(highlightedDate);

			webActionUtils.clickElement(highlightedDate, "high lighted Date");
			webActionUtils.info("Entering Account Manager Name ");
			webActionUtils.clickElement(accountManagerTXT, accountManager);
			webActionUtils.setText(accountManagerTXT, accountManager);
			webActionUtils.clickElement(accountManagerOpn, "Account Manager Option");

			chkIsEnabled.click();
			if (chkIsEnabled.isSelected()) {
				chkIsEnabled.click();
			}

			webActionUtils.info("Select required Policy Status");
			webActionUtils.selectByVisibleText(policyStatusDPDN, policyStatusOpn);
			webActionUtils.info("Selected Policy Status  :" + policyStatusOpn);

		} catch (Exception e) {

			webActionUtils.info("Unable to Submit the file - Pls check" + e.getMessage());
		}
	}

	/**
	 * 
	 * @param accountName
	 * @param departmentName
	 * @param lobValue
	 * @param policyNumber
	 * @param dateformat
	 * @param accountManager
	 * @param policyStatusOpn
	 * @param uploadfilepath1
	 * @param uploadfilepath2
	 * @param uploadfilepath3
	 * @param expectedText
	 * @param nameInsured
	 */
	public void enterUploadfileFieldvalues(String accountName, String departmentName, String lobValue,
			String policyNumber, String dateformat, String accountManager, String policyStatusOpn,
			String uploadfilepath1, String uploadfilepath2, String expectedText, String nameInsured) {

		try {
			webActionUtils.info("Selecting Account field");
			webActionUtils.selectByVisibleText(selectAccountDPDN, accountName);
			webActionUtils.info("Selected Account Name : " + accountName);

			webActionUtils.info("Selecting Department / Program DropDown value");
			webActionUtils.selectByVisibleText(selectDepartmentDPDN, departmentName);
			webActionUtils.info("Selected Department Name : " + departmentName);

			webActionUtils.info("Enter values to Named Insured textfield ");
			webActionUtils.setText(nameInsuredTXT, nameInsured);
			webActionUtils.info("Named Insured Name : " + nameInsured);

			webActionUtils.info("Selecting required LOB");
			// webActionUtils.waitSleep(1);
			webActionUtils.setText(lobTXT, lobValue);
			webActionUtils.info("Selected LOB Name :" + lobValue);

			webActionUtils.info("Enter values to policy Number # textfield");
			webActionUtils.setText(policyNoTXT, policyNumber);
			webActionUtils.info("Policy Number Name : " + policyNumber);

			webActionUtils.info("Enter values to  Effective Date");
			effectiveDateTXT.sendKeys(dateformat);
			webActionUtils.info("Effective Date : " + dateformat);

			webActionUtils.info("Selecting the Date From Client");
			webActionUtils.clickElement(dateFromClientTXT, "date From Client TXT");
			webActionUtils.waitUntilLoadedAndElementClickable(highlightedDate);
			webActionUtils.verifyElementIsPresent(highlightedDate);
			// webActionUtils.waitSleep(3);
			webActionUtils.clickElement(highlightedDate, "high lighted Date");
			// webActionUtils.waitSleep(3);

			webActionUtils.info("Entering Account Manager Name ");
			 webActionUtils.clickElement(accountManagerTXT, accountManager);
			webActionUtils.waitSleep(3);
			 webActionUtils.setText(accountManagerTXT, accountManager);
			 webActionUtils.waitSleep(5);
			 webActionUtils.clickElement(accountManagerOpn, "Account Manager Option");
			chkIsEnabled.click();

			if (chkIsEnabled.isSelected()) {
				chkIsEnabled.click();
			}

			webActionUtils.info("Select required Policy Status");
			webActionUtils.selectByVisibleText(policyStatusDPDN, policyStatusOpn);
			webActionUtils.info("Selected Policy Status  :" + policyStatusOpn);

			webActionUtils.info("uploading a file ");
			webActionUtils.waitInVisibilityOfElementLocated(uploadfilesBTN);
			webActionUtils.setText(uploadfilesBTN, uploadfilepath1 + "\n " + uploadfilepath2);
			webActionUtils.waitSleep(8);
			webActionUtils.waitSleep(8);

		} catch (Exception e) {

			webActionUtils.info("Unable to Submit the file - Pls check" + e.getMessage());
		}
	}

	public String getSerialNumber() {
		webActionUtils.info("get the First Serial Number");
		String text = alertMsg.getText().toString();
		String serialNumber = text.replaceAll("[^0-9.]", "");
		webActionUtils.info("Serial number is " + serialNumber);
		return serialNumber;

	}

	public WebElement getXpathSerail(String text) {
		String xp = "//a[@href='/Package/ManagePackage?ID=" + text + "']";
		return driver.findElement(By.xpath(xp));
	}

	public void clickOnSerialNumber() {
		webActionUtils.info("Click on the Serial Number");
		String text = alertMsg.getText().toString();
		String serialNumber = text.replaceAll("[^0-9.]", "");
		webActionUtils.waitSleep1(2);
		webActionUtils.info("Serial number is " + serialNumber);
		webActionUtils.clickElement(getXpathSerail(serialNumber), text);

		Set<String> allTabs = driver.getWindowHandles();
		List<String> windowHandlesList = new ArrayList<>(allTabs);
		// windowHandlesList.remove(oldTab);
		webActionUtils.waitSleep1(15);
		webActionUtils.getdriver().switchTo().window(windowHandlesList.get(1));
	}



	public boolean InternalPopUpYesBtn() {
		boolean confirmYesbool = false;
		try {
			if (confirmYesBTN.size() > 0 && confirmYesBTN.get(0) != null && confirmYesBTN1.isDisplayed()) {
				confirmYesbool = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
			e.getMessage();
		}
		return confirmYesbool;
	}

	public void prePopulutedSourceType() {
		try {
			webActionUtils.info("Prepopulated Source Type Values before updating");
			webActionUtils.waitSleep(5);

			Select sourcevalue1 = new Select(sourceDrpDnTXT);
			sourcevalue1.selectByVisibleText("Policy");
			webActionUtils.info("Prepopulated source Name 1: " + sourcevalue1.getFirstSelectedOption().getText());
			Select sourcevalue2 = new Select(sourceDrpDnTXT1);
			sourcevalue2.selectByVisibleText("Policy");
			webActionUtils.info("Prepopulated source Name 2: " + sourcevalue2.getFirstSelectedOption().getText());

		} catch (Exception e) {
			Reporter.log(e.getMessage(), true);
			webActionUtils.fail("" + e.getMessage());
		}
	}

	/**
	 * @author Chaithra
	 * @return
	 */
	public boolean renewalPopUpYesBtn() {
		boolean confirmYesbool = false;
		try {
			if (confirmYesBTN.size() > 0 && confirmYesBTN.get(0) != null && confirmYesBTN1.isDisplayed()) {
				confirmYesbool = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
			e.getMessage();
		}
		return confirmYesbool;
	}

	public void selectAccount(String account) {
		try {
			webActionUtils.waitSleep(10);
			webActionUtils.info("Select Required Acoount" + account);
			webActionUtils.selectByVisibleText(selectAccountDPDN, account);
			webActionUtils.info("Selected Account Name : " + account);

		} catch (Exception e) {
			webActionUtils.fail("Failed to select " + account + " in Upload file Page");
		}
	}

	public void selectDepartment(String departmentName) {
		try {
			webActionUtils.info("Selecting Department / Program DropDown value");
			webActionUtils.selectByVisibleText(selectDepartmentDPDN, departmentName);
			webActionUtils.info("Selected Department Name : " + departmentName);
			webActionUtils.waitSleep(4);

		} catch (Exception e) {
			webActionUtils.fail("Failed to select " + departmentName + " in Upload file Page");
		}
	}

	public void selectAccountDepartment(String account, String departmentName) {
		try {
			webActionUtils.waitSleep(10);
			webActionUtils.info("Select Required Acoount" + account);
			webActionUtils.selectByVisibleText(selectAccountDPDN, account);
			webActionUtils.info("Selected Account Name : " + account);

			webActionUtils.info("Selecting Department / Program DropDown value");
			webActionUtils.selectByVisibleText(selectDepartmentDPDN, departmentName);
			webActionUtils.info("Selected Department Name : " + departmentName);

		} catch (Exception e) {
			webActionUtils.fail("Failed to select " + account + " in Upload file Page");
		}
	}

	public void clickOnDepartment_OfficeCode() {
		try {
			webActionUtils.info("click On Department_OfficeCode field");
			webActionUtils.clickElement(DepartmentOfficeCode, "DepartmentOfficeCode");
			webActionUtils.doubleClick(DepartmentOfficeCodeLabel);
			webActionUtils.waitSleep(4);

		} catch (Exception e) {
			webActionUtils.fail(" TC failed");

		}

	}

	public void selectRushFixedWordings(String Str) {
		webActionUtils.info("select Rush Fixed Wordings from its dropdown");
		try {
			webActionUtils.waitSleep(3);
			webActionUtils.info("Selecting Rush Fixed Wordings dropdown field" + Str);
			webActionUtils.selectByVisibleText(RushFixWordingBoxDropdown, Str);
			webActionUtils.pass("Selected RushFixWordingBoxDropdown Name : " + Str);
			webActionUtils.waitSleep(3);
		} catch (Exception e) {
		}
	}

	/**
	 * @author Chaithra
	 */
	public void submitButton() {
		webActionUtils.info("Submitting the package details");
		webActionUtils.waitUntilLoadedAndVisibilityOfElementLocated(submitBTN);
		webActionUtils.info("Clicking on Submit Button");
		webActionUtils.clickElement(submitBTN, "Submit Button");
		webActionUtils.waitSleep(8);

	}

	/**
	 * Verification of Internal Due Date and Client Due Date changing to Old Date
	 * from Current Date on UnCheck of Rush Check box.
	 */
	public void uncheckRushCheckboxAndValidateDates() {

		try {
			webActionUtils.info(
					"Verification of Internal Due date and Client Due Date Changing back to Old date from current date On UnChecking Rush checkbox ");

			webActionUtils.info("Client Due Date and Internal Due Date before checking the Rush Check box : ");
			webActionUtils.waitSleep(5);
			String internalDate = internalDueDate.getAttribute("value");
			String clientDate = clientDueDateTXT.getAttribute("value");
			webActionUtils.info("Internal Due Date : " + internalDate);
			webActionUtils.info("Client Due Date : " + clientDate);

			webActionUtils.info("Checking the Rush check box");
			rushCheckBox.click();
			webActionUtils.info("Client Due Date and Internal Due Date before checking the Rush Check box : ");
			webActionUtils.waitSleep(5);

			String internalDates = internalDueDate.getAttribute("value");
			String clientDates = clientDueDateTXT.getAttribute("value");
			webActionUtils.info("Internal Due Date : " + internalDates);
			webActionUtils.info("Client Due Date : " + clientDates);
			webActionUtils.info("Client Due Date and Internal Due Date are changed to Current Date  ");
			webActionUtils.waitSleep(5);

			webActionUtils.info("UnChecking the Rush check box");
			rushCheckBox.click();
			webActionUtils.waitSleep(5);

			webActionUtils.info("Client Due Date and Internal Due Date After Unchecking the Rush Check box : ");
			String internalDateAfterUncheck = internalDueDate.getAttribute("value");
			String clientDateAfterUnCheck = clientDueDateTXT.getAttribute("value");
			webActionUtils.info("Internal Due Date : " + internalDateAfterUncheck);
			webActionUtils.info("Client Due Date: " + clientDateAfterUnCheck);

			try {
				if (internalDate.equals(internalDateAfterUncheck) && clientDate.equals(clientDateAfterUnCheck)) {
					webActionUtils.pass(
							"Internal Due date and Client Due Date are Populated based on Account and Department Selection on UnChecking the Rush Check Box : Passed ");
				}
			} catch (Exception e) {
				webActionUtils.fail(
						" Internal Due date and Client Due Date are not Populated based on Account and Department Selection on UnChecking the Rush Check Box : Failed ");
				Reporter.log(e.getMessage(), true);
				webActionUtils.fail("" + e.getMessage());
			}
		} catch (Exception e) {
			Reporter.log(e.getMessage(), true);
			webActionUtils.fail("" + e.getMessage());
		}
	}

	public void UpdatePrePopulatedSourceType() {
		webActionUtils.info("Updating the source name which is prepopulated");
		Select sourcevalueOpt = new Select(sourceDrpDnTXT);
		sourcevalueOpt.selectByVisibleText("Current Endorsement / Change Request");
		Select sourcevalueOpt1 = new Select(sourceDrpDnTXT1);
		sourcevalueOpt1.selectByVisibleText("Note & Missing Info List");
		webActionUtils.info("Source Type Values after updating");
		webActionUtils.info("Selected source Name 1 : " + sourcevalueOpt.getFirstSelectedOption().getText());
		webActionUtils.info("Selected source Name 2: " + sourcevalueOpt1.getFirstSelectedOption().getText());
		webActionUtils.waitSleep(10);
	}

	/**
	 * @author Varshitha
	 * @param accountName
	 * @param departmentName
	 * @param nameInsured
	 * @param lobValue
	 */
	public void uploadFieldValues(String accountName, String departmentName, String nameInsured, String lobValue,
			String policyNumber, String dateformat, String accountManager, String assitantAccountManager) {
		try {
			webActionUtils.info("Selecting required Account");
			webActionUtils.selectByVisibleText(selectAccountDPDN, accountName);
			webActionUtils.info("Selected Account Name : " + accountName);

			webActionUtils.info("Select Department / Program DropDown value");
			webActionUtils.selectByVisibleText(selectDepartmentDPDN, departmentName);
			webActionUtils.info("Selected Department Name : " + departmentName + "");

			webActionUtils.info("Enter values to Named Insured text field ");
			webActionUtils.setText(nameInsuredTXT, nameInsured);
			webActionUtils.info("Entered Named Insured : " + nameInsured);

			webActionUtils.info("Selecting required LOB");
			webActionUtils.setText(lobTXT, lobValue);
			webActionUtils.getElementText(lobTXT);
			webActionUtils.info("Selected LOB : " + lobValue);

			webActionUtils.info("Enter values to the Policy# text field ");
			webActionUtils.setText(policyNoTXT, policyNumber + RandomStringUtils.randomAlphanumeric(5));
			webActionUtils.info("Entered Policy# : " + policyNumber);

			webActionUtils.info("Enter Effective Date");
			effectiveDateTXT.sendKeys(dateformat);

			webActionUtils.info("Enter Date From Client");
			webActionUtils.clickElement(dateFromClientTXT, "date From Client TXT");
			webActionUtils.waitUntilLoadedAndElementClickable(highlightedDate);
			webActionUtils.verifyElementIsPresent(highlightedDate);
			webActionUtils.clickElement(highlightedDate, "Highlighted Date");

			webActionUtils.info("Entering account Manager Name ");
			webActionUtils.waitSleep(5);
			accountManagerTXT.clear();
			webActionUtils.clickElement(accountManagerTXT, accountManager);
			webActionUtils.setText(accountManagerTXT, accountManager);
			webActionUtils.clickElement(accountManagerOpn, "Account Manager Option");
			webActionUtils.pass("Entered Account Manager Name : " + accountManager);

			if (chkIsEnabled.isSelected()) {

				webActionUtils.info("Enter Assitant Account Manager Name ");
				assistantManager.clear();
				webActionUtils.waitSleep(5);
				webActionUtils.clickElement(assistantManager, assitantAccountManager);
				webActionUtils.setText(assistantManager, assitantAccountManager);
				webActionUtils.clickElement(assistantAccountManagerOpn, " Assitant Account Manager Option");
			} else {
				webActionUtils.info("Assign AM checkbox is not Selected");
			}

		} catch (Exception e) {
			Reporter.log(e.getMessage(), true);
			webActionUtils.fail("" + e.getMessage());
		}
	}

	public void uploadValuesForDateCheck(String accountName, String departmentName, String nameInsured, String lobValue,
			String policyNumber, String dateformat, String internalDueeDate) {
		try {
			webActionUtils.info("Selecting required Account");
			webActionUtils.selectByVisibleText(selectAccountDPDN, accountName);
			webActionUtils.info("Selected Account Name : " + accountName);

			webActionUtils.info("Select Department / Program DropDown value");
			webActionUtils.selectByVisibleText(selectDepartmentDPDN, departmentName);
			webActionUtils.info("Selected Department Name : " + departmentName + "");

			webActionUtils.info("Enter values to Named Insured text field ");
			webActionUtils.setText(nameInsuredTXT, nameInsured);
			webActionUtils.info("Entered Named Insured : " + nameInsured);

			webActionUtils.info("Selecting required LOB");
			webActionUtils.setText(lobTXT, lobValue);
			webActionUtils.getElementText(lobTXT);
			webActionUtils.info("Selected LOB : " + lobValue);

			webActionUtils.info("Enter values to the Policy# text field ");
			webActionUtils.setText(policyNoTXT, policyNumber + RandomStringUtils.randomAlphanumeric(5));
			webActionUtils.info("Entered Policy# : " + policyNumber);

			webActionUtils.info("Enter Effective Date");
			effectiveDateTXT.sendKeys(dateformat);
			webActionUtils.info("Entered Effective date : " + dateformat);

			webActionUtils.info("Enter Internal Due date ");
			internalDueDate.clear();
			internalDueDate.sendKeys(internalDueeDate);
			webActionUtils.info("Entered Internal Due Date : " + internalDueeDate);

		} catch (Exception e) {
			Reporter.log(e.getMessage(), true);
			webActionUtils.fail("" + e.getMessage());
		}
	}

	public void uploadValuesForInternalDateCheck(String accountName, String departmentName, String nameInsured,
			String lobValue, String policyNumber, String dateformat, String internalDueeDate) {
		try {
			webActionUtils.info("Selecting required Account");
			webActionUtils.selectByVisibleText(selectAccountDPDN, accountName);
			webActionUtils.info("Selected Account Name : " + accountName);

			webActionUtils.info("Select Department / Program DropDown value");
			webActionUtils.selectByVisibleText(selectDepartmentDPDN, departmentName);
			webActionUtils.info("Selected Department Name : " + departmentName + "");

			webActionUtils.info("Enter values to Named Insured text field ");
			webActionUtils.setText(nameInsuredTXT, nameInsured);
			webActionUtils.info("Entered Named Insured : " + nameInsured);

			webActionUtils.info("Selecting required LOB");
			webActionUtils.setText(lobTXT, lobValue);
			webActionUtils.getElementText(lobTXT);
			webActionUtils.info("Selected LOB : " + lobValue);

			webActionUtils.info("Enter values to the Policy# text field ");
			webActionUtils.setText(policyNoTXT, policyNumber + RandomStringUtils.randomAlphanumeric(5));
			webActionUtils.info("Entered Policy# : " + policyNumber);

			webActionUtils.info("Enter Effective Date");
			effectiveDateTXT.sendKeys(dateformat);
			webActionUtils.info("Entered Effective date : " + dateformat);

			webActionUtils.info("Enter Internal Due date ");
			internalDueDate.clear();
			internalDueDate.sendKeys(internalDueeDate);
			webActionUtils.info("Entered Internal Due Date : " + internalDueeDate);

		} catch (Exception e) {
			Reporter.log(e.getMessage(), true);
			webActionUtils.fail("" + e.getMessage());
		}
	}

	public void uploadValuesForLob(String accountName, String departmentName, String nameInsured, String lobValue) {
		try {
			webActionUtils.info("Selecting required Account");
			webActionUtils.selectByVisibleText(selectAccountDPDN, accountName);
			webActionUtils.info("Selected Account Name : " + accountName);

			webActionUtils.info("Select Department / Program DropDown value");
			webActionUtils.selectByVisibleText(selectDepartmentDPDN, departmentName);
			webActionUtils.info("Selected Department Name : " + departmentName + "");

			webActionUtils.info("Enter values to Named Insured text field ");
			webActionUtils.setText(nameInsuredTXT, nameInsured);
			webActionUtils.info("Entered Named Insured : " + nameInsured);

			webActionUtils.info("Selecting required LOB ");
			webActionUtils.clickElement(lobTXT, lobValue);
			webActionUtils.setText(lobTXT, lobValue);
			webActionUtils.clickElement(lobOption, "LOB Option");
			webActionUtils.pass("Selected LOB : " + lobValue);
			webActionUtils.waitSleep(5);

			webActionUtils.info("Verifying whether new LOB textfield is displayed");
			webActionUtils.waitUntilLoadedAndVisibilityOfElementLocated(newLOBTextField);

			if (webActionUtils.isElementDisplayed(newLOBTextField)) {
				webActionUtils.pass("Type in new LOB textfield Verification: Passed");
			} else {
				webActionUtils.fail("Type in new LOB textfield Verification: Failed");
			}

		} catch (Exception e) {
			Reporter.log(e.getMessage(), true);
			webActionUtils.fail("" + e.getMessage());
		}

	}

	/**
	 * @author Varshitha_Yadav
	 * @param accountName
	 * @param departmentName
	 * @param lobValue
	 * @param policyNumber
	 * @param dateformat
	 * @return Entering field values for Verification of Internal Due Date and
	 *         Client Due Date for Rush Check box.
	 */

	public String uploadValuesForRushCheckboxCheck(String accountName, String departmentName, String nameInsured,
			String lobValue, String policyNumber, String dateformat) {

		try {
			webActionUtils.info("Select required Account");
			webActionUtils.selectByVisibleText(selectAccountDPDN, accountName);
			webActionUtils.info("Selected Account Name : " + accountName);

			webActionUtils.info("Select Department / Program DropDown value");
			webActionUtils.selectByVisibleText(selectDepartmentDPDN, departmentName);
			webActionUtils.info("Selected Department Name : " + departmentName + "");

			webActionUtils.info("Enter values to Named Insured text field ");
			webActionUtils.setText(nameInsuredTXT, nameInsured);
			webActionUtils.info("Entered Named Insured : " + nameInsured);

			webActionUtils.info("Select required LOB");
			webActionUtils.setText(lobTXT, lobValue);
			webActionUtils.getElementText(lobTXT);
			webActionUtils.info("Selected LOB : " + lobValue);

			webActionUtils.info("Enter values to the Policy# text field ");
			webActionUtils.setText(policyNoTXT, policyNumber);
			webActionUtils.info("Entered Policy# : " + policyNumber);

			webActionUtils.info("Enter Effective Date");
			effectiveDateTXT.sendKeys(dateformat);
			webActionUtils.info("Entered Effective Date : " + dateformat);

			webActionUtils.info("Enter Date From Client");
			webActionUtils.clickElement(dateFromClientTXT, "date From Client TXT");
			webActionUtils.waitUntilLoadedAndElementClickable(highlightedDate);
			webActionUtils.verifyElementIsPresent(highlightedDate);
			webActionUtils.clickElement(highlightedDate, "Highlighted Date");

		} catch (Exception e) {
			Reporter.log(e.getMessage(), true);
			webActionUtils.fail("" + e.getMessage());
		}

		return nameInsured;
	}

	/**
	 * Chaithra
	 * 
	 * @param expectedText1
	 */
	public void verifingDuplicatePopUpToastMessage(String expectedText1) {
		{
			webActionUtils.info("Verification of Duplicate pop up");
			String actualText = popUpText.getText().toString();
			try {
				if (actualText.equalsIgnoreCase(expectedText1)) {
					webActionUtils.info("Actual text : " + actualText);
					webActionUtils.info("Expected text : " + expectedText1);
				}
				webActionUtils.pass("Duplicate pop up verification: Passed");
			} catch (AssertionError e) {
				webActionUtils.fail("Duplicate pop up verification: FAILED " + e.getMessage());
			}
		}
	}

	public void verifyCarrierDropdownOptionsPresent(String text) {
		try {
			webActionUtils.info("Carrier Dropdown Options");
			webActionUtils.setText(carrierTxtBx, text);
			carrierOpn.click();

		} catch (Exception e) {
			webActionUtils.fail("Failed to Click on the Carrier textbox present in Upload file Page");
		}

	}

	public void verifyCarrieFieldIsBlank() {
		try {
			webActionUtils.info("Verfiy Carrier Is blank ");
			boolean bool = carrierTxtBx.getText().toString().isBlank();

			if (bool) {
				webActionUtils.pass("Carrier Type Ahead is Blank");

			} else {
				webActionUtils.fail("Carrier Type Ahead is Blank");

			}

		} catch (Exception e) {
			webActionUtils.fail("Failed to Click on the Carrier textbox present in Upload file Page");
		}

	}

	public void verifyCarriertextbox() {
		try {
			webActionUtils.info("Verify Carrier textbox Present in Upload file page on select of PI Lite");
			webActionUtils.clickElement(carrierTxtBx, "Carrier textbox");
		} catch (Exception e) {
			webActionUtils.fail("Failed to Click on the Carrier textbox present in Upload file Page");
		}

	}

	public void verifyCarriertextboxIsNotPresent() {
		webActionUtils.info("Verify Carrier textbox is NOT Present of PI Radio button ");

		boolean isDisplayed;
		try {
			WebElement element = carrierTxtBx;
			isDisplayed = element.isDisplayed();
		} catch (NoSuchElementException e) {
			isDisplayed = false;
		}

		if (!isDisplayed) {
			System.out.println("Button is not displayed");
			webActionUtils.pass("Carrier textbox not present in Upload file Page: Passed");

		} else {
			webActionUtils.fail(" Carrier textbox present in Upload file Page:  Failed");

		}
	}

	/**
	 * @author Chaithra
	 * @param expectedText
	 */
	public void verifyDuplicatePopUpToastMessage(String expectedText1) {
		{
			webActionUtils.info("Verification of Duplicate pop up");
			String actualText = duplicatePopUp.getText().toString();
			try {
				if (expectedText1.contains(actualText)) {
					webActionUtils.info("Actual text : " + actualText);
					webActionUtils.info("Expected text : " + expectedText1);
				}
				webActionUtils.pass("Duplicate pop up Verification: Passed");
			} catch (AssertionError e) {
				webActionUtils.fail("Duplicate pop up verification: Failed" + e.getMessage());
			}
		}
	}

	public void verifyToastMessageNotContainsSerialnumber(String expectedText) {
		{
			webActionUtils.info("Verification of Toast Message Not Contains Serial number");
			try {
				webActionUtils.isElementDisplayed1(alertMsg, 40);
				webActionUtils.waitUntilLoadedAndVisibilityOfElementLocated(alertMsg);
				webActionUtils.verifyElementIsPresent(alertMsg);
				String str = alertMsg.getText();

				boolean containsNumbers = false;
				for (int i = 0; i < str.length(); i++) {
					if (Character.isDigit(str.charAt(i))) {
						containsNumbers = true;
						break;
					}
				}

				if (containsNumbers) {
					System.out.println("The string contains numbers.");
				} else {
					System.out.println("The string does not contain numbers in the Toast message");
				}

				webActionUtils.verifyElementContainsText(alertMsg, expectedText);
			} catch (Exception e) {
				Assert.fail();
			}
		}

	}

	public void VerifyEIXFieldInUploadPage(String accountName) {
		try {
			webActionUtils.info("Selecting Account field");
			webActionUtils.selectByVisibleText(selectAccountDPDN, accountName);
			webActionUtils.info("Selected Account Name : " + accountName);
			webActionUtils.waitSleep(3);
			webActionUtils.waitUntilLoadedAndVisibilityOfElementLocated(ExpressIXAccount);

			if (ExpressIXAccount.isDisplayed())
				webActionUtils.info("Checking Department Office Code text field and ExpressIX Account text field");

			webActionUtils.waitUntilLoadedAndVisibilityOfElementLocated(DepartmentOfficeCode);
			if (DepartmentOfficeCode.isDisplayed())
				webActionUtils.pass(
						"TC passed :  Department Office Code text field and ExpressIX Account text field are displayed for the EIX Account");

		} catch (Exception e) {
			webActionUtils.fail(" TC failed");

		}
	}

	/*
	 * Varshitha Yadav
	 */
	public void verifyFiledValuesAfterClear() {
		try {
			webActionUtils.info("Verifying the Entered Package Details....");

			WebElement t = driver.findElement(By.xpath("//*[@id='AccountID']"));
			Select select = new Select(t);
			WebElement o = select.getFirstSelectedOption();
			String selectedAccount = o.getText();

			WebElement t1 = driver.findElement(By.xpath("//*[@id='DepartmentID']"));
			Select select1 = new Select(t1);
			WebElement o1 = select1.getFirstSelectedOption();
			String selectedDept = o1.getText();

			WebElement t2 = driver.findElement(By.xpath("//*[@id='PackageTypeId']"));
			Select select2 = new Select(t2);
			WebElement o2 = select2.getFirstSelectedOption();
			String selectedPolicy = o2.getText();

			String nameInsured = nameInsuredTXT.getAttribute("value");
			String lob = lobTXT.getAttribute("value");
			String policy = policyNoTXT.getAttribute("value");
			String effectiveDate = effectiveDateTXT.getAttribute("value");
			String dateFromClient = dateFromClientTXT.getAttribute("value");
			String ClientDueDate = clientDueDateTXT.getAttribute("value");
			String internalDueDateValue = internalDueDate.getAttribute("value");
			String accountManager = accountManagerTXT.getAttribute("value");
			String StandardLOB = StandardLOBTXT.getAttribute("value");

			if (selectedAccount.equals("CBIZ") && selectedDept.equals("Please Select") && nameInsured.isEmpty()
					&& lob.isEmpty() && policy.isEmpty() && effectiveDate.isEmpty() && dateFromClient.isEmpty()
					&& ClientDueDate.isEmpty() && internalDueDateValue.isEmpty() && accountManager.isEmpty()
					&& StandardLOB.isEmpty() && selectedPolicy.equals("--- Please Select ---")) {

				webActionUtils.pass(
						"Verification of Clearance of Entered Package details except Account when User clicks on Clear button : Passed");
			} else {
				webActionUtils.fail(
						"Verification of Clearance of Entered Package details except Account when User clicks on Clear button : Failed");
			}
		} catch (Exception e) {

			webActionUtils.info(e.getMessage());
		}
	}

	public void verifyFormatNamedInsuredfunctionality(String text1) {
		try {

			webActionUtils.info("Verification of Format Named Insured functionality");
			webActionUtils.info("Enter the required name insured text");
			webActionUtils.setText(nameInsuredTXT, text1);
			webActionUtils.clickElement(formatNameInsured, "Format Name Insured icon");
			webActionUtils.waitSleep(5);
			webActionUtils.waitUntilLoadedAndElementClickable(formatNameInsured);
			String text2 = nameInsuredTXT.getAttribute("value");
			text1 = convertUppercaseEachWord(text1);

			System.out.println("text1 :" + text1);
			System.out.println("text2 :" + text2);

			webActionUtils.verifyText(text1, text2);
			webActionUtils.pass("TC Passed");
		} catch (Exception e) {
			webActionUtils.fail("TC Failed");
		}
	}

	public void verifyformSectionisDisplayed() {
		webActionUtils.info("verify form Section isDisplayed in Upload file page");
		webActionUtils.scrollBy(FormsectionTable);
		webActionUtils.isElementDisplayed(FormsectionTable);
		webActionUtils.pass("TC Passed  Form Section is displayed");

	}

	public void verifyingToast(String expectedText) {
		webActionUtils.info("Verifing Toast/Alert message:");
		try {
			webActionUtils.isElementDisplayed1(alertMsg, 40);
			webActionUtils.waitUntilLoadedAndVisibilityOfElementLocated(alertMsg);
			webActionUtils.verifyElementIsPresent(alertMsg);
			webActionUtils.verifyElementContains(alertMsg, expectedText);
		} catch (Exception e) {
			Assert.fail();
		}

	}

	public void verifyInternalPopUpToastMessage(String internalPopUpText) {
		{
			webActionUtils.info("Verification of Internal pop up");
			webActionUtils.waitSleep(6);
			webActionUtils.waitUntilLoadedAndTextToBePresentInElementLocated(internalPopUpTXT, internalPopUpText);
			String actualText = internalPopUpTXT.getText().toString();
			try {
				if (actualText.equalsIgnoreCase(internalPopUpText)) {
					webActionUtils.info("Actual text : " + actualText);
					webActionUtils.info("Expected text : " + internalPopUpText);
				}
				webActionUtils.pass("Internal pop up verification: Passed");
			} catch (AssertionError e) {
				webActionUtils.fail("Internal pop up verification: FAILED " + e.getMessage());
			}
		}
	}

	public void VerifyLOBValue(String lobValue) {
		webActionUtils.info("Verifying LOB is auto populating or not");
		webActionUtils.waitSleep(8);
		webActionUtils.setText(lobTXT, lobValue);
		webActionUtils.info("Auto populated LOB for the entered data :" + lobValue);
		webActionUtils.waitSleep(8);
	}


	

	public void verifyOnClientDueDateandInternalDueDatelogic() {
		webActionUtils.info("verifyOnClientDueDateandInternalDueDatelogic");
		webActionUtils.clickElement(clientDueDateTXT, "clientDueDateTXT");
		String StrDate1 = clientDueDateTXT.getAttribute("value");
		webActionUtils.clickElement(internalDueDate, "internalDueDate");
		String StrDate2 = clientDueDateTXT.getAttribute("value");
		System.out.println("client DueDate date is " + StrDate1);
		System.out.println("internal DueDate date is " + StrDate2);
		String StrDate3 = webActionUtils.getTodaysDate("MM/dd/yyyy");

		try {
			if ((StrDate1.equals(StrDate2)) && StrDate1.equals(StrDate3)) {
				webActionUtils.pass("Rush Checkbox is Checked and Today data is selected : TC passed");

			}
		} catch (Exception e) {
			webActionUtils.pass("Rush Checkbox is Checked and Today data is selected : TC failed");
		}

		webActionUtils.pass("Rush Checkbox is Checked");
	}

	public void verifyPolicyInsightsExpressCheckRadiobutton() {
		try {
			webActionUtils.info("Verfiy Policy Insights - ExpressCheck Radio button present in Upload file page");
			webActionUtils.verifyElementIsPresent(piLiteRadioBTN);
			webActionUtils.pass("Policy Insights - ExpressCheck Radio button present");
		} catch (Exception e) {
			webActionUtils.fail("Policy Insights - ExpressCheck Radio button present in Upload file page is not displayed");
		}

	}

	
	public void verifyPolicyInsightsAdvanceCheckRadiobutton() {
		try {
			webActionUtils.info("Verfiy  Policy Insights - AdvanceCheck Radio button present in Upload file page");
			webActionUtils.verifyElementIsPresent(piRadioBTN);
			webActionUtils.pass("Policy Insights - AdvanceCheck Radio button present");
		} catch (Exception e) {
			webActionUtils.fail("Policy Insights - AdvanceCheck Radio button present in Upload file page is not displayed");
		}

	}


	public void verifyPremiumRangeInputs(String premiumRangeText) {
		try {
			webActionUtils.info("Enter values to Premium Range textfield");
			webActionUtils.setText(PremiumRangeTXT, premiumRangeText);
			webActionUtils.info("Entered Premium Range Text : " + premiumRangeText);

			String beforePreimumRangeText = PremiumRangeTXT.getAttribute("value");
			webActionUtils.info("Premium Range Text from UI to be Verified : " + beforePreimumRangeText);

			String afterPremiumRangeText = premiumRangeText.replaceAll("[^0-9$.-]", "");
			webActionUtils.info("Premium Range Text to be Compared to : " + afterPremiumRangeText);
			if (beforePreimumRangeText.equals(afterPremiumRangeText)) {
				webActionUtils.pass(
						"Verification of Premium Range Textfield which accepts Combination of given special Characters and Numbers : Passed");
			} else {
				webActionUtils.fail(
						"Verification of Premium Range Textfield which accepts Combination of given special Characters and Numbers : Failed");
			}
		} catch (Exception e) {
			Reporter.log(e.getMessage(), true);
			webActionUtils.fail("" + e.getMessage());
		}
	}

	/**
	 * @author Chaithra
	 * @param renewalPopUpText
	 */
	public void verifyRenewalPopUpToastMessage(String renewalPopUpText) {
		webActionUtils.info("Verification of Renewal pop up");
		webActionUtils.waitSleep(2);
		String actualText = popUpText.getText().toString();
		try {
			if (actualText.equalsIgnoreCase(renewalPopUpText)) {
				webActionUtils.info("Actual text : " + actualText);
				webActionUtils.info("Expected text : " + renewalPopUpText);
			}
			webActionUtils.pass("Renewal pop up verification: Passed");
		} catch (AssertionError e) {
			webActionUtils.fail("Renewal pop up verification: FAILED " + e.getMessage());
		}
	}

	public void verifyRushCheckBoxPosition() {
		webActionUtils.info("Verify Rush CheckBox Position");
		webActionUtils.verifyElementIsPresent(rushCheckBox);
		webActionUtils.pass("Rush Check is next to Upload Files button");
	}

	public void verifyRushFixedWordingsDrodownPresent() {
		webActionUtils.info("Verify Rush Fixed wordings Dropdown is displayed in the upload file Page");
		webActionUtils.verifyElementIsPresent(RushFixWordingBox);
		webActionUtils.pass(" Rush Fix Wording dropdown is present : TC Passed");
	}

	public void verifyRushTextAreaBox() {
		webActionUtils.info("Verify Rush CheckBox Textbox is displayed");
		webActionUtils.verifyElementIsPresent(rushTextAreaBox);
		webActionUtils.pass("Rush Check is present next rush checkbox");

	}

	public void verifyRushTextAreaDropdownvalue(String str2) {
		webActionUtils.info("select Rush Fixed Wordings from its dropdown");
		webActionUtils.clickElement(rushTextAreaBox, "rush Text Area Box");
		String val = rushTextAreaBox.getAttribute("value");

		if (val.toString().equals(str2)) {
			webActionUtils.pass("Selected RushFixWordingBoxDropdown is verified " + val + "TC passed");
		} else {
			webActionUtils.fail("Selected RushFixWordingBoxDropdown is not verified " + val + "TC Fail");
		}
	}

	/**
	 * Chaithra
	 */
	public void verifySectionTable() {
		try {
			webActionUtils.info("Verification of Form Section Table");
			if (formSectionCheckBox.isSelected()) {
				webActionUtils.info("Selected Form Section Checkbox is : Policy ");
			} else {
				webActionUtils.info("Form Section Checkbox is not Selected ");
			}
		} catch (AssertionError e) {
			webActionUtils.info("Form Section Table is not displayed");
		}
	}

	public void verifySuccessMessage(String ExpectedText) {
		webActionUtils.info("Verifing Toast/Alert message:");

		try {
			if (alertMsg.isDisplayed()) {
				webActionUtils.info("Success message is displayed and Package is created");
			} else {
			}
		} catch (Exception e) {
			webActionUtils.info("Success message is not displayed and Package is not created");
		}

	}

	public void verifyUploadFile(String expectedTitle) {
	
		try {
			webActionUtils.info("Verifying Upload File Page Title");
			Boolean isTitleCorrect = webActionUtils.waitAndGetTitle(expectedTitle);
			if (isTitleCorrect) {
				String actualTitle = webActionUtils.getdriver().getTitle();
				webActionUtils.pass("Title is Verified");
				System.out.println("Title is correct: " +actualTitle);
				Assert.assertEquals(actualTitle, expectedTitle);

			} else {
				System.out.println("Title did not match the expected value.");
			}
		} catch (Exception e) {
			webActionUtils.info("Title is not Verified");
			 e.printStackTrace();
			Assert.fail();
		}
		

	}

	public void verifyOfficeLocationCleared() {
		webActionUtils.info("Verifying Office Location cleared");
		webActionUtils.waitSleep(2);
		Select select = new Select(selectDepartmentDPDN);
		if (select.getFirstSelectedOption().getText().equalsIgnoreCase("Please Select")) {
			webActionUtils.pass("Selected Department Name : Please Select");
		} else {
			webActionUtils.fail("Step Failed");
		}

	}

	public void verifyCheckingTypeCleared() {
		webActionUtils.info("Verifying Checking Type cleared");
		webActionUtils.waitSleep(2);
		Select select = new Select(selectDepartmentDPDN);
		if (select.getFirstSelectedOption().getText().equalsIgnoreCase("Please Select")) {
			webActionUtils.pass("Selected Department Name : Please Select");
		} else {
			webActionUtils.fail("Step Failed");
		}

	}

	public void verifySegmentationCleared() {
		webActionUtils.info("Verifying Segmentation cleared");
		webActionUtils.waitSleep(2);
		Select select = new Select(selectDepartmentDPDN);
		if (select.getFirstSelectedOption().getText().equalsIgnoreCase("Please Select")) {
			webActionUtils.pass("Selected Department Name : Please Select");
		} else {
			webActionUtils.fail("Step Failed");
		}

	}

	public void verifyDepartmentFieldCleared() {
		webActionUtils.info("Verifying department cleared");
		webActionUtils.waitSleep(4);
		Select select = new Select(selectDepartmentDPDN);
		if (select.getFirstSelectedOption().getText().equalsIgnoreCase("Please Select")) {
			webActionUtils.pass("Selected Department Name : Please Select");
		} else {
			webActionUtils.fail("Step Failed");
		}

	}

	public void selectRequiredCode(String code) {
		try {
			webActionUtils.info("select Required Code");
			webActionUtils.clickElement(DepartmentOfficeCode, "DepartmentOfficeCode");
			webActionUtils.setText(DepartmentOfficeCode, code);
			webActionUtils.doubleClick(DepartmentOfficeCodeLabel);

		} catch (Exception e) {
			webActionUtils.fail(" TC failed");
		}
	}

	public void enterFieldvaluesFOrPILiteAndCarrierfieldmandatory(String accountName, String departmentName,
			String lobValue, String policyNumber, String dateformat, String accountManager, String policyStatusOpn,
			String uploadfilepath1, String uploadfilepath2, String expectedText) {

		try {
			webActionUtils.info("Selecting Account field");
			webActionUtils.waitUntilLoadedAndVisibilityOfElementLocated(selectAccountDPDN);
			webActionUtils.selectByVisibleText(selectAccountDPDN, accountName);
			webActionUtils.info("Selected Account Name : " + accountName);

			webActionUtils.info("Selecting Department / Program DropDown value");
			webActionUtils.selectByVisibleText(selectDepartmentDPDN, departmentName);
			webActionUtils.info("Selected Department Name : " + departmentName + "");

			webActionUtils.info("Entering Named Insured ");
			webActionUtils.setText(nameInsuredTXT, "2024 " + RandomStringUtils.randomAlphanumeric(5));
			webActionUtils.waitSleep(5);

			webActionUtils.info("Entering LOB");
			webActionUtils.setText(lobTXT, lobValue);

			webActionUtils.info("Entering Policy#");
			webActionUtils.setText(policyNoTXT, policyNumber + RandomStringUtils.randomAlphanumeric(10));

			webActionUtils.info("Entering Effective Date");
			webActionUtils.clickElement(effectiveDateTXT, "Effective Date");
			webActionUtils.clickElement(effDate, "Effective Date");

			webActionUtils.info("Entering Date From Client");
			webActionUtils.clickElement(dateFromClientTXT, "date From Client TXT");
			webActionUtils.waitUntilLoadedAndElementClickable(highlightedDate);
			webActionUtils.verifyElementIsPresent(highlightedDate);
			webActionUtils.clickElement(highlightedDate, "high lighted Date");

			webActionUtils.info("Entering account Manager Name ");
			webActionUtils.clickElement(accountManagerTXT, accountManager);
			webActionUtils.setText(accountManagerTXT, accountManager);
			webActionUtils.clickElement(accountManagerOpn, "account Manager Opn");

			if (chkIsEnabled.isSelected()) {
				chkIsEnabled.click();
			} else {

			}

			webActionUtils.info("Selecting policy Status ");
			webActionUtils.selectByVisibleText(policyStatusDPDN, policyStatusOpn);
			webActionUtils.info("uploading a file ");
			webActionUtils.waitInVisibilityOfElementLocated(uploadfilesBTN);
			webActionUtils.setText(uploadfilesBTN, uploadfilepath1 + "\n " + uploadfilepath2);
			webActionUtils.waitSleep(10);
			
			webActionUtils.info("Submitting the Policy ");
			webActionUtils.clickElement(submitBTN, "Submit  Button");
			webActionUtils.waitSleep(1);

			webActionUtils.info("verifying Carrier Filed mandetory Error message ");
			System.out.println(alertMsg.getText());
			webActionUtils.verifyElementText(alertMsg, expectedText);

		} catch (Exception e) {

			e.printStackTrace();
		}
	}
	
	public String enterFieldvaluesRush(String accountName, String departmentName, String lobValue, String policyNumber,
			String dateformat, String accountManager, String policyStatusOpn, String uploadfilepath1,
			String expectedText,String Rush) {

		String nameInsured = "";

		try {

			webActionUtils.info("Selecting Account field");
			webActionUtils.selectByVisibleText(selectAccountDPDN, accountName);
			webActionUtils.info("Selected Account Name : " + accountName);

			webActionUtils.info("Selecting Department / Program DropDown value");
			webActionUtils.selectByVisibleText(selectDepartmentDPDN, departmentName);
			webActionUtils.info("Selected Department Name : " + departmentName + "");

			webActionUtils.info("Entering Named Insured ");
			nameInsured = "2024 TEST_00_ " + RandomStringUtils.randomAlphanumeric(5);
			webActionUtils.setText(nameInsuredTXT, nameInsured);

			webActionUtils.info("Entering LOB");
			webActionUtils.setText(lobTXT, lobValue);
			// webActionUtils.waitSleep(5);

			webActionUtils.info("Entering Policy#");
			webActionUtils.setText(policyNoTXT, policyNumber + RandomStringUtils.randomAlphanumeric(5));
			// webActionUtils.waitSleep(5);

			webActionUtils.info("Entering Effective Date");
			// webActionUtils.clickElement(effectiveDateTXT, "Effective Date");
			// webActionUtils.clickElement(effDate, "Effective Date");
			effectiveDateTXT.sendKeys(dateformat);

			webActionUtils.info("Entering Date From Client");
			webActionUtils.clickElement(dateFromClientTXT, "date From Client TXT");
			webActionUtils.waitUntilLoadedAndElementClickable(highlightedDate);
			webActionUtils.verifyElementIsPresent(highlightedDate);
			webActionUtils.clickElement(highlightedDate, "high lighted Date");
			// webActionUtils.waitSleep(5);

			webActionUtils.info("Entering account Manager Name ");
			// webActionUtils.waitSleep(5);
			webActionUtils.clickElement(accountManagerTXT, accountManager);
			// webActionUtils.waitSleep(5);
			webActionUtils.setText(accountManagerTXT, accountManager);
			// webActionUtils.waitSleep(5);
			webActionUtils.clickElement(accountManagerOpn, "account Manager Opn");
			if (chkIsEnabled.isSelected()) {
				chkIsEnabled.click();
			}

			webActionUtils.info("Selecting policy Status ");
			webActionUtils.selectByVisibleText(policyStatusDPDN, policyStatusOpn);
			// webActionUtils.waitSleep(5);

			webActionUtils.info("Uploading a file ");
			webActionUtils.setText(uploadfilesBTN, uploadfilepath1);
			webActionUtils.waitSleep(5);

			webActionUtils.info("Clicking On Rush CheckBox button");
			webActionUtils.clickElement(rushCheckBox, "rushCheckBox");
			webActionUtils.pass("Rush Checkbox is Checked");
			
			webActionUtils.info("select Rush Fixed Wordings from its dropdown");
				webActionUtils.info("Selecting Rush Fixed Wordings dropdown field" + Rush);
				webActionUtils.selectByVisibleText(RushFixWordingBoxDropdown, Rush);
				webActionUtils.pass("Selected RushFixWordingBoxDropdown Name : " + Rush);
				webActionUtils.waitSleep(3);
				
				
			webActionUtils.info("Submitting the Policy ");
			webActionUtils.invisibilityOfElementLocated(uploadingTxt);
			webActionUtils.scrolltoEnd();
			webActionUtils.waitSleep(10);
			webActionUtils.clickElement(submitBTN, "Submit  Button");
			// webActionUtils.waitSleep(10);
			clickConfirmYesAndCheckToast(expectedText);

		} catch (Exception e) {
			Reporter.log(e.getMessage(), true);
			webActionUtils.fail("Unable to submit the file - pls check" + e.getMessage());
			webActionUtils.fail(e.getMessage());
			e.printStackTrace();
		}
		return nameInsured;
	}

	
	public void enterRequiredAccount(String accountName) {
		try {
			webActionUtils.info("Selecting Account field");
			webActionUtils.selectByVisibleText(selectAccountDPDN, accountName);
			webActionUtils.info("Selected Account Name : " + accountName);

		} catch (Exception e) {
			Reporter.log(e.getMessage(), true);
			webActionUtils.fail(e.getMessage());
			e.printStackTrace();
		}
	}

	public void enterRequiredDepartment(String departmentName) {
		try {
			webActionUtils.waitSleep(4);
			webActionUtils.info("Selecting Department / Program DropDown value");
			webActionUtils.selectByVisibleText(selectDepartmentDPDN, departmentName);
			webActionUtils.info("Selected Department Name : " + departmentName + "");
		} catch (Exception e) {
			Reporter.log(e.getMessage(), true);
			webActionUtils.fail(e.getMessage());
			e.printStackTrace();
		}
	}

	public String enterRequiredNamedInsured1(String nameInsured) {
		
			String str =nameInsured + RandomStringUtils.randomAlphanumeric(5);
			webActionUtils.info("Entering Named Insured ");
			webActionUtils.setText(nameInsuredTXT, str );
			
			return str;

	}
	
	public void enterRequiredNamedInsured(String nameInsured) {
		try {
			webActionUtils.info("Entering Named Insured ");
			webActionUtils.setText(nameInsuredTXT, nameInsured + RandomStringUtils.randomAlphanumeric(5) );

		} catch (Exception e) {
			Reporter.log(e.getMessage(), true);
			webActionUtils.fail(e.getMessage());
			e.printStackTrace();
		}

	}
	
	public void enterRequiredLOB(String LOB) {
		try {
			webActionUtils.info("Entering LOB");
			webActionUtils.setText(lobTXT, LOB);

		} catch (Exception e) {
			Reporter.log(e.getMessage(), true);
			webActionUtils.fail(e.getMessage());
			e.printStackTrace();
		}

	}
	
	public void enterRequiredPolicyNumber(String policyNumber) {
		try {
			webActionUtils.info("Entering Policy#");
			webActionUtils.setText(policyNoTXT, policyNumber + RandomStringUtils.randomAlphanumeric(5));
		} catch (Exception e) {
			Reporter.log(e.getMessage(), true);
			webActionUtils.fail(e.getMessage());
			e.printStackTrace();
		}

	}
	
	public void enterRequiredEffectiveDate(String EffectiveDate) {
		try {
			webActionUtils.info("Entering Effective Date");
			effectiveDateTXT.sendKeys(EffectiveDate);
		} catch (Exception e) {
			Reporter.log(e.getMessage(), true);
			webActionUtils.fail(e.getMessage());
			e.printStackTrace();
		}

	}
	
	public void enterHighlightedDateFromClient() {
		try {
			webActionUtils.info("Entering Date From Client");
			webActionUtils.clickElement(dateFromClientTXT, "date From Client TXT");
			webActionUtils.waitUntilLoadedAndElementClickable(highlightedDate);
			webActionUtils.verifyElementIsPresent(highlightedDate);
			webActionUtils.clickElement(highlightedDate, "high lighted Date");
		} catch (Exception e) {
			Reporter.log(e.getMessage(), true);
			webActionUtils.fail(e.getMessage());
			e.printStackTrace();
		}
	}
	

//	
//	public void enterRequiredClientDueDate(String ClientDueDate) {
//		try {
//			webActionUtils.info("Entering Client Due Date");
//
//		} catch (Exception e) {
//			Reporter.log(e.getMessage(), true);
//			webActionUtils.fail(e.getMessage());
//			e.printStackTrace();
//		}
//
//	}
//	
//	
//	public void enterRequiredInternalDueDate(String InternalDueDate) {
//		try {
//			webActionUtils.info("Entering Internal Due Date");
//		} catch (Exception e) {
//			Reporter.log(e.getMessage(), true);
//			webActionUtils.fail(e.getMessage());
//			e.printStackTrace();
//		}
//
//	}
	
	public void enterRequiredAccountManager(String accountManager) {
		try {
			webActionUtils.info("Entering Account Manager");
			webActionUtils.clickElement(accountManagerTXT, accountManager);
			webActionUtils.waitSleep(3);
			webActionUtils.setText(accountManagerTXT, accountManager);
			webActionUtils.clickElement(accountManagerOpn, "account Manager Opn");
		} catch (Exception e) {
			Reporter.log(e.getMessage(), true);
			webActionUtils.fail(e.getMessage());
			e.printStackTrace();
		}

	}
	
	public Boolean verifyAAmCheckboxischeckedbyDeafultOrNot() {

		webActionUtils.info("Checking  Assitant Account Manager Checkbox is checked ");

		// variable = (condition) ? expressionTrue : expressionFalse;
		boolean bool = chkIsEnabled.isSelected() ?  true : false;
		
		if (bool) {
			webActionUtils.info(" Assitant Account Manager Checkbox is checked ");
		} else {
			webActionUtils.info(" Assitant Account Manager Checkbox is not checked ");
		}
		
		return bool;
	}
	
	
	public void checkNewAssitantAccountManagerCheckboxandClearDeafults(boolean bool) {

		webActionUtils.info("Check 'Assign Task to Asst.AM' Checkbox and clear defaults");
		try {

			if (bool) {
				webActionUtils.clickElement(chkIsEnabled, "Assign Task to Asst.AM");
				Thread.sleep(1000);
				assistantManager.click();
			    webActionUtils.clearText(assistantManager);
			    webActionUtils.info("'Assign Task to Asst.AM' Checkbox0 defaults cleared");

			} else {
				webActionUtils.clickElement(chkIsEnabled, "Assign Task to Asst.AM");
				Thread.sleep(1000);
				assistantManager.click();
				webActionUtils.clearText(assistantManager);
				webActionUtils.info("'Assign Task to Asst.AM' Checkbox1 defaults cleared");

			}

		} catch (Exception e) {
			Reporter.log(e.getMessage(), true);
			webActionUtils.fail(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void enterRequiredAssitantAccountManager(String assitantAccountManager) {
		try {
			webActionUtils.info("Enter Assitant Account Manager Name ");
			Thread.sleep(2000);
			
			webActionUtils.setText(assistantManager, assitantAccountManager);
			Thread.sleep(4000);
			assistantManager.click();
			assistantManager.sendKeys(Keys.ARROW_DOWN) ;
			Thread.sleep(2000);
			assistantManager.sendKeys(Keys.ENTER) ;
			Thread.sleep(2000);

			
//			try {
//				webActionUtils.clickElement(assistantAccountManagerOpn, " Assitant Account Manager Option");
//
//			} catch (Exception e1) {
//				webActionUtils.clickElement(assistantAccountManagerOpn1, " Assitant Account Manager Option");
//			}

		} catch (Exception e2) {
			Reporter.log(e2.getMessage(), true);
			webActionUtils.fail(e2.getMessage());
			e2.printStackTrace();
		}

	}
	
	/**
	 * @author Varshitha_Yadav
	 */
	public void verifyNewAccountManagerFieldIsDisplayed() {
		webActionUtils.info(
				"Verification of new Account Manager and Account Manager Email textfields are displayed for 'Other' option");
		try {
			if (webActionUtils.isElementDisplayed(newAccountManager)
					|| webActionUtils.isElementDisplayed(accountManagerEmail)) {
				webActionUtils.pass("New Account Manager text field is Displayed: Passed");
			} else {
				webActionUtils.fail("New Account Manager text field is Not Displayed : Failed");
			}
		} catch (Exception e) {
			Reporter.log(e.getMessage(), true);
			webActionUtils.fail("" + e.getMessage());
		}
	}
	
	
	


	
	
	


	
	

	
	
//	
//	/**
//	 * @author Varshitha_Yadav
//	 */
//	public void verifyNewAssitantAccountManagerFieldIsDisplayed() {
//
//		webActionUtils.info("Check 'Assign Task to Asst.AM' Checkbox");
//
//		if (chkIsEnabled.isSelected()) {
//			webActionUtils.info("Assign Task to Asst.AM Checkbox is selected ");
//			webActionUtils.info("Enter 'Other' to   text field ");
//			webActionUtils.waitSleep(2);
//			webActionUtils.setText(assistantManager, "Other");
//			webActionUtils.clickElement(assistantAccountManagerOpn, "Other option selected");
//
//		} else {
//			webActionUtils.info("Assign AM checkbox is not Selected");
//		}
//
//		webActionUtils.info(
//				"Verification of new Assitant Account Manager and Assitant Account Manager Email textfield are displayed for 'Other' Option ");
//		try {
//			if (webActionUtils.isElementDisplayed(newAssistantAccountManager)
//					|| webActionUtils.isElementDisplayed(assistantAccountManagerEmail)) {
//				webActionUtils.pass("New Assistant Account Manager text field is Displayed : Passed");
//			} else {
//				webActionUtils.fail("New Assistant Account Manager text field is Not Displayed : Failed");
//			}
//		} catch (Exception e) {
//			Reporter.log(e.getMessage(), true);
//			webActionUtils.fail("" + e.getMessage());
//		}
//	}
//	

//	
	

	

	
	
	public void enterRequiredAdditionalField(String Addtional) {
		try {
			webActionUtils.info("Entering Additional Field");
			webActionUtils.setText(AdditionalFieldTxt, "Add"+RandomStringUtils.randomAlphanumeric(5));
		} catch (Exception e) {
			Reporter.log(e.getMessage(), true);
			webActionUtils.fail(e.getMessage());
			e.printStackTrace();
		}

	}
	
	public void enterRequiredAssignTasKtoAsstAM(String assistantAccountMaAAmnagerOpn) {
		try {
		
		} catch (Exception e) {
			Reporter.log(e.getMessage(), true);
			webActionUtils.fail(e.getMessage());
			e.printStackTrace();
		}

	}
	
	public void enterRequiredPolicyStatus(String policyStatusOpn) {
		try {
			webActionUtils.info("Selecting policy Status ");
			webActionUtils.selectByVisibleText(policyStatusDPDN, policyStatusOpn);
			
		} catch (Exception e) {
			Reporter.log(e.getMessage(), true);
			webActionUtils.fail(e.getMessage());
			e.printStackTrace();
		}

	}
	
	public void enterRequiredPremiumRange(String PremiumRange) {
		try {
			webActionUtils.info("Selecting policy Status ");
			webActionUtils.setText(PremiumRangeTXT, "Premium"+RandomStringUtils.randomAlphanumeric(5));

		} catch (Exception e) {
			Reporter.log(e.getMessage(), true);
			webActionUtils.fail(e.getMessage());
			e.printStackTrace();
		}

	}

	public void uploadRequiredFiles(String uploadfilepath) {
		try {
			webActionUtils.info("Uploading a file ");
			webActionUtils.setText(uploadfilesBTN, uploadfilepath);
			
			webActionUtils.waitSleep1(5);
			
		} catch (Exception e) {
			Reporter.log(e.getMessage(), true);
			webActionUtils.fail(e.getMessage());
			e.printStackTrace();
		}		
	}

	public void clickonSubmitButton() {
		try {
			webActionUtils.info("Click on the Submit Button ");
			webActionUtils.waitSleep(5);
			webActionUtils.clickElement(submitBTN, "submit Button");
			webActionUtils.waitSleep(5);
			clickConfirmYesAndCheckToast("Message: You have successfully submitted a task!");
			
		} catch (Exception e) {
			Reporter.log(e.getMessage(), true);
			webActionUtils.fail(e.getMessage());
			e.printStackTrace();
		}				
	}
	
	public void ClickonSubmitButton() {
		try {
			webActionUtils.info("Click on the Submit Button ");
			webActionUtils.clickElement(submitBTN, "submit Button");
			webActionUtils.waitSleep(5);
					
		} catch (Exception e) {
			Reporter.log(e.getMessage(), true);
			webActionUtils.fail(e.getMessage());
			e.printStackTrace();
		}				
	}
	
	public void ClickonSubmitButton1() {
		try {
			webActionUtils.info("Click on the Submit Button ");
			webActionUtils.clickElement(submitBTN, "submit Button");
			webActionUtils.waitSleep(5);
					
		} catch (Exception e) {
			Reporter.log(e.getMessage(), true);
			webActionUtils.fail(e.getMessage());
			e.printStackTrace();
		}				
	}
	
	
	public void getMessageandVerify(String Str  ) {
		try {
			webActionUtils.info("Get Message and Verify");
			String text = alertMsg.getText().toString();
			webActionUtils.verifyText(Str, text);
		Assert.assertEquals(text, Str);
			
		} catch (Exception e) {
			Reporter.log(e.getMessage(), true);
			webActionUtils.fail(e.getMessage());
			e.printStackTrace();
		}				
	}
	
	
	public void getMessageContainsVerify(String Str  ) {
		try {
			webActionUtils.info("Get Message with serail and Verify");
			webActionUtils.verifyElementContains(alertMsg, Str);
			webActionUtils.waitSleep1(4);
			
				} catch (Exception e) {
			Reporter.log(e.getMessage(), true);
			webActionUtils.fail(e.getMessage());
			e.printStackTrace();
		}				
	}
	
	
	public void enterRequiredCarrier(String carrier  ) {
		try {
			webActionUtils.info("Enter the Carrier Dropdown Options");
			webActionUtils.setText(carrierTxtBx, carrier);
			webActionUtils.waitSleep(2);
			webActionUtils.jsclickElement(carrierOpn, "carrier");
//			carrierOpn.click();

		} catch (Exception e) {
			webActionUtils.fail("Failed to Click on the Carrier textbox present in Upload file Page");
		}			
	}

	public void enterNewAccountManagerName(String newAccountManager1){
		try {
		webActionUtils.info("Enter the Mew Account Manager");
		webActionUtils.setText(newAccountManager, newAccountManager1);

	} catch (Exception e) {
		webActionUtils.fail("Failed to enter 'Mew Account Manager in Upload file Page");
		Reporter.log(e.getMessage(), true);
		webActionUtils.fail("" + e.getMessage());
	}			
}
	
	
	public void enterNewAccountManagerEmail(String newAccountManagerEmail){
		try {
		webActionUtils.info("Enter the Mew Account Manager email");
		webActionUtils.setText(accountManagerEmail, newAccountManagerEmail);

	} catch (Exception e) {
		webActionUtils.fail("Failed to enter 'Mew Account Manager email in Upload file Page");
		Reporter.log(e.getMessage(), true);
		webActionUtils.fail("" + e.getMessage());
	}			
}
	
	
	public void verifyNewAssitantAccountManagerFieldIsDisplayed() {
		webActionUtils.info("Verification of new Assitant Account Manager and Assitant Account Manager Email textfield are displayed for 'Other' Option ");

			webActionUtils.info("Enter 'Other' to   text field ");
			webActionUtils.setText(assistantManager, "Other");
			webActionUtils.clickElement(assistantAccountManagerOpn, "Other option selected");
			webActionUtils.info("Assign AM checkbox is not Selected");
		try {
			if (webActionUtils.isElementDisplayed(newAssistantAccountManager)
					|| webActionUtils.isElementDisplayed(assistantAccountManagerEmail)) {
				webActionUtils.pass("New Assistant Account Manager text field is Displayed : Passed");
			} else {
				webActionUtils.fail("New Assistant Account Manager text field is Not Displayed : Failed");
			}
		} catch (Exception e) {
			Reporter.log(e.getMessage(), true);
			webActionUtils.fail("" + e.getMessage());
		}
	}
	
	public void checkNewAssitantAccountManagerCheckbox(boolean bool) {
		webActionUtils.info("Check 'Assign Task to Asst.AM' Checkbox");
		try {

			if (chkIsEnabled.isSelected()) {
				// webActionUtils.clickElement(chkIsEnabled, "Assign Task to Asst.AM");
				chkIsEnabled.click();
			} else {
				// webActionUtils.clickElement(chkIsEnabled, "Assign Task to Asst.AM");
				chkIsEnabled.click();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void checkNewAssitantAccountManagerCheckbox() {
		webActionUtils.info("Check 'Assign Task to Asst.AM' Checkbox");
		try {
			Thread.sleep(2000);
			if (!chkIsEnabled.isSelected()) {
				chkIsEnabled.click();
			}else {
			}
	
		} catch (InterruptedException e) {
			e.printStackTrace();
		}	
	}
	
	public void enterNewAssistentAccountManagerName(String newAssistentAccountManager) {
		try {
			webActionUtils.info("Enter the Mew Assistent Account Manager");
			webActionUtils.waitUntilLoadedAndPresenceOfElementLocated(newAssistantAccountManager);
			webActionUtils.setText(newAssistantAccountManager, newAssistentAccountManager);
			
		} catch (Exception e) {
			
			
			
			
			webActionUtils.fail("Failed to enter 'Mew Assistent Account Manager in Upload file Page");
			Reporter.log(e.getMessage(), true);
			webActionUtils.fail("" + e.getMessage()); 
		}					
	}

	public void enterNewAssistentAccountManagerEmail(String newAssistentAccountManagerEmail) {
		try {
			webActionUtils.info("Enter the Mew Assistent Account Manager Eamil");
			webActionUtils.setText(assistantAccountManagerEmail, newAssistentAccountManagerEmail);

		} catch (Exception e) {
			webActionUtils.fail("Failed to enter 'Mew Assistent Account Manager in Upload file Page");
			Reporter.log(e.getMessage(), true);
			webActionUtils.fail("" + e.getMessage());
		}					
	}
	
	
	
}