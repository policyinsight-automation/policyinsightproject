package com.automation.pi.pages;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;

import com.automation.pi.library.BasePage;
import com.automation.pi.library.WebActionUtils;

public class ManagePage extends BasePage {

	@FindBy(xpath = "(//*[@id='navbarToggler']/ul/li[2]/a)")
	private WebElement managePageTab;

	@FindBy(xpath = "//input[@placeholder='Name Insured']")
	private WebElement nameInsuredTxtBx;

	@FindBy(xpath = "//input[@placeholder='Serial#']")
	private WebElement serialTxtBx;

	@FindBy(xpath = "//input[@placeholder='Policy#']")
	private WebElement policyTxtBx;

	@FindBy(xpath = "(//button[@type='button'])[12]/span")
	private WebElement statusFilter;

	@FindBy(xpath = "//span[@id='spanSelectedAccount' and text()='Account']")
	private WebElement accountFilterTxt;

	@FindBy(xpath = "//span[@id='spanSelectedAccount' and text()='Account']/../../..//input[@type='text']")
	private WebElement accountFilterSearch;
	//
	// @FindBy(xpath = "//span[@id='spanStatusID' and
	// text()='Account']/../../..//input[@type='text']")
	// private WebElement accountFilterSearch;

	// @FindBy(xpath = "//span[@id='spanStatusID' and text()='Account']")
	// private WebElement accountFilterTxt;

	@FindBy(id = "taskDescription")
	private WebElement taskDescriptionTxtBx;

	// @FindBy(xpath = "//span[@id='spanStatusID' and text()='PI Auditor']")
	// private WebElement PIAuditorFilterTxt;
	//
	// @FindBy(xpath = "//span[@id='spanStatusID' and text()='PI
	// Auditor']/../../..//input[@type='text']")
	// private WebElement PIAuditorFilterSearch;
	//
	// @FindBy(xpath = "//span[@id='spanStatusID' and text()='FBE Auditor']")
	// private WebElement FBEAuditorFilterTxt;
	//
	// @FindBy(xpath = "//span[@id='spanStatusID' and text()='FBE
	// Auditor']/../../..//input[@type='text']")
	// private WebElement FBEAuditorFilterSearch;
	//
	// @FindBy(xpath = "//span[@id='spanStatusID' and text()='PI Processor']")
	// private WebElement PIProcessorFilterTxt;
	//
	// @FindBy(xpath = "//span[@id='spanStatusID' and text()='PI
	// Processor']/../../..//input[@type='text']")
	// private WebElement PIProcessorFilterSearch;
	//

	@FindBy(xpath = "//span[@id='spanddlAuditor' and text()='PI Auditor']")
	private WebElement PIAuditorFilterTxt;

	@FindBy(xpath = "//span[@id='spanddlAuditor' and text()='PI Auditor']/../../..//input[@type='text']")
	private WebElement PIAuditorFilterSearch;

	@FindBy(xpath = "//span[@id='spanddlFBEAuditor' and text()='FBE Auditor']")
	private WebElement FBEAuditorFilterTxt;

	@FindBy(xpath = "//span[@id='spanddlFBEAuditor' and text()='FBE Auditor']/../../..//input[@type='text']")
	private WebElement FBEAuditorFilterSearch;

	@FindBy(xpath = "//span[@id='spanddlProcessor' and text()='PI Processor']")
	private WebElement PIProcessorFilterTxt;

	@FindBy(xpath = "//span[@id='spanddlProcessor' and text()='PI Processor']/../../..//input[@type='text']")
	private WebElement PIProcessorFilterSearch;

	@FindBy(xpath = "//span[text()='All Your Tasks']")
	private WebElement AllYourTasksFilterTxt;

	@FindBy(xpath = "//span[text()='All Selected']")
	private WebElement AllSelectedFilterTXt;

	@FindBys({
			@FindBy(xpath = "//span[@id='spanddlSelectOffice']/../..//div[@class='ms-drop bottom']//input[@type='checkbox']") })
	private List<WebElement> allOfficeCheckbox;

	@FindBys({ @FindBy(xpath = "//span[@id='spanddlSelectOffice']/../..//div[@class='ms-drop bottom']//label") })
	private List<WebElement> allOfficeNames;

	@FindBy(xpath = "//span[@id='spanddlOverView' and text()='All Your Tasks']/../../..//input[@type='text']")
	private WebElement AllYourTasksFilterSearch;

	// @FindBy(xpath = "//span[@id='spanStatusID' and text()='All Your
	// Tasks']/../../..//input[@type='text']")
	// private WebElement AllYourTasksFilterSearch;

	@FindBy(xpath = "//button[@id='btnAdvancedFilters']")
	private WebElement AdvancedFiltersBTN;

	@FindAll({ @FindBy(xpath = "//button[@id='btnAdvancedFilters']"),
			@FindBy(xpath = "//em[@id='advancedSearchIcon']") })
	private List<WebElement> AdvancedFiltersBTN1;

	@FindBy(xpath = "//button[@title='Clear Filters']")
	private WebElement ClearFiltersBTN;

	@FindBy(id = "btnSearch")
	private WebElement searchBTN;

	@FindBy(xpath = "//button[@id='btnExport']")
	private WebElement ExportBTN;

	@FindBy(xpath = "//button[@id='btnDnldOrigFiles']")
	private WebElement DownloadAllFilesIcon1;

	@FindBy(xpath = "//button[@onclick='DownloadAllFiles()']")
	private WebElement DownloadAllFilesBTN2;

	@FindBy(xpath = "//button[@id='btnUpload']")
	private WebElement UploadfileIcon;

	@FindBy(xpath = "//input[@type='file']")
	private WebElement uploadfilesBTN;

	@FindBy(xpath = "//button[@onclick='AdditionalFilesSave()']")
	private WebElement uploadSubmit;

	@FindBy(xpath = "//button[@id='btnReworkTask']")
	private WebElement ReworkTaskIcon;

	@FindBy(xpath = "//textarea[@id='txtReworkTask']")
	private WebElement ReworkTaskTextAreaBoX;

	@FindBy(xpath = "//button[@onclick='ReworkTaskSave()']")
	private WebElement ReworkTaskSubmit;

	@FindBy(xpath = "//input[@id='txtLOBFilter']")
	private WebElement LOBTxtBx;

	@FindBy(xpath = "//div[@class='ms-parent span_select_long']//span[@id='spanSelecteDepartments']")
	private WebElement DepartmentDrpDwn;

	// @FindBy(xpath = "//div[@class='ms-parent
	// span_select_long']//span[@id='spanStatusID']")
	// private WebElement DepartmentDrpDwn;
	//

	@FindBy(xpath = "(//li[@class='ms-no-results'])[9]")
	private WebElement NoMatchesFoundTxt;

	@FindBy(xpath = "(//button[@type='button'])[20]/following-sibling::div/div/input")
	private WebElement departmentSearch;

	@FindBy(xpath = "//input[@name='selectAllSelecteDepartments']")
	private WebElement departmentSelectAllCheckBox;

	@FindBy(xpath = "//div[@class='ms-parent']//span[@id='spanStandardLOBDDLs']")
	private WebElement StandardLOBDrpDwn;

	@FindBy(xpath = "//li/div[text()='Contracting Bond (CBIC)']")
	private WebElement ContractingBondDropDown;

	@FindBy(xpath = "//div[@class='ms-parent span_select_long']//input[@type='text']")
	private WebElement DepartmenFilterSearch;

	@FindBy(xpath = "//tbody//div[@class='ms-parent']//input[@type='text']")
	private WebElement StandredLobFilterSearch;

	@FindBy(xpath = "//input[@id='txtCarrierFilter' and @placeholder='Carrier']")
	private WebElement CarrierFilterSearch;

	@FindBy(xpath = "//button[@id='btnDownload']")
	private WebElement downloadChecklistTemplateBTN;

	@FindBy(id = "loading")
	private WebElement loadingIcon;

	@FindBy(xpath = "//input[@id='ChkSelectAllPCSP']")
	private WebElement recordChkBx;

	@FindBy(xpath = "//input[@id='ChkSelectAllPCSP']/../../../..//input[@name='chkbox']")
	private WebElement firstRecordChkBx;

	@FindBy(xpath = "//input[@name='chkbox' and @type='checkbox']")
	private WebElement firstRecordChkBx1;

	@FindBy(id = "btnEdit")
	private WebElement EditBTN;

	@FindBy(id = "btnAssignAuditor")
	private WebElement assignAuditorBTN;

	@FindBy(id = "btnAssign")
	private WebElement assignProcessorBTN;

	@FindBy(id = "auditorOptions")
	private WebElement auditorOptions;

	@FindBy(id = "processorOptions")
	private WebElement processorOptions;

	@FindBy(id = "processor2Options")
	private WebElement processor2Options;

	@FindBy(id = "FBEauditorOptions")
	private WebElement FBEauditorOptions;

	@FindBy(xpath = "//button[@class='btn btn-primary btn-small' and @onclick='AssignAuditorSave()']")
	private WebElement auditorsaveBTN;

	@FindBy(xpath = "//button[@class='btn btn-primary btn-small' and @onclick='AssignSave()']")
	private WebElement processorSaveBTN;

	@FindBy(xpath = "//input[@name='selectAllddlStatus']")
	private WebElement selectAllStatusFilter;

	@FindBy(xpath = "//label[contains(.,'Uploaded')]")
	private WebElement UploadedStatusFilter;

	@FindBy(xpath = "//label[contains(.,'Active')]")
	private WebElement ActiveStatusFilter;

	@FindBy(xpath = "//label[contains(.,'Suspended')]")
	private WebElement SuspendedStatusFilter;

	@FindBy(xpath = "//label[contains(.,'Finished')]")
	private WebElement FinishedStatusFilter;

	@FindBy(xpath = "//label[contains(.,'Merged')]")
	private WebElement MergedStatusFilter;

	@FindBy(xpath = "//label[contains(.,'Cancelled')]")
	private WebElement CancelledStatusFilter;

	@FindBy(xpath = "//label[contains(.,' Ready to Upload')]")
	private WebElement ReadytoUploadStatusFilter;

	@FindBy(xpath = "//label[contains(.,'Reviewed')]")
	private WebElement ReviewedStatusFilter;

	@FindBy(xpath = "//label[contains(.,'Ready for DT Checklist')]")
	private WebElement ReadyforDTChecklistStatusFilter;

	@FindBy(xpath = "//label[contains(.,'DT Finished')]")
	private WebElement DTFinishedStatusFilter;

	@FindBy(xpath = "//label[contains(.,'DT Suspended')]")
	private WebElement DTSuspendedselectedStatusFilter;

	@FindBy(xpath = "//label[contains(.,'API Created')]")
	private WebElement APICreatedStatusFilter;

	@FindBy(xpath = "//label[contains(.,'API Saved')]")
	private WebElement APISavedStatusFilter;

	@FindBy(xpath = "//label[contains(.,' API Reassigned')]")
	private WebElement APIReassignedStatusFilter;

	@FindBy(xpath = "//label[contains(.,'CBIZ')]/input")
	private WebElement selectCBIZFilterAccount;

	@FindBy(xpath = "//label[contains(.,'SCM')]/input")
	private WebElement selectSCMFilterAccount;

	@FindBy(xpath = "(//li//label[contains(.,'Need Audit')])[1]//input[@type='checkbox']")
	private WebElement selectNeedAuditFilter;

	@FindBy(xpath = "//input[@type='checkbox' and  @name='selectAllddlFBEAuditor']")
	private WebElement selectallFBEProcessor;

	@FindBy(xpath = "/html//div[@id='GridContainer']//div[@class='search-controller']/div[5]//ul//input[@name='selectAllddlProcessor']")
	private WebElement selectallPIProcessor;

	@FindBy(xpath = "//input[@name='selectAllddlOverView']")
	private WebElement selectallAllYourTasks;

	@FindBy(xpath = "//input[@name='selectAllSelecteDepartments']")
	private WebElement selectallAllDepartments;

	@FindBy(xpath = "//input[@name='selectAllStandardLOBDDLs']")
	private WebElement selectallStandardLOB;

	@FindBy(xpath = "//input[@name='selectAllSelectedAccount']")
	private WebElement selectallAccount;

	@FindBy(xpath = "//th[contains(.,'PI Auditor')]/../../..//td[15]")
	private WebElement piAuditorRecordTXT;

	@FindBy(xpath = "//th[contains(.,'PI Processor 1')]/../../..//td[11]")
	private WebElement piProcessor1RecordTXT;

	@FindBy(xpath = "//th[contains(.,'PI Processor 2')]/../../..//td[12]")
	private WebElement piProcessor2RecordTXT;

	@FindBy(xpath = "//th[contains(.,'FBE Auditor')]/../../..//td[16]")
	private WebElement piFBEAuditorRecordTXT;

	@FindBy(xpath = "(//h2[contains(.,'Transaction Details')])[2]")
	private WebElement editPackTxt;

	@FindBy(xpath = "//a[@id='packageID']")
	private WebElement serialTXT;

	@FindBy(xpath = "//td[@id='icon178446']/..//td[text()='Vicky_assistant_package1']")
	private WebElement nameInsuredTXT;

	@FindBy(xpath = "//td[text()='CTA_08_03 Vulcan Labs, LLC']")
	private WebElement nameInsuredTXT2;

	@FindBy(xpath = "//td[@id='icon178446']/..//td[text()=$text]")
	private WebElement nameInsuredTXT1;

	@FindBy(xpath = "//td[@id='icon194657']/..//td[text()='Policy Shobhan 1309']")
	private WebElement policyNumberTXT;

	@FindBy(xpath = "//td[text()='Uploaded']/..//td[3]")
	private List<WebElement> listOfUploadedRecords;

	@FindBy(xpath = "//td[text()='Active']/..//td[3]")
	private List<WebElement> listOfActiveRecords;

	@FindBy(xpath = "//td[text()='Suspended']/..//td[3]")
	private List<WebElement> listOfSuspendedRecords;

	@FindBy(xpath = "//td[text()='Finished']/..//td[3]")
	private List<WebElement> listOfFinishedRecords;

	@FindBy(xpath = "//td[text()='Merged']/..//td[3]")
	private List<WebElement> listOfMergedRecords;

	@FindBy(xpath = "//td[text()='Cancelled']/..//td[3]")
	private List<WebElement> listOfCancelledRecords;

	@FindBy(xpath = "//td[text()='Ready to Upload']/..//td[3]")
	private List<WebElement> listOfReadytoUploadRecords;

	@FindBy(xpath = "//td[text()='Reviewed']/..//td[3]")
	private List<WebElement> listOfReviewedRecords;

	@FindBy(xpath = "//td[text()='DT Finished']/..//td[3]")
	private List<WebElement> listOfDTFinishedRecords;

	@FindBy(xpath = "//td[text()='Ready for DT Checklist']/..//td[3]")
	private List<WebElement> listOfReadyforDTChecklistRecords;

	@FindBy(xpath = "//td[text()='DT Suspended']/..//td[3]")
	private List<WebElement> listOfDTSuspendedRecords;

	@FindBy(xpath = "//td[text()='API Created']/..//td[3]")
	private List<WebElement> listOfAPICreatedRecords;

	@FindBy(xpath = "//td[text()='API Saved']/..//td[3]")
	private List<WebElement> listOfAPISavedRecords;

	@FindBy(xpath = "//td[text()='API Reassigned']/..//td[3]")
	private List<WebElement> listOfAPIReassignedRecords;

	@FindBy(xpath = "//tbody[@id='tbody']//tr")
	private List<WebElement> emptybody;

	@FindBy(xpath = "//td[text()='CBIZ']/..//td[3]")
	private List<WebElement> listOfCBIZAccountRecords;

	@FindBy(xpath = "//td[text()='Need Audit']/..//td[3]")
	private List<WebElement> listOfNeedAuditRecords;

	@FindBy(xpath = "//td[text()='Abby J Li']/..//td[3]")
	private List<WebElement> listOfPIProcessorRecords;

	@FindBy(xpath = "//td[text()='Abby J Li']/..//td[3]")
	private List<WebElement> listOfZoeZouProcessorRecords;

	@FindBy(xpath = "//td[7][text()='Auto correct']/..//td[5][contains(.,'CBIZ')]/..//td[3]") // ""//td[7][text()='AIR']/..//td[5][text()='CBIZ']/..//td[3]")
	private List<WebElement> listOfCBIZAIRRecords;

	@FindBy(xpath = "//td[contains(text(),'Alpharetta, GA - Full Check')]/..//td[3]")
	private List<WebElement> listOfCBIZAlpharettaGAFullCheckRecords;

	@FindBy(xpath = "//td[22][text()='Property']/..//td[3]")
	private List<WebElement> listOfStandredLobRecords;

	@FindBy(xpath = "//td[text()='Contracting Bond (CBIC)']/..//td[3]")
	private List<WebElement> listOfContractingBondRecords;

	@FindBy(xpath = "//td[25][text()='Vicky J Zhang']/..//td[3]")
	private List<WebElement> listOfDTProcessorRecords1;

	@FindBy(xpath = "//td[27][text()='Shobhan Shiva']/..//td[3]")
	private List<WebElement> listOfDTProcessorRecords;

	@FindBy(xpath = "//td[29][text()='Test Additional Field']/..//td[3]")
	private List<WebElement> listOfAddtionalFieldRecords;

	@FindBy(xpath = "//td[19]")
	private List<WebElement> listDateToClients;

	@FindBy(xpath = "//td[20]")
	private List<WebElement> listDateFromClients;

	@FindBy(xpath = "//td[14]")
	private List<WebElement> listDateToDT;

	@FindBy(xpath = "//td[21]")
	private List<WebElement> listDateFromDT;

	@FindBy(xpath = "//td[18]/..//td[3]")
	private List<WebElement> recordLists;

	@FindBy(xpath = "//td[8]/..//td[3]")
	private List<WebElement> record1Lists;

	@FindBy(xpath = "//a[normalize-space()='218']")
	private List<WebElement> selectpage;

	@FindBy(xpath = "//button[@id='btnImageRight']")
	private WebElement validateIRBTN;

	@FindBy(xpath = "//a[@class='jstree-anchor']")
	private List<WebElement> fileTree;

	@FindBy(xpath = "//input[@type='file' and @name='file[]']")
	private WebElement uploadfiles;

	@FindBy(xpath = "(//button[@type='button' and @onclick='SaveLater()'])[2]")
	private WebElement irSaveLaterBTN;

	@FindBy(xpath = "(//button[@type='button' and @onclick='SubmitIRValue()'])[2]")
	private WebElement irSubmitBTN;

	@FindBy(xpath = "(//button[@class='btn' and @onclick='ImageRightCancel()']/i)[2]")
	private WebElement irCancelBTN;

	@FindBy(xpath = "//div[@id='divSerialNo']/div[2]/button[@onclick='SaveLater()']")
	private WebElement irRibbonSaveLaterBTN;

	@FindBy(xpath = "//div[@id='divSerialNo']/div[2]/button[@onclick='SubmitIRValue()']")
	private WebElement irRibbonSubmitBTN;

	@FindBy(xpath = "//div[@id='divSerialNo']/div[2]/button[@onclick='ImageRightCancel()']")
	private WebElement irRibbonCancelBTN;

	@FindBy(xpath = "//img[@src='/Content/img/double-down-arrow-icon1.png' and @id='arrowSrc']")
	private WebElement irArrowDownBTN;

	@FindBy(xpath = "//img[@src='/Content/img/double-up-arrow-icon1.png' and @id='arrowSrc']")
	private WebElement irArrowUpBTN;

	@FindBy(xpath = "//input[@id='specificPageNo']")
	private WebElement specificPageNoTxtBx;

	@FindBy(xpath = "//button[@id='btnGo']")
	private WebElement goBTN;

	@FindBy(id = "txtOptionField")
	private WebElement AdditionalFieldTxtBx;

	@FindBy(id = "txtDtProcessor")
	private WebElement DtProcessorTxtBx;

	@FindBy(xpath = "//input[@id='sttoClientDatepicker']")
	private WebElement DateToClientFromTxtBx;

	@FindBy(xpath = "//input[@id='endtoClientDatepicker']")
	private WebElement DateToClientToTxtBx;

	@FindBy(xpath = "//input[@id='stfromClientDatepicker']")
	private WebElement DateFromClientFromTxtBx;

	@FindBy(xpath = "//input[@id='endfromClientDatepicker']")
	private WebElement DateFromClientToTxtBx;

	@FindBy(xpath = "//input[@id='sttoDTDatepicker']")
	private WebElement DateToDTFromTxtBx;

	@FindBy(xpath = "//input[@id='endtoDTDatepicker']")
	private WebElement DateToDTTOTxtBx;

	@FindBy(xpath = "//input[@id='stfromDTDatepicker']")
	private WebElement DateFromDTFromTxtBx;

	@FindBy(xpath = "//input[@id='endfromDTDatepicker']")
	private WebElement DateFromDTToTxtBx;

	@FindBy(css = "div[id='GridContainer'] li:nth-child(9) a:nth-child(1)")
	private WebElement lastElementOfPage;

	@FindBy(xpath = "//tbody[@id='tbody']/tr[1]/td[2]/input")
	private WebElement Active1stElement;

	@FindBy(xpath = "//tbody[@id='tbody']/tr[2]/td[2]/input")
	private WebElement Active2ndElement;

	@FindBy(xpath = "//tbody[@id='tbody']/tr[1]/td[3]")
	private WebElement Active1stPKGElement;

	@FindBy(xpath = "//tbody[@id='tbody']/tr[2]/td[3]")
	private WebElement Active2ndPKGElement;

	@FindBy(xpath = "//em[@id='mergeIcon']")
	private WebElement MergeIcon;

	@FindBy(xpath = "//button[@onclick='MergeSave()']")
	private WebElement MergeSave;

	@FindBy(xpath = "//em[@class='icon-plus']")
	private WebElement plusIcon;

	@FindBy(xpath = "//i[@class='icon-minus']")
	private WebElement minusIcon;

	@FindBy(xpath = "(//*[contains(text(),'Merged')])[3]")
	private WebElement ckmerge;

	@FindBy(xpath = "//span[@id='spanddlSelectOffice']")
	private WebElement SelectOffice;

	By SelectOffice1 = By.xpath("//span[@id='spanddlSelectOffice']");

	@FindBy(xpath = "//input[@name='chkbox']")
	private WebElement Checkboxe;

	@FindBy(id = "btnEdit")
	private WebElement btnEdit;

	@FindBy(id = "Carrier")
	private WebElement Carrier;

	@FindBy(id = "Premium")
	private WebElement Premium;

	@FindBy(id = "SendEmail")
	private WebElement SendEmail;

	@FindBy(id = "Processor")
	private WebElement Processor;

	@FindBy(xpath = "//a[text()='CBIC (Contractors Bonding Insurance Company)']")
	private WebElement id11;

	@FindBy(id = "yearFrom")
	private WebElement selectDateCreated;

	@FindBy(xpath = "(//label[@class='left-inner-addon' and text()=' Date Created ']/..//div)[19]")
	private WebElement defaultDateCreated;

	@FindBy(xpath = "//input[@name='selectItemddlStatus']/../../../li//label")
	private List<WebElement> getAllStatus;

	@FindBy(xpath = "//input[@name='selectItemSelecteDepartments']/../../..//li/label")
	private List<WebElement> getAllDepartment;

	@FindBy(xpath = "(//li[text()='No matches found'])[10]")
	private WebElement NoMatchesFound;

	@FindBy(xpath = "//div[@id='pageDiv']")
	private WebElement FullPagination;

	@FindBy(xpath = "//div[@id='pageDiv']/ul/li")
	private WebElement getAllPages;

	@FindBy(id = "spanSelecteDepartments")
	private WebElement departmentsBlank;

	@FindBy(xpath = "(//button[@onclick='SubmitIRValue()'])[2]/..//button[2]")
	private WebElement savelaterBTN;

	@FindBy(xpath = "//button[contains(.,'Save Later')]")
	private List<WebElement> savelaterBTNCount;

	@FindBy(xpath = "//a[@id='19271695_anchor']']")
	private WebElement fileTreeChekbox1;

	@FindBy(xpath = "/html//span[@id='spanMessageContent']")
	private WebElement alertMsg;

	@FindBy(xpath = "//th[@id='thDescription']")
	private WebElement taskDescription;

	@FindBy(xpath = "//button[@id='btnCancel']")
	private WebElement CancelBTN;

	@FindBy(xpath = "//em[@id='CancelTaskIcon']")
	private WebElement CancelledBTN;

	@FindBy(xpath = "//button[@onclick='CancelTaskSave()']/em")
	private WebElement CancelTaskBTN;

	@FindBy(id = "txtCancelTask")
	private WebElement CancelledTXTBX;

	@FindBy(id = "btnConfirmYes")
	private WebElement yeslBTN;

	@FindBy(xpath = "//td[text()='Cancelled']")
	private WebElement CancelledRecords;

	@FindBy(xpath = "//th[@id='thDescription']/../../../..//td[4]")
	private WebElement thDescriptionRecords;

	@FindBy(id = "taskDescription")
	private WebElement TaskDescriptionFilter;

	@FindBy(xpath = "//div[@id='tree']")
	private WebElement fileTree1;

	@FindBy(id = "btnIrExcelExport")
	private WebElement IrExcelExportBTN;

	@FindBy(id = "btnNewCommunication")
	private WebElement CreateOneCommunication;

	@FindBy(id = "btnDtSuppend")
	private WebElement DTSuspend;

	@FindBy(id = "RushTaskIcon")
	private WebElement RushIcon;

	@FindBy(id = "txtRushTask")
	private WebElement RushTxtArea;

	@FindBy(xpath = "//button[@class='btn btn-primary btn-small' and @onclick='RushTaskSave()']")
	private WebElement RushButton;

	@FindBy(id = "uploadsIcon")
	private WebElement UploadDTFinishedChecklistIcon;

	@FindBy(xpath = "//div[@id='uploadifive-fileUploads']//input[@type='file']")
	private WebElement UploadDTFinishedChecklistButton;

	@FindBy(xpath = "//button[@class='btn btn-primary btn-small' and @onclick='UploadDTFilesSave()']")
	private WebElement UploadDTFinishedChecklistSubmitButton;

	@FindBy(id = "emailIcon")
	private WebElement SendErrorReport;

	@FindBy(id = "downloadFinishedChecklistsIcon")
	private WebElement DownloadFinishedChecklistsIcon;

	@FindBy(id = "btnEditDateToClient")
	private WebElement EditDateToClientIcon;

	@FindBy(id = "EditDateToClient")
	private WebElement EditDateToClientval;

	@FindBy(xpath = "//td[@data-handler='selectDay']//a[@class='ui-state-default ui-state-highlight']/a|//div[@id='ui-datepicker-div']/table/tbody/tr/td[contains(@class,'ui-datepicker-days-cell-over  ui-datepicker-today')]")
	private WebElement selectDate;

	@FindBy(xpath = "//button[@class='btn btn-primary btn-small' and @onclick='DateToClientSave()']")
	private WebElement DateToClientSave;

	@FindBy(xpath = "//td[contains(.,'Merged')]/../..//tr[2]/td[2]/input[@type='checkbox']")
	private WebElement recordCheckbox2ndrow;

	@FindBy(id = "btnRetrieve")
	private WebElement unMergeBTN;

	@FindBy(xpath = "(//a[contains(.,'Page 1: 99608367_2024-2025 Multinational Policy.pdf')])[1]")
	private WebElement customPDFChecbox;

	@FindBy(xpath = "(//a[contains(.,'Page 1: 99608367_2024-2025 Multinational Policy.pdf')])[1]//img")
	private WebElement customPDFIcon;

	@FindBy(xpath = "//a[contains(.,'Page 2: Cemtek Environmental, Inc. - Policy Delivery 72HIPCE2240.msg')]")
	private WebElement customMSGChecbox;

	@FindBy(xpath = "//a[contains(.,'Page 2: Cemtek Environmental, Inc. - Policy Delivery 72HIPCE2240.msg')]//img[@id='prevFile']")
	private WebElement customMSGIcon;

	@FindBy(xpath = "//a[contains(.,'Page 2: Cemtek Environmental, Inc. - Policy Delivery 72HIPCE2240.msg')]//img[@id='downLoadFile']")
	private WebElement customMSGDownloadIcon;

	@FindBy(id = "NameInsured")
	private WebElement editNameInsuredTXT;

	@FindBy(id = "LOB")
	private WebElement editLobDD;

	@FindBy(id = "Processor")
	private WebElement editPIProcessor1;

	@FindBy(id = "Carrier")
	private WebElement editCarrierTXT;

	@FindBy(id = "Premium")
	private WebElement editPremiumTXT;

	@FindBy(xpath = "//div[@class=\"ui-menu-item-wrapper\"]")
	private WebElement editCarrierTXTDD;

	@FindBy(xpath = "//input[@type='radio' and @value='1']")
	private WebElement editCarrierTXTRD;

	@FindBy(id = "DiscrepanciesAvailable")
	private WebElement editDiscrepanciesAvailableDD;

//	@FindBy(id="SaveButtonOne")
//	private WebElement editSave;

	@FindBy(xpath = "(//div[@id='divSerialNo']//div)[4]/button[1]")
	private WebElement editSave;

	@FindBy(id = "SaveButtonTwo")
	private WebElement editSave1;

	@FindBy(xpath = "//i[@class=\"icon-commmunicationInfo\"]")
	private WebElement commmunicationInfoICON;

	String package1 = "";
	String package2 = "";

	@FindBy(xpath = "(//i[@class='jstree-icon jstree-ocl'])[5]")
	private WebElement plus1;

	/**
	 * chaithra
	 */
	@FindBy(id = "DepartmentDrop")
	private WebElement selectDepartmentDPDN;
	@FindBy(xpath = "//input[@id='DateFromClient']")
	private WebElement dateFromClientTXT;

	@FindBy(xpath = "//div[@id=\"ui-datepicker-div\"]/table/tbody/tr[4]/td[2]/a")
	private WebElement highlightedDate;

	@FindBy(xpath = "//input[@id='InternalDueDate']")
	private WebElement internalDueDate;

	@FindBy(xpath = "//input[@id='ClientDueDate']")
	private WebElement clientDueDate;

	@FindBy(xpath = "//input[@id='ClientDueDate']")
	private WebElement clientDueDateTXT;

	@FindBy(xpath = "//input[@id='InternalDueDate']")
	private WebElement internalDueDateTXT;

	@FindBy(xpath = "//input[@id='DateFromClient']")
	private WebElement DateFromClient;

	@FindBy(xpath = "//input[@id='DateFromClient']")
	private WebElement DateFromClientTXT;

	@FindBy(id = "AccountManager")
	private WebElement irAccountManagerTXT;

	@FindBy(xpath = "//ul//li//div[@class='ui-menu-item-wrapper']")
	private WebElement irAccountManagerOpn;

	@FindBy(id = "ddlAssignBackToIR")
	private WebElement irAssignToTXT;

	public ManagePage(WebDriver driver, WebActionUtils webActionUtils) {
		super(driver, webActionUtils);
	}

	public WebElement getFirstElement(String txt1) {
		String txt = "tr" + txt1;
		String str = "//tbody[@id='tbody']/tr[1][@id='" + txt + "']/td[2]/input";
		Wait<WebDriver> wait = new WebDriverWait(driver, Duration.ofSeconds(60));
		// WebElement getFirstEle1 =
		// webActionUtils.getdriver().findElement(By.xpath(str));

		WebElement getFirstEle1 = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(str)));
		return getFirstEle1;
	}

	public void assignCheckingAuditor(String option) {
		webActionUtils.info("Please select an Checking Auditor");
		webActionUtils.waitSleep1(3);
		webActionUtils.waitUntilLoadedAndVisibilityOfElementLocated(auditorOptions);
		webActionUtils.waitUntilLoadedAndElementClickable(auditorOptions);
		webActionUtils.selectByVisibleText(auditorOptions, option);
		// webActionUtils.selectByIndex(auditorOptions, 2);
	}

	public void assignFBEAuditor(String option) {
		webActionUtils.info("Please select an FBE Auditor");
		webActionUtils.waitSleep(5);
		webActionUtils.waitUntilLoadedAndPresenceOfElementLocated(FBEauditorOptions);
		webActionUtils.waitUntilLoadedAndElementClickable(FBEauditorOptions);
		webActionUtils.selectByVisibleText(FBEauditorOptions, option);
		// webActionUtils.selectByIndex(auditorOptions, 2);
	}

	public void assignProcessor1n2(String option1, String option2) {
		webActionUtils.info("Please select an PI processor 1*");
		webActionUtils.waitSleep(5);
		webActionUtils.waitUntilLoadedAndElementClickable(processorOptions);
		webActionUtils.selectByVisibleText(processorOptions, option1);

		webActionUtils.info("Please select an PI processor 2");
		webActionUtils.waitSleep(5);
		webActionUtils.waitUntilLoadedAndElementClickable(processor2Options);
		webActionUtils.selectByVisibleText(processor2Options, option2);
		webActionUtils.waitSleep(5);

	}

	public void changeNameinsuredandLOB() {
		webActionUtils.info("Change Nameinsured and LOB fields");

		webActionUtils.info("Changing Name insured in the EditPage");
		webActionUtils.clearText(editNameInsuredTXT);
		String NI = "Com_NameInsured_" + RandomStringUtils.randomAlphanumeric(10);
		webActionUtils.setText(editNameInsuredTXT, NI);
		webActionUtils.info("Named Insured Name : " + NI);

		webActionUtils.info("Changing LOB in the EditPage ");
		String LOB = "AIR";
		// webActionUtils.setText(editLobTXT, LOB);
		webActionUtils.selectByVisibleText(editLobDD, LOB);
		webActionUtils.info("Selected LOB Name :" + LOB);
	}

	public void checkCheckboxandClickmerge() {
		webActionUtils.info("Check the Checkbox");
		try {
			webActionUtils.info("Selecting all the records");
			webActionUtils.clickElement(recordChkBx, "Record selected");
			// webActionUtils.waitSleep(3)
			webActionUtils.info("Opening the Merge Window Slicer");
			webActionUtils.clickElement(MergeIcon, "Merge button");
			webActionUtils.waitSleep(3);
			webActionUtils.info("Clicking on the Save button");
			webActionUtils.waitForPageLoad();
			webActionUtils.waitUntilLoadedAndVisibilityOfElementLocated(MergeSave);
			webActionUtils.jsclickElement(MergeSave, "Merge Save");
			webActionUtils.info("Checking + or - Icon on records");
			try {
				webActionUtils.info("Checking - Icon on records");

				if (minusIcon.isDisplayed()) {
					webActionUtils.info(" - Icon is present");
					webActionUtils.waitSleep(1);
				}

				else if (plusIcon.isDisplayed()) {
					webActionUtils.info(" + Icon is present");
					webActionUtils.clickElement(plusIcon, "Plus Icon");
				}
			} catch (NoSuchElementException e) {
				webActionUtils.waitSleep(3);
			}
			// webActionUtils.info("Check the Checkbox");
			// System.out.println(ckmerge.getText());
			// webActionUtils.verifyElementText(ckmerge, "Merged");
			webActionUtils.info("TC Passed : Merged");

		} catch (Exception e) {
			webActionUtils.info("TC failed");
			e.printStackTrace();
			;
		}

	}

	public void checkTwoActivePackageforMerge() {
		webActionUtils.waitSleep(20);
		webActionUtils.clickElement(lastElementOfPage, "Latest Page");
		webActionUtils.waitSleep(20);
		webActionUtils.waitForPageLoad();
		webActionUtils.info("select Two Active Package for Merge");
		package1 = Active1stPKGElement.getText();
		package2 = Active2ndPKGElement.getText();

		webActionUtils.info("selected 1st  Active Package for Merge " + package1);
		webActionUtils.info("selected 2nd  Active Package for Merge " + package2);

		webActionUtils.info("Check the" + package1 + "Checkbox");
		webActionUtils.clickElement(Active1stElement, "Active 1st Checkbox ");
		webActionUtils.info("Check the" + package2 + "Checkbox");
		webActionUtils.clickElement(Active2ndElement, "Active 2nd Checkbox ");
	}

	public void clickAccountFilter() {
		webActionUtils.info("Click on Account filter");
		// webActionUtils.waitUntilLoadedAndStalenessOffElement(accountFilterTxt);
		webActionUtils.clickElement(accountFilterTxt, "Account Filter");
	}

	public void clickActiveStatus() {
		webActionUtils.info("Click on ActiveStatus Status Filter");
		webActionUtils.waitUntilLoadedAndElementClickable(ActiveStatusFilter);
		webActionUtils.clickElement(ActiveStatusFilter, " Active Status Filter");
	}

	public void clickAdvanceFiltersButton() {
		webActionUtils.info("Click on Advance Filter Button");
		try {
			webActionUtils.waitForPageLoad();
			webActionUtils.waitSleep1(15);
			webActionUtils.waitUntilLoadedAndVisibilityOfElementLocated(AdvancedFiltersBTN);
			webActionUtils.waitUntilLoadedAndElementClickable(AdvancedFiltersBTN);
			// webActionUtils.waitUntilLoadedAndStalenessOffElement(AdvancedFiltersBTN);
			webActionUtils.clickElement(AdvancedFiltersBTN, "Advanced Filters Button");
			webActionUtils.waitForPageLoad();
			webActionUtils.waitSleep1(10);

		} catch (Exception e) {
			webActionUtils.waitSleep(10);
		}

		// try {
		// if (webActionUtils.invisibilityOfElementLocated(By.id("loading"))) {
		// webActionUtils.info("Page loaded");
		// }
		// } catch (Exception e) {
		//
		// webActionUtils.isElementNotDisplayed(loadingIcon, 40);
		// webActionUtils.info("......");
		// }
	}

	public void clickAllSelectedFilter() {
		webActionUtils.info("Click on All Selected filter");
		webActionUtils.clickElement(AllSelectedFilterTXt, "All Selected Filter");
	}

	public void clickAllYourTasksFilter() {
		webActionUtils.info("Click on All Your Tasks filter");
		webActionUtils.clickElement(AllYourTasksFilterTxt, "All your tasks Filter");
	}

	public void clickandVerfiyDatecreatedFilters() {
		webActionUtils.waitSleep(3);
		webActionUtils.info("Verfiying Date created options");

		List<WebElement> op = webActionUtils.getAllSelectOptions(selectDateCreated);
		int size = op.size();
		for (int i = 0; i < size; i++) {
			String options = op.get(i).getText();
			webActionUtils.pass("Date Created OPTIONS are : " + options);
		}
	}

	public void clickAPICreatedStatus() {
		webActionUtils.info("Click on  APICreated Status Status Filter");
		webActionUtils.waitUntilLoadedAndElementClickable(APICreatedStatusFilter);
		webActionUtils.clickElement(APICreatedStatusFilter, "APICreated Status Filter");
	}

	public void clickAPIreassignedStatus() {
		webActionUtils.info("Click on  API Reassigned Status Status Filter");
		webActionUtils.waitUntilLoadedAndElementClickable(APIReassignedStatusFilter);
		webActionUtils.clickElement(APIReassignedStatusFilter, "API Reassigned Status Filter");
	}

	public void clickAPISavedStatus() {
		webActionUtils.info("Click on  APISaved Status Status Filter");
		webActionUtils.waitUntilLoadedAndElementClickable(APISavedStatusFilter);
		webActionUtils.clickElement(APISavedStatusFilter, "API Saved Status Filter");
	}

	public void clickAssignAuditorButton() {
		webActionUtils.info("Click on Assign Auditor Button");
		webActionUtils.clickElement(assignAuditorBTN, "Assign Auditor Button");
		webActionUtils.waitForPageLoad();
	}

	public void clickAssignProcessorButton() {
		webActionUtils.info("Click on Assign Processor Button");
		webActionUtils.clickElement(assignProcessorBTN, "Assign Auditor Button");
	}

	public void clickCancelButton() {
		webActionUtils.info("Click on the Cancel button ");
		webActionUtils.waitSleep(5);
		webActionUtils.clickElement(CancelBTN, "CancelBTN");
		webActionUtils.clickElement(yeslBTN, "Yes");
		webActionUtils.waitSleep(5);

	}

	public void clickCancelledStatus() {
		webActionUtils.info("Click on  CancelledStatus Status Filter");
		webActionUtils.waitUntilLoadedAndElementClickable(CancelledStatusFilter);
		webActionUtils.clickElement(CancelledStatusFilter, "Cancelled Status Filter");
	}

	public void clickContractingBondDropDown() {
		webActionUtils.info("Click on Contracting Bond DropDown");
		webActionUtils.waitSleep(3);
		webActionUtils.waitUntilLoadedAndVisibilityOfElementLocated(StandardLOBDrpDwn);
		webActionUtils.waitUntilLoadedAndElementClickable(ContractingBondDropDown);
		webActionUtils.waitForPageLoad();
		webActionUtils.clickElement(ContractingBondDropDown, "Contracting BondDropDown");
		// try {
		// if (webActionUtils.invisibilityOfElementLocated(By.id("loading"))) {
		// webActionUtils.info("Page loaded");
		// }
		// } catch (Exception e) {
		//
		// webActionUtils.isElementNotDisplayed(loadingIcon, 40);
		// webActionUtils.info("......");
		// }
	}

	public void clickDepartmentDropDown() {
		webActionUtils.info("Click on Department DropDown");
		webActionUtils.waitSleep(3);
		webActionUtils.waitUntilLoadedAndVisibilityOfElementLocated(DepartmentDrpDwn);
		webActionUtils.waitUntilLoadedAndElementClickable(DepartmentDrpDwn);
		webActionUtils.waitForPageLoad();
		webActionUtils.clickElement(DepartmentDrpDwn, "Department DropDown text");
		// try {
		// if (webActionUtils.invisibilityOfElementLocated(By.id("loading"))) {
		// webActionUtils.info("Page loaded");
		// }
		// } catch (Exception e) {
		//
		// webActionUtils.isElementNotDisplayed(loadingIcon, 40);
		// webActionUtils.info("......");
		// }
	}

	public void clickDownloadchecklistTemplate() {
		webActionUtils.info("click on  the Download checklist Template icon ");
		webActionUtils.waitForPageLoad();
		webActionUtils.waitSleep(3);
		webActionUtils.waitUntilLoadedAndElementClickable(downloadChecklistTemplateBTN);
		webActionUtils.clickElement(downloadChecklistTemplateBTN, "download Checklist Template Icon");
		webActionUtils.waitForPageLoad();
		webActionUtils.waitSleep(10);
		webActionUtils.pass("File is download locally");
	}

	public void clickDownloadChecklistTemplate() {
		webActionUtils.info("Click on Download Checklist Template");
		webActionUtils.waitUntilLoadedAndVisibilityOfElementLocated(downloadChecklistTemplateBTN);
		webActionUtils.clickElement(downloadChecklistTemplateBTN, " Download Checklist Template");
		webActionUtils.waitForPageLoad();

	}

	public void clickDownloadFinishedTemplate() {
		webActionUtils.info("click on  the Download Finished Template icon ");
		webActionUtils.waitForPageLoad();
		webActionUtils.waitSleep(3);
		webActionUtils.waitUntilLoadedAndElementClickable(DownloadFinishedChecklistsIcon);
		webActionUtils.clickElement(DownloadFinishedChecklistsIcon, "Download FinishedChecklists Icon Icon");
		webActionUtils.waitForPageLoad();
		webActionUtils.waitSleep(10);
		webActionUtils.pass("File is download locally");
	}

	public void clickDTFinishedStatus() {
		webActionUtils.info("Click on  DTFinished Status Filter");
		webActionUtils.waitUntilLoadedAndElementClickable(DTFinishedStatusFilter);
		webActionUtils.clickElement(DTFinishedStatusFilter, "DTFinished Status Filter");
	}

	public void clickDTSuspendedselectedStatus() {
		webActionUtils.info("Click on  DTSuspendedselectedStatus Status Filter");
		webActionUtils.waitUntilLoadedAndElementClickable(DTSuspendedselectedStatusFilter);
		webActionUtils.clickElement(DTSuspendedselectedStatusFilter, "DTSuspendedselected Status Filter");
	}

	public void clickEditButton() {
		webActionUtils.info("Click on Edit Button");
		webActionUtils.clickElement(EditBTN, "Edit Button");
	}

	public void clickEditDateToClientIcon() {
		webActionUtils.info("click on the EditDate To Client Icon ");
		webActionUtils.waitForPageLoad();
		webActionUtils.waitSleep(10);
		webActionUtils.waitUntilLoadedAndElementClickable(EditDateToClientIcon);
		webActionUtils.clickElement(EditDateToClientIcon, "Date To Client Icon");
		webActionUtils.waitForPageLoad();
		webActionUtils.waitSleep(10);
	}

	public void clickFBEAuditorFilter() {
		webActionUtils.info("Click on FBE Auditor filter");
		webActionUtils.clickElement(FBEAuditorFilterTxt, "FBE Auditor Filter  Filter");
	}

	public void clickFinishedStatus() {
		webActionUtils.info("Click on  FinishedStatus Status Filter");
		webActionUtils.waitUntilLoadedAndElementClickable(FinishedStatusFilter);
		webActionUtils.clickElement(FinishedStatusFilter, "Finished Status Filter");
	}

	public void clickIRCancelbutton() {
		webActionUtils.info("Click on IR Cancel Button");
		webActionUtils.waitUntilLoadedAndElementClickable(irCancelBTN);
		webActionUtils.clickElement(irCancelBTN, "IR Cancel Button");
		webActionUtils.waitForPageLoad();

	}

	public void clickManagePageTab() {
		webActionUtils.info("Clicking on the Manage PageTab");
		webActionUtils.waitSleep(12);
		// webActionUtils.waitFluent();
		// webActionUtils.waitUntilLoadedAndPresenceOfElementLocated(managePageTab);
		// webActionUtils.waitUntilLoadedAndElementClickable(managePageTab);
		// webActionUtils.waitUntilLoadedAndVisibilityOfElementLocated(managePageTab);
		// webActionUtils.scrollBy(managePageTab);
		webActionUtils.clickElement(managePageTab, "Manage Page Tab");
		webActionUtils.waitUntilLoadedAndVisibilityOfElementLocated(SelectOffice);
		webActionUtils.waitSleep(10);
	}

	public void clickMergedStatus() {
		webActionUtils.info("Click on  MergedStatus Status Filter");
		webActionUtils.waitUntilLoadedAndElementClickable(MergedStatusFilter);
		webActionUtils.clickElement(MergedStatusFilter, "Merged Status Filter");
	}

	public void clickOnCancelledIcon() {
		webActionUtils.info("Click on the Cancelled button ");
		webActionUtils.waitSleep(5);
		webActionUtils.clickElement(CancelledBTN, "Cancelled Button");
		webActionUtils.setText(CancelledTXTBX, "Test Cancelled " + RandomStringUtils.randomAlphanumeric(15));
		webActionUtils.waitSleep(2);
		webActionUtils.clickElement(CancelTaskBTN, "Yes");
		webActionUtils.waitSleep(10);
		webActionUtils.pass("Cancelled task TC passed");
	}

	public void ClickOnClearFiltersBtn() {
		webActionUtils.info("Click on Clear Filter Button");
		webActionUtils.waitSleep(3);
		webActionUtils.waitUntilLoadedAndVisibilityOfElementLocated(ClearFiltersBTN);
		webActionUtils.waitUntilLoadedAndElementClickable(ClearFiltersBTN);
		webActionUtils.waitForPageLoad();
		webActionUtils.clickElement(ClearFiltersBTN, "Clear Filters Button");
		// try {
		// if (webActionUtils.invisibilityOfElementLocated(By.id("loading"))) {
		// webActionUtils.info("Page loaded");
		// }
		// } catch (Exception e) {
		//
		// webActionUtils.isElementNotDisplayed(loadingIcon, 40);
		// webActionUtils.info("......");
		// }

	}

	public void clickOnCommunicationIcon() {
		webActionUtils.info("click On Communication Icon ");
		webActionUtils.clickElement(commmunicationInfoICON, "CommmunicationInfo ICON");
		webActionUtils.waitSleep1(10);
//		String mainWindowHandle = webActionUtils.getdriver().getWindowHandle();
//		Set<String> allWindowHandles = webActionUtils.getdriver().getWindowHandles();
//		Iterator<String> iterator = allWindowHandles.iterator();
//
//		// Here we will check if child window has other child windows and will
//		// fetch the heading of the child window
//		while (iterator.hasNext()) {
//			String ChildWindow = iterator.next();
//			if (!mainWindowHandle.equalsIgnoreCase(ChildWindow) && ChildWindow.toString().equalsIgnoreCase("CreateEditCommunication - Policy Checking Platform")) {
//				driver.switchTo().window(ChildWindow);
//				String text = webActionUtils.getdriver().getTitle();
//				System.out.println("Heading of child window is " + text);
//			}
//			

		// String Parent_id = driver.getWindowHandle();
		System.out.println(driver.getTitle());

		// getWindowHandle method to get ID of new window (child window)
		Set<String> Child_id = driver.getWindowHandles();
		System.out.println(driver.getTitle());

		// for each loop
		for (String a : Child_id) {

			// it will print IDs of both window
			if (webActionUtils.getdriver().switchTo().window(a).getTitle()
					.equals("CreateEditCommunication - Policy Checking Platform")) {
				System.out.println(webActionUtils.getdriver().switchTo().window(a).getTitle());
			}

		}
	}

	public void clickOnCreateOneCommunication(String Str) {
		webActionUtils.info("Click On Create One Communication");
		webActionUtils.clickElement(CreateOneCommunication, "Create One Communication Button");

		String mainWindowHandle = webActionUtils.getdriver().getWindowHandle();
		Set<String> allWindowHandles = webActionUtils.getdriver().getWindowHandles();
		Iterator<String> iterator = allWindowHandles.iterator();

		// Here we will check if child window has other child windows and will
		// fetch the heading of the child window
		while (iterator.hasNext()) {
			String ChildWindow = iterator.next();
			if (!mainWindowHandle.equalsIgnoreCase(ChildWindow)) {
				driver.switchTo().window(ChildWindow);
				String text = webActionUtils.getdriver().getTitle();
				System.out.println("Heading of child window is " + text);
			}
		}

	}

	public void ClickOnDownloadOriginalFilesIcon() {
		webActionUtils.info("Click on Download Original Files Window slicer Icon");
		webActionUtils.waitSleep(10);
		webActionUtils.clickElement(DownloadAllFilesIcon1, "Download All Files ICon");

		// webActionUtils.waitUntilLoadedAndVisibilityOfElementLocated(DownloadAllFilesBTN2);
		// webActionUtils.waitUntilLoadedAndElementClickable(DownloadAllFilesBTN2);
		// webActionUtils.waitForPageLoad();
		webActionUtils.waitSleep(5);
		webActionUtils.clickElement(DownloadAllFilesBTN2, "Download All Files Button");
		int Count = webActionUtils.getdriver().getWindowHandles().size();

		webActionUtils.waitForPageLoad();
		webActionUtils.waitSleep(10);
		try {
			// if
			// (webActionUtils.invisibilityOfElementLocated(By.id("loading"))) {
			// webActionUtils.info("Page loaded");
			// }

			ArrayList<String> multipleTabs = new ArrayList<String>(webActionUtils.getdriver().getWindowHandles());
			int tabsCount = multipleTabs.size();
			System.out.println("Tabs Count: " + tabsCount);

			while (webActionUtils.getdriver().getWindowHandles().size() == 2) {
				System.out.println("start Count:" + Count);
				System.out.println("multipleTabs.size(): " + tabsCount);
				System.out.println("webActionUtils.getdriver().getWindowHandles().size():"
						+ webActionUtils.getdriver().getWindowHandles().size());

				webActionUtils.waitSleep(10);
			}
			webActionUtils.info("Download IS SUCCESSFULL");

		} catch (Exception e) {

			webActionUtils.isElementNotDisplayed(loadingIcon, 40);
			webActionUtils.info("......");
		}

	}

	public void clickOnDTSuspended() {
		webActionUtils.info("Click on DT Suspended Status");
		webActionUtils.waitSleep(10);
		webActionUtils.clickElement(DTSuspend, "DT Suspended Button");
		webActionUtils.waitSleep(30);
		webActionUtils.waitForPageLoad();
	}

	public void clickOnEditSaveButton() {
		try {
			webActionUtils.scrolltoEnd();
			webActionUtils.info("click On Edit Save Button ");
			webActionUtils.waitSleep(60);	
			webActionUtils.clickElement(editSave1, "Save");
			
			webActionUtils.waitSleep(20);
		} catch (Exception e) {
			editSave1.click();
		}
	}

	public void ClickOnExportTOExcelBtn() {
		webActionUtils.info("Click on Export to Excel Button");
		webActionUtils.waitSleep(15);
		// webActionUtils.waitUntilLoadedAndVisibilityOfElementLocated(ExportBTN);
		// webActionUtils.waitUntilLoadedAndElementClickable(ExportBTN);
		webActionUtils.waitForPageLoad();
		webActionUtils.clickElement(ExportBTN, "Export to Excel Button");
		webActionUtils.waitSleep(5);
		int Count = webActionUtils.getdriver().getWindowHandles().size();

		webActionUtils.waitForPageLoad();
		webActionUtils.waitSleep(10);
		try {
			// if
			// (webActionUtils.invisibilityOfElementLocated(By.id("loading"))) {
			// webActionUtils.info("Page loaded");
			// }

			ArrayList<String> multipleTabs = new ArrayList<String>(webActionUtils.getdriver().getWindowHandles());
			int tabsCount = multipleTabs.size();
			System.out.println("Tabs Count: " + tabsCount);

			while (webActionUtils.getdriver().getWindowHandles().size() == 2) {
				System.out.println("start Count:" + Count);
				System.out.println("multipleTabs.size(): " + tabsCount);
				System.out.println("webActionUtils.getdriver().getWindowHandles().size():"
						+ webActionUtils.getdriver().getWindowHandles().size());

				webActionUtils.waitSleep(10);
			}
			webActionUtils.info("EXPORT IS SUCCESSFULL");

		} catch (Exception e) {

			webActionUtils.isElementNotDisplayed(loadingIcon, 40);
			webActionUtils.info("......");
		}

	}

	public void clickOnMergeButton() {
		webActionUtils.info("Click on Merge Button");
		webActionUtils.waitSleep(5);
		webActionUtils.clickElement(MergeIcon, "Merge Button ");
	}

	public void ClickOnReworkTaskIcon(String text) {
		webActionUtils.info("Click on Rework Task Icon Window slicer Icon");
		webActionUtils.waitSleep(10);
		webActionUtils.clickElement(ReworkTaskIcon, "Rework Task Icon");
		webActionUtils.waitSleep(10);
		webActionUtils.clickElement(ReworkTaskTextAreaBoX, "Rework Task Text Area BoX");
		webActionUtils.setText(ReworkTaskTextAreaBoX, text);
		webActionUtils.waitSleep(10);
		webActionUtils.clickElement(ReworkTaskSubmit, "Rework Task Submit");
		webActionUtils.waitSleep(20);
		webActionUtils.pass("TC Passed");
	}

	public void ClickonRushButtontWS() {
		webActionUtils.info("Click on the Rush button");
		webActionUtils.clickElement(RushButton, "Rush Button");
		webActionUtils.pass("TC passed :Able to send email sucessfully");
	}

	public void clickOnSaveButton() {
		webActionUtils.info("Click on Merge Save Button");
		webActionUtils.clickElement(MergeSave, "Merge Save Button");
		webActionUtils.waitSleep(5);
		webActionUtils.waitForPageLoad();
	}

	// WebDriverWait wait = new WebDriverWait(driver, 100);
	// String tmpFolderPath =
	// System.getProperty("user.dir")+"/TestDataFiles/DownloadTestFiles/";
	// String expectedFileName = "178446 - CBIZ - Vicky_assistant_package1 -
	// BLR
	// - 12_11_2020 Policy Checklist.xlsx";
	// File file = new File(tmpFolderPath + expectedFileName);
	// if (file.exists())
	// file.delete();
	//// Start downloading here.
	// wait.until((ExpectedCondition<Boolean>) webDriver -> file.exists());
	// webActionUtils.waitSleep(10);

	public void ClickonSubmitButtontWS() {
		webActionUtils.info("Click on the Submit button");
		webActionUtils.clickElement(UploadDTFinishedChecklistSubmitButton, "Submit  Button");
		webActionUtils.pass("TC passed : Able tio Upload DT Finished Checklist Submit Button sucessfully");

	}

	public void clickonUploadDTFinishedChecklistIcon(String text) {
		webActionUtils.info("Click on the Upload DT Finished Checklist*");
		webActionUtils.waitForPageLoad();
		webActionUtils.waitSleep(3);
		webActionUtils.waitUntilLoadedAndElementClickable(UploadDTFinishedChecklistIcon);
		webActionUtils.clickElement(UploadDTFinishedChecklistIcon, "Upload DT Finished Checklist Icon");
		webActionUtils.waitSleep(3);
		webActionUtils.setText(UploadDTFinishedChecklistButton, text);
		webActionUtils.waitForPageLoad();
	}

	public void ClickOnuploadFileIcon(String text) {
		webActionUtils.info("Click on Upload Files Window slicer Icon");
		webActionUtils.waitSleep(10);
		webActionUtils.clickElement(UploadfileIcon, "Upload file ICon");
		webActionUtils.waitSleep(10);
		webActionUtils.setText(uploadfilesBTN, text);
		webActionUtils.waitSleep(10);
		webActionUtils.clickElement(uploadSubmit, "uploadSubmit");
		webActionUtils.waitSleep(10);
	}

	public void clickOnValidateIRButton() {
		webActionUtils.info("click On Validate IR Button ");
		webActionUtils.clickElement(validateIRBTN, "validate IR Button");
		webActionUtils.waitSleep(10);
	}

	public void clickPIAuditorFilter() {
		webActionUtils.info("Click on PI Auditor filter");
		webActionUtils.clickElement(PIAuditorFilterTxt, "PI Auditor Filter  Filter");
	}

	public void clickPIProcessorFilter() {
		webActionUtils.info("Click on PI Processor filter");
		webActionUtils.clickElement(PIProcessorFilterTxt, "PI Prrocessor  Filter");
	}

	public void clickReadyforDTChecklistStatus() {
		webActionUtils.info("Click on  ReadyforDTChecklistStatus Status Filter");
		webActionUtils.waitUntilLoadedAndElementClickable(ReadyforDTChecklistStatusFilter);
		webActionUtils.clickElement(ReadyforDTChecklistStatusFilter, "ReadyforDTChecklist Status Filter");
	}

	public void clickReadytoUploadStatus() {
		webActionUtils.info("Click on  ReadytoUploadStatus Status Filter");
		webActionUtils.waitUntilLoadedAndElementClickable(ReadytoUploadStatusFilter);
		webActionUtils.clickElement(ReadytoUploadStatusFilter, "ReadytoUpload Status Filter");
	}

	public void clickReviewedStatus() {
		webActionUtils.info("Click on  Status Status Filter");
		webActionUtils.waitUntilLoadedAndElementClickable(ReviewedStatusFilter);
		webActionUtils.clickElement(ReviewedStatusFilter, "Reviewed Status Filter");
	}

	public void clickRushIcon() {
		webActionUtils.info("Click on the Rush icon ");
		webActionUtils.waitForPageLoad();
		webActionUtils.waitSleep(3);
		webActionUtils.waitUntilLoadedAndElementClickable(RushIcon);
		webActionUtils.clickElement(RushIcon, "Rush Icon");
		webActionUtils.waitForPageLoad();
	}

	public void clickSearchButton() {
		try {
			webActionUtils.info("Click on Search Button");
			webActionUtils.clickElement(searchBTN, "Search Button");
			webActionUtils.waitForPageLoad();
			webActionUtils.info("control is here : Search Button");
			Thread.sleep(2000);
		} catch (Exception e) {
			e.printStackTrace();
		}

//		webActionUtils.waitUntilLoadedAndPresenceOfElementLocated(searchBTN);
//		webActionUtils.waitUntilLoadedAndVisibilityOfElementLocated(searchBTN);
		// webActionUtils.waitUntilLoadedAndElementClickable(searchBTN);
		// webActionUtils.waitSleep(3);
		webActionUtils.waitSleep(3);

		// webActionUtils.waitForPageLoad();
		webActionUtils.waitSleep(5);

		// try {
		// if (webActionUtils.invisibilityOfElementLocated(By.id("loading"))) {
		// webActionUtils.info("Page loaded");
		// }
		// } catch (Exception e) {
		//
		// webActionUtils.isElementNotDisplayed(loadingIcon, 40);
		// webActionUtils.info("Page not loaded...");
		// }

	}

	public void clickSelectAll() {
		webActionUtils.info("Click on Select All Status Filter");
		webActionUtils.waitUntilLoadedAndVisibilityOfElementLocated(selectAllStatusFilter);
		webActionUtils.waitUntilLoadedAndElementClickable(selectAllStatusFilter);
		webActionUtils.clickElement(selectAllStatusFilter, "Select All Status Filter");
		webActionUtils.waitForPageLoad();

		try {

		} catch (Exception e) {
			e.printStackTrace();

		}
	}

	public void clickSendErrorReportIcon() {
		webActionUtils.info("Click on the Send Error Report icon ");
		webActionUtils.waitForPageLoad();
		webActionUtils.waitSleep(3);
		webActionUtils.waitUntilLoadedAndElementClickable(SendErrorReport);
		webActionUtils.clickElement(SendErrorReport, "Send Error Report Icon");
		webActionUtils.waitForPageLoad();
		webActionUtils.waitSleep(10);
	}

	public void clickStandardLOBDropDown() {
		webActionUtils.info("Click on Standard LOB DropDown");
		webActionUtils.waitSleep(3);
		webActionUtils.waitUntilLoadedAndVisibilityOfElementLocated(StandardLOBDrpDwn);
		webActionUtils.waitUntilLoadedAndElementClickable(StandardLOBDrpDwn);
		webActionUtils.waitForPageLoad();
		webActionUtils.clickElement(StandardLOBDrpDwn, "Standard LOB DropDown");
		// try {
		// if (webActionUtils.invisibilityOfElementLocated(By.id("loading"))) {
		// webActionUtils.info("Page loaded");
		// }
		// } catch (Exception e) {
		//
		// webActionUtils.isElementNotDisplayed(loadingIcon, 40);
		// webActionUtils.info("......");
		// }
	}

	public void clickStatus() {
		webActionUtils.info("Click on Status filter");
		webActionUtils.clickElement(statusFilter, "statusFilter");
	}

	public void clickSuspendedStatus() {
		webActionUtils.info("Click on  SuspendedStatus Status Filter");
		webActionUtils.waitUntilLoadedAndElementClickable(SuspendedStatusFilter);
		webActionUtils.clickElement(SuspendedStatusFilter, "Suspended Status Filter");
	}

	public void clickUploadedStatus() {
		webActionUtils.info("Click on  UploadedStatus Status Filter");
		webActionUtils.waitUntilLoadedAndElementClickable(UploadedStatusFilter);
		webActionUtils.clickElement(UploadedStatusFilter, "  UploadedStatus Filter");
	}

	public void clickValidateIRButton() {
		webActionUtils.info("click on Validate IR Button");
		webActionUtils.waitForPageLoad();
		webActionUtils.waitUntilLoadedAndElementClickable(validateIRBTN);
		webActionUtils.clickElement(validateIRBTN, "Validate IR Button");
		webActionUtils.waitForPageLoad();
		webActionUtils.waitUntilLoadedAndVisibilityOfAllElements(fileTree);
		webActionUtils.waitForPageLoad();
		String script = "$(\"#tree\").jstree(true).open_all();";
		((JavascriptExecutor) driver).executeScript(script).equals("complete");

	}

	public void clickValidateIRButton1() {
		webActionUtils.info("Verify Task Description ");
		webActionUtils.waitForPageLoad();
		webActionUtils.waitSleep(3);
		webActionUtils.info("click on Validate IR Button");
		webActionUtils.waitForPageLoad();
		webActionUtils.waitUntilLoadedAndElementClickable(validateIRBTN);
		webActionUtils.clickElement(validateIRBTN, "Validate IR Button");
		// webActionUtils.waitForPageLoad();
		webActionUtils.waitUntilLoadedAndVisibilityOfAllElements(fileTree);
		webActionUtils.waitForPageLoad();

	}

	public void Compare2ArraylistofPackage(ArrayList<Integer> listOne, ArrayList<Integer> listTwo) {
		{

			// Compare unequal lists example
			webActionUtils.info("Comparing list of Packages Before and After Clearing Filters  ");
			Collections.sort(listOne);
			Collections.sort(listTwo);
			webActionUtils.info("Comparision is in Progress.. ");
			boolean isEqual = listOne.equals(listTwo); // false
			if (isEqual == false) {
				webActionUtils.pass("Advanced clearing Filter is working properly ");
			} else {
				webActionUtils.fail("Advanced clearing Filter is not working properly ");
			}
		}
	}

	// ==============================================
	public void dateRangeTest(String date, String fromDate, String Todate) {
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
			sdf.setLenient(false);

			Date d1 = sdf.parse(fromDate);
			Date givenDate = sdf.parse(getDateandSetDate(date.toString()));
			Date d3 = sdf.parse(Todate);

			webActionUtils.info("Checking Given date : " + date.toString() + " is in  between " + fromDate.toString()
					+ " and " + Todate.toString());
			if (givenDate.compareTo(d1) >= 0) {
				if (givenDate.compareTo(d3) <= 0) {
					webActionUtils.pass("GivenDate  is in between range  d1 and d3");
				} else {
					webActionUtils.info("GivenDate is NOT in between range d1 and d3");
				}
			} else {
				webActionUtils.info("GivenDate is NOT in between range d1 and d3");
			}

		} catch (ParseException pe) {
			pe.printStackTrace();
		}

	}

	public void diplayActiveStatusRecords() {
		try {

			for (WebElement activerecords : listOfActiveRecords) {
				webActionUtils.info("Package ID:" + activerecords.getText() + " is in Active Status");
			}
			webActionUtils.info("Some Active Records are Printed in console");
		} catch (Exception e) {
			webActionUtils.info("No Records in Active status");
		}

	}

	public void diplayAllYourTasksProcesserfilterRecords() {
		try {
			webActionUtils.waitSleep(5);
			if (listOfZoeZouProcessorRecords.size() != 0) {
				System.out.println(listOfZoeZouProcessorRecords.size());
				for (WebElement Records : listOfZoeZouProcessorRecords) {
					webActionUtils.info("Package ID :" + Records.getText()
							+ " is loaded for 'All Your Tasks' processor as search result");
				}
			} else {
				webActionUtils.info("No records  loaded for 'All Your Tasks' processor as search result  ");
			}
		} catch (Exception e) {
			webActionUtils.info("No records  loaded for 'All Your Tasks' processor as search result ");
		}
	}

	public void diplayAPICreatedRecords() {
		try {
			for (WebElement APICreatedRecords : listOfAPICreatedRecords) {
				webActionUtils.info("Package ID:" + APICreatedRecords.getText() + " is in API Created Records Status");
			}
		} catch (Exception e) {
			webActionUtils.info("No records in API Created Records status");
		}
	}

	public void diplayAPIreassignedRecords() {
		try {
			for (WebElement APIreassignedRecords : listOfAPIReassignedRecords) {
				webActionUtils
						.info("Package ID:" + APIreassignedRecords.getText() + " is in API Reassigned Records Status");
			}
		} catch (Exception e) {
			webActionUtils.info("No records in API Reassigned Records status");
		}
	}

	public void diplayAPISavedRecords() {
		try {
			for (WebElement APISavedRecords : listOfAPISavedRecords) {
				webActionUtils.info("Package ID:" + APISavedRecords.getText() + " is in API Saved Records Status");
			}
		} catch (Exception e) {
			webActionUtils.info("No records in API Saved Records status");
		}
	}

	public void diplayCancelledStatusRecords() {
		try {
			for (WebElement cancelledrecords : listOfCancelledRecords) {
				webActionUtils.info("Package ID:" + cancelledrecords.getText() + " is in Cancelled Status");
			}
		} catch (Exception e) {
			webActionUtils.info("No records in cancelled status");
		}

	}

	public void diplayDTFinishedStatusRecords() {
		try {
			for (WebElement DTFinishedrecords : listOfDTFinishedRecords) {
				webActionUtils.info("Package ID:" + DTFinishedrecords.getText() + " is in DT Finished Status");
			}
		} catch (Exception e) {
			webActionUtils.info("No records in DT Finished status");
		}
	}

	public void diplayDTSuspendedRecords() {
		try {
			for (WebElement DTSuspendedRecords : listOfDTSuspendedRecords) {
				webActionUtils.info("Package ID:" + DTSuspendedRecords.getText() + " is in DT Suspended Status");
			}
		} catch (Exception e) {
			webActionUtils.info("No records in DT Suspended status");
		}
	}

	public void diplayFinishedStatusRecords() {
		try {
			for (WebElement finishedrecords : listOfFinishedRecords) {
				webActionUtils.info("Package ID:" + finishedrecords.getText() + " is in Finished Status");

			}
			webActionUtils.info("Some Finished Records are Printed in console");
		} catch (Exception e) {
			webActionUtils.info("No Records in Finished status");
		}

	}

	public void diplayMergedStatusRecords() {
		try {
			for (WebElement mergedrecords : listOfMergedRecords) {
				webActionUtils.info("Package ID:" + mergedrecords.getText() + " is in Merged Status");
			}
		} catch (Exception e) {
			webActionUtils.info("No records in Merged status");
		}

	}

	public void diplayNeedAuditRecords() {
		try {

			if (listOfNeedAuditRecords.size() != 0) {
				System.out.println(listOfNeedAuditRecords.size());
				for (WebElement NeedAuditRecords : listOfNeedAuditRecords) {
					webActionUtils.info("Package ID :" + NeedAuditRecords.getText() + " is a Need Audit Records ");
				}
			} else {
				webActionUtils.info("No records in Need Audit Records ");
			}
		} catch (Exception e) {
			webActionUtils.info("No records in Need Audit Records");
		}
	}

	public void diplayPIProcesserfilterRecords() {
		try {

			if (listOfPIProcessorRecords.size() != 0) {
				System.out.println(listOfPIProcessorRecords.size());
				for (WebElement Records : listOfPIProcessorRecords) {
					webActionUtils.info("Package ID :" + Records.getText()
							+ " is loaded for 'Abby J Li' processor as search result");
				}
			} else {
				webActionUtils.info("No records  loaded for 'Abby J Li'' processor as search result  ");
			}
		} catch (Exception e) {
			webActionUtils.info("No records  loaded for 'Abby J Li'' processor as search result ");
		}
	}

	public void diplayReadyforDTChecklistRecords() {

		webActionUtils.info("Waiting for the Ready for DT Checklist filtered  records");

		try {

			if (emptybody.size() != 0) {
				System.out.println(emptybody.size());
				for (WebElement ReadyforDTChecklist : listOfReadyforDTChecklistRecords) {
					webActionUtils.info(
							"Package ID:" + ReadyforDTChecklist.getText() + " is in Ready for DT Checklist Status");
				}

			} else {
				webActionUtils.info("No records in Ready for DT Checklist status");
			}

		} catch (Exception e) {
			webActionUtils.info("No records in Ready for DT Checklist status");
		}
	}

	public void diplayReadyToUploadStatusRecords() {

		webActionUtils.info("waiting for the Ready to Upload filtered  records");
		webActionUtils.waitSleep(5);
		try {

			if (emptybody.size() != 0) {
				System.out.println(emptybody.size());

				for (WebElement ReadytoUploadrecords : listOfReadytoUploadRecords) {
					webActionUtils
							.info("Package ID:" + ReadytoUploadrecords.getText() + " is in Ready to Upload Status");
				}
			} else {
				webActionUtils.info("No records in Ready to Upload status");
			}
		} catch (Exception e) {
			webActionUtils.info("No records in Ready to Upload status");
		}

	}

	public void diplayReviewedStatusRecords() {
		try {
			for (WebElement reviewedrecords : listOfReviewedRecords) {
				webActionUtils.info("Package ID:" + reviewedrecords.getText() + " is in Reviewed Status");
			}
		} catch (Exception e) {
			webActionUtils.info("No records in Reviewed status");
		}
	}

	public void diplaySuspendedStatusRecords() {
		try {
			for (WebElement suspendedrecords : listOfSuspendedRecords) {
				webActionUtils.info("Package ID:" + suspendedrecords.getText() + " is in Suspended Status");
			}
		} catch (Exception e) {
			webActionUtils.info("No Records in Suspended status");
		}

	}

	public void diplayUploadedStatusRecords() {
		try {
			for (WebElement uploadedrecords : listOfUploadedRecords) {
				webActionUtils.info("Package ID:" + uploadedrecords.getText() + " is in Uploaded Status");
			}
		} catch (Exception e) {
			webActionUtils.info("No Records in uploaded status");
		}

	}

	public void displayAllDepartmentforAccount() {
		webActionUtils.info("Display All the department for the Fror the CBIZ");
		webActionUtils.waitUntilLoadedAndVisibilityOfAllElements(getAllDepartment);

		int alldeptsize = getAllDepartment.size();

		if (alldeptsize > 1) {
			webActionUtils.info("Department Size is " + alldeptsize);
			List<String> aList = new ArrayList<String>();
			try {
				if (getAllDepartment.size() != 0) {
					for (WebElement recordList : getAllDepartment) {
						aList.add(recordList.getText().toString());
						webActionUtils.pass("All department are" + aList);
					}
				}
				webActionUtils.waitSleep(5);
			} catch (Exception e) {
				webActionUtils.pass("Unable to display the departments");
			}
		}
	}

	public void displayAllRecords() {
		webActionUtils.waitSleep(3);
		try {
			if (listOfDTProcessorRecords.size() != 0) {
				System.out.println(listOfDTProcessorRecords.size());
				for (WebElement listOfDTProcessorRecord : listOfDTProcessorRecords) {
					webActionUtils.info("DT Processor -- Package ID :" + listOfDTProcessorRecord.getText());
				}
			} else {
				webActionUtils.info("No records is in DT Processor ");
			}
		} catch (Exception e) {
			webActionUtils.info("No records is in DT Processor ");
		}
	}

	public void displayAndVerifyAdditionalFieldsRecords() {
		webActionUtils.waitSleep(3);
		try {
			webActionUtils.info("Checking Additional field records");

			if (listOfAddtionalFieldRecords.size() != 0) {
				System.out.println(listOfAddtionalFieldRecords.size());
				for (WebElement listOfAddtionalFieldRecord : listOfAddtionalFieldRecords) {
					webActionUtils.pass(
							"list Of Addtional Field Records -- Package ID :" + listOfAddtionalFieldRecord.getText());
				}
			} else {
				webActionUtils.info("No records is in Addtional Field  ");
			}
		} catch (Exception e) {
			webActionUtils.info("No records is in Addtional Field  ");
		}
	}

	public void displayandVerifyDatefromClientRecords(String fromDate, String Todate) {
		webActionUtils.waitSleep(3);
		try {
			webActionUtils.info("Checking date to Client filter");
			if (listDateFromClients.size() != 0) {
				System.out.println(listDateFromClients.size());
				for (WebElement listDateFromClient : listDateFromClients) {

					dateRangeTest(listDateFromClient.getText(), fromDate.toString(), Todate.toString());
				}
			}
		} catch (Exception e) {
			webActionUtils.info("No records in CBIZ Account ");
		}

		try {
			webActionUtils.info("Packages Numbers are.. ");
			if (recordLists.size() != 0) {

				for (WebElement recordList : recordLists) {

					webActionUtils.info("Records :" + recordList.getText());
				}
			} else {
				webActionUtils.info("No records ");
			}
		} catch (Exception e) {
			webActionUtils.info("No records");
		}

	}

	public ArrayList<Integer> displayandVerifyDatefromDTRecords(String fromDate, String Todate) {
		ArrayList<Integer> al1 = new ArrayList<Integer>();
		webActionUtils.waitSleep(3);
		try {
			webActionUtils.info("Checking Date From DT filter");
			if (listDateFromDT.size() != 0) {
				System.out.println(listDateFromDT.size());
				for (WebElement listDateFromDTs : listDateFromDT) {

					dateRangeTest(listDateFromDTs.getText(), fromDate.toString(), Todate.toString());
				}
			} else {
				webActionUtils.info("No records in CBIZ Account");
			}
		} catch (Exception e) {
			webActionUtils.info("No records in CBIZ Account ");
		}

		try {
			webActionUtils.info("Packages Numbers are.. ");
			if (recordLists.size() != 0) {

				for (WebElement recordList : recordLists) {

					webActionUtils.info("Records :" + recordList.getText());
					al1.add(Integer.parseInt(recordList.getText().toString()));
				}
			} else {
				webActionUtils.info("No records ");
			}
		} catch (Exception e) {
			webActionUtils.info("No records");
		}
		return al1;

	}

	public void displayandVerifyDateToClientRecords(String fromDate, String Todate) {
		webActionUtils.waitSleep(3);
		try {
			webActionUtils.info("Checking date to Client filter");
			if (listDateToClients.size() != 0) {
				System.out.println(listDateToClients.size());
				for (WebElement listDateToClient : listDateToClients) {

					dateRangeTest(listDateToClient.getText(), fromDate.toString(), Todate.toString());
				}
			} else {
				webActionUtils.info("No records in CBIZ Account");
			}
		} catch (Exception e) {
			webActionUtils.info("No records in CBIZ Account ");
		}

		try {
			webActionUtils.info("Packages Numbers are.. ");
			if (recordLists.size() != 0) {

				for (WebElement recordList : recordLists) {

					webActionUtils.info("Records :" + recordList.getText());
				}
			} else {
				webActionUtils.info("No records ");
			}
		} catch (Exception e) {
			webActionUtils.info("No records");
		}

	}

	public void displayandVerifyDateToDTRecords(String fromDate, String Todate) {
		webActionUtils.waitSleep(3);
		try {
			webActionUtils.info("Checking Date to DT filter");
			if (listDateToDT.size() != 0) {
				System.out.println(listDateToDT.size());
				for (WebElement listDateToDTs : listDateToDT) {

					dateRangeTest(listDateToDTs.getText(), fromDate.toString(), Todate.toString());
				}
			}
//			else {
//				webActionUtils.info("No records in CBIZ Account");
//			}
		} catch (Exception e) {
			webActionUtils.info("No records in CBIZ Account ");
		}

		try {
			webActionUtils.info("Packages Numbers are.. ");
			if (recordLists.size() != 0) {

				for (WebElement recordList : recordLists) {

					webActionUtils.info("Records :" + recordList.getText());
				}
			} else {
				webActionUtils.info("No records ");
			}
		} catch (Exception e) {
			webActionUtils.info("No records");
		}

	}

	public void displayCBIZAccountRecords() {
		try {

			if (listOfCBIZAccountRecords.size() != 0) {
				System.out.println(listOfCBIZAccountRecords.size());
				for (WebElement CBIZAccountRecords : listOfCBIZAccountRecords) {
					webActionUtils.info("Package ID:" + CBIZAccountRecords.getText() + " is a CBIZ Account Records ");
				}
			}
		} catch (Exception e) {
			webActionUtils.info("catch : No records in CBIZ Account ");
		}
	}

	public void displayCBIZwithAIRLOBAccountRecords() {
		try {
			if (listOfCBIZAIRRecords.size() != 0) {
				System.out.println(listOfCBIZAIRRecords.size());
				for (WebElement CBIZAIRAccountRecords : listOfCBIZAIRRecords) {
					webActionUtils.info(" AIR LOB Package ID :" + CBIZAIRAccountRecords.getText());
				}

			} else {
				webActionUtils.info(" CBIZ- AIR LOB :No records Found ");
			}
		} catch (Exception e) {
			webActionUtils.info(" CBIZ- AIR LOB :No records Found  ");
		}
	}

	public void displayCBIZwithAlpharettaGAFullCheckAccountRecords() {
		// selectPage("218");
		webActionUtils.waitSleep(3);
		try {
			if (listOfCBIZAlpharettaGAFullCheckRecords.size() != 0) {
				System.out.println(listOfCBIZAlpharettaGAFullCheckRecords.size());
				for (WebElement CBIZAlpharettaGAFullCheckRecords : listOfCBIZAlpharettaGAFullCheckRecords) {
					webActionUtils
							.info("Alpharetta GA Full Check Package ID :" + CBIZAlpharettaGAFullCheckRecords.getText());
				}
			} else {
				webActionUtils.info("No records in CBIZ- Alpharetta GA Full Check Account");
			}
		} catch (Exception e) {
			webActionUtils.info("No records in  CBIZ- Alpharetta GA Full Check Account ");
		}
	}

	public void displayListOfContractingBondtRecords() {
		webActionUtils.waitSleep(3);
		try {
			if (listOfContractingBondRecords.size() != 0) {
				System.out.println(listOfContractingBondRecords.size());
				for (WebElement listOfContractingBondRecord : listOfContractingBondRecords) {
					webActionUtils
							.info("Carrier:ContractingBond -- Package ID :" + listOfContractingBondRecord.getText());
				}
			} else {
				webActionUtils.info("No records is in Carrier:ContractingBond");
			}
		} catch (Exception e) {
			webActionUtils.info("No records in Carrier:ContractingBond");
		}
	}

	public void displayListOfDTProcessorRecords() {
		webActionUtils.waitSleep(3);
		try {
			if (listOfDTProcessorRecords.size() != 0) {
				System.out.println(listOfDTProcessorRecords.size());
				for (WebElement listOfDTProcessorRecord : listOfDTProcessorRecords) {
					webActionUtils.info("DT Processor -- Package ID :" + listOfDTProcessorRecord.getText());
				}
			} else {
				webActionUtils.info("No records is in DT Processor ");
			}
		} catch (Exception e) {
			webActionUtils.info("No records is in DT Processor ");
		}
	}

	public ArrayList<Integer> displayRecordsAfterClearingfilters() {
		webActionUtils.waitSleep(10);
		webActionUtils.waitUntilLoadedAndVisibilityOfAllElements(record1Lists);
		ArrayList<Integer> al1 = new ArrayList<Integer>();
		try {
			webActionUtils.info("Packages ID's are.. ");
			if (record1Lists.size() != 0) {

				for (WebElement recordList : record1Lists) {

					webActionUtils.info("Package ID :" + recordList.getText());
					webActionUtils.waitSleep(2);
					al1.add(Integer.parseInt(recordList.getText().toString()));
				}
			} else {
				webActionUtils.info("No Package ID Found ");
			}
		} catch (Exception e) {
			webActionUtils.info("No Package ID Found ");
		}
		return al1;
	}

	public ArrayList<Integer> displayRecordsAvailble() {
		webActionUtils.waitSleep(10);
		webActionUtils.waitUntilLoadedAndVisibilityOfAllElements(record1Lists);
		ArrayList<Integer> al1 = new ArrayList<Integer>();
		try {
			webActionUtils.info("Packages ID's are.. ");
			if (record1Lists.size() != 0) {

				for (WebElement recordList : record1Lists) {

					webActionUtils.info("Package ID :" + recordList.getText());
					webActionUtils.waitSleep(2);
					al1.add(Integer.parseInt(recordList.getText().toString()));
				}
			} else {
				webActionUtils.info("No Package ID Found ");
			}
		} catch (Exception e) {
			webActionUtils.info("No Package ID Found ");
		}
		return al1;
	}

	public void displayStandardLOBAccountRecords() {
		webActionUtils.waitSleep(3);
		try {
			if (listOfStandredLobRecords.size() != 0) {
				System.out.println(listOfStandredLobRecords.size());
				for (WebElement listOfStandredLobRecord : listOfStandredLobRecords) {
					webActionUtils.info("Standred Lob: Propery -- Package ID :" + listOfStandredLobRecord.getText());
				}
			} else {
				webActionUtils.info("No records is in Standred Lob: Propery");
			}
		} catch (Exception e) {
			webActionUtils.info("No records in Standred Lob: Propery");
		}
	}

	public void enterAccountSearch(String text) {
		webActionUtils.info("Enter the " + text + " Account Search");
		accountFilterSearch.clear();
		webActionUtils.setText(accountFilterSearch, text);
	}

	public void enterAddtionalFieldSearchText(String text) {
		webActionUtils.info("Enter the " + text + " Additional Field Filter Search ");
		webActionUtils.waitUntilLoadedAndVisibilityOfElementLocated(AdditionalFieldTxtBx);
		webActionUtils.setText(AdditionalFieldTxtBx, text);
	}

	public void enterAllYourTasksSearchText(String text) {
		webActionUtils.info("Enter the " + text + " All Your Tasks Search");
		webActionUtils.setText(AllYourTasksFilterSearch, text);
		webActionUtils.waitSleep(3);
	}

	public void enterCarrierSearchText(String text) {
		webActionUtils.info("Enter the " + text + " Carrier Filter Search ");
		webActionUtils.waitUntilLoadedAndVisibilityOfElementLocated(CarrierFilterSearch);
		webActionUtils.setText(CarrierFilterSearch, text);
	}

	public void EnterClientDueDate(String clientduedate) {
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		Date date = new Date();
		sdf.setLenient(false);
		String currentDate = sdf.format(date);
		webActionUtils.waitSleep(10);
		webActionUtils.info("Entering ClientDueDate");
		webActionUtils.waitSleep(5);
		clientDueDateTXT.clear();
		webActionUtils.waitSleep(5);
		clientDueDateTXT.sendKeys(currentDate);
		webActionUtils.waitSleep(5);

	}

	public void enterDateFromClientfromTo(String fromDate, String Todate) {

		webActionUtils.info("Enter the Date From Client- From Text Field");
		webActionUtils.setText(DateFromClientFromTxtBx, fromDate);
		webActionUtils.info("Enter the Date From Client- TO Text Field");
		webActionUtils.setText(DateFromClientToTxtBx, Todate);
	}

	public void enterDateFromDT(String fromDate, String Todate) {

		webActionUtils.info("Enter the Date From DT- From Text Field");
		webActionUtils.setText(DateFromDTFromTxtBx, fromDate);
		webActionUtils.info("Enter the Date From DT- TO Text Field");
		webActionUtils.setText(DateFromDTToTxtBx, Todate);
	}

	public void enterDateToClientfromTo(String fromDate, String Todate) {

		webActionUtils.info("Enter the Date To Client- From Text Field");
		webActionUtils.setText(DateToClientFromTxtBx, fromDate);
		webActionUtils.info("Enter the Date To Client- TO Text Field");
		webActionUtils.setText(DateToClientToTxtBx, Todate);
	}

	public void enterDateToDT(String fromDate, String Todate) {

		webActionUtils.info("Enter the Date To DT- From Text Field");
		webActionUtils.setText(DateToDTFromTxtBx, fromDate);
		webActionUtils.info("Enter the Date To DT- TO Text Field");
		webActionUtils.setText(DateToDTTOTxtBx, Todate);
	}

	public void enterDepartmentSearchText(String text) {
		webActionUtils.info("Enter the " + text + " Department Filter Search ");
		webActionUtils.waitUntilLoadedAndVisibilityOfElementLocated(DepartmenFilterSearch);
		webActionUtils.setText(DepartmenFilterSearch, text);
	}

	public void enterDtProcessorSearchText(String text) {
		webActionUtils.info("Enter the " + text + " DT Processor Filter Search ");
		webActionUtils.waitUntilLoadedAndVisibilityOfElementLocated(DtProcessorTxtBx);
		webActionUtils.setText(DtProcessorTxtBx, text);
	}

	public void enterFBEAuditorSearchText(String text) {
		webActionUtils.info("Enter the " + text + " FBE Auditor Search");
		webActionUtils.setText(FBEAuditorFilterSearch, text);
	}

	public void EnterinternalAndClientDate(CharSequence InternalDate, CharSequence clientDate) {
		webActionUtils.info("Entering internal due Date");
		internalDueDateTXT.clear();
		internalDueDateTXT.sendKeys(InternalDate);
		webActionUtils.waitSleep(5);
		webActionUtils.info("Entering ClientDueDate");
		webActionUtils.waitSleep(5);
		clientDueDateTXT.clear();
		webActionUtils.waitSleep(5);
		clientDueDateTXT.sendKeys(clientDate);
		webActionUtils.waitSleep(10);

	}

	public void EnterinternalDate(String internalDueDate) {
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		Date date = new Date();
		sdf.setLenient(false);
		String currentDate = sdf.format(date);
		webActionUtils.waitSleep(10);
		webActionUtils.info("Entering internal due Date");
		internalDueDateTXT.clear();
		internalDueDateTXT.sendKeys(currentDate);
		webActionUtils.waitSleep(5);

	}

	public void EnterinternalDueDate(String internalDate) {
		webActionUtils.info("Entering internal due Date");
		internalDueDateTXT.clear();
		internalDueDateTXT.sendKeys(internalDate);
		webActionUtils.waitSleep(5);

	}

	public void enterLOBSearchText(String text) {
		webActionUtils.info("Enter the " + text + " LOB Search");
		webActionUtils.waitUntilLoadedAndVisibilityOfElementLocated(LOBTxtBx);
		webActionUtils.setText(LOBTxtBx, text);
	}

	public void enterPIAuditorSearchText(String text) {
		webActionUtils.info("Enter the " + text + " PI Auditor Search");
		webActionUtils.setText(PIAuditorFilterSearch, text);
	}

	public void enterPIProcessorSearchText(String text) {
		webActionUtils.info("Enter the " + text + " PI Processor Search");
		webActionUtils.setText(PIProcessorFilterSearch, text);
		webActionUtils.waitSleep(3);
	}

	public void enterRequiredEditPagedetails() {
		webActionUtils.info("Selected PI Processor 1* ");
		webActionUtils.selectByIndex(editPIProcessor1, 25);

		webActionUtils.info("Selecting Carrier");
		webActionUtils.setText(editCarrierTXT, "Contracting");
		webActionUtils.clickElement(editCarrierTXTDD, "Contracting Bond (CBIC)");

		webActionUtils.info("Entering Premium");
		webActionUtils.setText(editPremiumTXT, "5");

		webActionUtils.info("Selecting edit Discrepancies Available DD Option");
		webActionUtils.clickElement(editCarrierTXTRD, "Yes Radio button");
		// selectByIndex(editDiscrepanciesAvailableDD, 1);
	}

	public void EnterRushTextWS() {
		webActionUtils.info("Enter the value for the Rush Text area");
		webActionUtils.setText(RushTxtArea, "RUSH desc");
	}

	public void enterStandardLOBSearchText(String text) {
		webActionUtils.info("Enter the " + text + " Standard LOB Filter Search ");
		webActionUtils.waitUntilLoadedAndVisibilityOfElementLocated(StandredLobFilterSearch);
		webActionUtils.setText(StandredLobFilterSearch, text);
	}

	public void ExportIRfile() {
		webActionUtils.info("Click on the Export IR file ");
		webActionUtils.clickElement(IrExcelExportBTN, "Ir Excel Export Button");
		webActionUtils.waitSleep(10);
	}

	public String getDateandSetDate(String date) {

		String substr1 = date.substring(0, date.length() - 2);
		String substr2 = date.substring(date.length() - 2);
		if (substr2.equals("21")) {
			substr2 = "2021";
		} else if (substr2.equals("22")) {
			substr2 = "2022";
		}
		String dd = substr1 + substr2;
		// System.out.println("Changed format date: "+dd);
		return dd;
	}

	public WebElement getPackageStatusElement(String pkgid) {
		String xpathexp = "//td[@id='icon" + pkgid + "']//a[@id='packageID']/../..//td[10]";
		return driver.findElement(By.xpath(xpathexp));
	}

	public void name1() {

		webActionUtils.clickElement(Checkboxe, "0Checkboxe");
		webActionUtils.waitSleep(1);
		webActionUtils.clickElement(btnEdit, "btnEdit");
		webActionUtils.waitSleep(10);
		webActionUtils.selectByValue(Processor, "17428");
		webActionUtils.waitSleep(1);
		webActionUtils.clickElement(Carrier, "Carrier");
		webActionUtils.setText(Carrier, "CBIC");
		webActionUtils.clickElement(id11, "id11");
		webActionUtils.waitSleep(3);
		webActionUtils.clickElement(Premium, "Premium");
		webActionUtils.setText(Premium, "1");
		webActionUtils.clickElement(SendEmail, "SendEmail");
		webActionUtils.waitSleep(15);
	}

	public void saveAuditor() {
		webActionUtils.info("Click on Auditor - Save Button");
		webActionUtils.clickElement(auditorsaveBTN, "Auditor Save Button");
		webActionUtils.waitSleep(10);
		webActionUtils.waitForPageLoad();

	}

	public void saveProcessor() {
		webActionUtils.info("Click on Auditor - Save Button");
		webActionUtils.clickElement(processorSaveBTN, "Processor Save Button");
		webActionUtils.waitSleep(10);
	}

	public void searchDeparment(String search) {
		webActionUtils.clearText(DepartmenFilterSearch);
		webActionUtils.setText(DepartmenFilterSearch, search);
		webActionUtils.clickElement(departmentSelectAllCheckBox, "");
		webActionUtils.waitSleep(20);
	}

	public void searchNameInsured(String text) {
		webActionUtils.info("Searching Name Insured");
		webActionUtils.waitForPageLoad();
		webActionUtils.waitUntilLoadedAndElementClickable(nameInsuredTxtBx);
		webActionUtils.clickElement(nameInsuredTxtBx, "Name Insured fields");
		webActionUtils.setText(nameInsuredTxtBx, text);
		webActionUtils.waitForPageLoad();
		// webActionUtils.waitSleep(5);
	}

	public void searchPolicyNumber(String text) {
		webActionUtils.info("searching Policy Number ");
		webActionUtils.waitUntilLoadedAndElementClickable(policyTxtBx);
		webActionUtils.clickElement(policyTxtBx, "PolicyNumber fields");
		webActionUtils.setText(policyTxtBx, text);
	}

	public void searchSerialNumber(String serialNumber) {
		webActionUtils.info("Searching Serial Number");
		webActionUtils.waitUntilLoadedAndElementClickable(serialTxtBx);
		webActionUtils.clickElement(serialTxtBx, "Serial field");
		webActionUtils.setText(serialTxtBx, serialNumber);
	}

	public void searchTaskDescription(String text) {
		webActionUtils.info("Searching Task Description");
		webActionUtils.waitUntilLoadedAndElementClickable(taskDescriptionTxtBx);
		webActionUtils.clickElement(taskDescriptionTxtBx, "task Description fields");
		webActionUtils.setText(taskDescriptionTxtBx, text);
		webActionUtils.waitForPageLoad();
		webActionUtils.waitSleep(5);
	}

	public void selectAllAccount() {
		webActionUtils.info("Click on Select all Account'");
		webActionUtils.waitSleep(3);
		webActionUtils.clickElement(selectallAccount, "Select All checkbox In 'Account '");
		webActionUtils.info("select all Account checkbox checked");
	}

	public void selectAllCheckboxAllYourTasks() {
		webActionUtils.info("Click on Select all checkbox In 'All Your Tasks' Filter");
		webActionUtils.waitSleep(3);
		webActionUtils.clickElement(selectallAllYourTasks, "Select All checkbox In 'All Your Tasks'");
		webActionUtils.info("select all checkbox checked");
	}

	public void selectAllCheckboxInDepartment() {
		webActionUtils.info("Click on Select all checkbox In 'Department'");
		webActionUtils.waitSleep(3);
		webActionUtils.clickElement(selectallAllDepartments, "Select All checkbox In 'Department'");
		webActionUtils.info("select all checkbox checked");
	}

	public void selectAllCheckboxInFBEAuditor() {
		webActionUtils.info("Click on Select all checkbox In FBE Auditor ");
		webActionUtils.clickElement(selectallFBEProcessor, "Select All checkbox In FBE Auditor ");
		webActionUtils.info("select all checkbox checked");
	}

	public void selectAllCheckboxInPIProcessor() {
		webActionUtils.info("Click on Select all checkbox In Pi Processor ");
		webActionUtils.waitSleep(3);
		webActionUtils.clickElement(selectallPIProcessor, "Select All checkbox In PI Processor");
		webActionUtils.info("select all checkbox checked");
	}

	public void selectAllCheckboxInStandredLob() {
		webActionUtils.info("Click on Select all checkbox In 'Standred Lob'");
		webActionUtils.waitSleep(3);
		webActionUtils.clickElement(selectallStandardLOB, "Select All checkbox In 'Standred Lob'");
		webActionUtils.info("select all checkbox checked");
	}

	public void selectCBIZAccount() {
		webActionUtils.info("Select CBIZ Account");
		webActionUtils.clickElement(selectCBIZFilterAccount, "CBIZ Account");
		webActionUtils.info("CBIZ Account is Selected");
	}

	public void selectDateCreated1() {
		webActionUtils.info("Date created 1 year");
		webActionUtils.clickElement(defaultDateCreated, "element");
		webActionUtils.waitSleep(10);
		webActionUtils.selectByValue(selectDateCreated, "1");
		webActionUtils.waitSleep(10);
	}

	public void selectDateCreated2() {
		webActionUtils.info("Date created 2 year");

		// webActionUtils.waitInVisibilityOfElementLocated(DateCreated);
		webActionUtils.waitSleep(10);
		webActionUtils.selectByValue(selectDateCreated, "2");
		webActionUtils.waitSleep(10);
	}

	public void selectDateCreated3() {
		webActionUtils.info("Date created 3 year");

		// webActionUtils.waitInVisibilityOfElementLocated(DateCreated);
		webActionUtils.waitSleep(10);
		webActionUtils.selectByValue(selectDateCreated, "3");
		webActionUtils.waitSleep(10);
	}

	public void selectDateCreated3more() {
		// webActionUtils.waitInVisibilityOfElementLocated(DateCreated);
		// webActionUtils.waitSleep(3);
		webActionUtils.waitUntilLoadedAndVisibilityOfElementLocated(selectDateCreated);
		webActionUtils.selectByValue(selectDateCreated, "0");
		webActionUtils.waitSleep(3);
	}

	public void selectFirstRecordCheckBox(String nameInsured) {
		webActionUtils.info("Click on select First Record CheckBox");
		//
		webActionUtils.waitSleep(10);
		webActionUtils.waitForPageLoad();
		webActionUtils.waitSleep(10);
		try {
			webActionUtils.waitUntilLoadedAndVisibilityOfElementLocated(firstRecordChkBx);
			if (firstRecordChkBx.isDisplayed()) {
				webActionUtils.clickElement(firstRecordChkBx, "Select CheckBox");
			}

		} catch (Exception e) {
			webActionUtils.info("Records are not available : Retrying");
			webActionUtils.refreshPage();
			searchNameInsured(nameInsured);
			clickStatus();
			clickSelectAll();
			clickSearchButton();
		}
	}

	public void selectFirstRecordCheckBox() {
		webActionUtils.info("Click on select First Record CheckBox");
		webActionUtils.waitForPageLoad();
		webActionUtils.clickElement(firstRecordChkBx1, "Select CheckBox");
//			webActionUtils.waitSleep(4);
//			webActionUtils.waitSleep(4);

		try {
			// Thread.sleep(4);
			// webActionUtils.waitForPageLoad();
			// Thread.sleep(10);
			// webActionUtils.waitUntilLoadedAndPresenceOfElementLocated(firstRecordChkBx);
			// webActionUtils.waitUntilLoadedAndVisibilityOfElementLocated(firstRecordChkBx);

//				if (firstRecordChkBx.isDisplayed()) {
//					webActionUtils.clickElement(firstRecordChkBx, "Select CheckBox");
//				}

		} catch (Exception e) {
			webActionUtils.info("Records are not available : Retrying");
			e.printStackTrace();
		}
	}

	public void selectFirstRecordCheckBox1(String str) {
		webActionUtils.info("Click on select First Record CheckBox");
		try {
			webActionUtils.waitSleep(10);
			webActionUtils.waitForPageLoad();
			webActionUtils.waitUntilLoadedAndVisibilityOfElementLocated(getFirstElement(str));
			System.out.println(getFirstElement(str));
			if (getFirstElement(str).isDisplayed()) {
				webActionUtils.clickElement(getFirstElement(str), "Select CheckBox");
			}

		} catch (Exception e) {
			webActionUtils.info("Records are not available");
			e.printStackTrace();
			Assert.fail();

		}

	}

	public void selectNeedAuditPIAuditor() {
		webActionUtils.info("Select Need AudiT PI Auditor text");
		webActionUtils.clickElement(selectNeedAuditFilter, "select PI Auditor Account");
		webActionUtils.info("Need Audit PIAuditor is Selected");
	}

	public void selectPage(String text) {

		webActionUtils.setText(specificPageNoTxtBx, text);
		webActionUtils.clickElement(goBTN, "GO Button");
	}

	public void selectSCMAccount() {
		webActionUtils.info("Select SCM Account");
		selectCBIZFilterAccount.clear();
		webActionUtils.clickElement(selectSCMFilterAccount, "SCM Account");
		webActionUtils.info("SCM Account is Selected");
	}

	public void selectSecondCheckbox() {
		webActionUtils.info("Select Merged record checkbox");
		webActionUtils.clickElement(recordCheckbox2ndrow, "Merged Checkbox");
	}

	public void selectTodaysDate() {
		webActionUtils.info("select todays date ");
		webActionUtils.waitForPageLoad();
		webActionUtils.waitSleep(3);
		webActionUtils.waitUntilLoadedAndElementClickable(EditDateToClientIcon);
		webActionUtils.clickElement(EditDateToClientval, "");
		webActionUtils.info(" todays date is selected ");
		webActionUtils.clickElement(selectDate, "date seleted");
		webActionUtils.waitForPageLoad();
		webActionUtils.clickElement(DateToClientSave, "DateTo Client Save button");
		webActionUtils.pass("TC passed date to Client chnaged to todays date ");
	}

	public void unMergedbutton() {
		webActionUtils.info("Select Merged record checkbox");

		try {

			webActionUtils.clickElement(unMergeBTN, "Un-Merged Checkbox");
			webActionUtils.waitSleep(10);
			webActionUtils.pass("TC pass :Packages are unmerged in the Manage page");

		} catch (Exception e) {
			webActionUtils.fail("TC fail");

		}

	}

	/**
	 * chaithra
	 * 
	 * @param departmentName
	 * @return
	 */
	public void validateIRFieldvalues(String departmentName) {
		webActionUtils.info("Selecting Department / Program DropDown value");
		webActionUtils.selectByVisibleText(selectDepartmentDPDN, departmentName);
		webActionUtils.pass("Selected Department Name : " + departmentName + "");
		webActionUtils.info("Entering Date From Client");
		// webActionUtils.waitSleep(5);
		webActionUtils.clickElement(dateFromClientTXT, "date From Client TXT");
		webActionUtils.waitSleep(5);
		webActionUtils.waitUntilLoadedAndElementClickable(highlightedDate);
		webActionUtils.verifyElementIsPresent(highlightedDate);
		webActionUtils.clickElement(highlightedDate, "high lighted Date");
		// webActionUtils.waitSleep(5);

	}

	public void verfiyDateCreatedDefaultedToOptions() {
		try {
			webActionUtils.info("Verify Date created seleted option to 1st year ");
			webActionUtils.waitSleep(5);
			// webActionUtils.selectByValue(selectDateCreated, "1");

			if (webActionUtils.getAllSelectOptions(selectDateCreated).get(1).getText().toString().equals("1 Year")) {
				webActionUtils.pass("Date created seleted option to 1st year ");

			} else {
				System.out.println(webActionUtils.getAllSelectOptions(selectDateCreated).get(0).getText().toString());
				webActionUtils.fail("error ");

			}

			clickSearchButton();
			displayCBIZAccountRecords();
			webActionUtils.waitSleep(3);
		} catch (Exception e) {

			webActionUtils.info("No records");
		}
	}

	public void verfiyDateCreatedOptions() {
		try {
			webActionUtils.info("Selecting Date created :1st year option");
			webActionUtils.selectByValue(selectDateCreated, "1");
			clickSearchButton();
			webActionUtils.waitForPageLoad();
			webActionUtils.waitSleep(10);
			displayCBIZAccountRecords();

			webActionUtils.info("Selecting Date created :2nd year option");
			webActionUtils.selectByValue(selectDateCreated, "2");
			webActionUtils.waitForPageLoad();
			clickSearchButton();
			webActionUtils.waitSleep(10);
			displayCBIZAccountRecords();

			webActionUtils.info("Selecting Date created :3rd year option");
			webActionUtils.selectByValue(selectDateCreated, "3");
			clickSearchButton();
			webActionUtils.waitForPageLoad();
			webActionUtils.waitSleep(10);
			displayCBIZAccountRecords();

			webActionUtils.info("Selecting Date created :3> year option");
			webActionUtils.selectByValue(selectDateCreated, "0");
			clickSearchButton();
			webActionUtils.waitForPageLoad();
			webActionUtils.waitSleep(10);
			displayCBIZAccountRecords();

		} catch (Exception e) {

			webActionUtils.info("No records");
			// e.printStackTrace();
		}

	}

	public void verifyAllSelectedOffice() {
		webActionUtils.waitSleep(5);
		webActionUtils.info("Display all available Package");
		webActionUtils.waitSleep(3);
		try {
			if (allOfficeCheckbox.size() != 0) {
				int i = allOfficeCheckbox.size();
				System.out.println(i);

				for (WebElement allOfficeNameslist : allOfficeNames) {
					webActionUtils.info("OFiice name is " + allOfficeNameslist.getText());
				}

				for (WebElement listOfOffice : allOfficeCheckbox) {
					webActionUtils.pass("Is checkbox selcted " + listOfOffice.isSelected());
				}
			} else {
				webActionUtils.info("No office found1");
			}
		} catch (Exception e) {
			webActionUtils.info("No office found2");
		}
	}

	public void verifyApiCreatedButtonIsNotPresent(String text) {
		webActionUtils.info("Verification of the Api Created Option is not Present");

		webActionUtils.waitUntilLoadedAndVisibilityOfAllElements(getAllStatus);
		List<String> aList = new ArrayList<String>();

		try {

			if (getAllStatus.size() != 0) {
				for (WebElement recordList : getAllStatus) {
					aList.add(recordList.getText().toString());

				}
			}

			if (aList.contains(text)) {

			} else {
				System.out.println(aList);
				webActionUtils.info("Api Created Option is not Present: TC passed");
			}

		} catch (Exception e) {
			webActionUtils.pass("Api Created Option is  Present: TC Failed");
		}
	}

	public void verifyApiReassignedButtonIsNotPresent(String text) {
		webActionUtils.info("Verification of the Api Reassigned Option is not Present");

		webActionUtils.waitUntilLoadedAndVisibilityOfAllElements(getAllStatus);
		List<String> aList = new ArrayList<String>();

		try {

			if (getAllStatus.size() != 0) {
				for (WebElement recordList : getAllStatus) {
					aList.add(recordList.getText().toString());
				}
			}

			if (aList.contains(text)) {
			} else {
				System.out.println(aList);
				webActionUtils.info("Api Reassigned Option is not Present: TC passed");
			}

		} catch (Exception e) {
			webActionUtils.pass("Api Reassigned Option is  Present: TC Failed");
		}
	}

	public void verifyApiReassignedButtonIsPresent(String text) {
		webActionUtils.info("Verification of the Api Reassigned Option is Present");

		webActionUtils.waitUntilLoadedAndVisibilityOfAllElements(getAllStatus);
		List<String> aList = new ArrayList<String>();

		try {

			if (getAllStatus.size() != 0) {
				for (WebElement recordList : getAllStatus) {
					aList.add(recordList.getText().toString());

				}
			}

			if (aList.contains(text)) {
				webActionUtils.info("Api Reassigned Option is  Present: TC passed");
			} else {
				System.out.println(aList);
			}

		} catch (Exception e) {
			webActionUtils.pass("Api Reassigned Option is not Present: TC Failed");
		}
	}

	public void verifyApiSavedButtonIsNotPresent(String text) {
		webActionUtils.info("Verification of the Api Saved Option is not Present");

		webActionUtils.waitUntilLoadedAndVisibilityOfAllElements(getAllStatus);
		List<String> aList = new ArrayList<String>();

		try {

			if (getAllStatus.size() != 0) {
				for (WebElement recordList : getAllStatus) {
					aList.add(recordList.getText().toString());

				}
			}

			if (aList.contains(text)) {

			} else {
				System.out.println(aList);
				webActionUtils.info("Api Saved Option is not Present: TC passed");
			}

		} catch (Exception e) {
			webActionUtils.pass("Api Saved Option is  Present: TC Failed");
		}
	}

	public void verifyAuditor(String piAuditor) {

		webActionUtils.info("Verfiy  pi Auditor " + piAuditor);

//		Wait<WebDriver> wait = new WebDriverWait(driver, Duration.ofSeconds(30));
//		wait.until(ExpectedConditions.textToBePresentInElementValue(piAuditorRecordTXT, piAuditor));
		webActionUtils.waitUntilLoadedAndVisibilityOfElementLocated(piAuditorRecordTXT);
//		webActionUtils.waitUntilLoadedAndTextToBePresentInElementLocated(piAuditorRecordTXT, piAuditor);
		webActionUtils.verifyText(piAuditorRecordTXT.getText(), piAuditor);
	}

	public void verifyBlankDepartment() {
		webActionUtils.info("verfiy no department are displayed ");
		webActionUtils.verifyElementIsPresent(NoMatchesFound);
		webActionUtils.pass("no Departments are displayed Blank");

	}

	public void verifyCancelStatus() {

		webActionUtils.waitSleep(30);
		webActionUtils.waitUntilLoadedAndVisibilityOfElementLocated(CancelledRecords);
		System.out.println(CancelledRecords.getText().toString());
		webActionUtils.verifyText(CancelledRecords.getText().toString(), "Cancelled");
		webActionUtils.pass("verified Cancel Status of Package");

	}

	public void verifyDatefromDTClearedFilter() {
		webActionUtils.info("Verifying Date from DT Date fields are Cleared");
		webActionUtils.waitSleep(5);
		try {
			if (DateFromDTFromTxtBx.getAttribute("value").isEmpty() == DateToDTFromTxtBx.getAttribute("value")
					.isEmpty()) {
				webActionUtils.info(
						"Date From DT From Text Box : Is Empty" + DateFromDTFromTxtBx.getAttribute("value").isEmpty());
				webActionUtils.info(
						"Date From DT TO   Text Box : Is Empty" + DateToDTFromTxtBx.getAttribute("value").isEmpty());
				webActionUtils.pass("Clear filter is working properly");
			} else {
				webActionUtils.info("Verification failed");
			}
		} catch (Exception e) {
			webActionUtils.fail("Verification failed");
		}

	}

	public void verifyDownloadIconPresentNextToPreviewIcon() {
		webActionUtils.info("Verify Download Icon Present Next To Preview Icon");
		try {
			webActionUtils.scrolltoEnd();
			webActionUtils.verifyElementIsPresent(customMSGDownloadIcon);
			webActionUtils.pass("TC is passed : DownloadIcon Present Next To Preview Icon");
		} catch (Exception e) {
			webActionUtils.fail("TC is Failed");
		}

	}

	public void verifyDownloadoriginalFile() {
		webActionUtils.info("Verifying File is Downloaded from Export");
		String DOWNLOAD_FOLDER_PATH1 = System.getProperty("user.dir") + File.separator + "DownloadedFiles"
				+ File.separator;
		webActionUtils.waitSleep(15);
		File filesInDownloadFolder = new File(DOWNLOAD_FOLDER_PATH1);
		try {
			for (File file1 : filesInDownloadFolder.listFiles()) {
				webActionUtils.waitSleep(5);
				webActionUtils.pass(file1.getName());
				// System.out.println(file1.getName());
			}
			Assert.assertTrue(filesInDownloadFolder.listFiles().length > 0, "TC failed");
		} catch (Exception e) {
			webActionUtils.fail("Verifying failed");
		}
	}

	public void verifyEditButtonEnabled() {
		webActionUtils.info("Verification of the Edit Button is Enabled");

		if (btnEdit.isEnabled()) {
			webActionUtils.info("Edit button is Enabled :TC Failed");
		} else {
			webActionUtils.pass("Edit button is Disabled :TC Passed");
		}
	}

	public void verifyEditpage(String pp1) {
		webActionUtils.info("Verify Edit Page");
		webActionUtils.waitUntilLoadedAndVisibilityOfElementLocated(editPackTxt);
		webActionUtils.waitUntilLoadedAndTextToBePresentInElementLocated(editPackTxt, "Transaction Details");
		webActionUtils.verifyElementContains(editPackTxt, "Transaction Details");
	}

	public void verifyExportFileDownloaded() {
		webActionUtils.info("Verifying File is Downloaded from Export");
		// webActionUtils.waitForPageLoad();
		// webActionUtils.waitSleep(10);
		// webActionUtils.waitSleep(5);
		//
		//
		// HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
		// chromePrefs.put("profile.default_content_settings.popups", 0);
		// chromePrefs.put("download.default_directory",
		// System.getProperty("user.dir"));
		// ChromeOptions options = new ChromeOptions();
		// options.setExperimentalOption("prefs", chromePrefs);
		// WebDriver dri = webActionUtils.getdriver();
		// dri= new ChromeDriver(options);
		// dri.getTitle();

		try {
			File f = new File(System.getProperty("user.home") + "/Downloads/");
			// File f = new File("./DownloadedFiles");

			String filenames[] = f.list();
			for (String filename : filenames) {
				webActionUtils.pass(filename);
			}
		} catch (Exception e) {
			webActionUtils.fail("Verifying failed");
		}
	}

	public void verifyExportFileDownloaded1() {
		webActionUtils.info("Verifying File is Downloaded from Export");
		String DOWNLOAD_FOLDER_PATH = System.getProperty("user.dir") + File.separator + "DownloadedFiles"
				+ File.separator;
		webActionUtils.waitSleep(20);
		File filesInDownloadFolder = new File(DOWNLOAD_FOLDER_PATH);
		try {
			for (File file1 : filesInDownloadFolder.listFiles()) {
				webActionUtils.waitSleep(5);
				webActionUtils.pass(file1.getName());
				// System.out.println(file1.getName());
			}
			Assert.assertTrue(filesInDownloadFolder.listFiles().length > 0, "TC failed");
		} catch (Exception e) {
			webActionUtils.fail("Verifying failed");
		}
	}

	public void verifyFBEAuditor(String piFBEAuditor) {

		webActionUtils.waitUntilLoadedAndVisibilityOfElementLocated(piFBEAuditorRecordTXT);
		webActionUtils.waitUntilLoadedAndTextToBePresentInElementLocated(piFBEAuditorRecordTXT, piFBEAuditor);
		webActionUtils.verifyText(piFBEAuditorRecordTXT.getText(), piFBEAuditor);
	}

	public void verifyFileTreeElementCheck() {
		webActionUtils.info("Verify File Tree Email Check ");

		// webActionUtils.clickElement(plus1, "plus1 Element");
		String a = "$(" + "#tree" + ").jstree(true).open_all()";
		webActionUtils.jsexecutor(a);
		webActionUtils.waitSleep(60);
		webActionUtils.pass("File tree is there in the Validate IR page");

	}

	public void verifyFileTreeLoaded() {
		webActionUtils.info("Verify File Tree loaded ");

		webActionUtils.waitForPageLoad();
		webActionUtils.waitSleep(3);
		webActionUtils.verifyElementIsPresent(fileTree1);
		webActionUtils.pass("File tree is there in the Validate IR page");

	}

	public void verifyInternalAndClientDuedate() {

		try {
			webActionUtils.info("Verification of Internal Due");
			SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
			Date date = new Date();
			sdf.setLenient(false);
			String currentDate = sdf.format(date);
			webActionUtils.waitSleep(10);
			String InternalDate = internalDueDate.getAttribute("value");
			webActionUtils.waitSleep(5);
			String clientDate = clientDueDate.getAttribute("value");
			webActionUtils.info("Internal Due : " + InternalDate);
			webActionUtils.info("Client Due Date : " + clientDate);
			webActionUtils.info("Current Date : " + currentDate);
			webActionUtils.info("Internal Due Date after Verification : " + InternalDate);
			webActionUtils.waitSleep(10);
			try {
				if (InternalDate.equals(clientDate) && InternalDate.equals(currentDate)) {
					webActionUtils.info(
							"Verification of Internal Due Date when Internal Due date is Equal to Client Due Date and Internal Due Date is Equal to Current Date");

					webActionUtils.info("Internal Due : " + InternalDate);
					webActionUtils.info("Client Due Date : " + clientDate);
					webActionUtils.info("Current Date : " + currentDate);
					webActionUtils.info("Internal Due Date after Verification : " + InternalDate);

					if (InternalDate.equals(InternalDate)) {
						webActionUtils.pass(
								"Verification of Un-change of Internal Due Date when Internal Due date is Equal to Client Due Date and Internal Due Date is Equal to Current Date : Passed ");
					} else {
						webActionUtils.fail(
								"Verification of Un-change of Internal Due Date when Internal Due date is Equal to Client Due Date and Internal Due Date is less than Current Date : Failed ");
					}
				} else if (InternalDate.compareTo(clientDate) == 0 && InternalDate.compareTo(currentDate) < 0) {
					webActionUtils.info(
							"Verification of Internal Due Date when Internal Due Date is Equal to Client Due Date and Internal Due Date is Less than Current Date");

					webActionUtils.info("Internal Due Date : " + InternalDate);
					webActionUtils.info("Client Due Date : " + clientDate);
					webActionUtils.info("Current Date : " + currentDate);
					webActionUtils.info("Internal Due Date after Verification : " + InternalDate);

					if (InternalDate.equals(InternalDate)) {
						webActionUtils.pass(
								"Verification of Un-change of Internal Due Date when Internal Due date is Equal to Client Due Date and Internal Due Date is Less than Current Date : Passed ");
					} else {
						webActionUtils.fail(
								"Verification of Un-change of Internal Due Date when Internal Due date is Equal to Client Due Date and Internal Due Date is Less than Current Date : Failed ");
					}
				} else if (InternalDate.compareTo(clientDate) == 0 && InternalDate.compareTo(currentDate) > 0) {
					webActionUtils.info(
							"Verification of Internal Due Date when Internal Due date is Equal to Client Due Date and Internal Due Date is Greater than Current Date");

					Calendar calendar = Calendar.getInstance();
					Date date1 = new SimpleDateFormat("MM/dd/yyyy").parse((String) InternalDate);
					calendar.setTime(date1);
					calendar.add(Calendar.DAY_OF_MONTH, -1);
					Date date2 = calendar.getTime();
					String internalDueDates = sdf.format(date2);

					webActionUtils.info("Internal Due Date : " + InternalDate);
					webActionUtils.info("Client Due Date : " + clientDate);
					webActionUtils.info("Current Date : " + currentDate);
					webActionUtils.info("Internal Due Date after Verification : " + internalDueDates);

					if (InternalDate.compareTo((String) InternalDate) == 0) {
						webActionUtils.pass(
								"Verification of Internal Due Date changing to previous date when Internal Due date is Equal to Client Due Date and Internal Due Date is Greater than Current Date : Passed  ");
					} else {
						webActionUtils.fail(
								"Verification of Internal Due Date changing to previous date when Internal Due date is Equal to Client Due Date and Internal Due Date is Greater than Current Date : Failed  ");
					}
				}

			}

			catch (Exception e) {
				Reporter.log(e.getMessage(), true);
				webActionUtils.fail("" + e.getMessage());
			}
		} catch (Exception e) {
			Reporter.log(e.getMessage(), true);
			webActionUtils.fail("" + e.getMessage());
		}
	}

	/*
	 * Varshitha Yadav
	 */
	public void verifyInternalDueDateInValidateIR(String departmentName) {
		try {
			webActionUtils.waitSleep(10);
			webActionUtils.info("Selecting Department / Program DropDown value");
			webActionUtils.selectByVisibleText(selectDepartmentDPDN, departmentName);
			webActionUtils.pass("Selected Department Name : " + departmentName + "");

			String beforeInternalDate = internalDueDate.getAttribute("value");

			webActionUtils.info("Entering Date From Client");
			webActionUtils.clickElement(dateFromClientTXT, "date From Client TXT");
			webActionUtils.waitUntilLoadedAndElementClickable(highlightedDate);
			webActionUtils.verifyElementIsPresent(highlightedDate);
			webActionUtils.clickElement(highlightedDate, "high lighted Date");
			webActionUtils.waitSleep(10);

			String clientDate = clientDueDateTXT.getAttribute("value");
			String afterInternalDate = internalDueDate.getAttribute("value");

			if (beforeInternalDate.compareTo(clientDate) != 0) {
				webActionUtils.info(
						"Verification of Internal Due Date when Internal Due Date is Not Equal to Client Due Date in Validate IR Package");

				webActionUtils.info("Internal Due Date before entering Client Due Date : " + beforeInternalDate);
				webActionUtils.info("Entered Client Due Date : " + clientDate);
				webActionUtils.info("Internal Due Date after entering Client Due Date : " + afterInternalDate);

				if (afterInternalDate.equals(beforeInternalDate)) {
					webActionUtils.pass(
							"Verification of Un-change of Internal Due Date when Internal Due date is Not Equal to Client Due Date in Validate IR Package : Passed ");
				} else {
					webActionUtils.fail(
							"Verification of Un-change of Internal Due Date when Internal Due date is Not Equal to Client Due Date in Validate IR Package  : Failed ");
				}
			}
		} catch (Exception e) {
			Reporter.log(e.getMessage(), true);
			webActionUtils.fail("" + e.getMessage());
		}
	}

	public void verifyManagePage(String title) {
		webActionUtils.info("Verifying Manage Page Title");
		// webActionUtils.waitSleep(4);
		webActionUtils.waitUntilLoadedAndPresenceOfAllElementsLocated(SelectOffice1);
		webActionUtils.waitUntilLoadedAndVisibilityOfElementLocated(SelectOffice);
		webActionUtils.waitForPageLoad();
		webActionUtils.verifyTitle(title);
	}

	public void verifyMergedStatus() {
		if (webActionUtils.isElementDisplayed(plusIcon)) {
			webActionUtils.clickElement(plusIcon, "Plus Icon");
			// webActionUtils.clickElement(plusIcon, "Plus Icon");
			webActionUtils
					.info("PackageID : " + package1 + " Package Status " + getPackageStatusElement(package1).getText());
			webActionUtils
					.info("PackageID : " + package2 + " Package Status " + getPackageStatusElement(package2).getText());

			if ((getPackageStatusElement(package1).getText()).equals("Merged")) {
				webActionUtils.info(
						"PackageID : " + package1 + "Package Status " + getPackageStatusElement(package1).getText());
				webActionUtils.pass("Test case Passed");
			} else if (getPackageStatusElement(package2).getText().equals("Merged")) {
				webActionUtils.info(
						"PackageID : " + package2 + "Package Status " + getPackageStatusElement(package2).getText());
				webActionUtils.pass("Test case Passed");
			} else {

				webActionUtils.fail("Test case Failed");
			}

		}
	}

	public void verifyMSGDownload() {
		webActionUtils.info("Verify Download Files");
		try {
			webActionUtils.scrolltoEnd();
			webActionUtils.clickElement(customMSGDownloadIcon, "Download Icon");
			webActionUtils.waitSleep(10);
			webActionUtils.pass("TC is passed : DownloadIcon Present Next To Preview Icon");

		} catch (Exception e) {
			webActionUtils.pass("TC is Failed");
		}
	}

	public void verifyNameInsured(String nameInsured) {
		webActionUtils.waitSleep(5);

		webActionUtils.waitUntilLoadedAndVisibilityOfElementLocated(nameInsuredTXT2);
		webActionUtils.waitUntilLoadedAndTextToBePresentInElementLocated(nameInsuredTXT2, nameInsured);
		Wait<WebDriver> wait = new WebDriverWait(driver, Duration.ofSeconds(100));
		wait.until(ExpectedConditions.visibilityOf(nameInsuredTXT2));
		webActionUtils.verifyText(nameInsuredTXT2.getText(), nameInsured);
	}

	public void verifyNameInsuredCust(String nameInsured) {
		webActionUtils.waitSleep(5);
		webActionUtils.waitUntilLoadedAndVisibilityOfElementLocated(nameInsuredTXT);
		webActionUtils.waitUntilLoadedAndTextToBePresentInElementLocated(nameInsuredTXT, nameInsured);
		webActionUtils.verifyText(nameInsuredTXT.getText(), nameInsured);
	}

	public void verifyNOdepartment() {
		webActionUtils.info("Verify No Department is displayed");

		try {
			webActionUtils.verifyElementIsPresent(NoMatchesFound);
			webActionUtils.pass("Pass");
		} catch (Exception e) {
			webActionUtils.fail("Fail");
		}

	}

	public void verifyPackageStatusAfterMerge(String Package1, String Package2) {
		webActionUtils.info("Verify Package Status After Merge");
		webActionUtils.waitSleep(3);		String PStatus1 = getPackageStatusElement(Package1).getText();
		String PStatus2 = getPackageStatusElement(Package1).getText();
		String PStatus3 = "Merged";
		System.out.println(PStatus1);
		System.out.println(PStatus2);
		System.out.println(PStatus3);
		if (!PStatus1.equals(PStatus3) || !PStatus2.equals(PStatus3)) {
			webActionUtils
					.pass("PackageID : " + package1 + " Package Status " + getPackageStatusElement(Package1).getText());
			webActionUtils
					.pass("PackageID : " + package2 + " Package Status " + getPackageStatusElement(Package2).getText());
		} else {
			webActionUtils.fail("TC Failed");
		}
	}

	public void verifyPackageStatusAfterUnmerge(String Package1, String Package2) {
		webActionUtils.info("Verify Package Status After Unmerge");

		String PStatus1 = getPackageStatusElement(Package1).getText();
		String PStatus2 = getPackageStatusElement(Package2).getText();
		String PStatus3 = "Merged";

		if (!PStatus1.equals(PStatus3) && !PStatus2.equals(PStatus3)) {
			webActionUtils
					.pass("PackageID : " + package1 + " Package Status " + getPackageStatusElement(Package1).getText());
			webActionUtils
					.pass("PackageID : " + package2 + " Package Status " + getPackageStatusElement(Package2).getText());
		} else {
			webActionUtils.fail("TC Failed");
		}
	}

	// public void clickDownloadFinishedTemplate() {
	// webActionUtils.info("click on the Download Finished Template icon ");
	// webActionUtils.waitForPageLoad();
	// webActionUtils.waitSleep(3);
	// webActionUtils.waitUntilLoadedAndElementClickable(DownloadFinishedChecklistsIcon);
	// webActionUtils.clickElement(DownloadFinishedChecklistsIcon, "Download
	// Finished checklist Icon");
	// webActionUtils.waitForPageLoad();
	// webActionUtils.waitSleep(10);
	// webActionUtils.pass("File is download locally");
	// }

	public void verifyPaginationWithgoField() {
		webActionUtils.info("verify Pagination with goto Available");
		webActionUtils.isElementDisplayed(specificPageNoTxtBx);
		webActionUtils.isElementDisplayed(goBTN);
		webActionUtils.isElementDisplayed(FullPagination);
		webActionUtils.isElementDisplayed(getAllPages);
		webActionUtils.pass("All Pagination elements are available");

	}

	public void verifyPolicyNumber(String policyNumber) {
		webActionUtils.waitSleep(5);
		webActionUtils.waitUntilLoadedAndVisibilityOfElementLocated(policyNumberTXT);
		webActionUtils.waitUntilLoadedAndTextToBePresentInElementLocated(policyNumberTXT, policyNumber);
		webActionUtils.verifyText(policyNumberTXT.getText(), policyNumber);

	}

//	public void verifyInternalAndClientDuedate() {
//
//		try {
//			webActionUtils.info("Verification of Internal Due");
//			SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
//			Date date = new Date();
//			sdf.setLenient(false);
//			String currentDate = sdf.format(date);
//			webActionUtils.waitSleep(10);
//			String InternalDate = internalDueDate.getAttribute("value");
//			webActionUtils.waitSleep(5);
//			String clientDate = clientDueDate.getAttribute("value");
//			webActionUtils.info("Internal Due : " + InternalDate);
//			webActionUtils.info("Client Due Date : " + clientDate);
//			webActionUtils.info("Current Date : " + currentDate);
//			webActionUtils.info("Internal Due Date after Verification : " + InternalDate);
//			webActionUtils.waitSleep(10);
//			try {
//				if (InternalDate.equals(clientDate) && InternalDate.equals(currentDate)) {
//					webActionUtils.info(
//							"Verification of Internal Due Date when Internal Due date is Equal to Client Due Date and Internal Due Date is Equal to Current Date");
//
//					webActionUtils.info("Internal Due : " + InternalDate);
//					webActionUtils.info("Client Due Date : " + clientDate);
//					webActionUtils.info("Current Date : " + currentDate);
//					webActionUtils.info("Internal Due Date after Verification : " + InternalDate);
//
//					if (InternalDate.equals(InternalDate)) {
//						webActionUtils.pass(
//								"Verification of Un-change of Internal Due Date when Internal Due date is Equal to Client Due Date and Internal Due Date is Equal to Current Date : Passed ");
//					} else {
//						webActionUtils.fail(
//								"Verification of Un-change of Internal Due Date when Internal Due date is Equal to Client Due Date and Internal Due Date is less than Current Date : Failed ");
//					}
//				}
//				if (InternalDate.compareTo(clientDate) == 0 && InternalDate.compareTo(currentDate) < 0) {
//					webActionUtils.info(
//							"Verification of Internal Due Date when Internal Due Date is Equal to Client Due Date and Internal Due Date is Less than Current Date");
//
//					webActionUtils.info("Internal Due Date : " + InternalDate);
//					webActionUtils.info("Client Due Date : " + clientDate);
//					webActionUtils.info("Current Date : " + currentDate);
//					webActionUtils.info("Internal Due Date after Verification : " + InternalDate);
//
//					if (InternalDate.equals(InternalDate)) {
//						webActionUtils.pass(
//								"Verification of Un-change of Internal Due Date when Internal Due date is Equal to Client Due Date and Internal Due Date is Less than Current Date : Passed ");
//					} else {
//						webActionUtils.fail(
//								"Verification of Un-change of Internal Due Date when Internal Due date is Equal to Client Due Date and Internal Due Date is Less than Current Date : Failed ");
//					}
//				}
//				if (InternalDate.compareTo(clientDate) == 0 && InternalDate.compareTo(currentDate) > 0) {
//					webActionUtils.info(
//							"Verification of Internal Due Date when Internal Due date is Equal to Client Due Date and Internal Due Date is Greater than Current Date");
//
//					Calendar calendar = Calendar.getInstance();
//					Date date1 = new SimpleDateFormat("MM/dd/yyyy").parse((String) InternalDate);
//					calendar.setTime(date1);
//					calendar.add(Calendar.DAY_OF_MONTH, -1);
//					Date date2 = calendar.getTime();
//					String internalDueDates = sdf.format(date2);
//
//					webActionUtils.info("Internal Due Date : " + InternalDate);
//					webActionUtils.info("Client Due Date : " + clientDate);
//					webActionUtils.info("Current Date : " + currentDate);
//					webActionUtils.info("Internal Due Date after Verification : " + internalDueDates);
//
//					if (InternalDate.compareTo((String) InternalDate) == 0) {
//						webActionUtils.pass(
//								"Verification of Internal Due Date changing to previous date when Internal Due date is Equal to Client Due Date and Internal Due Date is Greater than Current Date : Passed  ");
//					} else {
//						webActionUtils.fail(
//								"Verification of Internal Due Date changing to previous date when Internal Due date is Equal to Client Due Date and Internal Due Date is Greater than Current Date : Failed  ");
//					}
//				}
//
//			}
//
//			catch (Exception e) {
//				Reporter.log(e.getMessage(), true);
//				webActionUtils.fail("" + e.getMessage());
//			}
//		} catch (Exception e) {
//			Reporter.log(e.getMessage(), true);
//			webActionUtils.fail("" + e.getMessage());
//		}
//	}

	public void verifyPreviewFunctionalityMSG() {
		webActionUtils.info("verify Preview Functionalityof the files : MSG/EML");
		webActionUtils.scrolltoEnd();
//		webActionUtils.waitUntilLoadedAndElementClickable(customMSGChecbox);
//		webActionUtils.clickElement(customMSGChecbox, "MSG link");
		webActionUtils.clickElement(customMSGIcon, "MSG preview Icon");
		webActionUtils.waitForPageLoad();
		webActionUtils.waitSleep(15);

		String mainWindowHandle = webActionUtils.getdriver().getWindowHandle();
		Set<String> allWindowHandles = webActionUtils.getdriver().getWindowHandles();
		Iterator<String> iterator = allWindowHandles.iterator();

		// Here we will check if child window has other child windows and will
		// fetch the heading of the child window
		while (iterator.hasNext()) {
			String ChildWindow = iterator.next();
			if (!mainWindowHandle.equalsIgnoreCase(ChildWindow)) {
				driver.switchTo().window(ChildWindow);
//				String text = webActionUtils.getdriver().getTitle();
//				System.out.println("Heading of child window is MSG" + text);
//				windowClose(ChildWindow);
			}
			// webActionUtils.pass("TC Passed: MSG Preview worked fine");

		}
	}

	public void verifyPreviewFunctionalityPDF() {
		webActionUtils.info("verify Preview Functionalityof the files : PDF");
		webActionUtils.scrolltoEnd();
		webActionUtils.waitUntilLoadedAndElementClickable(customPDFChecbox);
		// webActionUtils.clickElement(customPDFChecbox, "PDF link");
		webActionUtils.clickElement(customPDFIcon, "PDF preview Icon");
		webActionUtils.waitForPageLoad();
		webActionUtils.waitSleep(15);

		String mainWindowHandle = webActionUtils.getdriver().getWindowHandle();
		Set<String> allWindowHandles = webActionUtils.getdriver().getWindowHandles();
		Iterator<String> iterator = allWindowHandles.iterator();

		// Here we will check if child window has other child windows and will
		// fetch the heading of the child window
		while (iterator.hasNext()) {
			String ChildWindow = iterator.next();
			if (!mainWindowHandle.equalsIgnoreCase(ChildWindow)) {
				driver.switchTo().window(ChildWindow);
				String text = webActionUtils.getdriver().getTitle();
				System.out.println("Heading of child window is PDF" + text);
			}
			webActionUtils.pass("TC Passed: PDF Preview worked fine");

		}

	}

	public void verifyProcessor(String pp1, String pp2) {

		webActionUtils.waitUntilLoadedAndVisibilityOfElementLocated(piProcessor1RecordTXT);
		webActionUtils.waitUntilLoadedAndTextToBePresentInElementLocated(piProcessor1RecordTXT, pp1);
		webActionUtils.verifyText(piProcessor1RecordTXT.getText(), pp1);

		webActionUtils.waitUntilLoadedAndVisibilityOfElementLocated(piProcessor2RecordTXT);
		webActionUtils.waitUntilLoadedAndTextToBePresentInElementLocated(piProcessor2RecordTXT, pp2);
		webActionUtils.verifyText(piProcessor2RecordTXT.getText(), pp2);
	}

	public void verifySaveLaterBeforeSubmitButton() {
		webActionUtils.info("Verify Save later button before Submit is displayed ");

		webActionUtils.verifyElementIsPresent(savelaterBTN);
		webActionUtils.pass("Save later button before Submit is available");

	}

	public void verifySaveLaterMessageCheck(String message) {
		webActionUtils.info("Verify Save later : message in the validate IR Page");
		webActionUtils.waitSleep(5);
		// webActionUtils.clickElement(fileTreeChekbox1,"Check box selcted");
		webActionUtils.clickElement(savelaterBTN, "Save later button");
		// webActionUtils.waitSleep(1);
		webActionUtils.waitUntilLoadedAndVisibilityOfElementLocated(alertMsg);
		if (alertMsg.getText().contains(message)) {
			System.out.println(alertMsg.getText());
			webActionUtils.pass("Save later message is validated");

		} else {
			webActionUtils.fail("Save later message is not working");
		}
	}

	public void verifySaveLaterPresentTwoPlaces() {
		webActionUtils.info("Verify Save later button is Present on Ribbon and validate IR Page");
		int count = savelaterBTNCount.size();
		if (count == 2) {
			webActionUtils.pass("Save later button is Present on Ribbon and validate IR Page");

		} else {
			webActionUtils.fail("Save later button is not Present on Ribbon and validate IR Page");
		}
	}

	public void verifySearchedRecord(String serialNumber) {
		webActionUtils.info("verifySearchedRecord after control");

		// webActionUtils.waitForPageLoad();
		// webActionUtils.waitSleep(10);
		// webActionUtils.waitUntilLoadedAndStalenessOffElement(serialTXT);
		// webActionUtils.waitUntilLoadedAndVisibilityOfElementLocated(serialTXT);
		// webActionUtils.waitSleep(5);
		webActionUtils.waitUntilLoadedAndTextToBePresentInElementLocated(serialTXT, serialNumber);
		// webActionUtils.waitSleep(5);
		webActionUtils.verifyText(serialTXT.getText(), serialNumber.toString());
		// webActionUtils.waitSleep(10);
	}

	public void verifyTaskDescription(String TaskDescription) {
		webActionUtils.info("Verify Task Description ");
		webActionUtils.waitForPageLoad();
		webActionUtils.waitSleep(3);
		webActionUtils.verifyElementText(thDescriptionRecords, TaskDescription);
	}

	public void verifyTaskDescriptionColumnNotPresent() {
		webActionUtils.info("Verification of the Task Description Column is not Present");
		try {
			if (webActionUtils.isElementDisplayed(taskDescription)) {
				webActionUtils.pass("Task Description is preset");
			} else {
				webActionUtils.fail("Task Description is not present1");
			}
		} catch (Exception e) {
			webActionUtils.pass("Task Description is not present2");
		}

	}

	public void verifyTaskDescriptionColumnPresent() {
		webActionUtils.info("verify TaskDescription Column Present ");

		try {
			webActionUtils.verifyElementIsPresent(taskDescription);
			webActionUtils.pass("Task Description is present");
		} catch (Exception e) {
			webActionUtils.fail("Task Description is not present");
		}

	}

	public void verifyTaskDescriptionforDTUser() {
		webActionUtils.info("Verify Task Description Filter is present for DT User ");
		webActionUtils.waitForPageLoad();
		webActionUtils.waitSleep(3);
		webActionUtils.verifyElementIsPresent(TaskDescriptionFilter);
	}

	public void verifyUploadedRecord(String policyNumber) {
		webActionUtils.waitUntilLoadedAndVisibilityOfElementLocated(policyNumberTXT);
		webActionUtils.waitUntilLoadedAndTextToBePresentInElementLocated(policyNumberTXT, policyNumber);
		webActionUtils.verifyText(policyNumberTXT.getText(), policyNumber);

	}

	public void windowClose(String windowname) {
		String mainWindowHandle = webActionUtils.getdriver().getWindowHandle();
		Set<String> allWindowHandles = webActionUtils.getdriver().getWindowHandles();
		Iterator<String> iterator = allWindowHandles.iterator();
		// fetch the heading of the child window
		while (iterator.hasNext()) {
			String ChildWindow = iterator.next();
			if (!mainWindowHandle.equalsIgnoreCase(ChildWindow)) {
				driver.switchTo().window(ChildWindow);
				String text = webActionUtils.getdriver().getTitle();
				System.out.println("Heading of child window is " + text);
				if (text.equals(windowname))
					;
				driver.close();
			}
		}
	}

	@FindBy(id = "btnSagiRegenerate")
	private WebElement regenerateSystemApplicationIcon;

	@FindBy(id = "btnConfirmYes")
	private WebElement yesBTN;

	@FindBy(id = "spanMessageContent")
	private WebElement regenerateMessage;

	public void verifyRegenaratebuttonEnabled() {
		webActionUtils.info("verify verifyRegenaratebuttonEnabled");
		webActionUtils.waitSleep(5);
		webActionUtils.waitUntilLoadedAndVisibilityOfElementLocated(regenerateSystemApplicationIcon);

		if (regenerateSystemApplicationIcon.isEnabled() == true) {
			webActionUtils.pass("Regenerate System Application Icon is Enabled verification is PASSED");

		} else {
			webActionUtils.fail("Regenerate System Application Icon is not Enabled verification is Failed");

		}

	}

	public void verifyRegenaratebuttonNotEnabled() {
		webActionUtils.info("verify Regenarate button Enabled for not Cancelled status");
		webActionUtils.waitUntilLoadedAndVisibilityOfElementLocated(regenerateSystemApplicationIcon);

		if (regenerateSystemApplicationIcon.isEnabled() == false) {
			webActionUtils.pass("Regenerate System Application Icon is not Enabled verification is PASSED ");
		} else {
			webActionUtils.fail("Regenerate System Application Icon is  Enabled verification is Failed ");

		}

	}

	public void hoverOnRegenaratebutton() {
		webActionUtils.info("Hover on Submenu  : Regenarate button");
		webActionUtils.mouseHover(regenerateSystemApplicationIcon);
	}

	public void verifyRegenerateSystemApplicationToolTip(String toolTip) {
		webActionUtils.info("verify Regenerate System Application ToolTip");
		webActionUtils.waitSleep(3);
		String actualToolTip = regenerateSystemApplicationIcon.getAttribute("title");
		System.out.println(actualToolTip);
		webActionUtils.verifyText(actualToolTip, toolTip);

	}

	public void clickRegenaratebutton() {
		webActionUtils.info("Click on Regenerate System Application");
		webActionUtils.clickElement(regenerateSystemApplicationIcon, "Regenerate System Application Icon");

	}

	public void clickRegenaratePopUpYesbutton() {
		webActionUtils.info("Click Yes on  Regenerate System Application Confirmation Popup");
		webActionUtils.clickElement(yesBTN, "yes Button");

	}

	public void verifyRegenaratemessage(String message) {
		webActionUtils.info("verify Regenarate message");
		webActionUtils.waitUntilLoadedAndTextToBePresentInElementLocated(regenerateMessage, message);
		webActionUtils.verifyElement(regenerateMessage, message);
	}

	public void verifyIRAccountManagerFieldsisBlank() {
		webActionUtils.info("Verify IR Account Manager Fields is Blank");
		webActionUtils.scrollBy(irAccountManagerTXT);

		try {
			if (webActionUtils.getElementText(irAccountManagerTXT).isBlank()
					|| webActionUtils.getElementText(irAccountManagerTXT).isEmpty()) {
				webActionUtils.pass(" IR Account Manager Fiels is Blank");

			} else {
				webActionUtils.fail(" IR Account Manager Fiels is not Blank");

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void verifyIRAssignToDefaulted() {
		webActionUtils.info("Verify IR AssignTo Field is Defaulted to 'Please Select an option'");
		webActionUtils.scrollBy(irAssignToTXT);
		Select select = new Select(irAssignToTXT);
		WebElement ele = select.getFirstSelectedOption();

		webActionUtils.selectByValue(irAssignToTXT, "0");

		try {
			if (webActionUtils.getElementText(ele).equals("Please Select an option")) {
				webActionUtils.pass(" IR AssignTo is  Defaulted to 'Please Select an option");

			} else {
				webActionUtils.fail(" IR AssignTo is Not Defaulted to 'Please Select an option");

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void enterIrAccountManager(String accountManager) {
		try {
			webActionUtils.scrollBy(irAccountManagerTXT);

			webActionUtils.info("Entering IR Account Manager");
			webActionUtils.clickElement(irAccountManagerTXT, accountManager);
			webActionUtils.setText(irAccountManagerTXT, accountManager);
			webActionUtils.clickElement(irAccountManagerOpn, "account Manager Opn");
		} catch (Exception e) {
			Reporter.log(e.getMessage(), true);
			webActionUtils.fail(e.getMessage());
			e.printStackTrace();
		}
	}

	public void selectRequiredIRAssignToValue(String assignedTO) {
		webActionUtils.scrollBy(irAssignToTXT);

		try {
			webActionUtils.info("Selecting Required IR assigned TO value ");
			webActionUtils.selectByVisibleText(irAssignToTXT, assignedTO);

		} catch (Exception e) {
			Reporter.log(e.getMessage(), true);
			webActionUtils.fail(e.getMessage());
			e.printStackTrace();
		}
	}

	public void verifyPIExpressCheckPackageRecordisNotDisplayed(String nameInsured) {

		String str = "";
		try {
			webActionUtils.info("Verify PI Express Check Package Record is Not Displayed ");
			str = webActionUtils.getElementText(Active1stElement);

		} catch (org.openqa.selenium.NoSuchElementException | TimeoutException e) {

			if (str.isBlank() || str.isEmpty()) {
				webActionUtils.pass(" PI Express Check Package Record is Not Present in Manage Page ");

			} else {
				webActionUtils.pass(" PI Express Check Package Record is Present  in Manage Page ");

			}

		}

	}
}