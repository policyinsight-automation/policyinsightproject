package com.automation.pi.pages;


import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;

import com.automation.pi.library.BasePage;
import com.automation.pi.library.WebActionUtils;


public class CommunicationsPage extends BasePage {

	@FindBy(xpath = "//a[text()='Communications']")
	private WebElement communicationPageTab;

	@FindBy(xpath = "//span[@class='bootstrap-switch-handle-on bootstrap-switch-primary']")
	private WebElement mineEle;
	
	@FindBy(xpath = "//span[@class='bootstrap-switch-label']")
	private WebElement allEle;
	
	@FindBy(id="ddlStatus")
	private WebElement commStatus;
//	
//	@FindBy(id="spanStatusID")
//	private WebElement StatusDD;
	
	@FindBy(xpath="//span[@id='span']")
	private WebElement StatusDD;

	
	@FindBy(xpath="(//div[@id='divDiscussion']//td)[3]/a")
	private WebElement packageCommunicationTitle;
	
	@FindBy(xpath="//h4[text()='Communication Details']")
	private WebElement communicationDetailsHeader;
	
	
	@FindBy(xpath="//div[@class='text-center']")
	private WebElement commPackageDetailsTxt;
	
	@FindBy(xpath="//textarea[@id='message']")
	private WebElement commCommentsTextArea;
	
	@FindBy(xpath="//button[text()='Send']")
	private WebElement commSendBTN;
	
	@FindBy(xpath="(//label[text()='Hi Test Here']//parent::li)[last()]//label[text()='Hi Test Here']")
	private WebElement msgArea;
	
	
	@FindBy(xpath="(//label[contains(text(),'Hi there, The policy is not a full copy and the DE')]//parent::li)[last()]//label[2]")
	private WebElement msgArea2;
	
	@FindBy(id="uploadifive-btnUpload-queue")
	private WebElement uploadifive;
	
	
	@FindBy(id="ddlFixedWording")
	private WebElement selectCommDd;
	
	@FindBy(xpath="//input[@type='file']")
	private WebElement uploadBTN;
	
	@FindBy(xpath = "//*[@id='divLoading']")
	private WebElement uploadingTxt2;

	@FindBy(xpath="//div[@id='attachments']//a[last()]")
	private WebElement downloadfile;
	
	@FindBy(xpath="(//label[last()]//a[last()])[last()]")
	private WebElement downloadfile1;
	
	@FindBy(xpath="//button[@id='btnEndCommunication']")
	private WebElement EndCommunicationBTN;
	
	
	
	@FindBy(id="btnConfirmYes")
	private WebElement confirmYesBTN;
	
	
	@FindBy(xpath = "//label[contains(text(),'Shobhan Shiva closed this communication.')]")
	private WebElement closedCommText;
	

	
	@FindBy(xpath = "//ul[@id=\"uldiscussion\"]//li[1]/label[2]")
	private WebElement NIandLOBCommText;

	@FindBys({
		@FindBy(xpath = "//tr//td[2]//a")
	})
	private List<WebElement> allPackages;





	
	
	
	
	
		
	@FindBy(id="ddlAccount")
	private WebElement selectAccount;
	
	@FindBy(id="btnSearch")
	private WebElement searchBtn;
	
	@FindBy(xpath=" //input[@id='txtSerialNo']")
	private WebElement serialTxtBx;
	
	
	@FindBy(xpath="//input[@id='txtTitle']")
	private WebElement txtTitleTxtBx;
	
	@FindBy(xpath=" //span[text()='Mine']")
	private WebElement mineSlider;
	
	@FindBy(xpath="//span[@class='bootstrap-switch-handle-off bootstrap-switch-default']")
	private WebElement allSlider;
	
	@FindBy(id="txtConversation")
	private WebElement mainTextTxtBx;
	
	@FindBy(xpath ="//div[@id='divDiscussion']//tr[2]//label[@class='text-left']")
	private List<WebElement>  mainTextRecord;
	
	@FindBy(xpath="//input[@type='checkbox' and @name='selectAll']")
	private WebElement selectAll;
	
	
	@FindBy(xpath="//label[text()=' Uploaded']/input")
	 private WebElement cUploaded;

	@FindBy(xpath="//label[text()=' Active']/input")
	 private WebElement cActive;

	@FindBy(xpath="//label[text()=' Suspended']/input")
	 private WebElement cSuspended;
	
	@FindBy(xpath="//label[text()=' Finished']/input")
	 private WebElement cFinished;

	@FindBy(xpath="//label[text()=' Merged']/input")
	 private WebElement cMerged;

	@FindBy(xpath="//label[text()=' Cancelled']/input")
	 private WebElement cCancelled;
	
	
	@FindBy(xpath="//label[text()=' Ready to Upload']/input")
	 private WebElement cReadytoUpload;
	
	@FindBy(xpath="//label[text()=' Reviewed']/input")
	 private WebElement cReviewed;
	
	

	@FindBy(xpath="//label[text()=' Ready for DT Checklist']/input")
	 private WebElement cReadyforDTChecklist;

	
	@FindBy(xpath="//label[text()=' DT Finished']/input")
	 private WebElement cDTFinished;
	
	@FindBy(xpath="//label[text()=' DT Suspended']/input")
	 private WebElement cDTSuspended;
	
	public CommunicationsPage(WebDriver driver, WebActionUtils webActionUtils) {
		super(driver, webActionUtils);
	}
	
	
	public void clickCommunicationPageTab() {
		webActionUtils.info("Clicking on the Communication Page Tab");
		webActionUtils.waitUntilLoadedAndVisibilityOfElementLocated(communicationPageTab);
		webActionUtils.waitUntilLoadedAndElementClickable(communicationPageTab);
		focusOnNewWin(communicationPageTab, "Communication Page Tab");
		webActionUtils.pass("Communication Page is displayed");
	}

	public void clickOnAllfilter() {
		webActionUtils.info("Clicking on the All Page Tab");
		webActionUtils.waitUntilLoadedAndVisibilityOfElementLocated(allEle);
		webActionUtils.waitUntilLoadedAndElementClickable(allEle);
		allEle.click();
		webActionUtils.waitSleep(3);
		}

	public void clickOnCommnicationCloseButton() {
		webActionUtils.info("click On Commnication Close Button");
		webActionUtils.clickElement(EndCommunicationBTN, "EndCommunicationBTN");;	

	}

	public void clickOnCommSend() {
		webActionUtils.info("Click on Communication : Send Buttton");
		webActionUtils.clickElement(commSendBTN, "Send Button");
		webActionUtils.refreshPage();
		webActionUtils.scrolltoEnd();
		webActionUtils.pass("Communication message sent");
	}
	
	public void clickOnMinefilter() {
		webActionUtils.info("Clicking on the Mine Page Tab");
		webActionUtils.waitSleep(3);
		webActionUtils.waitUntilLoadedAndVisibilityOfElementLocated(mineSlider);
		webActionUtils.waitUntilLoadedAndElementClickable(mineSlider);
		webActionUtils.clickElement(mineSlider, "Mine filter");
		
		}
	
	
	public void clickOnPackageCommunicationTitle() {
		webActionUtils.info("Clicking on Package Communication Title");
		webActionUtils.waitSleep1(4);
		webActionUtils.waitUntilLoadedAndElementClickable(packageCommunicationTitle);
		webActionUtils.clickElement(packageCommunicationTitle, "Package- Communication Title");
		webActionUtils.waitSleep1(4);
	}
		

	public void clickOnSearch() {
		webActionUtils.info("Click on search button");
		webActionUtils.waitSleep(2);
		webActionUtils.clickElement(searchBtn, "Search Button");
		webActionUtils.waitSleep(7);
	
	}
	
	public void clickOnYesOnCommnicationPopUp() {
		webActionUtils.info("Click On 'YES' Commnication POP up ");
		webActionUtils.clickElement(confirmYesBTN, "YES on the POP");

	}

	public void closeCommunicationTab() {
		ArrayList<String> allTabs = new ArrayList<String>(driver.getWindowHandles());

		for (int i = 0; i < allTabs.size(); i++) {

			driver.switchTo().window(allTabs.get(i));
			driver.close();

		}
		webActionUtils.info("Closing Communication Page");
	}
	 
	 public void displayListofCommunicationPackage() {
		webActionUtils.info("Display all available Package");
		webActionUtils.waitSleep1(5);
		webActionUtils.waitForPageLoad();
		try {
			if (allPackages.size() != 0) {
				System.out.println(allPackages.size());
				for (WebElement listOfallPackages : allPackages) {
					webActionUtils
							.pass("Package ID :" + listOfallPackages.getText());
				}
			} else {
				webActionUtils.info("No records found");
			}
		} catch (Exception e) {
			webActionUtils.info("No records found");
		}
	}

	 public void downloadfileAttachment() {
		webActionUtils.info("Download the Attachment file");
		webActionUtils.refreshPage();
		webActionUtils.waitSleep(10);
		webActionUtils.waitForPageLoad();
		webActionUtils.clickElement(downloadfile, "Downloaded file");;	
		webActionUtils.waitSleep(10);
		webActionUtils.pass("Downloaded sucessfully ");

	}
	 
	  public void downloadfilechat() {
		webActionUtils.info("Download the chat file");
		webActionUtils.refreshPage();
		webActionUtils.waitSleep(10);
		webActionUtils.waitForPageLoad();
		webActionUtils.clickElement(downloadfile1, "Downloaded file");;	
		webActionUtils.waitSleep(10);
		webActionUtils.pass("Downloaded sucessfully ");

	}
	 
	  public void EnterChat(String msgText) {
		webActionUtils.info("Communication : Enter Text to Send Chat");
		webActionUtils.clickElement(commCommentsTextArea, "Communication Comments Text Area");
		webActionUtils.setText(commCommentsTextArea, msgText);
		webActionUtils.waitSleep(6);
	}
	 
	  public void enterMainText(String text) {
		webActionUtils.info("Entering the Main Text ");
		webActionUtils.waitSleep(3);
		webActionUtils.setText(mainTextTxtBx, text);
		webActionUtils.waitSleep(3);
	}
	 
	  public void enterSerialNumber(String text) {
		webActionUtils.info("Entering the Package Number");
		webActionUtils.waitSleep(3);
		webActionUtils.setText(serialTxtBx, text);
		webActionUtils.waitSleep(3);
	}
	 
	  public void enterTitle(String text) {
		webActionUtils.info("Entering the Title name");
		webActionUtils.waitSleep(3);
		webActionUtils.setText(txtTitleTxtBx, text);
		webActionUtils.waitSleep(3);
	}

	 public void focusOnNewWin(WebElement element, String text) {
		//String oldTab = driver.getWindowHandle();
		System.out.println(driver.getTitle().toString());
		webActionUtils.clickElement(element, text);
		Set<String> allTabs = driver.getWindowHandles();
		List<String> windowHandlesList = new ArrayList<>(allTabs);
		// windowHandlesList.remove(oldTab);
		webActionUtils.waitSleep(5);
		// driver.switchTo().window(windowHandlesList.get(0));
		// System.out.println(driver.getTitle().toString());
		driver.switchTo().window(windowHandlesList.get(1));
	}
	 
	  public void getAllMinePackageDetails() {
		webActionUtils.info("Getting all the package details");
		webActionUtils.waitUntilLoadedAndVisibilityOfElementLocated(mineEle);
		webActionUtils.waitUntilLoadedAndElementClickable(mineEle);
		webActionUtils.getallElementText(allPackages);
		}

	 public WebElement getPackageElement(String i) {
		String xpathexp="//tr//td[2]/a["+i+"]";
				return driver.findElement(By.xpath(xpathexp));
	}
	 
	 	public void selectAllStatus() {
			webActionUtils.info("Select selectAll status");
			StatusDD.click();
			webActionUtils.clickElement(selectAll, "selectAll");
			webActionUtils.waitSleep(1);
		}
		
		public void selectRequiredAccount(String account) {
			webActionUtils.info("Select Required Account");
			webActionUtils.waitSleep(3);
			webActionUtils.selectByVisibleText(selectAccount,account);
			webActionUtils.info("Selected Account Name :"+account);
			webActionUtils.waitSleep(3);
		
		}
		
		
		
		public void selectRequiredCommunicationfunction(String selectCommOption) {
			webActionUtils.info("Select Required Communication option from Dropdownn");
			webActionUtils.selectByVisibleText(selectCommDd, selectCommOption);
			webActionUtils.waitSleep(6);
		}
		
		public void selectRequiredStatus(String status) {
			webActionUtils.info("Select Required Communication status");
			//commStatus.click();
			webActionUtils.waitSleep(3);
			webActionUtils.selectByVisibleText(commStatus,status);
			webActionUtils.info("Selected status Name :"+status);
			webActionUtils.waitSleep(5);
		
		}
		
		
		
		public void selectRequiredStatuses(String status) {
			webActionUtils.info("Select Required statuses");
			StatusDD.click();
			webActionUtils.waitSleep(3);
			webActionUtils.selectByVisibleText(StatusDD,status);
			webActionUtils.info("Selected status Name :"+status);
			webActionUtils.waitSleep(3);
		
		}
		
		public void selectStatusActive() {
			webActionUtils.info("Select Active status");
			StatusDD.click();
			webActionUtils.waitSleep(1);
			selectAll.click();
			webActionUtils.clickElement(cActive, "Active");
			webActionUtils.waitSleep(1);
		}
		
		public void selectStatusCancelled() {
			webActionUtils.info("Select Cancelled status");
			StatusDD.click();
			webActionUtils.waitSleep(1);
			selectAll.click();
			webActionUtils.clickElement(cCancelled, "Cancelled");
			webActionUtils.waitSleep(1);
		}
		public void selectStatuscDTSuspended() {
			webActionUtils.info("Select DT Suspended status");
			StatusDD.click();
			webActionUtils.waitSleep(1);
			selectAll.click();
			webActionUtils.clickElement(cDTSuspended, "cDTSuspended");
			webActionUtils.waitSleep(1);
		}
		
		public void selectStatuscReadyforDTChecklist() {
			webActionUtils.info("Select Ready for DT Checklist status");
			StatusDD.click();
			webActionUtils.waitSleep(1);
			selectAll.click();
			webActionUtils.clickElement(cReadyforDTChecklist, "Ready for DT Checklist");
			webActionUtils.waitSleep(1);
		}
		
		public void selectStatusDTFinished() {
			webActionUtils.info("Select DT Finished status");
			StatusDD.click();
			webActionUtils.waitSleep(1);
			selectAll.click();
			webActionUtils.clickElement(cDTFinished, "DT Finished");
			webActionUtils.waitSleep(1);
		}
		
		public void selectStatusFinished() {
			webActionUtils.info("Select Finished status");
			webActionUtils.waitSleep(4);
			StatusDD.click();
			webActionUtils.waitSleep(2);
			selectAll.click();
			webActionUtils.clickElement(cFinished, "Finished");
			webActionUtils.waitSleep(1);
		}
		
		public void selectStatusMerged() {
			webActionUtils.info("Select Merged status");
			StatusDD.click();
			webActionUtils.waitSleep(1);
			selectAll.click();
			webActionUtils.clickElement(cMerged, "Merged");
			webActionUtils.waitSleep(1);
		}
		
		public void selectStatusReadytoUpload() {
			webActionUtils.info("Select Ready to Upload status");
			webActionUtils.waitSleep1(3);
			StatusDD.click();
			webActionUtils.waitSleep1(1);
			selectAll.click();
			webActionUtils.clickElement(cReadytoUpload, "Ready to Upload");
			webActionUtils.waitSleep1(1);
		}
		
		public void selectStatusReviewed() {
			webActionUtils.info("Select Reviewed status");
			StatusDD.click();
			webActionUtils.waitSleep(1);
			selectAll.click();
			webActionUtils.clickElement(cReviewed, "Reviewed");
			webActionUtils.waitSleep(1);
		}
		
		public void selectStatusSuspended() {
			webActionUtils.info("Select Suspended status");
			StatusDD.click();
			webActionUtils.waitSleep(1);
			selectAll.click();
			webActionUtils.clickElement(cSuspended, "Suspended");
			webActionUtils.waitSleep(1);
		}
		public void selectStatusUploaded() {
			webActionUtils.info("Select Uploaded status");
			StatusDD.click();
			webActionUtils.waitSleep(1);
			selectAll.click();
			webActionUtils.clickElement(cUploaded, "Uploaded");
			webActionUtils.waitSleep(1);
		}
		
		public void uploadfiles(String uploadfile) {
			webActionUtils.info("Uploading file");
			webActionUtils.waitSleep(3);
			webActionUtils.scrolltoEnd();
			webActionUtils.setText(uploadBTN, uploadfile);

			webActionUtils.waitSleep(10);

			webActionUtils.info("uploadinggg ");
			try {
				webActionUtils.waitInVisibilityOfElementLocated(uploadingTxt2);
			} catch (Exception e1) {
				e1.printStackTrace();
				System.out.println( "uploadingTxt2 element issue ");
			}

			webActionUtils.waitSleep(5);
			webActionUtils.pass("Uploaded sucessfully ");

		}

		public void verifyAllFilter(String text) {
			webActionUtils.info("Verify All filter is displayed ");
			webActionUtils.waitUntilLoadedAndVisibilityOfElementLocated(allSlider);
			webActionUtils.waitUntilLoadedAndElementClickable(allSlider);
			webActionUtils.verifyElementText(allSlider,text);
			
		}

		public void VerifyCommunicationEnded(String Endcomm) {
			webActionUtils.info("Verify Communication Ended ");
			
			try {
				webActionUtils.waitSleep(10);	
				webActionUtils.waitUntilLoadedAndVisibilityOfElementLocated(closedCommText);
				webActionUtils.verifyText(closedCommText.getText(), Endcomm);
				webActionUtils.pass("TC Passed : Communication Ended");
			} catch (Exception e) {
				webActionUtils.fail("TC Failed");
				e.printStackTrace();
			}
		
		}


		public void verifyCommunicationHeaderPage(String expectedHeader) {
			webActionUtils.info("Verifying Communication Page header");
			webActionUtils.waitSleep(5);
			webActionUtils.verifyElementText(communicationDetailsHeader, expectedHeader);
			webActionUtils.pass("Communication Header Verified " + expectedHeader);
			webActionUtils.info("Communication Package Details :"+commPackageDetailsTxt.getText());
		}

	

		public void verifyCommunicationNIandLOBtext() {
			webActionUtils.info("Verify Communication NI and LOB text ");
			
			try {
				webActionUtils.waitSleep(5);	
				webActionUtils.verifyElementContains(NIandLOBCommText, "Hi there, Please kindly note that the Named Insured has been changed from");
				webActionUtils.verifyElementContains(NIandLOBCommText, " and the LOB has been changed from ");

				webActionUtils.pass("TC Passed : Communication NI and LOB");
			} catch (Exception e) {
				webActionUtils.fail("TC Fail : Communication Ended");
			}
		
		}

		public void verifyCommunicationPage(String title) {
			webActionUtils.info("Verifying Communication Page Title");
			webActionUtils.waitSleep1(5);
			webActionUtils.verifyTitle(title);
			webActionUtils.pass("Communication Page Title Verified " + title);
		}

		public void verifyCommunicationSentMessage1(String msgText) {
			webActionUtils.info("Verifying Communication Sent Message");
			webActionUtils.verifyElementText(msgArea, msgText);
			webActionUtils.pass("Communication Message Verified " + msgText);
		}
		
		public void verifyCommunicationSentMessage2(String msgText) {
			webActionUtils.info("Verifying Communication Sent Message");
			webActionUtils.verifyElementText(msgArea2, msgText);
			webActionUtils.pass("Communication Message Verified " + msgText);
		}

		public void verifyCommunicationSentMessage3(String msgText) {
			webActionUtils.info("Verifying Communication Sent Message");
			webActionUtils.verifyElementContainsText(msgArea2, msgText);
			webActionUtils.pass("Communication Message Verified " + msgText);
		}

		
		public void verifyCreateEditCommunicationPage(String title) {
			webActionUtils.info("Verifying Communication Page Title");
			webActionUtils.waitSleep1(6);
			webActionUtils.verifyTitle(title);
			webActionUtils.pass("Communication Page Title Verified " + title);
		}
		
		public void verifyMainTextIsPreset(String text) {
			webActionUtils.info("Verifying the Main Text ");
			webActionUtils.waitSleep(3);
			try {
				if (mainTextRecord.size() != 0) {
					System.out.println(mainTextRecord.size());
					for (WebElement listOfallrecord : mainTextRecord) {
						webActionUtils.info("Main text"+text +": is displayed " +listOfallrecord.getText().contains(text));
					}
				} else {
					webActionUtils.info("No records found");
				}
			} catch (Exception e) {
				webActionUtils.info("No records found");
			}
						
		}
		
		
		public void verifyMineFilter(String text) {
			webActionUtils.info("Verify Mine filter is displayed ");
			webActionUtils.verifyElementText(mineSlider,text);
		}
		
		

	
		
//		public void UploadCommfile(String uploadfile) {
//			webActionUtils.info("Uploading file");
//			webActionUtils.setText(element, text);
//			webActionUtils.waitSleep(6);
//		}
		}
