package com.automation.pi.test000.smoketest;

import java.lang.reflect.Method;

import org.apache.commons.lang3.RandomStringUtils;
import org.testng.annotations.Test;

import com.automation.pi.library.BaseTest;
import com.automation.pi.pages.HomePage;
import com.automation.pi.pages.LoginPage;
import com.automation.pi.pages.UploadFilePage;

public class SmokeTest2 extends BaseTest {

	@Test
	public void pidc_tc_826(Method m) {

		webActionUtils.extentTestName(m.getName());
		webActionUtils.TestCaseinfo("================================");
		webActionUtils.TestCaseinfo(" Test Case : Upload - Create a New Package");
		webActionUtils.TestCaseinfo("================================");

		String accountName = excelLibrary.getExcelData(XL_TESTPATH, "SmokeTC", 4, 1);
		String departmentName = excelLibrary.getExcelData(XL_TESTPATH, "SmokeTC", 4, 2);
		String namedInsured = excelLibrary.getExcelData(XL_TESTPATH, "SmokeTC", 4, 3);
		String lob = excelLibrary.getExcelData(XL_TESTPATH, "SmokeTC", 4, 4);
		String policyNumber = excelLibrary.getExcelData(XL_TESTPATH, "SmokeTC", 4, 5);
		String effectiveDate = excelLibrary.getExcelData(XL_TESTPATH, "SmokeTC", 4, 6);
		String accountManager = excelLibrary.getExcelData(XL_TESTPATH, "SmokeTC", 4, 7);
		String policyStatusOpn = excelLibrary.getExcelData(XL_TESTPATH, "SmokeTC", 4, 8);
		String expectedText = excelLibrary.getExcelData(XL_TESTPATH, "SmokeTC", 4, 9);

		String uploadfilepath1 = DIR_PATH + "/TestDataFiles/5-Policy.pdf";
		String uploadfilepath2 = DIR_PATH + "/TestDataFiles/5-Proposal.pdf";
		String uploadfilepath3 = uploadfilepath1 + "\n" + uploadfilepath2;
		
		namedInsured = namedInsured +" Test" + RandomStringUtils.randomAlphanumeric(5);

		LoginPage loginPage = new LoginPage(driver, webActionUtils);
		loginPage.enterUserName(USER_NAME);
		loginPage.enterPassword(PASSWORD);
		loginPage.clickOnSignin();
		UploadFilePage uploadFile = new UploadFilePage(driver, webActionUtils);
		uploadFile.clickOnUploadFile();
		uploadFile.enterRequiredAccount(accountName);
		uploadFile.enterRequiredDepartment(departmentName);
		uploadFile.enterRequiredNamedInsured(namedInsured);
		uploadFile.enterRequiredLOB(lob);
		uploadFile.enterRequiredPolicyNumber(policyNumber);
		uploadFile.enterRequiredEffectiveDate(effectiveDate);
		uploadFile.enterHighlightedDateFromClient();
		uploadFile.enterRequiredAccountManager(accountManager);
		uploadFile.enterRequiredPolicyStatus(policyStatusOpn);
		uploadFile.uploadRequiredFiles(uploadfilepath3);
		uploadFile.clickonSubmitButton();
		uploadFile.getMessageContainsVerify(expectedText);

		HomePage homePage = new HomePage(driver, webActionUtils);
		homePage.clickOnSignOutIcon();
		homePage.clickOnYes();
	}
}