package com.automation.pi.test000.smoketest;

import java.lang.reflect.Method;

import org.testng.annotations.Test;

import com.automation.pi.library.BaseTest;
import com.automation.pi.pages.LoginPage;
import com.automation.pi.pages.ManagePage;
import com.automation.pi.pages.UploadFilePage;

public class SmokeTest3 extends BaseTest {

	@Test
	public void pidc_tc_827(Method m) {

		webActionUtils.extentTestName(m.getName());
		webActionUtils.TestCaseinfo("================================");
		webActionUtils.TestCaseinfo(" Test Case : Assign - Assign the Checking Auditor.");
		webActionUtils.TestCaseinfo("================================");

		String accountName = excelLibrary.getExcelData(XL_TESTPATH, "SmokeTC", 7, 1);
		String departmentName = excelLibrary.getExcelData(XL_TESTPATH, "SmokeTC", 7, 2);
		String namedInsured = excelLibrary.getExcelData(XL_TESTPATH, "SmokeTC", 7, 3);
		String lob = excelLibrary.getExcelData(XL_TESTPATH, "SmokeTC", 7, 4);
		String policyNumber = excelLibrary.getExcelData(XL_TESTPATH, "SmokeTC", 7, 5);
		String effectiveDate = excelLibrary.getExcelData(XL_TESTPATH, "SmokeTC", 7, 6);
		String accountManager = excelLibrary.getExcelData(XL_TESTPATH, "SmokeTC", 7, 7);
		String policyStatusOpn = excelLibrary.getExcelData(XL_TESTPATH, "SmokeTC", 7, 8);
		String expectedText = excelLibrary.getExcelData(XL_TESTPATH, "SmokeTC", 7, 9);
		String managePageTitle = excelLibrary.getExcelData(XL_TESTPATH, "SmokeTC", 7, 10);
		String checkingAuditoroption = excelLibrary.getExcelData(XL_TESTPATH, "SmokeTC", 7, 11);

		String uploadfilepath1 = DIR_PATH + "/TestDataFiles/5-Policy.pdf";
		String uploadfilepath2 = DIR_PATH + "/TestDataFiles/5-Proposal.pdf";
		String uploadfilepath3 = uploadfilepath1 + "\n" + uploadfilepath2 ;

		LoginPage loginPage = new LoginPage(driver, webActionUtils);
		loginPage.enterUserName(USER_NAME);
		loginPage.enterPassword(PASSWORD);
		loginPage.clickOnSignin();
		UploadFilePage uploadFile = new UploadFilePage(driver, webActionUtils);
		uploadFile.clickOnUploadFile();
		uploadFile.enterRequiredAccount(accountName);
		uploadFile.enterRequiredDepartment(departmentName);
		uploadFile.enterRequiredNamedInsured(namedInsured);
		uploadFile.enterRequiredLOB(lob);
		uploadFile.enterRequiredPolicyNumber(policyNumber);
		uploadFile.enterRequiredEffectiveDate(effectiveDate);
		uploadFile.enterHighlightedDateFromClient();
		uploadFile.enterRequiredAccountManager(accountManager);
		uploadFile.enterRequiredPolicyStatus(policyStatusOpn);
		uploadFile.uploadRequiredFiles(uploadfilepath3);
		uploadFile.clickonSubmitButton();
		uploadFile.getMessageContainsVerify(expectedText);
		String serialNo = uploadFile.getSerialNumber();
		uploadFile.clickOnSerialNumber();

		ManagePage managePage = new ManagePage(driver, webActionUtils);
		managePage.verifyManagePage(managePageTitle);
		managePage.selectFirstRecordCheckBox();
		managePage.clickAssignAuditorButton();
		managePage.assignCheckingAuditor(checkingAuditoroption);
		managePage.saveAuditor();
		managePage.searchSerialNumber(serialNo);
		managePage.clickStatus();
		managePage.clickSelectAll();
		managePage.clickSearchButton();
		managePage.verifyAuditor(checkingAuditoroption);
	}
}