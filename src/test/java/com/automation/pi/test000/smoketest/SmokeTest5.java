package com.automation.pi.test000.smoketest;

import java.lang.reflect.Method;

import org.testng.annotations.Test;

import com.automation.pi.library.BaseTest;
import com.automation.pi.pages.LoginPage;
import com.automation.pi.pages.ManagePage;

public class SmokeTest5 extends BaseTest {// 691

	@Test
	public void pidc_tc_825(Method m) {

		webActionUtils.extentTestName(m.getName());
		webActionUtils.TestCaseinfo("================================");
		webActionUtils.TestCaseinfo(" Test Case : Download - Download the checklist template");
		webActionUtils.TestCaseinfo("================================");

		String serialNumber = excelLibrary.getExcelData(XL_TESTPATH, "SmokeTC", 13, 1);
		String managePageTitle = excelLibrary.getExcelData(XL_TESTPATH, "SmokeTC", 13, 2);

		LoginPage loginPage = new LoginPage(driver, webActionUtils);
		loginPage.enterUserName(USER_NAME);
		loginPage.enterPassword(PASSWORD);
		loginPage.clickOnSignin();

		ManagePage managePage = new ManagePage(driver, webActionUtils);
		managePage.clickManagePageTab();
		managePage.verifyManagePage(managePageTitle);
		managePage.searchSerialNumber(serialNumber);
		managePage.clickStatus();
		managePage.clickSelectAll();
		managePage.clickAdvanceFiltersButton();
		managePage.selectDateCreated3more();
		managePage.clickAdvanceFiltersButton();
		managePage.clickSearchButton();
		managePage.verifySearchedRecord(serialNumber);
		managePage.selectFirstRecordCheckBox();
		managePage.clickDownloadChecklistTemplate();
	}
}