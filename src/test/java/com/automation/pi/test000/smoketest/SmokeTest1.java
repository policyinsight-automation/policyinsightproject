package com.automation.pi.test000.smoketest;

import java.lang.reflect.Method;

import org.testng.annotations.Test;

import com.automation.pi.library.BaseTest;
import com.automation.pi.pages.HomePage;
import com.automation.pi.pages.LoginPage;

public class SmokeTest1 extends BaseTest {

	@Test
	public void pidc_tc_822(Method m) {

		webActionUtils.extentTestName(m.getName());
		webActionUtils.TestCaseinfo("================================");
		webActionUtils.TestCaseinfo("Test Case : Log In - Access to the System");
		webActionUtils.TestCaseinfo("================================");

		String loginTitle = excelLibrary.getExcelData(XL_TESTPATH, "SmokeTC", 1, 1);
		String DepartmentSettingPageTitle = excelLibrary.getExcelData(XL_TESTPATH, "SmokeTC", 1, 2);

		LoginPage loginPage = new LoginPage(driver, webActionUtils);
		loginPage.verifyLoginPageTitle(loginTitle);
		loginPage.enterUserName(USER_NAME);
		loginPage.enterPassword(PASSWORD);
		loginPage.clickOnSignin();
		HomePage homePage = new HomePage(driver, webActionUtils);
		homePage.verifyHomePageTitle(DepartmentSettingPageTitle);
	}
}