package com.automation.pi.test000.smoketest;

import java.lang.reflect.Method;

import org.testng.annotations.Test;

import com.automation.pi.library.BaseTest;
import com.automation.pi.pages.LoginPage;
import com.automation.pi.pages.ReportsPage;

public class SmokeTest7 extends BaseTest {// 695
	
	@Test
	public void pidc_tc_829(Method m) {
		
		webActionUtils.extentTestName(m.getName());
		webActionUtils.TestCaseinfo("================================");
		webActionUtils.TestCaseinfo(" Test Case Name : Reports - Reports page options check.");
		webActionUtils.TestCaseinfo("================================");

		LoginPage loginPage = new LoginPage(driver, webActionUtils);
		loginPage.enterUserName(USER_NAME);
		loginPage.enterPassword(PASSWORD);
		loginPage.clickOnSignin();

		ReportsPage reports = new ReportsPage(driver, webActionUtils);
		reports.clickReportsTab();
		reports.verifyReportsOptions();
	}
}