package com.automation.pi.test007.cbiz;
import java.lang.reflect.Method;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.automation.pi.library.BaseTest;
import com.automation.pi.pages.LoginPage;
import com.automation.pi.pages.UploadFilePage;

public class KA4 extends BaseTest {

	@DataProvider(name = "data-provider")
	public Object[][] data() {
		Object[][] data = new Object[6][4];

		data[0][0] = "Cameron Interest Limited Partnership";
		data[0][1] = "CXS 9587762 21";
		data[0][2] = "11/01/2024";
		data[0][3] = "CUM";

		data[1][0] = "Barnhart Co., LLC";
		data[1][1] = "H-660-4S884797-TIL-24";
		data[1][2] = "11/01/2024";
		data[1][3] = "CGL";

		data[2][0] = "Nelson Equipment, Ltd.";
		data[2][1] = "61 XS ON00T4";
		data[2][2] = "07/21/2024";
		data[2][3] = "CUX";

		data[3][0] = "Advantage Interests, Inc.";
		data[3][1] = "QT-660-2L231804-TLC-24";
		data[3][2] = "10/01/2024";
		data[3][3] = "CPR";

		data[4][0] = "Apache Fabricators, LLC";
		data[4][1] = "CYB-108071445-00";
		data[4][2] = "07/08/2024";
		data[4][3] = "CYL";

		data[5][0] = "Benchmark Houston Builders LP";
		data[5][1] = "105655722";
		data[5][2] = "07/25/2024";
		data[5][3] = "CMI";

		return data;
	}

	@Test(dataProvider = "data-provider")
	public void name(Method m, String naminsured, String policy, String effdate, String lob) {

		webActionUtils.extentTestName(m.getName());

		String accountName = "BCH";
		String departmentName = "BCH-Demo";// "Denver - Large";

		String accountManager = "AM Monica";

		String policyStatusOpn = "New Business";
		String uploadfilepath1 = DIR_PATH + "/TestDataFiles/5-Policy.pdf";
		String expectedText = excelLibrary.getExcelData(XL_TESTPATH, "SmokeTestSheet", 1, 8);

		LoginPage loginPage = new LoginPage(driver, webActionUtils);
		loginPage.enterUserName(USER_NAME);
		loginPage.enterPassword(PASSWORD);
		loginPage.clickOnSignin();

		UploadFilePage uploadFilePage = new UploadFilePage(driver, webActionUtils);
		uploadFilePage.clickOnUploadFile();

		uploadFilePage.enterFieldvalues4("STG_09092024_ " + naminsured, accountName, departmentName, lob, policy,
				effdate, accountManager, policyStatusOpn, uploadfilepath1, expectedText);
		webActionUtils.refreshPage();
		webActionUtils.waitSleep(3);
	}
}