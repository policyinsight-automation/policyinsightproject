package com.automation.pi.test007.cbiz;

import java.lang.reflect.Method;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.automation.pi.library.BaseTest;
import com.automation.pi.pages.LoginPage;
import com.automation.pi.pages.UploadFilePage;

public class KA2 extends BaseTest {

	@DataProvider(name = "data-provider")
	public Object[][] data() {
		Object[][] data = new Object[5][4];

		data[0][0] = "ATC Fitness Exeter Village";
		data[0][1] = "21WECAP1W9Z1234";
		data[0][2] = "1/5/2023";
		data[0][3] = "WCO";

		data[1][0] = "Vulcan Labs, LLC";
		data[1][1] = "TWC4162291234";
		data[1][2] = "9/14/2022";
		data[1][3] = "WCO";

		data[2][0] = "Test Shobhan Labs, LLC";
		data[2][1] = "22726231234";
		data[2][2] = "01/01/2023";
		data[2][3] = "PRO";

		data[3][0] = "Old Colony Group, LLC";
		data[3][1] = "LHYD814690041234";
		data[3][2] = "02/01/2023";
		data[3][3] = "EPL";

		data[4][0] = "Suncoast Communities Blood Bank Inc	";
		data[4][1] = "ENP05087151234";
		data[4][2] = "12/01/2022";
		data[4][3] = "PKG";

//	        data[0][0] ="Powerhouse Electrical Contractors Inc";
//	        data[0][1] ="4869413100";
//	        data[0][2] ="12/31/2022";
//	        data[0][3] ="CAU";
//
//	        data[1][0] ="Event & Media Technologies";
//	        data[1][1] ="7100397360002";
//	        data[1][2] ="10/17/2022";
//	        data[1][3] ="CPP";
//
//	        data[2][0] ="Prelude Solutions, LLC";
//	        data[2][1] ="J06101070";
//	        data[2][2] ="10/06/2022";
//	        data[2][3] ="CRM";
//
//	        data[3][0] ="Cumberland Outdoor Club, Inc.";
//	        data[3][1] ="NPP1558916I";
//	        data[3][2] ="10/26/2022";
//	        data[3][3] ="DOL";
//
//	        data[4][0] ="Nederland Refrigeration, Air Conditioning And Heating Corporation";
//	        data[4][1] ="105493136";
//	        data[4][2] ="09/09/2022";
//	        data[4][3] ="E&O";
//
//	        data[5][0] ="3D2HR, LLC";
//	        data[5][1] ="SPP153622005";
//	        data[5][2] ="10/05/2022";
//	        data[5][3] ="EPL";
//
//	        data[6][0] ="Iron Workers Local 568 Retirement Plan";
//	        data[6][1] ="106182692";
//	        data[6][2] ="01/01/2023";
//	        data[6][3] ="FIL";
//
//	        data[7][0] ="Auto Central Services, Inc.";
//	        data[7][1] ="6203079804";
//	        data[7][2] ="06/30/2022";
//	        data[7][3] ="GAR";
//
//	        data[8][0] ="Med-Scene LLC";
//	        data[8][1] ="GL0551102";
//	        data[8][2] ="10/11/2022";
//	        data[8][3] ="GLI";
//
//	        data[9][0] ="Blue Heron Whitewater LLC";
//	        data[9][1] ="6045696482";
//	        data[9][2] ="11/18/2022";
//	        data[9][3] ="MAR";
//
//	        data[10][0] ="Epic Early Learning LLC";
//	        data[10][1] ="BSRE90287300";
//	        data[10][2] ="09/22/2022";
//	        data[10][3] ="AIR";
//
//	        data[11][0] ="Cumberland Outdoor Club, Inc.";
//	        data[11][1] ="CL1780215F";
//	        data[11][2] ="10/26/2022";
//	        data[11][3] ="LIQ";
//
//	        data[12][0] ="Action Products, Inc.";
//	        data[12][1] ="ZPP15P73448";
//	        data[12][2] ="09/01/2022";
//	        data[12][3] ="PRL";
//
//	        data[13][0] ="Judy'S Mobile Homes, Inc";
//	        data[13][1] ="EPP0669344";
//	        data[13][2] ="11/01/2022";
//	        data[13][3] ="PKG";
//
//	        data[14][0] ="E-lectric Corp.";
//	        data[14][1] ="PWC1074382";
//	        data[14][2] ="11/01/2022";
//	        data[14][3] ="WCO";
//
//	        data[15][0] ="Friends Aware Inc.";
//	        data[15][1] ="PHUB836925";
//	        data[15][2] ="10/15/2022";
//	        data[15][3] ="XLS";
//

		return data;
	}

	@Test(dataProvider = "data-provider")
	public void name(Method m, String naminsured, String policy, String effdate, String lob) {

		webActionUtils.extentTestName(m.getName());

		String accountName = "CBIZ";
		String departmentName = "Alpharetta, GA";

		String accountManager = "Aimee Harrison";
		excelLibrary.getExcelData(XL_TESTPATH, "SmokeTestSheet", 1, 6);

		String policyStatusOpn = "New Business";
		String uploadfilepath1 = DIR_PATH + "/TestDataFiles/5-Policy.pdf";
		String expectedText = excelLibrary.getExcelData(XL_TESTPATH, "SmokeTestSheet", 1, 8);

		LoginPage loginPage = new LoginPage(driver, webActionUtils);
		loginPage.enterUserName(USER_NAME);
		loginPage.enterPassword(PASSWORD);
		loginPage.clickOnSignin();

		UploadFilePage uploadFilePage = new UploadFilePage(driver, webActionUtils);
		uploadFilePage.clickOnUploadFile();

		uploadFilePage.enterFieldvalues3("CTA_08_03 " + naminsured, accountName, departmentName, lob, policy, effdate,
				accountManager, policyStatusOpn, uploadfilepath1, expectedText);
		webActionUtils.refreshPage();
		webActionUtils.waitSleep(30);

	}

}
