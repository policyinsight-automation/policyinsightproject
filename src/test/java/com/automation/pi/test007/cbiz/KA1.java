package com.automation.pi.test007.cbiz;

import java.lang.reflect.Method;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.automation.pi.library.BaseTest;
import com.automation.pi.pages.LoginPage;
import com.automation.pi.pages.UploadFilePage;

class KA1 extends BaseTest {

	@DataProvider(name = "data-provider")
	public Object[][] data() {

		Object[][] data = new Object[3][4];

		data[0][0] = "ShobhanTest2024";
		data[0][1] = "TESTANG";
		data[0][2] = "11/12/2022";
		data[0][3] = "PKG";

		data[1][0] = "ShobhanTest2024";
		data[1][1] = "TEST1234567";
		data[1][2] = "04/01/2021";
		data[1][3] = "PKG";

		data[2][0] = "ShobhanTest2024";
		data[2][1] = "FLOOD";
		data[2][2] = "10/17/2017";
		data[2][3] = "FLC";

		return data;
	}

	@Test(dataProvider = "data-provider")
	public void name(Method m, String naminsured, String policy, String effdate, String lob) {

		webActionUtils.extentTestName(m.getName());

		webActionUtils.extentTestName(m.getName());
		webActionUtils.TestCaseinfo("================================");
		webActionUtils.TestCaseinfo(" Test Case Name : Settings - Order of Settings page ");
		webActionUtils.TestCaseinfo("================================");

		String accountName = "CBIZ";
		String departmentName = "CBIZ Office - CBIZ Segment - CBIZ Check";

//		String accountName = "SCM";
//		String departmentName = "CL";

		String accountManager = "CBIZ- Shobhan";
		// excelLibrary.getExcelData(XL_TESTPATH, "SmokeTestSheet", 1, 6);

		String policyStatusOpn = "New Business";
		String uploadfilepath1 = "C:\\Users\\Shobhan_Shiva\\policyinsightproject\\TestDataFiles\\5-Policy.pdf";
		String expectedText = "";

		LoginPage loginPage = new LoginPage(driver, webActionUtils);
		loginPage.enterUserName(USER_NAME);
		loginPage.enterPassword(PASSWORD);
		loginPage.clickOnSignin();

		UploadFilePage uploadFilePage = new UploadFilePage(driver, webActionUtils);
		uploadFilePage.clickOnUploadFile();

		uploadFilePage.enterFieldvalues3("2024_Shobhan" + naminsured, accountName, departmentName, lob, policy, effdate,
				accountManager, policyStatusOpn, uploadfilepath1, expectedText);
		webActionUtils.refreshPage();
		webActionUtils.waitSleep(30);

	}
}
