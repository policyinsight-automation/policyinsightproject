//package com.automation.pi.test007.cbiz;
//
//import java.lang.reflect.Method;
//
//import org.testng.annotations.DataProvider;
//import org.testng.annotations.Test;
//
//import com.automation.pi.library.BaseTest;
//import com.automation.pi.pages.LoginPage;
//import com.automation.pi.pages.UploadFilePage;
//
//public class KA3 extends BaseTest {
//
//	@DataProvider(name = "data-provider")
//	public Object[][] data() {
//		Object[][] data = new Object[1][4];
//
//		data[0][0] = "ATC Fitness Exeter Village";
//		data[0][1] = "21WECAP1W9Z1234";
//		data[0][2] = "1/5/2023";
//		data[0][3] = "WCO";
//
//		return data;
//	}
//
//	@Test(dataProvider = "data-provider")
//	public void name(Method m, String naminsured, String policy, String effdate, String lob) {
//
//		webActionUtils.extentTestName(m.getName());
//
//		String accountName = "CBIZ";
//		String departmentName = "Alpharetta, GA - Full Check";
//		String accountManager = "Jessica Lamar";
//		excelLibrary.getExcelData(XL_TESTPATH, "SmokeTestSheet", 1, 6);
//		String policyStatusOpn = "Renewal";
//		String expectedText = excelLibrary.getExcelData(XL_TESTPATH, "SmokeTestSheet", 1, 8);
//		String uploadfilepath1 = DIR_PATH + "/TestDataFiles/5-Policy.pdf";
////		String uploadfilepath2 = DIR_PATH + "/TestDataFiles/Exp Pol.pdf";
////		String uploadfilepath4 = DIR_PATH + "/TestDataFiles/Traveller Binder.pdf";
////		String uploadfilepath3 = DIR_PATH + "/TestDataFiles/5-Proposal.pdf";
//
//		LoginPage loginPage = new LoginPage(driver, webActionUtils);
//		loginPage.enterUserName(USER_NAME);
//		loginPage.enterPassword(PASSWORD);
//		loginPage.clickOnSignin();
//
//		UploadFilePage uploadFilePage = new UploadFilePage(driver, webActionUtils);
//		uploadFilePage.clickOnUploadFile();
//
//		uploadFilePage.enterFieldvalues3("Mia_19_01 " + naminsured, accountName, departmentName, lob, policy, effdate,
//				accountManager, policyStatusOpn, uploadfilepath1, expectedText);
//		webActionUtils.refreshPage();
//		webActionUtils.waitSleep(30);
//	}
//}