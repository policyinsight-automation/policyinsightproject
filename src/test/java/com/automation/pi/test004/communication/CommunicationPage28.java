package com.automation.pi.test004.communication;

import java.lang.reflect.Method;

import org.testng.annotations.Test;

import com.automation.pi.library.BaseTest;
import com.automation.pi.pages.CommunicationsPage;
import com.automation.pi.pages.LoginPage;
import com.automation.pi.pages.ManagePage;

public class CommunicationPage28 extends BaseTest 
{//797
	@Test
	public void PPR_7943_Test(Method m) 
	{
		webActionUtils.extentTestName(m.getName());
		webActionUtils.TestCaseinfo("================================");
		webActionUtils.TestCaseinfo("Communication Page - Communication function - Create one communication");
		webActionUtils.TestCaseinfo("================================");
		
		String title = "ManagePackage - Policy Checking Platform";
		String commTitle="CreateEditCommunication - Policy Checking Platform";
		String msgText= "Hi Test Here";
		String serialNumber = "196529";//"197196";

		LoginPage loginPage = new LoginPage(driver, webActionUtils);
		loginPage.enterUserName("Shobhan_shiva:sarah_suo");
		loginPage.enterPassword(PASSWORD);
		loginPage.clickOnSignin();
		
		ManagePage managePage = new ManagePage(driver, webActionUtils);
		managePage.clickManagePageTab();
		managePage.verifyManagePage(title);
		managePage.searchSerialNumber(serialNumber);
		managePage.clickStatus();
		managePage.clickSelectAll();
		managePage.clickAdvanceFiltersButton();
		managePage.selectDateCreated3more();
		managePage.clickAdvanceFiltersButton();
		managePage.clickSearchButton();
		managePage.verifySearchedRecord(serialNumber);
		managePage.selectFirstRecordCheckBox();
		managePage.clickOnCreateOneCommunication(commTitle);
		
		CommunicationsPage communicationsPage = new CommunicationsPage(driver, webActionUtils);
		communicationsPage.verifyCommunicationPage(commTitle);
		communicationsPage.EnterChat(msgText);
		communicationsPage.clickOnCommSend();
		managePage.windowClose(commTitle);
	}
}