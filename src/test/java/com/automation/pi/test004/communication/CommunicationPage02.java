package com.automation.pi.test004.communication;

import java.lang.reflect.Method;

import org.testng.annotations.Test;

import com.automation.pi.library.BaseTest;
import com.automation.pi.pages.CommunicationsPage;
import com.automation.pi.pages.LoginPage;

public class CommunicationPage02 extends BaseTest 
{//740
	@Test
	public void PPR_4407_Test(Method m)
	{
		webActionUtils.extentTestName(m.getName());
		webActionUtils.TestCaseinfo("================================");
		webActionUtils.TestCaseinfo(" Test Case Name : Communication Page - Filter function - Owner switch - ALL");
		webActionUtils.TestCaseinfo("================================");
		
		String title = "ManageCommunication - Policy Checking Platform";
		String text = "All";

		LoginPage loginPage = new LoginPage(driver, webActionUtils);
		loginPage.enterUserName(USER_NAME);
		loginPage.enterPassword(PASSWORD);
		loginPage.clickOnSignin();
		
		CommunicationsPage communicationsPage = new CommunicationsPage(driver, webActionUtils);
		communicationsPage.clickCommunicationPageTab();
		communicationsPage.verifyCommunicationPage(title);
		communicationsPage.clickOnMinefilter();
		communicationsPage.verifyAllFilter(text);
		communicationsPage.clickOnSearch();
		communicationsPage.displayListofCommunicationPackage();	
	}
}