package com.automation.pi.test004.communication;

import java.lang.reflect.Method;

import org.testng.annotations.Test;

import com.automation.pi.library.BaseTest;
import com.automation.pi.pages.CommunicationsPage;
import com.automation.pi.pages.LoginPage;

public class CommunicationPage14 extends BaseTest
{//752
	@Test
	public void PPR_4421_Test(Method m) 
	{
		webActionUtils.extentTestName(m.getName());
		webActionUtils.TestCaseinfo("================================");
		webActionUtils.TestCaseinfo(" Communication Page - Filter function - Status filter - Merged  status");
		webActionUtils.TestCaseinfo("================================");

		String title = "ManageCommunication - Policy Checking Platform";

		LoginPage loginPage = new LoginPage(driver, webActionUtils);
		loginPage.enterUserName(USER_NAME);
		loginPage.enterPassword(PASSWORD);
		loginPage.clickOnSignin();

		CommunicationsPage communicationsPage = new CommunicationsPage(driver, webActionUtils);
		communicationsPage.clickCommunicationPageTab();
		communicationsPage.verifyCommunicationPage(title);
		communicationsPage.selectStatusMerged();
		communicationsPage.clickOnSearch();
		communicationsPage.displayListofCommunicationPackage();
	}
}