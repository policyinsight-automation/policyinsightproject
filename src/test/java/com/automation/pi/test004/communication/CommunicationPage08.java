package com.automation.pi.test004.communication;

import java.lang.reflect.Method;

import org.testng.annotations.Test;

import com.automation.pi.library.BaseTest;
import com.automation.pi.pages.CommunicationsPage;
import com.automation.pi.pages.LoginPage;

public class CommunicationPage08 extends BaseTest 
{//746
	@Test
	public void PPR_4413_Test(Method m)
	{
		webActionUtils.extentTestName(m.getName());
		webActionUtils.TestCaseinfo("================================");
		webActionUtils.TestCaseinfo(" Test Case Name : Communication Page - Search function - Main Text");
		webActionUtils.TestCaseinfo("================================");
		
		String title = "ManageCommunication - Policy Checking Platform";
		String status= "All Status";
		String text =	"Please";

		LoginPage loginPage = new LoginPage(driver, webActionUtils);
		loginPage.enterUserName(USER_NAME);
		loginPage.enterPassword(PASSWORD);
		loginPage.clickOnSignin();
		
		CommunicationsPage communicationsPage = new CommunicationsPage(driver, webActionUtils);
		communicationsPage.clickCommunicationPageTab();
		communicationsPage.verifyCommunicationPage(title);
	
		communicationsPage.selectRequiredStatus(status);
		communicationsPage.enterMainText(text);
		communicationsPage.clickOnSearch();
		communicationsPage.displayListofCommunicationPackage();
		communicationsPage.verifyMainTextIsPreset(text);
	}
}