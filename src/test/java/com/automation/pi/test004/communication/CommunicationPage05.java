package com.automation.pi.test004.communication;

import java.lang.reflect.Method;

import org.testng.annotations.Test;

import com.automation.pi.library.BaseTest;
import com.automation.pi.pages.CommunicationsPage;
import com.automation.pi.pages.LoginPage;

public class CommunicationPage05 extends BaseTest 
{//743
	@Test
	public void PPR_4410_Test(Method m)
	{
		webActionUtils.extentTestName(m.getName());
		webActionUtils.TestCaseinfo("================================");
		webActionUtils.TestCaseinfo(" Test Case Name : Communication Page - Filter function - Title");
		webActionUtils.TestCaseinfo("================================");
		
		String title = "ManageCommunication - Policy Checking Platform";
		String text = "Ni Shobhan - BOP - POL";;//"NI Named Insured-1 sat - Abuse or Molestation";//"Test-ugf - Earthquake ";//	"sherry0803test1234";

		LoginPage loginPage = new LoginPage(driver, webActionUtils);
		loginPage.enterUserName(USER_NAME);
		loginPage.enterPassword(PASSWORD);
		loginPage.clickOnSignin();
		
		CommunicationsPage communicationsPage = new CommunicationsPage(driver, webActionUtils);
		communicationsPage.clickCommunicationPageTab();
		communicationsPage.verifyCommunicationPage(title);
		communicationsPage.enterTitle(text);
		communicationsPage.clickOnSearch();
		communicationsPage.displayListofCommunicationPackage();
	}
}