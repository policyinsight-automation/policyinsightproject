package com.automation.pi.test004.communication;

import java.lang.reflect.Method;

import org.apache.commons.lang3.RandomStringUtils;
import org.testng.annotations.Test;

import com.automation.pi.library.BaseTest;
import com.automation.pi.pages.CommunicationsPage;
import com.automation.pi.pages.LoginPage;
import com.automation.pi.pages.ManagePage;
import com.automation.pi.pages.UploadFilePage;

public class CommunicationPage29 extends BaseTest 
{//829  Insert into EmployeeRole Values (14335, 2)
	@Test
	public void PPR_4414_Test(Method m) 
	{
		webActionUtils.extentTestName(m.getName());
		webActionUtils.TestCaseinfo("================================");
		webActionUtils.TestCaseinfo("Communication Page - Communication function - Close communication");
		webActionUtils.TestCaseinfo("================================");
		

		String accountName = "CBIZ";
		String departmentName = "Columbia, MD - Auto Renewal";
		String nameInsured = "TestKS001 Shobhan_" + RandomStringUtils.randomAlphabetic(5);
		//String nameInsured1 = nameInsured;
		String lob = "PKG";
		String policyNumber = "PHPK2241920" + RandomStringUtils.randomAlphabetic(3);
		//String policyNumber1 = policyNumber;
		String effDate = "03/05/2021";
		String accountManager = "Carcarey Dolores";// "Avery Clark";
		String policyStatusOpn = "New Business";
		String uploadfilepath1 = DIR_PATH + "/TestDataFiles/5-Policy.pdf";
		//String expectedText = "Message: You have successfully submitted a task! - Serial# :";
		//String expectedText1="There is already policy uploaded for the same Policy#, Effective Date & LOB. Do you want to continue?";
		String title = "ManagePackage - Policy Checking Platform";
		String Message = "Shobhan Shiva closed this communication.";
		//String title2 = "CreateEditCommunication - Policy Checking Platform";

		
//		LoginPage loginPage = new LoginPage(driver, webActionUtils);
//		//loginPage.enterUserName("Shobhan_shiva:balu_subbu");
//		loginPage.enterUserName(USER_NAME);
//		loginPage.enterPassword(PASSWORD);
//		loginPage.clickOnSignin();
//
//		UploadFilePage uploadFile = new UploadFilePage(driver, webActionUtils);
//		uploadFile.clickOnUploadFile();
//		uploadFile.enterFieldValuesInUpload1(nameInsured, accountName, departmentName, lob, policyNumber, effDate,accountManager, policyStatusOpn, uploadfilepath1, expectedText);
//		uploadFile.submitButton();
//		String serialNumber = uploadFile.getSerialNumber();
//		ManagePage managePage = new ManagePage(driver, webActionUtils);
//		managePage.clickManagePageTab();
//		managePage.verifyManagePage(title);
//		managePage.searchSerialNumber(serialNumber);
//		managePage.clickStatus();
//		managePage.clickSelectAll();
//		//webActionUtils.waitSleep(10);
//		managePage.clickSearchButton();
//		managePage.selectFirstRecordCheckBox1(serialNumber);
//		managePage.clickEditButton();
//		managePage.changeNameinsuredandLOB();
//		managePage.enterRequiredEditPagedetails();
//		managePage.clickOnEditSaveButton();
//		managePage.clickOnCommunicationIcon();
//		
//		CommunicationsPage communicationsPage = new CommunicationsPage(driver, webActionUtils);
//		communicationsPage.clickOnCommnicationCloseButton();
//		communicationsPage.clickOnYesOnCommnicationPopUp();
//		communicationsPage.VerifyCommunicationEnded(Message);
//
//		
		
		
		LoginPage loginPage = new LoginPage(driver, webActionUtils);
		loginPage.enterUserName(USER_NAME);
		loginPage.enterPassword(PASSWORD);
		loginPage.clickOnSignin();
		UploadFilePage uploadFile = new UploadFilePage(driver, webActionUtils);
		uploadFile.clickOnUploadFile();
		uploadFile.enterRequiredAccount(accountName);
		uploadFile.enterRequiredDepartment(departmentName);
		uploadFile.enterRequiredNamedInsured(nameInsured);
		uploadFile.enterRequiredLOB(lob);
		uploadFile.enterRequiredPolicyNumber(policyNumber);
		uploadFile.enterRequiredEffectiveDate(effDate);
		uploadFile.enterHighlightedDateFromClient();
		uploadFile.enterRequiredAccountManager(accountManager);
		uploadFile.enterRequiredPolicyStatus(policyStatusOpn);
		uploadFile.uploadRequiredFiles(uploadfilepath1);
		uploadFile.clickonSubmitButton();
		String serialNumber = uploadFile.getSerialNumber();
		uploadFile.clickOnSerialNumber();
		
		ManagePage managePage = new ManagePage(driver, webActionUtils);
		managePage.verifyManagePage(title);
		managePage.selectFirstRecordCheckBox1(serialNumber);
		managePage.clickEditButton();
		managePage.changeNameinsuredandLOB();
		managePage.enterRequiredEditPagedetails();
		managePage.clickOnEditSaveButton();
		managePage.clickOnCommunicationIcon();
		
		CommunicationsPage communicationsPage = new CommunicationsPage(driver, webActionUtils);
		communicationsPage.clickOnCommnicationCloseButton();
		communicationsPage.clickOnYesOnCommnicationPopUp();
		communicationsPage.VerifyCommunicationEnded(Message);		
	}
}