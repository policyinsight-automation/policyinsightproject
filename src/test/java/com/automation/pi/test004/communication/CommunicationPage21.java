package com.automation.pi.test004.communication;

import java.lang.reflect.Method;

import org.testng.annotations.Test;

import com.automation.pi.library.BaseTest;
import com.automation.pi.pages.CommunicationsPage;
import com.automation.pi.pages.LoginPage;

public class CommunicationPage21 extends BaseTest 
{//759
	@Test
	public void PPR_4429_Test(Method m)
	{
		webActionUtils.extentTestName(m.getName());
		webActionUtils.TestCaseinfo("================================");
		webActionUtils.TestCaseinfo("Communication Page - Communication function - Navigate to communication detail page.");
		webActionUtils.TestCaseinfo("================================");
		
		String title = "ManageCommunication - Policy Checking Platform";
		String title2="CreateEditCommunication - Policy Checking Platform";
		String expectedHeader ="Communication Details";
		String pkgid ="188061";//"188061";//"187528"; 

		LoginPage loginPage = new LoginPage(driver, webActionUtils);
		loginPage.enterUserName(USER_NAME);
		loginPage.enterPassword(PASSWORD);
		loginPage.clickOnSignin();
		
		CommunicationsPage communicationsPage = new CommunicationsPage(driver, webActionUtils);
		communicationsPage.clickCommunicationPageTab();
		communicationsPage.verifyCommunicationPage(title);
		communicationsPage.enterSerialNumber(pkgid);
		communicationsPage.clickOnSearch();
		communicationsPage.displayListofCommunicationPackage();
		communicationsPage.clickOnPackageCommunicationTitle();			
		communicationsPage.verifyCreateEditCommunicationPage(title2);
		communicationsPage.verifyCommunicationHeaderPage(expectedHeader);
	}
}