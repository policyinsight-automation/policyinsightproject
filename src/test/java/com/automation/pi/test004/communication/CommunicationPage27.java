package com.automation.pi.test004.communication;

import java.lang.reflect.Method;

import org.testng.annotations.Test;

import com.automation.pi.library.BaseTest;
import com.automation.pi.pages.CommunicationsPage;
import com.automation.pi.pages.LoginPage;

public class CommunicationPage27 extends BaseTest 
{//760
	@Test
	public void PPR_4430_Test(Method m) 
	{
		webActionUtils.extentTestName(m.getName());
		webActionUtils.TestCaseinfo("================================");
		webActionUtils.TestCaseinfo("Communication Page - Communication function - Back to Main Page");
		webActionUtils.TestCaseinfo("================================");
		
		String title = "ManageCommunication - Policy Checking Platform";
		String title2="CreateEditCommunication - Policy Checking Platform";
		String expectedHeader ="Communication Details";
		String pkgid ="188061";//"187168";//"187528"; 
		String msgText= "Hi Test Here";

		LoginPage loginPage = new LoginPage(driver, webActionUtils);
		loginPage.enterUserName(USER_NAME);
		loginPage.enterPassword(PASSWORD);
		loginPage.clickOnSignin();
		
		CommunicationsPage communicationsPage = new CommunicationsPage(driver, webActionUtils);
		communicationsPage.clickCommunicationPageTab();
		communicationsPage.verifyCommunicationPage(title);
		communicationsPage.enterSerialNumber(pkgid);
		communicationsPage.clickOnSearch();
		communicationsPage.displayListofCommunicationPackage();
		communicationsPage.clickOnPackageCommunicationTitle();
		communicationsPage.verifyCreateEditCommunicationPage(title2);
		communicationsPage.verifyCommunicationHeaderPage(expectedHeader);
		communicationsPage.EnterChat(msgText);
		communicationsPage.clickOnCommSend();
		webActionUtils.navigateBack();
		communicationsPage.displayListofCommunicationPackage();
	}
}