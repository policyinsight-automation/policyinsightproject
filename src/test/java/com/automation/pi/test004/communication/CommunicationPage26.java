package com.automation.pi.test004.communication;

import java.lang.reflect.Method;

import org.testng.annotations.Test;

import com.automation.pi.library.BaseTest;
import com.automation.pi.pages.CommunicationsPage;
import com.automation.pi.pages.LoginPage;

public class CommunicationPage26 extends BaseTest 
{//764
	@Test
	public void PPR_4434_Test(Method m)
	{	
		webActionUtils.extentTestName(m.getName());
		webActionUtils.TestCaseinfo("================================");
		webActionUtils.TestCaseinfo("Communication Page -  Communication function - Download Attachment from attachment box.");
		webActionUtils.TestCaseinfo("================================");
		
		String title = "ManageCommunication - Policy Checking Platform";
		String title2="CreateEditCommunication - Policy Checking Platform";
		String expectedHeader ="Communication Details";
		String pkgid ="188061";//"187168";//187528"; 
		String msgText2= "Hi there, The policy is not a full copy and the DEC page misses some key information. Please check if there is a full copy for this policy in system. If not, please help ask for a full copy from client. Thank you! 5-Policy.pdf";
		String selectCommOption= "Full Copy – Confirm with Client";
		String uploadfilepath1 = DIR_PATH+"/TestDataFiles/5-Policy.pdf";

		LoginPage loginPage = new LoginPage(driver, webActionUtils);
		loginPage.enterUserName(USER_NAME);
		loginPage.enterPassword(PASSWORD);
		loginPage.clickOnSignin();
		
		CommunicationsPage communicationsPage = new CommunicationsPage(driver, webActionUtils);
		communicationsPage.clickCommunicationPageTab();
		communicationsPage.verifyCommunicationPage(title);
		communicationsPage.enterSerialNumber(pkgid);
		communicationsPage.clickOnSearch();
		communicationsPage.displayListofCommunicationPackage();
		communicationsPage.clickOnPackageCommunicationTitle();
		communicationsPage.verifyCreateEditCommunicationPage(title2);
		communicationsPage.verifyCommunicationHeaderPage(expectedHeader);
		communicationsPage.selectRequiredCommunicationfunction(selectCommOption);
		communicationsPage.uploadfiles(uploadfilepath1);
		communicationsPage.clickOnCommSend();
		communicationsPage.verifyCommunicationSentMessage3(msgText2);
		communicationsPage.downloadfileAttachment();
		
	}
}