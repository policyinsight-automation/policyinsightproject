package com.automation.pi.test001.loginlogoutpage;

import java.lang.reflect.Method;

import org.testng.annotations.Test;

import com.automation.pi.library.BaseTest;
import com.automation.pi.pages.HomePage;
import com.automation.pi.pages.LoginPage;
import com.automation.pi.pages.ManagePage;

public class LoginLogout06 extends BaseTest {// 775

	@Test
	public void pidc_tc_909(Method m) {

		webActionUtils.extentTestName(m.getName());
		webActionUtils.TestCaseinfo("================================");
		webActionUtils.TestCaseinfo(" Test Case Name : Logout from the PI Application as PI user");
		webActionUtils.TestCaseinfo("================================");

		String loginTitle = excelLibrary.getExcelData(XL_TESTPATH, "LoginTC", 16, 1);
		String managePageTitle = excelLibrary.getExcelData(XL_TESTPATH, "LoginTC", 16, 2);

		LoginPage loginPage = new LoginPage(driver, webActionUtils);
		loginPage.verifyLoginPageTitle(loginTitle);
		loginPage.enterUserName(USER_NAME_PI);
		loginPage.enterPassword(PASSWORD);
		loginPage.clickOnSignin();

		ManagePage managePage = new ManagePage(driver, webActionUtils);
		managePage.verifyManagePage(managePageTitle);

		HomePage homePage = new HomePage(driver, webActionUtils);
		homePage.clickOnSignOutIcon();
		homePage.clickOnYes();
		loginPage.verifyLoginPageTitle(loginTitle);
	}
}