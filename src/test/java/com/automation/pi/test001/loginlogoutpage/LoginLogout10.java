package com.automation.pi.test001.loginlogoutpage;

import java.lang.reflect.Method;

import org.testng.annotations.Test;

import com.automation.pi.library.BaseTest;
import com.automation.pi.pages.LoginPage;

public class LoginLogout10 extends BaseTest {// 780
	
	@Test
	public void pidc_tc_914(Method m) {
		webActionUtils.extentTestName(m.getName());
		webActionUtils.TestCaseinfo("================================");
		webActionUtils.TestCaseinfo(" Test Case Name : Invalid Username and Invalid Password verification");
		webActionUtils.TestCaseinfo("================================");

		String loginTitle = excelLibrary.getExcelData(XL_TESTPATH, "LoginTC", 28, 1);
		String expectedError = excelLibrary.getExcelData(XL_TESTPATH, "LoginTC", 28, 2);

		LoginPage loginPage = new LoginPage(driver, webActionUtils);
		loginPage.verifyLoginPageTitle(loginTitle);
		loginPage.enterUserName(INVALID_USER_NAME);
		loginPage.enterPassword(INVALID_PASSWORD);
		loginPage.clickOnSignin();
		loginPage.verifyErrorMessage(expectedError);
	}
}