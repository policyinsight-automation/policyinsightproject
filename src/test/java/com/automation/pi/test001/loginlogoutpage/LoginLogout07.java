package com.automation.pi.test001.loginlogoutpage;

import java.lang.reflect.Method;

import org.testng.annotations.Test;

import com.automation.pi.library.BaseTest;
import com.automation.pi.pages.HomePage;
import com.automation.pi.pages.LoginPage;
import com.automation.pi.pages.UploadFilePage;

public class LoginLogout07 extends BaseTest {// 776

	@Test
	public void pidc_tc_910(Method m) {

		webActionUtils.extentTestName(m.getName());
		webActionUtils.TestCaseinfo("================================");
		webActionUtils.TestCaseinfo(" Test Case Name : Logout from the PI Application as FBE user");
		webActionUtils.TestCaseinfo("================================");

		String loginTitle = excelLibrary.getExcelData(XL_TESTPATH, "LoginTC", 19, 1);
		String uploadFileTitle = excelLibrary.getExcelData(XL_TESTPATH, "LoginTC", 19, 2);

		LoginPage loginPage = new LoginPage(driver, webActionUtils);
		loginPage.verifyLoginPageTitle(loginTitle);
		loginPage.enterUserName(USER_NAME_FBE);
		loginPage.enterPassword(PASSWORD);
		loginPage.clickOnSignin();

		UploadFilePage uploadFilePage = new UploadFilePage(driver, webActionUtils);
		uploadFilePage.verifyUploadFile(uploadFileTitle);

		HomePage homePage = new HomePage(driver, webActionUtils);
		homePage.clickOnSignOutIcon();
		homePage.clickOnYes();
		loginPage.verifyLoginPageTitle(loginTitle);
	}
}