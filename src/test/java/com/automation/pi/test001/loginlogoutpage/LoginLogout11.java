package com.automation.pi.test001.loginlogoutpage;

import java.lang.reflect.Method;

import org.testng.annotations.Test;

import com.automation.pi.library.BaseTest;
import com.automation.pi.pages.LoginPage;

public class LoginLogout11 extends BaseTest {// 781

	@Test
	public void pidc_tc_915(Method m) {

		webActionUtils.extentTestName(m.getName());
		webActionUtils.TestCaseinfo("================================");
		webActionUtils.TestCaseinfo(" Test Case Name : Verify that Error message is getting displayed");
		webActionUtils.TestCaseinfo("================================");

		String loginTitle = excelLibrary.getExcelData(XL_TESTPATH, "LoginTC", 31, 1);
		String expectedError = excelLibrary.getExcelData(XL_TESTPATH, "LoginTC", 31, 2);

		LoginPage loginPage = new LoginPage(driver, webActionUtils);
		loginPage.verifyLoginPageTitle(loginTitle);
		loginPage.clickOnSignin();
		loginPage.verifyErrorMessage(expectedError);
	}
}