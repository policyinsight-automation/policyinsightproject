package com.automation.pi.test001.loginlogoutpage;

import java.lang.reflect.Method;

import org.testng.annotations.Test;

import com.automation.pi.library.BaseTest;
import com.automation.pi.pages.LoginPage;

public class LoginLogout09 extends BaseTest {// 778

	@Test
	public void pidc_tc_912(Method m) {

		webActionUtils.extentTestName(m.getName());
		webActionUtils.TestCaseinfo("================================");
		webActionUtils.TestCaseinfo(" Test Case Name : Valid Username and Invalid Password Verification");
		webActionUtils.TestCaseinfo("================================");

		String loginTitle = excelLibrary.getExcelData(XL_TESTPATH, "LoginTC", 25, 1);
		String expectedError = excelLibrary.getExcelData(XL_TESTPATH, "LoginTC", 25, 2);

		LoginPage loginPage = new LoginPage(driver, webActionUtils);
		loginPage.verifyLoginPageTitle(loginTitle);
		loginPage.enterUserName(USER_NAME);
		loginPage.enterPassword(INVALID_PASSWORD);
		loginPage.clickOnSignin();
		loginPage.verifyErrorMessage(expectedError);
	}
}