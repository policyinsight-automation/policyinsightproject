package com.automation.pi.test001.loginlogoutpage;

import java.lang.reflect.Method;

import org.testng.annotations.Test;

import com.automation.pi.library.BaseTest;
import com.automation.pi.pages.HomePage;
import com.automation.pi.pages.LoginPage;
import com.automation.pi.pages.UploadFilePage;

public class LoginLogout03 extends BaseTest {// 772

	@Test
	public void pidc_tc_906(Method m) {

		webActionUtils.extentTestName(m.getName());
		webActionUtils.TestCaseinfo("================================");
		webActionUtils.TestCaseinfo(" Test Case Name : Log In - Access to the system as FBE User");
		webActionUtils.TestCaseinfo("================================");

		String loginTitle = excelLibrary.getExcelData(XL_TESTPATH, "LoginTC", 7, 1);
		String uploadFileTitle = excelLibrary.getExcelData(XL_TESTPATH, "LoginTC", 7, 2);
		String uploadFileTabText = excelLibrary.getExcelData(XL_TESTPATH, "LoginTC", 7, 3);
		String managePageTabText = excelLibrary.getExcelData(XL_TESTPATH, "LoginTC", 7, 4);
		String communicationsTabText = excelLibrary.getExcelData(XL_TESTPATH, "LoginTC", 7, 5);
		String reportsTabText = excelLibrary.getExcelData(XL_TESTPATH, "LoginTC", 7, 6);

		LoginPage loginPage = new LoginPage(driver, webActionUtils);
		loginPage.verifyLoginPageTitle(loginTitle);
		loginPage.enterUserName(USER_NAME_FBE);
		loginPage.enterPassword(PASSWORD);
		loginPage.clickOnSignin();

		HomePage homePage = new HomePage(driver, webActionUtils);
		homePage.verifyFBEUserTabs(uploadFileTabText, managePageTabText, communicationsTabText, reportsTabText);

		UploadFilePage uploadFilePage = new UploadFilePage(driver, webActionUtils);
		uploadFilePage.verifyUploadFile(uploadFileTitle);
	}
}