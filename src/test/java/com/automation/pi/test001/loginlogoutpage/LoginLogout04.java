package com.automation.pi.test001.loginlogoutpage;

import java.lang.reflect.Method;

import org.testng.annotations.Test;

import com.automation.pi.library.BaseTest;
import com.automation.pi.pages.HomePage;
import com.automation.pi.pages.LoginPage;
import com.automation.pi.pages.Settings;

public class LoginLogout04 extends BaseTest {// 773

	@Test
	public void pidc_tc_907(Method m) {

		webActionUtils.extentTestName(m.getName());
		webActionUtils.TestCaseinfo("================================");
		webActionUtils.TestCaseinfo(" Test Case Name : Log In - Access to the system as Admin");
		webActionUtils.TestCaseinfo("================================");

		String loginTitle = excelLibrary.getExcelData(XL_TESTPATH, "LoginTC", 10, 1);
		String uploadFileTabText = excelLibrary.getExcelData(XL_TESTPATH, "LoginTC", 10, 2);
		String managePageTabText = excelLibrary.getExcelData(XL_TESTPATH, "LoginTC", 10, 3);
		String communicationsTabText = excelLibrary.getExcelData(XL_TESTPATH, "LoginTC", 10, 4);
		String reportsTabText = excelLibrary.getExcelData(XL_TESTPATH, "LoginTC", 10, 5);
		String settingsTabText = excelLibrary.getExcelData(XL_TESTPATH, "LoginTC", 10, 6);
		String DepartmentSettingPageTitle = excelLibrary.getExcelData(XL_TESTPATH, "LoginTC", 10, 7);

		LoginPage loginPage = new LoginPage(driver, webActionUtils);
		loginPage.verifyLoginPageTitle(loginTitle);
		loginPage.enterUserName(USER_NAME);
		loginPage.enterPassword(PASSWORD);
		loginPage.clickOnSignin();

		HomePage homePage = new HomePage(driver, webActionUtils);
		homePage.verifyAdminUserTabs(uploadFileTabText, managePageTabText, communicationsTabText, reportsTabText,
				settingsTabText);

		Settings Settings = new Settings(driver, webActionUtils);
		Settings.verifyDepartmentSettingPage(DepartmentSettingPageTitle);
	}
}