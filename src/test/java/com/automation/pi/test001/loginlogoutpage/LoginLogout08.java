package com.automation.pi.test001.loginlogoutpage;

import java.lang.reflect.Method;

import org.testng.annotations.Test;

import com.automation.pi.library.BaseTest;
import com.automation.pi.pages.HomePage;
import com.automation.pi.pages.LoginPage;
import com.automation.pi.pages.Settings;

public class LoginLogout08 extends BaseTest {

	@Test
	public void pidc_tc_911(Method m) {

		webActionUtils.extentTestName(m.getName());
		webActionUtils.TestCaseinfo("================================");
		webActionUtils.TestCaseinfo(" Test Case Name : Logout from the PI Application as Admin");
		webActionUtils.TestCaseinfo("================================");

		String loginTitle = excelLibrary.getExcelData(XL_TESTPATH, "LoginTC", 22, 1);
		String uploadFileTabText = excelLibrary.getExcelData(XL_TESTPATH, "LoginTC", 22, 2);
		String managePageTabText = excelLibrary.getExcelData(XL_TESTPATH, "LoginTC", 22, 3);
		String communicationsTabText = excelLibrary.getExcelData(XL_TESTPATH, "LoginTC", 22, 4);
		String reportsTabText = excelLibrary.getExcelData(XL_TESTPATH, "LoginTC", 22, 5);
		String settingsTabText = excelLibrary.getExcelData(XL_TESTPATH, "LoginTC", 22, 6);
		String DepartmentSettingPageTitle = excelLibrary.getExcelData(XL_TESTPATH, "LoginTC", 22, 7);

		LoginPage loginPage = new LoginPage(driver, webActionUtils);
		loginPage.verifyLoginPageTitle(loginTitle);
		loginPage.enterUserName(USER_NAME);
		loginPage.enterPassword(PASSWORD);
		loginPage.clickOnSignin();
		// webActionUtils.waitSleep(5);

		HomePage homePage = new HomePage(driver, webActionUtils);
		homePage.verifyAdminUserTabs(uploadFileTabText, managePageTabText, communicationsTabText, reportsTabText,
				settingsTabText);

		Settings Settings = new Settings(driver, webActionUtils);
		Settings.verifyDepartmentSettingPage(DepartmentSettingPageTitle);

		homePage.clickOnSignOutIcon();
		homePage.clickOnYes();
		loginPage.verifyLoginPageTitle(loginTitle);
	}
}