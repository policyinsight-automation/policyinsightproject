package com.automation.pi.test001.loginlogoutpage;

import java.lang.reflect.Method;

import org.testng.annotations.Test;

import com.automation.pi.library.BaseTest;
import com.automation.pi.pages.HomePage;
import com.automation.pi.pages.LoginPage;
import com.automation.pi.pages.ManagePage;

public class LoginLogout02 extends BaseTest {

	@Test
	public void pidc_tc_905(Method m) {

		webActionUtils.extentTestName(m.getName());
		webActionUtils.TestCaseinfo("================================");
		webActionUtils.TestCaseinfo(" Test Case Name : Log In - Access to the system as PI User");
		webActionUtils.TestCaseinfo("================================");

		String loginTitle = excelLibrary.getExcelData(XL_TESTPATH, "LoginTC", 4, 1);
		String managePageTitle = excelLibrary.getExcelData(XL_TESTPATH, "LoginTC", 4, 2);
		String managePageTabText = excelLibrary.getExcelData(XL_TESTPATH, "LoginTC", 4, 4);
		String reportsTabText = excelLibrary.getExcelData(XL_TESTPATH, "LoginTC", 4, 5);

		LoginPage loginPage = new LoginPage(driver, webActionUtils);
		loginPage.verifyLoginPageTitle(loginTitle);
		loginPage.enterUserName(USER_NAME_PI);
		loginPage.enterPassword(PASSWORD);
		loginPage.clickOnSignin();
		webActionUtils.waitSleep(4);

		HomePage homePage = new HomePage(driver, webActionUtils);
		homePage.verifyPIUserTabs(managePageTabText, reportsTabText);

		ManagePage managePage = new ManagePage(driver, webActionUtils);
		managePage.verifyManagePage(managePageTitle);
	}
}