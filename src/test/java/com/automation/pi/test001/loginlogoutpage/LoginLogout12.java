package com.automation.pi.test001.loginlogoutpage;

import java.lang.reflect.Method;

import org.testng.annotations.Test;

import com.automation.pi.library.BaseTest;
import com.automation.pi.pages.LoginPage;

public class LoginLogout12 extends BaseTest {// 804

	@Test
	public void pidc_tc_938(Method m) {

		webActionUtils.extentTestName(m.getName());
		webActionUtils.TestCaseinfo("================================");
		webActionUtils.TestCaseinfo(" Test Case Name : Invalid Username and Valid Password verification");
		webActionUtils.TestCaseinfo("================================");

		String loginTitle = excelLibrary.getExcelData(XL_TESTPATH, "LoginTC", 34, 1);
		String expectedError = excelLibrary.getExcelData(XL_TESTPATH, "LoginTC", 34, 2);

		LoginPage loginPage = new LoginPage(driver, webActionUtils);
		loginPage.verifyLoginPageTitle(loginTitle);
		loginPage.enterUserName(INVALID_USER_NAME);
		loginPage.enterPassword(PASSWORD);
		loginPage.clickOnSignin();
		loginPage.verifyErrorMessage(expectedError);
	}
}