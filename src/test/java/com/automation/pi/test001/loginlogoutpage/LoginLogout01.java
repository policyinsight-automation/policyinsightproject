package com.automation.pi.test001.loginlogoutpage;

import java.lang.reflect.Method;

import org.testng.annotations.Test;

import com.automation.pi.library.BaseTest;
import com.automation.pi.pages.HomePage;
import com.automation.pi.pages.LoginPage;
import com.automation.pi.pages.UploadFilePage;

public class LoginLogout01 extends BaseTest {

	@Test
	public void pidc_tc_904(Method m) {

		webActionUtils.extentTestName(m.getName());
		webActionUtils.TestCaseinfo("================================");
		webActionUtils.TestCaseinfo(" Test Case Name : Log In - Access to the system as DT User");
		webActionUtils.TestCaseinfo("================================");

		String loginTitle = excelLibrary.getExcelData(XL_TESTPATH, "LoginTC", 1, 1);
		String uploadFileTitle = excelLibrary.getExcelData(XL_TESTPATH, "LoginTC", 1, 2);
		String uploadFileTabText = excelLibrary.getExcelData(XL_TESTPATH, "LoginTC", 1, 3);
		String managePageTabText = excelLibrary.getExcelData(XL_TESTPATH, "LoginTC", 1, 4);
		String reportsTabText = excelLibrary.getExcelData(XL_TESTPATH, "LoginTC", 1, 5);

		LoginPage loginPage = new LoginPage(driver, webActionUtils);
		loginPage.verifyLoginPageTitle(loginTitle);
		loginPage.enterUserName(USER_NAME_DT);
		loginPage.enterPassword(PASSWORD);
		loginPage.clickOnSignin();

		HomePage homePage = new HomePage(driver, webActionUtils);
		homePage.verifyDTUserTabs(uploadFileTabText, managePageTabText, reportsTabText);

		UploadFilePage uploadFilePage = new UploadFilePage(driver, webActionUtils);
		uploadFilePage.verifyUploadFile(uploadFileTitle);
	}
}