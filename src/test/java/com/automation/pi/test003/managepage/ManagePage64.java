package com.automation.pi.test003.managepage;

import java.lang.reflect.Method;

import org.testng.annotations.Test;

import com.automation.pi.library.BaseTest;
import com.automation.pi.pages.LoginPage;
import com.automation.pi.pages.ManagePage;

public class ManagePage64 extends BaseTest 
{//846
	@Test
	public void PPR_8397_Test(Method m)
	{
		webActionUtils.extentTestName(m.getName());
		webActionUtils.TestCaseinfo("================================");
		webActionUtils.TestCaseinfo(" Test Case Name : Manage Page - Advanced Filter function - Date Created, 1,2,3 year");
		webActionUtils.TestCaseinfo("================================");

		String title = "ManagePackage - Policy Checking Platform";
		LoginPage loginPage = new LoginPage(driver, webActionUtils);
		loginPage.enterUserName(USER_NAME);
		loginPage.enterPassword(PASSWORD);
		loginPage.clickOnSignin();

		ManagePage managePage = new ManagePage(driver, webActionUtils);
		managePage.clickManagePageTab();
		managePage.verifyManagePage(title);
		managePage.clickAdvanceFiltersButton();
		managePage.selectDateCreated1();
		managePage.clickSearchButton();
		managePage.selectDateCreated2();
		managePage.clickSearchButton()	;
		managePage.selectDateCreated3();
		managePage.clickSearchButton();
		managePage.selectDateCreated3more();
		managePage.clickSearchButton();
		
	}
}