package com.automation.pi.test003.managepage;

import java.lang.reflect.Method;

import org.testng.annotations.Test;

import com.automation.pi.library.BaseTest;
import com.automation.pi.pages.LoginPage;
import com.automation.pi.pages.ManagePage;

public class ManagePage49 extends BaseTest
{//934
	@Test
	public void PPR_8402_Test(Method m) 
	{
		webActionUtils.extentTestName(m.getName());
		webActionUtils.TestCaseinfo("================================");
		webActionUtils.TestCaseinfo(" Test Case Name : Save Later' button is just before for Submit");
		webActionUtils.TestCaseinfo("================================");

		String title = "ManagePackage - Policy Checking Platform";
		String serialNumber="203732";

		LoginPage loginPage = new LoginPage(driver, webActionUtils);
		loginPage.enterUserName(USER_NAME_DT);
		loginPage.enterPassword(PASSWORD);
		loginPage.clickOnSignin();

		ManagePage managePage = new ManagePage(driver, webActionUtils);
		managePage.clickManagePageTab();
		managePage.verifyManagePage(title);
		
		managePage.searchSerialNumber(serialNumber);
//		managePage.clickStatus();
//		managePage.clickSelectAll();
//		managePage.clickAdvanceFiltersButton();
//		managePage.selectDateCreated3more();
//		managePage.clickAdvanceFiltersButton();
		managePage.clickSearchButton();
		managePage.verifySearchedRecord(serialNumber);
		managePage.selectFirstRecordCheckBox();
		managePage.clickOnValidateIRButton();
		managePage.verifySaveLaterBeforeSubmitButton();
	}
}