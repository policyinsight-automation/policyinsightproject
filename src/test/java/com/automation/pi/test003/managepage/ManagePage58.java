package com.automation.pi.test003.managepage;

import java.lang.reflect.Method;

import org.testng.annotations.Test;

import com.automation.pi.library.BaseTest;
import com.automation.pi.pages.LoginPage;
import com.automation.pi.pages.ManagePage;
import com.automation.pi.pages.UploadFilePage;

public class ManagePage58 extends BaseTest
{//791
	@Test
	public void PPR_7937_Test(Method m) {
		webActionUtils.extentTestName(m.getName());
		webActionUtils.TestCaseinfo("================================");
		webActionUtils.TestCaseinfo(" Test Case Name : Edit task ");
		webActionUtils.TestCaseinfo("================================");

		String accountName = excelLibrary.getExcelData(XL_TESTPATH, "SmokeTestSheet", 1, 1);
		String departmentName = "Alpharetta, GA - Auto Renewal";
		String lobValue = excelLibrary.getExcelData(XL_TESTPATH, "SmokeTestSheet", 1, 3);
		String policyNumber = excelLibrary.getExcelData(XL_TESTPATH, "SmokeTestSheet", 1, 4);
	//	String accountManager = excelLibrary.getExcelData(XL_TESTPATH, "SmokeTestSheet", 1, 6);
		String accountManager= "Priscilla Tatum";	
		String dateformat = excelLibrary.getExcelData(XL_TESTPATH, "SmokeTestSheet", 4, 5);
		String policyStatusOpn = excelLibrary.getExcelData(XL_TESTPATH, "SmokeTestSheet", 4, 7);
		String expectedText = excelLibrary.getExcelData(XL_TESTPATH, "SmokeTestSheet", 4, 8);
		String uploadfilepath1 = DIR_PATH + "/TestDataFiles/5-Policy.pdf";
		String uploadfilepath2 = DIR_PATH + "/TestDataFiles/Exp Pol.pdf";
		String uploadfilepath = uploadfilepath1 + "\n " + uploadfilepath2;

		String title = "ManagePackage - Policy Checking Platform";
		String option1="";

		LoginPage loginPage = new LoginPage(driver, webActionUtils);
		loginPage.enterUserName(USER_NAME);
		loginPage.enterPassword(PASSWORD);
		loginPage.clickOnSignin();

		UploadFilePage uploadFilePage = new UploadFilePage(driver, webActionUtils);
		uploadFilePage.clickOnUploadFile();
		String nameInsured = uploadFilePage.enterFieldvalues(accountName, departmentName, lobValue, policyNumber,
				dateformat, accountManager, policyStatusOpn, uploadfilepath, expectedText);

		ManagePage managePage = new ManagePage(driver, webActionUtils);
		managePage.clickManagePageTab();
		managePage.verifyManagePage(title);
		managePage.searchNameInsured(nameInsured);
		managePage.clickStatus();
		managePage.clickSelectAll();
		managePage.clickSearchButton();
		managePage.selectFirstRecordCheckBox();
		managePage.clickEditButton();
		managePage.verifyEditpage(option1);
	}
}