package com.automation.pi.test003.managepage;

import java.lang.reflect.Method;

import org.testng.annotations.Test;

import com.automation.pi.library.BaseTest;
import com.automation.pi.pages.LoginPage;
import com.automation.pi.pages.ManagePage;

public class ManagePage36 extends BaseTest
{//725
	@Test
	public void PPR_4390_Test(Method m) 
	{
		webActionUtils.extentTestName(m.getName());
		webActionUtils.TestCaseinfo("================================");
		webActionUtils.TestCaseinfo(" Test Case Name : Manage Page - Export to Excel");
		webActionUtils.TestCaseinfo("================================");

		String title = "ManagePackage - Policy Checking Platform";
		String text = "CBIZ";
			
		LoginPage loginPage = new LoginPage(driver, webActionUtils);
		loginPage.enterUserName(USER_NAME);
		loginPage.enterPassword(PASSWORD);
		loginPage.clickOnSignin();

		ManagePage managePage = new ManagePage(driver, webActionUtils);
		managePage.clickManagePageTab();
		managePage.verifyManagePage(title);
		managePage.clickAccountFilter();
		managePage.enterAccountSearch(text);
		managePage.selectCBIZAccount();
		managePage.clickSearchButton();
		managePage.displayCBIZAccountRecords();
		managePage.ClickOnExportTOExcelBtn();
		managePage.verifyExportFileDownloaded1();  	
	}
}