package com.automation.pi.test003.managepage;

import java.lang.reflect.Method;

import org.apache.commons.lang3.RandomStringUtils;
import org.testng.annotations.Test;

import com.automation.pi.library.BaseTest;
import com.automation.pi.pages.LoginPage;
import com.automation.pi.pages.ManagePage;
import com.automation.pi.pages.UploadFilePage;

public class ManagePage81 extends BaseTest {// 704
	@Test
	public void PPR_4369_Test(Method m) {

		webActionUtils.extentTestName(m.getName());
		webActionUtils.TestCaseinfo("================================");
		webActionUtils.TestCaseinfo(" Test Case Name : Merge + unmerge ");
		webActionUtils.TestCaseinfo("================================");

		String accountName = "CBIZ";
		String departmentName = "Alpharetta, GA - Auto Renewal with Proposal";
		String nameInsured = "TestKS001 Shobhan_" + RandomStringUtils.randomAlphabetic(5);
		String nameInsured1 = nameInsured;
		String lobValue = "PKG";
		String policyNumber = "PHPK2241920" + RandomStringUtils.randomAlphabetic(3);
		String policyNumber1 = policyNumber;
		String dateformat = "03/05/2021";
		String accountManager = "Aimee Harrison";// "Avery Clark";
		String policyStatusOpn = "New Business";
		String uploadfilepath1 = DIR_PATH + "/TestDataFiles/5-Policy.pdf";
		String expectedText = "Message: You have successfully submitted a task! - Serial# :";
		String expectedText1="There is already policy uploaded for the same Policy#, Effective Date & LOB. Do you want to continue?";
		
		LoginPage loginPage = new LoginPage(driver, webActionUtils);
		loginPage.enterUserName(USER_NAME);
		loginPage.enterPassword(PASSWORD);
		loginPage.clickOnSignin();

		UploadFilePage uploadFile = new UploadFilePage(driver, webActionUtils);
		uploadFile.clickOnUploadFile();
		uploadFile.enterFieldValuesInUpload1(nameInsured, accountName, departmentName, lobValue, policyNumber, dateformat,accountManager, policyStatusOpn, uploadfilepath1, expectedText);
		uploadFile.submitButton();
		String serial1 = uploadFile.getSerialNumber();
//		webActionUtils.refreshPage();
		webActionUtils.refreshPage();
		webActionUtils.waitForPageLoad();
		uploadFile.enterFieldValuesInUpload1(nameInsured1, accountName, departmentName, lobValue, policyNumber1, dateformat,accountManager, policyStatusOpn, uploadfilepath1, expectedText);
		uploadFile.submitButton();
		uploadFile.verifyDuplicatePopUpToastMessage(expectedText1);
		uploadFile.clickConfirmYesAndCheckToast(expectedText);
		String serial2 = uploadFile.getSerialNumber();

		String serialNumber =  serial1+";"+serial2;// "197979" + ";" + "197980";
		String title = "ManagePackage - Policy Checking Platform";

		ManagePage managePage = new ManagePage(driver, webActionUtils);
		managePage.clickManagePageTab();
		managePage.verifyManagePage(title);
		managePage.searchSerialNumber(serialNumber);
		managePage.clickStatus();
		managePage.clickSelectAll();
		managePage.clickSearchButton();
		managePage.checkCheckboxandClickmerge();
		managePage.verifyPackageStatusAfterMerge(serial1,serial2);
		managePage.selectSecondCheckbox();
		managePage.unMergedbutton();
		managePage.verifyPackageStatusAfterUnmerge(serial1,serial2);
	}
}