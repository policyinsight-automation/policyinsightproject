package com.automation.pi.test003.managepage;

import java.lang.reflect.Method;

import org.testng.annotations.Test;

import com.automation.pi.library.BaseTest;
import com.automation.pi.pages.LoginPage;
import com.automation.pi.pages.ManagePage;

public class ManagePage59 extends BaseTest
{//847
	@Test
	public void PPR_8398_Test(Method m) {
		webActionUtils.extentTestName(m.getName());
		webActionUtils.TestCaseinfo("================================");
		webActionUtils.TestCaseinfo(" Test Case Name : Task description + task id ");
		webActionUtils.TestCaseinfo("================================");

		String title = "ManagePackage - Policy Checking Platform";
		String TaskDescription = "2024/08/12 WCO - RN (792653)";//""2023/11/01 PKG (754055)";
	
		LoginPage loginPage = new LoginPage(driver, webActionUtils);
		loginPage.enterUserName(USER_NAME_DT);
		loginPage.enterPassword(PASSWORD);
		loginPage.clickOnSignin();


		ManagePage managePage = new ManagePage(driver, webActionUtils);
		managePage.clickManagePageTab();
		managePage.verifyManagePage(title);
		managePage.searchTaskDescription("792653");
		managePage.clickStatus();
		managePage.clickSelectAll();
		managePage.clickSearchButton();
		managePage.selectFirstRecordCheckBox();
		managePage.verifyTaskDescription(TaskDescription);

	}
}