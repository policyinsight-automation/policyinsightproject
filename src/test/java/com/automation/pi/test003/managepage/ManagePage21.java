package com.automation.pi.test003.managepage;

import java.lang.reflect.Method;

import org.testng.annotations.Test;

import com.automation.pi.library.BaseTest;
import com.automation.pi.pages.LoginPage;
import com.automation.pi.pages.ManagePage;

public class ManagePage21 extends BaseTest
{//713
	@Test
	public void PPR_4378_Test(Method m)
	{
		webActionUtils.extentTestName(m.getName());
		webActionUtils.TestCaseinfo("================================");
		webActionUtils.TestCaseinfo(" Test Case Name : Manage Page - Filter function - PI Processer filter");
		webActionUtils.TestCaseinfo("================================");

		String title = "ManagePackage - Policy Checking Platform";
		String text = "Abby J Li";

		LoginPage loginPage = new LoginPage(driver, webActionUtils);
		loginPage.enterUserName(USER_NAME);
		loginPage.enterPassword(PASSWORD);
		loginPage.clickOnSignin();

		ManagePage managePage = new ManagePage(driver, webActionUtils);
		managePage.clickManagePageTab();
		managePage.verifyManagePage(title);
		managePage.clickPIProcessorFilter();
		managePage.enterPIProcessorSearchText(text);
		managePage.selectAllCheckboxInPIProcessor();
		managePage.clickAdvanceFiltersButton();
		managePage.selectDateCreated3more();
		managePage.clickSearchButton();
		managePage.diplayPIProcesserfilterRecords();
	}
}