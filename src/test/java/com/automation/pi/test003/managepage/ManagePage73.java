package com.automation.pi.test003.managepage;

import java.lang.reflect.Method;

import org.apache.commons.lang3.RandomStringUtils;
import org.testng.annotations.Test;

import com.automation.pi.library.BaseTest;
import com.automation.pi.pages.LoginPage;
import com.automation.pi.pages.ManagePage;
import com.automation.pi.pages.UploadFilePage;

	public class ManagePage73 extends BaseTest 
	{//729
		@Test
		public void PPR_4396_Test(Method m) {
			webActionUtils.extentTestName(m.getName());
			webActionUtils.TestCaseinfo("================================");
			webActionUtils.TestCaseinfo(" Test Case Name : Assign - Assign Auditor.");
			webActionUtils.TestCaseinfo("================================");

			String accountName = "CBIZ";
		
			String departmentName = "Alpharetta, GA - Auto Renewal";
			String lobValue ="PKG";
			String policyNumber = "Policy "+ RandomStringUtils.randomAlphanumeric(15);
			String accountManager = "Jessica Lamar";
			String dateformat = "09/27/2023";
			String policyStatusOpn = "New Business";
			String expectedText = excelLibrary.getExcelData(XL_TESTPATH, "SmokeTestSheet", 4, 8);
			String uploadfilepath1 = DIR_PATH + "/TestDataFiles/5-Policy.pdf";

			String title = "ManagePackage - Policy Checking Platform";
			String option1 = "Aishwarya Devananda";
			String option2 ="Divya Gowda";

			LoginPage loginPage = new LoginPage(driver, webActionUtils);
			loginPage.enterUserName(USER_NAME);
			loginPage.enterPassword(PASSWORD);
			loginPage.clickOnSignin();

			UploadFilePage uploadFilePage = new UploadFilePage(driver, webActionUtils);
			uploadFilePage.clickOnUploadFile();
			
			String nameInsured = uploadFilePage.enterFieldvalues(accountName, departmentName, lobValue, policyNumber,
					dateformat, accountManager, policyStatusOpn, uploadfilepath1, expectedText);
			String SerialNumber=uploadFilePage.getSerialNumber();
			uploadFilePage.clickOnSerialNumber();
		
			ManagePage managePage = new ManagePage(driver, webActionUtils);
			managePage.selectFirstRecordCheckBox1(SerialNumber);
			managePage.verifyManagePage(title);
			managePage.clickAssignAuditorButton();
			managePage.assignCheckingAuditor(option1);
			managePage.assignFBEAuditor(option2);
			managePage.saveAuditor();
			managePage.searchNameInsured(nameInsured);
			managePage.clickStatus();
			managePage.clickSelectAll();
			managePage.clickSearchButton();
			managePage.verifyAuditor(option1);
		}
	}