package com.automation.pi.test003.managepage;

import java.lang.reflect.Method;

import org.testng.annotations.Test;

import com.automation.pi.library.BaseTest;
import com.automation.pi.pages.LoginPage;
import com.automation.pi.pages.ManagePage;

public class ManagePage89 extends BaseTest
{//802	
	@Test
	public void PPR_TC_1429_Test(Method m)
	{
		webActionUtils.extentTestName(m.getName());
		webActionUtils.TestCaseinfo("================================");
		webActionUtils.TestCaseinfo(" Test Case Name : Regenerate System Application message ");
		webActionUtils.TestCaseinfo("================================");

		String title = "ManagePackage - Policy Checking Platform";
//		String toolTip = "Regenerate System Application";
		String message ="Message: Request is been submitted successfully. Please check the regenerated file in sometime.";

		String serialNumber="200561";

		LoginPage loginPage = new LoginPage(driver, webActionUtils);
		loginPage.enterUserName(USER_NAME);
		loginPage.enterPassword(PASSWORD);
		loginPage.clickOnSignin();

		ManagePage managePage = new ManagePage(driver, webActionUtils);
		managePage.clickManagePageTab();
		managePage.verifyManagePage(title);
		managePage.searchSerialNumber(serialNumber);
		managePage.clickStatus();
		managePage.clickSelectAll();
		managePage.clickSearchButton();
		managePage.selectFirstRecordCheckBox();
		managePage.verifyRegenaratebuttonEnabled();
		managePage.clickRegenaratebutton();
		managePage.clickRegenaratePopUpYesbutton();	
		managePage.verifyRegenaratemessage(message);
	
		}
	
}