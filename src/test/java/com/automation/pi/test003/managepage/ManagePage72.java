package com.automation.pi.test003.managepage;

import java.lang.reflect.Method;

import org.testng.annotations.Test;

import com.automation.pi.library.BaseTest;
import com.automation.pi.pages.LoginPage;
import com.automation.pi.pages.ManagePage;

public class ManagePage72 extends BaseTest
{//734
	@Test
	public void PPR_4401_Test(Method m) 
	{
		webActionUtils.extentTestName(m.getName());
		webActionUtils.TestCaseinfo("================================");
		webActionUtils.TestCaseinfo(" Test Case Name : Manage Page - Upload DT Finished Checklist");
		webActionUtils.TestCaseinfo("================================");

		String serialNumber = "195176";
		String title = "ManagePackage - Policy Checking Platform";

		String uploadfilepath1 = DIR_PATH + "/TestDataFiles/195176 - SCM - Bradley Wong DBA Bayside Properties - PKG - 10_1_2023 Policy Checklist.xlsx";
		
		LoginPage loginPage = new LoginPage(driver, webActionUtils);
		loginPage.enterUserName(USER_NAME);
		loginPage.enterPassword(PASSWORD);
		loginPage.clickOnSignin();

		ManagePage managePage = new ManagePage(driver, webActionUtils);
		managePage.clickManagePageTab();
		managePage.verifyManagePage(title);
		managePage.searchSerialNumber(serialNumber);
		managePage.clickStatus();
		managePage.clickSelectAll();
		managePage.clickAdvanceFiltersButton();
		managePage.selectDateCreated3more();
		managePage.clickAdvanceFiltersButton();
		managePage.clickSearchButton();
		managePage.verifySearchedRecord(serialNumber);
		managePage.selectFirstRecordCheckBox();
		managePage.clickonUploadDTFinishedChecklistIcon(uploadfilepath1);
		managePage.ClickonSubmitButtontWS();
		
	}
}