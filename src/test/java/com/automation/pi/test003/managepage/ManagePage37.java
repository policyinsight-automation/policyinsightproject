package com.automation.pi.test003.managepage;

import java.lang.reflect.Method;

import org.testng.annotations.Test;

import com.automation.pi.library.BaseTest;
import com.automation.pi.pages.LoginPage;
import com.automation.pi.pages.ManagePage;
import com.automation.pi.pages.UploadFilePage;

public class ManagePage37 extends BaseTest 
{//769
	@Test
	public void PPR_5732_Test(Method m)
	{
		webActionUtils.extentTestName(m.getName());
		webActionUtils.TestCaseinfo("================================");
		webActionUtils.TestCaseinfo(" Test Case Name : Assign - Assign the FBE auditor.");
		webActionUtils.TestCaseinfo("================================");

		String accountName = excelLibrary.getExcelData(XL_TESTPATH, "SmokeTestSheet", 1, 1);
		String departmentName = excelLibrary.getExcelData(XL_TESTPATH, "SmokeTestSheet", 1, 2);
		String lobValue = excelLibrary.getExcelData(XL_TESTPATH, "SmokeTestSheet", 1, 3);
		String policyNumber = excelLibrary.getExcelData(XL_TESTPATH, "SmokeTestSheet", 1, 4);
		String dateformat = excelLibrary.getExcelData(XL_TESTPATH, "SmokeTestSheet", 4, 5);
		String accountManager =""; excelLibrary.getExcelData(XL_TESTPATH, "SmokeTestSheet", 1, 6);
		String policyStatusOpn = excelLibrary.getExcelData(XL_TESTPATH, "SmokeTestSheet", 4, 7);
		String expectedText = excelLibrary.getExcelData(XL_TESTPATH, "SmokeTestSheet", 4, 8);
		String uploadfilepath1 = DIR_PATH+ "/TestDataFiles/5-Policy.pdf";
		String uploadfilepath2 = DIR_PATH+ "/TestDataFiles/Exp Pol.pdf";
		String uploadfilepath = uploadfilepath1 + "\n " + uploadfilepath2;	
		

		String title = "ManagePackage - Policy Checking Platform";
		String option = "Aishwarya Devananda";
		
		LoginPage loginPage = new LoginPage(driver, webActionUtils);
		loginPage.enterUserName(USER_NAME);
		loginPage.enterPassword(PASSWORD);
		loginPage.clickOnSignin();

		UploadFilePage uploadFilePage = new UploadFilePage(driver, webActionUtils);
		uploadFilePage.clickOnUploadFile();
		 String nameInsured =  uploadFilePage.enterFieldvalues(accountName, departmentName, lobValue, policyNumber, dateformat, accountManager,policyStatusOpn, uploadfilepath, expectedText);

		ManagePage managePage = new ManagePage(driver, webActionUtils);
		managePage.clickManagePageTab();
		managePage.verifyManagePage(title);
		managePage.searchNameInsured(nameInsured);
		managePage.clickStatus();
		managePage.clickSelectAll();
		managePage.clickSearchButton();
		managePage.selectFirstRecordCheckBox();
		managePage.clickAssignAuditorButton();
		managePage.assignFBEAuditor(option);
		managePage.saveAuditor();
		managePage.clickSearchButton();
		managePage.verifyFBEAuditor(option);
	}
}