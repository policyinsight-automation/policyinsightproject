	
package com.automation.pi.test003.managepage;

import java.lang.reflect.Method;
import java.text.ParseException;

import org.testng.annotations.Test;

import com.automation.pi.library.BaseTest;
import com.automation.pi.pages.LoginPage;
import com.automation.pi.pages.ManagePage;

public class ManagePage78 extends BaseTest 
{//43
	

	@Test
	public void PPR_8368_Test(Method m) throws ParseException {
		webActionUtils.extentTestName(m.getName());
		webActionUtils.TestCaseinfo("================================");
		webActionUtils.TestCaseinfo(
				" Test Case Name :Verification of Internal Due Date when Internal Due Date and Client Due Date are Equal and Internal Due Date is greater than Current Date ");
		webActionUtils.TestCaseinfo("================================");

		String title = "ManagePackage - Policy Checking Platform";
		String departmentName = "CL";
		String serialNumber = "203732";
		String InternalDate = "02/25/2024";
		String clientDate = "02/25/2024";

		LoginPage loginPage = new LoginPage(driver, webActionUtils);
		loginPage.enterUserName(USER_NAME_DT);
		loginPage.enterPassword(PASSWORD);
		loginPage.clickOnSignin();

		ManagePage managePage = new ManagePage(driver, webActionUtils);
		managePage.clickManagePageTab();
		managePage.verifyManagePage(title);
		managePage.searchSerialNumber(serialNumber);
		managePage.clickSearchButton();
		managePage.verifySearchedRecord(serialNumber);
		managePage.selectFirstRecordCheckBox();
		managePage.clickOnValidateIRButton();
		managePage.validateIRFieldvalues(departmentName);
		managePage.EnterinternalAndClientDate(InternalDate, clientDate);
		managePage.verifyInternalAndClientDuedate();
	}
}
