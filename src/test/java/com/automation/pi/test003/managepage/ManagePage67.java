package com.automation.pi.test003.managepage;

import java.lang.reflect.Method;

import org.testng.annotations.Test;

import com.automation.pi.library.BaseTest;
import com.automation.pi.pages.LoginPage;
import com.automation.pi.pages.ManagePage;

public class ManagePage67 extends BaseTest 
{//840
	@Test
	public void PPR_8392_Test(Method m) 
	{
		webActionUtils.extentTestName(m.getName());
		webActionUtils.TestCaseinfo("================================");
		webActionUtils.TestCaseinfo(" Test Case Name : Manage Page - Advanced Filter function - multi select Departments");
		webActionUtils.TestCaseinfo("================================");
		String title = "ManagePackage - Policy Checking Platform";
		String text = "CBIZ";
		String Search1 = "Alpharetta, GA - Full Check";
		String Search2 = "Boca Raton, FL - Auto Renewal with Proposal";

		LoginPage loginPage = new LoginPage(driver, webActionUtils);
		loginPage.enterUserName(USER_NAME);
		loginPage.enterPassword(PASSWORD);
		loginPage.clickOnSignin();

		ManagePage managePage = new ManagePage(driver, webActionUtils);
		managePage.clickManagePageTab();
		managePage.verifyManagePage(title);
		managePage.clickAccountFilter();
		managePage.enterAccountSearch(text);
		managePage.selectCBIZAccount();
		managePage.clickAdvanceFiltersButton();
		managePage.clickDepartmentDropDown();
		managePage.searchDeparment(Search1);
		managePage.clickSearchButton();
		managePage.clickDepartmentDropDown();
		managePage.searchDeparment(Search2);
		managePage.displayCBIZAccountRecords();
	}
}