package com.automation.pi.test003.managepage;

import java.lang.reflect.Method;

import org.testng.annotations.Test;

import com.automation.pi.library.BaseTest;
import com.automation.pi.pages.LoginPage;
import com.automation.pi.pages.ManagePage;

public class ManagePage79 extends BaseTest 
{//41
       @Test
       public void PPR_8366_Test(Method m)
       {
              webActionUtils.extentTestName(m.getName());
              webActionUtils.TestCaseinfo("================================");
              webActionUtils.TestCaseinfo(" Test Case Name :Verification of Internal Due Date when Internal Due Date is Not Equal to Client Due Date in Validate IR package ");
              webActionUtils.TestCaseinfo("================================");

              String title = "ManagePackage - Policy Checking Platform";
              String serialNumber="203732";
              String departmentName="CL";

              LoginPage loginPage = new LoginPage(driver, webActionUtils);
              loginPage.enterUserName(USER_NAME_DT);
              loginPage.enterPassword(PASSWORD);
              loginPage.clickOnSignin();

              ManagePage managePage = new ManagePage(driver, webActionUtils);
              managePage.clickManagePageTab();
              managePage.verifyManagePage(title);
              managePage.searchSerialNumber(serialNumber);
              managePage.clickSearchButton();
              managePage.verifySearchedRecord(serialNumber);
              managePage.selectFirstRecordCheckBox();
              managePage.clickOnValidateIRButton();
              managePage.verifyInternalDueDateInValidateIR(departmentName);
       }
}           