package com.automation.pi.test003.managepage;

import java.lang.reflect.Method;

import org.testng.annotations.Test;

import com.automation.pi.library.BaseTest;
import com.automation.pi.pages.HomePage;
import com.automation.pi.pages.LoginPage;
import com.automation.pi.pages.ManagePage;

public class ManagePage51 extends BaseTest
{//933
	@Test
	public void PPR_8401_Test(Method m) 
	{
		webActionUtils.extentTestName(m.getName());
		webActionUtils.TestCaseinfo("================================");
		webActionUtils.TestCaseinfo(" Test Case Name : Check Task Description option not present in FBE, PI, ADMIN ");
		webActionUtils.TestCaseinfo("================================");

		String title = "ManagePackage - Policy Checking Platform";
		String loginTitle = "Log In Page";

		LoginPage loginPage = new LoginPage(driver, webActionUtils);
		loginPage.enterUserName(USER_NAME);
		loginPage.enterPassword(PASSWORD);
		loginPage.clickOnSignin();

		ManagePage managePage = new ManagePage(driver, webActionUtils);
		managePage.clickManagePageTab();
		managePage.verifyManagePage(title);
		managePage.clickStatus();
		managePage.clickSelectAll();
		managePage.clickSearchButton();
		managePage.verifyTaskDescriptionColumnNotPresent();

		HomePage homePage = new HomePage(driver, webActionUtils);
		homePage.clickOnSignOutIcon();
		homePage.clickOnYes();
		webActionUtils.waitSleep(3);
		loginPage.verifyLoginPageTitle(loginTitle);
		webActionUtils.refreshPage();
		loginPage.enterUserName(USER_NAME_FBE);
		loginPage.enterPassword(PASSWORD);
		loginPage.clickOnSignin();

		managePage.clickManagePageTab();
		managePage.verifyManagePage(title);
		managePage.clickStatus();
		managePage.clickSelectAll();
		managePage.clickSearchButton();
		managePage.verifyTaskDescriptionColumnNotPresent();

		homePage.clickOnSignOutIcon();
		homePage.clickOnYes();
		webActionUtils.waitSleep(3);
		loginPage.verifyLoginPageTitle(loginTitle);
		webActionUtils.refreshPage();

		loginPage.enterUserName(USER_NAME_PI);
		loginPage.enterPassword(PASSWORD);
		loginPage.clickOnSignin();
		managePage.clickStatus();
		managePage.clickSelectAll();
		managePage.clickSearchButton();
		managePage.verifyTaskDescriptionColumnNotPresent();
	}
}