package com.automation.pi.test003.managepage;

import java.lang.reflect.Method;
import java.util.ArrayList;

import org.testng.annotations.Test;

import com.automation.pi.library.BaseTest;
import com.automation.pi.pages.LoginPage;
import com.automation.pi.pages.ManagePage;

public class ManagePage35 extends BaseTest
{//724
	@Test
	public void PPR_4389_Test(Method m)
	{
		webActionUtils.extentTestName(m.getName());
		webActionUtils.TestCaseinfo("================================");
		webActionUtils.TestCaseinfo(" Test Case Name : Manage Page - Advanced Filter function - Clear filter");
		webActionUtils.TestCaseinfo("================================");

		String title = "ManagePackage - Policy Checking Platform";
		String text = "CBIZ";
		String fromDate = "01/01/2024";
		String Todate = "06/30/2024";

		LoginPage loginPage = new LoginPage(driver, webActionUtils);
		loginPage.enterUserName(USER_NAME);
		loginPage.enterPassword(PASSWORD);
		loginPage.clickOnSignin();

		ManagePage managePage = new ManagePage(driver, webActionUtils);
		managePage.clickManagePageTab();
		managePage.verifyManagePage(title);
		managePage.clickAccountFilter();
		managePage.enterAccountSearch(text);
		managePage.selectCBIZAccount();
		managePage.clickAdvanceFiltersButton();
		managePage.enterDateFromDT(fromDate, Todate);
		managePage.clickSearchButton();
		ArrayList<Integer> listOne = managePage.displayandVerifyDatefromDTRecords(fromDate, Todate);
		managePage.ClickOnClearFiltersBtn();
		managePage.clickAdvanceFiltersButton();
		ArrayList<Integer> listTwo = managePage.displayRecordsAfterClearingfilters();
		managePage.Compare2ArraylistofPackage(listOne, listTwo);
		managePage.clickAdvanceFiltersButton();
		managePage.verifyDatefromDTClearedFilter();	
	}
}