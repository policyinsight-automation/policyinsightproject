package com.automation.pi.test003.managepage;

import java.lang.reflect.Method;

import org.apache.commons.lang3.RandomStringUtils;
import org.testng.annotations.Test;

import com.automation.pi.library.BaseTest;
import com.automation.pi.pages.LoginPage;
import com.automation.pi.pages.ManagePage;

public class ManagePage56 extends BaseTest
{//736
	@Test
	public void PPR_4403_Test(Method m) 
	{
		webActionUtils.extentTestName(m.getName());
		webActionUtils.TestCaseinfo("================================");
		webActionUtils.TestCaseinfo(" Test Case Name : Manage Page - Rework Task");
		webActionUtils.TestCaseinfo("================================");

		String title = "ManagePackage - Policy Checking Platform";
		String packages ="201622"; //"191006";
		String text = " Test Rework" + RandomStringUtils.randomAlphanumeric(250);
			
		LoginPage loginPage = new LoginPage(driver, webActionUtils);
		loginPage.enterUserName(USER_NAME);
		loginPage.enterPassword(PASSWORD);
		loginPage.clickOnSignin();

		
		ManagePage managePage = new ManagePage(driver, webActionUtils);
		managePage.clickManagePageTab();
		managePage.verifyManagePage(title);
		managePage.clickStatus();
		managePage.clickSelectAll();
		managePage.searchSerialNumber(packages);
		managePage.clickSearchButton();
		managePage.selectFirstRecordCheckBox();
		managePage.ClickOnReworkTaskIcon(text);
	}
}