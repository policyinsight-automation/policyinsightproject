package com.automation.pi.test003.managepage;

import java.lang.reflect.Method;

import org.testng.annotations.Test;

import com.automation.pi.library.BaseTest;
import com.automation.pi.pages.LoginPage;
import com.automation.pi.pages.ManagePage;
import com.automation.pi.pages.UploadFilePage;

public class ManagePage57 extends BaseTest {// 795
	@Test
	public void PPR_7941_Test(Method m) {
		webActionUtils.extentTestName(m.getName());
		webActionUtils.TestCaseinfo("================================");
		webActionUtils.TestCaseinfo(" Test Case Name : Manage Page - Assign Processor");
		webActionUtils.TestCaseinfo("================================");

		String accountName = excelLibrary.getExcelData(XL_TESTPATH, "SmokeTestSheet", 1, 1);
		String departmentName = "Alpharetta, GA - Auto Renewal";
		String lobValue = excelLibrary.getExcelData(XL_TESTPATH, "SmokeTestSheet", 1, 3);
		String policyNumber = excelLibrary.getExcelData(XL_TESTPATH, "SmokeTestSheet", 1, 4);
		String accountManager = "Laura Daigle"; // excelLibrary.getExcelData(XL_TESTPATH, "SmokeTestSheet", 1, 6);
		String dateformat = excelLibrary.getExcelData(XL_TESTPATH, "SmokeTestSheet", 4, 5);
		String policyStatusOpn = excelLibrary.getExcelData(XL_TESTPATH, "SmokeTestSheet", 4, 7);
		String expectedText = excelLibrary.getExcelData(XL_TESTPATH, "SmokeTestSheet", 4, 8);
		String uploadfilepath1 = DIR_PATH + "/TestDataFiles/5-Policy.pdf";
		String uploadfilepath2 = DIR_PATH + "/TestDataFiles/Exp Pol.pdf";
		String uploadfilepath = uploadfilepath1 + "\n " + uploadfilepath2;

		String title = "ManagePackage - Policy Checking Platform";
		String option1 = "Abby J Li";
		String option2 = "Abby J Li";

		LoginPage loginPage = new LoginPage(driver, webActionUtils);
		loginPage.enterUserName(USER_NAME);
		loginPage.enterPassword(PASSWORD);
		loginPage.clickOnSignin();

		UploadFilePage uploadFilePage = new UploadFilePage(driver, webActionUtils);
		uploadFilePage.clickOnUploadFile();
		String nameInsured = uploadFilePage.enterFieldvalues(accountName, departmentName, lobValue, policyNumber,
				dateformat, accountManager, policyStatusOpn, uploadfilepath, expectedText);

		String SerialNumber = uploadFilePage.getSerialNumber();
		uploadFilePage.clickOnSerialNumber();
		ManagePage managePage = new ManagePage(driver, webActionUtils);
		managePage.selectFirstRecordCheckBox1(SerialNumber);
		managePage.verifyManagePage(title);
		managePage.clickAssignProcessorButton();
		managePage.assignProcessor1n2(option1, option2);
		managePage.saveProcessor();
		managePage.searchNameInsured(nameInsured);
		managePage.clickStatus();
		managePage.clickSelectAll();
		managePage.clickSearchButton();
		managePage.verifyProcessor(option1, option2);
	}
}