package com.automation.pi.test003.managepage;

import java.lang.reflect.Method;

import org.testng.annotations.Test;

import com.automation.pi.library.BaseTest;
import com.automation.pi.pages.LoginPage;
import com.automation.pi.pages.ManagePage;

public class ManagePage46 extends BaseTest 
{//841
	@Test
	public void PPR_8393_Test(Method m) 
	{
		webActionUtils.extentTestName(m.getName());
		webActionUtils.TestCaseinfo("================================");
		webActionUtils.TestCaseinfo("Test Case Name : Manage Page - Blank Deparment Check for the multiple departments");
		webActionUtils.TestCaseinfo("================================");
		String title = "ManagePackage - Policy Checking Platform";
	
		LoginPage loginPage = new LoginPage(driver, webActionUtils);
		loginPage.enterUserName(USER_NAME);
		loginPage.enterPassword(PASSWORD);
		loginPage.clickOnSignin();

		ManagePage managePage = new ManagePage(driver, webActionUtils);
		managePage.clickManagePageTab();
		managePage.verifyManagePage(title);
		managePage.clickAccountFilter();
		managePage.selectAllAccount();
		managePage.clickAdvanceFiltersButton();
		managePage.clickDepartmentDropDown();
		managePage.verifyBlankDepartment();
	}
}