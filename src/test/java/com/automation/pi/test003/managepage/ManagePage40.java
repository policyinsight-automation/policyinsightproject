package com.automation.pi.test003.managepage;

import java.lang.reflect.Method;

import org.testng.annotations.Test;

import com.automation.pi.library.BaseTest;
import com.automation.pi.pages.HomePage;
import com.automation.pi.pages.LoginPage;
import com.automation.pi.pages.ManagePage;

public class ManagePage40 extends BaseTest
{//835
	@Test
	public void PPR_8386_Test(Method m)
	{
		webActionUtils.extentTestName(m.getName());
		webActionUtils.TestCaseinfo("================================");
		webActionUtils.TestCaseinfo(" Test Case Name : Check API created option not present in FBE, PI, ADMIN ");
		webActionUtils.TestCaseinfo("================================");

		String title = "ManagePackage - Policy Checking Platform";
		String loginTitle = "Log In Page";
		String text="API Created";

		LoginPage loginPage = new LoginPage(driver, webActionUtils);
		loginPage.enterUserName(USER_NAME);
		loginPage.enterPassword(PASSWORD);
		loginPage.clickOnSignin();
		

		ManagePage managePage = new ManagePage(driver, webActionUtils);
		managePage.clickManagePageTab();
		managePage.verifyManagePage(title);
		managePage.clickStatus();
		managePage.clickSelectAll();
		managePage.clickSelectAll();
		
		managePage.verifyApiCreatedButtonIsNotPresent(text);
		HomePage homePage = new HomePage(driver, webActionUtils);
		homePage.clickOnSignOutIcon();
		homePage.clickOnYes();
		webActionUtils.waitSleep(5);
		loginPage.verifyLoginPageTitle(loginTitle);
		webActionUtils.refreshPage();
		webActionUtils.waitSleep(5);

		
		loginPage.enterUserName(USER_NAME_FBE);
		loginPage.enterPassword(PASSWORD);
		loginPage.clickOnSignin();
		webActionUtils.waitSleep(5);
		
		managePage.clickManagePageTab();
		managePage.verifyManagePage(title);
		managePage.clickStatus();
		managePage.clickSelectAll();
		managePage.clickSelectAll();
		managePage.verifyApiCreatedButtonIsNotPresent(text);
		
		homePage.clickOnSignOutIcon();
		homePage.clickOnYes();
		webActionUtils.waitSleep(5);
		loginPage.verifyLoginPageTitle(loginTitle);
		webActionUtils.refreshPage();
		webActionUtils.waitSleep(5);
		//webActionUtils.getdriver().get(APP_URL);
			
		loginPage.enterUserName(USER_NAME_PI);
		loginPage.enterPassword(PASSWORD);
		loginPage.clickOnSignin();
		webActionUtils.waitSleep(5);

		managePage.clickStatus();
		managePage.clickSelectAll();
		managePage.clickSelectAll();
		managePage.verifyApiCreatedButtonIsNotPresent(text);
	}
}