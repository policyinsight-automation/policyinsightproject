package com.automation.pi.test003.managepage;

import java.lang.reflect.Method;

import org.testng.annotations.Test;

import com.automation.pi.library.BaseTest;
import com.automation.pi.pages.LoginPage;
import com.automation.pi.pages.ManagePage;

public class ManagePage31 extends BaseTest
{//721
	@Test
	public void PPR_4386_Test(Method m)
	{
		webActionUtils.extentTestName(m.getName());
		webActionUtils.TestCaseinfo("================================");
		webActionUtils.TestCaseinfo(" Test Case Name : Manage Page - Advanced Filter function - Date from Client filter");
		webActionUtils.TestCaseinfo("================================");

		String title = "ManagePackage - Policy Checking Platform";
		String text = "CBIZ";
		String fromDate = "01/01/2024";//03/01/2022";
		String Todate = "06/30/2024";//04/01/2022";

		LoginPage loginPage = new LoginPage(driver, webActionUtils);
		loginPage.enterUserName(USER_NAME);
		loginPage.enterPassword(PASSWORD);
		loginPage.clickOnSignin();

		ManagePage managePage = new ManagePage(driver, webActionUtils);
		managePage.clickManagePageTab();
		managePage.verifyManagePage(title);
		managePage.clickAccountFilter();
		managePage.enterAccountSearch(text);
		managePage.selectCBIZAccount();
		managePage.clickAdvanceFiltersButton();
		managePage.enterDateFromClientfromTo(fromDate, Todate);
		managePage.clickSearchButton();
		managePage.displayandVerifyDatefromClientRecords(fromDate, Todate);
	}
}