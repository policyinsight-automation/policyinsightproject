package com.automation.pi.test003.managepage;

import java.lang.reflect.Method;

import org.testng.annotations.Test;

import com.automation.pi.library.BaseTest;
import com.automation.pi.pages.LoginPage;
import com.automation.pi.pages.ManagePage;

public class ManagePage92 extends BaseTest
{//802	
	@Test
	public void PIDC_TC_753_Test(Method m)
	{
		///Verify that "Please Select an option'  is displayed in 'Assign To' field of Validate Image Right Data of Manage page.
		webActionUtils.extentTestName(m.getName());
		webActionUtils.TestCaseinfo("================================");
		webActionUtils.TestCaseinfo(" Test Case Name : Verify that 'Please Select an option' is displayed in 'Assign To' field' in IRpage");
		webActionUtils.TestCaseinfo("================================");

		String title = "ManagePackage - Policy Checking Platform";
		String serialNumber="203732";
		
		LoginPage loginPage = new LoginPage(driver, webActionUtils);
		loginPage.enterUserName(USER_NAME_DT);
		loginPage.enterPassword(PASSWORD);
		loginPage.clickOnSignin();

		ManagePage managePage = new ManagePage(driver, webActionUtils);
		managePage.clickManagePageTab();
		managePage.verifyManagePage(title);
		managePage.searchSerialNumber(serialNumber);
		managePage.clickSearchButton();
		managePage.verifySearchedRecord(serialNumber);
		managePage.selectFirstRecordCheckBox();
		managePage.clickOnValidateIRButton();
		managePage.verifyIRAssignToDefaulted();
	
		}
	
}