package com.automation.pi.test002.uploadfilepage;

import java.lang.reflect.Method;

import org.testng.annotations.Test;

import com.automation.pi.library.BaseTest;
import com.automation.pi.pages.LoginPage;

import com.automation.pi.pages.ManagePage;
import com.automation.pi.pages.UploadFilePage;

public class UploadFile54 extends BaseTest {

	@Test
	public void PIDC_TC_1085(Method m) {
		
		//Verify that package created from the upload files are not displayed in the manage page, for the selected 'PI lite' radio button in the upload file page.
		webActionUtils.extentTestName(m.getName());
		webActionUtils.TestCaseinfo("================================");
		webActionUtils.TestCaseinfo(" Test Case Name :  Verify that create an task for the PI Lite not present in manage page");
		webActionUtils.TestCaseinfo("================================");
		
		String accountName = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 148, 1);
		String departmentName = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 148, 2);
		String namedInsured = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 148, 3);
		String lob = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 148, 4);
		String policyNumber = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 148, 5);
		String effectiveDate = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 148, 6);
		String accountManager = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 148, 7);
		String addtional = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 148, 8);
		String policyStatusOpn = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 148, 9);
		String premiumValue = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 148, 10);
		String carrier  = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 148, 11);
		String expectedText  = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 148, 12);
		String title =  excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 148, 13);
	
		String uploadfilepath1 = DIR_PATH + "/TestDataFiles/5-Policy.pdf";
		String uploadfilepath2 = DIR_PATH + "/TestDataFiles/5-Proposal.pdf";
		String uploadfilepath4 = uploadfilepath1+"\n"+uploadfilepath2;
	
		LoginPage loginPage = new LoginPage(driver, webActionUtils);
		loginPage.enterUserName(USER_NAME);
		loginPage.enterPassword(PASSWORD);
		loginPage.clickOnSignin();
		UploadFilePage uploadFile = new UploadFilePage(driver, webActionUtils);
		uploadFile.clickOnUploadFile();
		uploadFile.clickOnPolicyInsightsExpressCheckRadiobutton();
		uploadFile.enterRequiredAccount(accountName);
		uploadFile.enterRequiredDepartment(departmentName);
		String nameInsured=uploadFile.enterRequiredNamedInsured1(namedInsured);
		uploadFile.enterRequiredLOB(lob);
		uploadFile.enterRequiredPolicyNumber(policyNumber);
		uploadFile.enterRequiredEffectiveDate(effectiveDate);
		uploadFile.enterHighlightedDateFromClient();
		uploadFile.enterRequiredAccountManager(accountManager);
		uploadFile.enterRequiredAdditionalField(addtional);
		uploadFile.enterRequiredPolicyStatus(policyStatusOpn);
		uploadFile.enterRequiredPremiumRange(premiumValue);
		uploadFile.uploadRequiredFiles(uploadfilepath4);
		uploadFile.enterRequiredCarrier(carrier);
		uploadFile.clickonSubmitButton();
		uploadFile.getMessageandVerify(expectedText);

		ManagePage managePage = new ManagePage(driver, webActionUtils);
		managePage.clickManagePageTab();
		managePage.verifyManagePage(title);
		managePage.clickStatus();
		managePage.clickSelectAll();
		managePage.searchNameInsured(nameInsured);
		managePage.clickSearchButton();
		managePage.verifyPIExpressCheckPackageRecordisNotDisplayed(nameInsured);
		}
}
