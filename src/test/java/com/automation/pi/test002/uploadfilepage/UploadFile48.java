package com.automation.pi.test002.uploadfilepage;

import java.lang.reflect.Method;

import org.testng.annotations.Test;

import com.automation.pi.library.BaseTest;
import com.automation.pi.pages.LoginPage;
import com.automation.pi.pages.UploadFilePage;

public class UploadFile48 extends BaseTest {

	@Test
	public void PIDC_TC_235(Method m) {
		
		// Verify that the departments mapped under PI Lite are only displayed in
		// Department/Program field, when user select the value in drop down of
		// Department /office code field.
		webActionUtils.extentTestName(m.getName());
		webActionUtils.TestCaseinfo("================================");
		webActionUtils.TestCaseinfo(" Test Case Name :PI expressCheck : Department_OfficeCode option select + department selection ");
		webActionUtils.TestCaseinfo("================================");

		String accountName = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 130, 1);
		String departmentName = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 130, 2);
		String code = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 130, 3);

		LoginPage loginPage = new LoginPage(driver, webActionUtils);
		loginPage.enterUserName(USER_NAME);
		loginPage.enterPassword(PASSWORD);
		loginPage.clickOnSignin();
		UploadFilePage uploadFile = new UploadFilePage(driver, webActionUtils);
		uploadFile.clickOnUploadFile();
		uploadFile.clickOnPolicyInsightsExpressCheckRadiobutton();
		uploadFile.selectAccount(accountName);
		uploadFile.clickOnDepartment_OfficeCode();
		uploadFile.selectRequiredCode(code);
		uploadFile.selectDepartment(departmentName);
	}
}
