package com.automation.pi.test002.uploadfilepage;

import java.lang.reflect.Method;

import org.testng.annotations.Test;

import com.automation.pi.library.BaseTest;
import com.automation.pi.pages.LoginPage;
import com.automation.pi.pages.UploadFilePage;

public class UploadFile61 extends BaseTest {

	@Test
	public void PIDC_TC_669(Method m) {

		webActionUtils.extentTestName(m.getName());
		webActionUtils.TestCaseinfo("================================");
		webActionUtils.TestCaseinfo(" Test Case Name : Verify error ' Message: Please enter valid email address for Account Manager is displayed");
		webActionUtils.TestCaseinfo("================================");

		String accountName = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 169, 1);
		String departmentName = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 169, 2);
		String namedInsured = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 169, 3);
		String lob = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 169, 4);
		String policyNumber = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 169, 5);
		String effDate = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 169, 6);
		String accountManager = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 169, 7);
		String newAccountManager = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 169, 8);
		String assistentAccountManager = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 169, 9);
		String newAssistentAccountManager = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 169, 10);
		String message = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 169, 11);

		LoginPage loginPage = new LoginPage(driver, webActionUtils);
		loginPage.enterUserName(USER_NAME);
		loginPage.enterPassword(PASSWORD);
		loginPage.clickOnSignin();
		
		UploadFilePage uploadFile = new UploadFilePage(driver, webActionUtils);
		uploadFile.clickOnUploadFile();
		uploadFile.clickOnPolicyInsightsAdvanceCheckbutton();
		uploadFile.enterRequiredAccount(accountName);
		uploadFile.enterRequiredDepartment(departmentName);
		uploadFile.enterRequiredNamedInsured(namedInsured);
		uploadFile.enterRequiredLOB(lob);
		uploadFile.enterRequiredPolicyNumber(policyNumber);
		uploadFile.enterRequiredEffectiveDate(effDate);
		uploadFile.enterHighlightedDateFromClient();
		uploadFile.enterRequiredAccountManager(accountManager);
		uploadFile.enterNewAccountManagerName(newAccountManager);
		uploadFile.checkNewAssitantAccountManagerCheckbox();
		uploadFile.enterRequiredAssitantAccountManager(assistentAccountManager);
		uploadFile.enterNewAssistentAccountManagerName(newAssistentAccountManager);
		uploadFile.clickonSubmitButton();
		uploadFile.verifyToastMessage(message);
	}
}