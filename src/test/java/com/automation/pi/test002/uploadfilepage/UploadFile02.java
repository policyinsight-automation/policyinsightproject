package com.automation.pi.test002.uploadfilepage;

import java.lang.reflect.Method;

import org.testng.annotations.Test;

import com.automation.pi.library.BaseTest;
import com.automation.pi.pages.LoginPage;
import com.automation.pi.pages.UploadFilePage;

public class UploadFile02 extends BaseTest {

	@Test
	public void PIDC_TC_949(Method m) {

		webActionUtils.extentTestName(m.getName());
		webActionUtils.TestCaseinfo("================================");
		webActionUtils.TestCaseinfo(
				" Test Case Name : Verify that for specific account selection, 'Form Setting'  table is getting displayed");
		webActionUtils.TestCaseinfo("================================");

		String accountName = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 4, 1);
		String departmentName = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 4, 2);

		LoginPage loginPage = new LoginPage(driver, webActionUtils);
		loginPage.enterUserName(USER_NAME);
		loginPage.enterPassword(PASSWORD);
		loginPage.clickOnSignin();
		UploadFilePage uploadFile = new UploadFilePage(driver, webActionUtils);

		uploadFile.clickOnUploadFile();
		uploadFile.enterRequiredAccount(accountName);
		uploadFile.enterRequiredDepartment(departmentName);
		uploadFile.verifyformSectionisDisplayed();
	}
}
