package com.automation.pi.test002.uploadfilepage;

import java.lang.reflect.Method;

import org.apache.commons.lang3.RandomStringUtils;
import org.testng.annotations.Test;

import com.automation.pi.library.BaseTest;
import com.automation.pi.pages.LoginPage;
import com.automation.pi.pages.UploadFilePage;

public class UploadFile15 extends BaseTest 
{
	@Test
	public void PIDC_TC_171(Method m)
	{
		//Verify that when internal due date = client due date and internal due date = current date, the internal due date is not changed in the Upload file page .
		webActionUtils.extentTestName(m.getName());
		webActionUtils.TestCaseinfo("================================");
		webActionUtils.TestCaseinfo(" Test Case Name : Verification of Internal Due Date "
				+ "when Internal Due Date and Client Due Date are Equal"
				+ "and Internal Due Date is Equal to Current Date"
				+ "Internal Due date not changed");
		webActionUtils.TestCaseinfo("================================");

		String accountName="CBIZ";
		String departmentName="Alpharetta, GA - Auto Renewal";
		String nameInsured = "Test" + RandomStringUtils.randomAlphanumeric(8);
		String lobValue="PKG";
		String policyNumber="1234" + RandomStringUtils.randomAlphanumeric(8);
		String dateformat="09/11/2024";
		String dateFromClient = "02/13/2025";
		String internalDueDate = "02/28/2025";
		String policyStatusOpn = "New Business";
		
		LoginPage loginPage = new LoginPage(driver, webActionUtils);
		loginPage.enterUserName(USER_NAME );
		loginPage.enterPassword(PASSWORD);
		loginPage.clickOnSignin();

		UploadFilePage uploadFile=new UploadFilePage(driver,webActionUtils);
		uploadFile.clickOnUploadFile();
		uploadFile.uploadValuesForDateCheck(accountName, departmentName,nameInsured, lobValue, policyNumber, dateformat, internalDueDate);
		uploadFile.checkInternalAndClientDuedate(internalDueDate,dateFromClient,policyStatusOpn);		
	}
}
