package com.automation.pi.test002.uploadfilepage;

import java.lang.reflect.Method;

import org.testng.annotations.Test;

import com.automation.pi.library.BaseTest;
import com.automation.pi.pages.LoginPage;
import com.automation.pi.pages.UploadFilePage;

public class UploadFile33 extends BaseTest 
{//2695

	@Test
	public void PIDC_TC_2829(Method m)
	{
		//Verify that 'Rush Fixed Wordings' dropdown is displayed , when user click on the rush checkbox on the 'Upload file' page
		webActionUtils.extentTestName(m.getName());
		webActionUtils.TestCaseinfo("================================");
		webActionUtils.TestCaseinfo(" Test Case Name :'Rush Fixed Wordings' dropdown check on the 'Upload file' page ");
		webActionUtils.TestCaseinfo("================================");

		LoginPage loginPage = new LoginPage(driver, webActionUtils);
		loginPage.enterUserName(USER_NAME);
		loginPage.enterPassword(PASSWORD);
		loginPage.clickOnSignin();
		UploadFilePage uploadFile = new UploadFilePage(driver, webActionUtils);
		uploadFile.clickOnUploadFile();
		uploadFile.clickOnRushCheckBox();
		uploadFile.verifyRushFixedWordingsDrodownPresent();
	}
}