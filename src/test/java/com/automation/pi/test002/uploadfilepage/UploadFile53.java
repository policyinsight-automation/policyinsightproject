package com.automation.pi.test002.uploadfilepage;

import java.lang.reflect.Method;

import org.testng.annotations.Test;

import com.automation.pi.library.BaseTest;
import com.automation.pi.pages.LoginPage;
import com.automation.pi.pages.UploadFilePage;

public class UploadFile53 extends BaseTest {

	@Test
	public void PIDC_TC_253(Method m) {
		
		//Verify that atleast 2 documents is uploaded to create an task for the PI Lite.
		webActionUtils.extentTestName(m.getName());
		webActionUtils.TestCaseinfo("================================");
		webActionUtils.TestCaseinfo(" Test Case Name :  Verify that atleast 2 documents is uploaded to create an task for the PI Lite");
		webActionUtils.TestCaseinfo("================================");
		
		String accountName = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 145, 1);
		String departmentName = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 145, 2);
		String namedInsured = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 145, 3);
		String lob = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 145, 4);
		String policyNumber = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 145, 5);
		String effectiveDate = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 145, 6);
		String accountManager = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 145, 7);
		String addtional = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 145, 8);
		String policyStatusOpn = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 145, 9);
		String premiumValue = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 145, 10);
		String carrier  = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 145, 11);
		String expectedText  = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 145, 12);
			
		String uploadfilepath1 = DIR_PATH + "/TestDataFiles/5-Policy.pdf";
		String uploadfilepath2 = DIR_PATH + "/TestDataFiles/5-Proposal.pdf";
		String uploadfilepath4 = uploadfilepath1+"\n"+uploadfilepath2;

		LoginPage loginPage = new LoginPage(driver, webActionUtils);
		loginPage.enterUserName(USER_NAME);
		loginPage.enterPassword(PASSWORD);
		loginPage.clickOnSignin();
		UploadFilePage uploadFile = new UploadFilePage(driver, webActionUtils);
		uploadFile.clickOnUploadFile();
		uploadFile.clickOnPolicyInsightsExpressCheckRadiobutton();
		uploadFile.enterRequiredAccount(accountName);
		uploadFile.enterRequiredDepartment(departmentName);
		uploadFile.enterRequiredNamedInsured(namedInsured);
		uploadFile.enterRequiredLOB(lob);
		uploadFile.enterRequiredPolicyNumber(policyNumber);
		uploadFile.enterRequiredEffectiveDate(effectiveDate);
		uploadFile.enterHighlightedDateFromClient();
		uploadFile.enterRequiredAccountManager(accountManager);
		uploadFile.enterRequiredAdditionalField(addtional);
		uploadFile.enterRequiredPolicyStatus(policyStatusOpn);
		uploadFile.enterRequiredPremiumRange(premiumValue);
		uploadFile.uploadRequiredFiles(uploadfilepath4);
		uploadFile.enterRequiredCarrier(carrier);
		uploadFile.clickonSubmitButton();
		uploadFile.getMessageandVerify(expectedText);
		uploadFile.verificationMessage();
}
}
