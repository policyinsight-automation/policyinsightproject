package com.automation.pi.test002.uploadfilepage;

import java.lang.reflect.Method;

import org.apache.commons.lang3.RandomStringUtils;
import org.testng.annotations.Test;

import com.automation.pi.library.BaseTest;
import com.automation.pi.pages.LoginPage;
import com.automation.pi.pages.UploadFilePage;

public class UploadFile01 extends BaseTest {

	@Test
	public void PIDC_TC_948(Method m) {

		webActionUtils.extentTestName(m.getName());
		webActionUtils.TestCaseinfo("================================");
		webActionUtils.TestCaseinfo("Test Case Name : First letter of the each word is converted into UpperCase on click of 'Format Named Insured' Icon");
		webActionUtils.TestCaseinfo("================================");

		String formatNamedInsuredText = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 1, 1)
				+ RandomStringUtils.randomAlphabetic(3);

		LoginPage loginPage = new LoginPage(driver, webActionUtils);
		loginPage.enterUserName(USER_NAME);
		loginPage.enterPassword(PASSWORD);
		loginPage.clickOnSignin();

		UploadFilePage uploadFile = new UploadFilePage(driver, webActionUtils);
		uploadFile.clickOnUploadFile();
		uploadFile.verifyFormatNamedInsuredfunctionality(formatNamedInsuredText);
	}
}