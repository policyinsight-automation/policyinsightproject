package com.automation.pi.test002.uploadfilepage;

import java.lang.reflect.Method;

import org.apache.commons.lang3.RandomStringUtils;
import org.testng.annotations.Test;

import com.automation.pi.library.BaseTest;
import com.automation.pi.pages.LoginPage;
import com.automation.pi.pages.UploadFilePage;

public class UploadFile71 extends BaseTest {

	@Test
	public void PIDC_TC_237(Method m) {

		webActionUtils.extentTestName(m.getName());
		webActionUtils.TestCaseinfo("================================");
		webActionUtils.TestCaseinfo(
				" Test Case Name : Verify that the department codes which are mapped to PI Lite Departments should only be auto suggested in the Department/office code field, when user select the PI Lite radio button.");
		webActionUtils.TestCaseinfo("================================");

		String Account="CREST";
		String Department="JRU-Location - RLY-Segmentation - TKT-Checking Type";
		String code="Hello 3";
		
		
		LoginPage loginPage = new LoginPage(driver, webActionUtils);
		loginPage.enterUserName(USER_NAME);
		loginPage.enterPassword(PASSWORD);
		loginPage.clickOnSignin();
		UploadFilePage uploadFile = new UploadFilePage(driver, webActionUtils);
		uploadFile.clickOnUploadFile();
		uploadFile.selectAccount(Account);
		uploadFile.clickOnDepartment_OfficeCode();
		uploadFile.selectRequiredCode(code);
		uploadFile.selectDepartment(Department);
	}
}