package com.automation.pi.test002.uploadfilepage;

import java.lang.reflect.Method;

import org.testng.annotations.Test;

import com.automation.pi.library.BaseTest;
import com.automation.pi.pages.LoginPage;
import com.automation.pi.pages.UploadFilePage;

public class UploadFile50 extends BaseTest {

	@Test
	public void PIDC_TC_240(Method m) {
		
		// Verify that error message is displayed as "Only one source type provided" ,
		// when user uploads only 1 file and only 1 source type is selected.
		webActionUtils.extentTestName(m.getName());
		webActionUtils.TestCaseinfo("================================");
		webActionUtils.TestCaseinfo(
				" Test Case Name : PI Express Check :Error message is displayed as 'Only one source type provided'");
		webActionUtils.TestCaseinfo("================================");

		String accountName = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 136, 1);
		String departmentName = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 136, 2);
		String namedInsured = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 136, 3);
		String lob = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 136, 4);
		String policyNumber = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 136, 5);
		String effectiveDate = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 136, 6);
		String accountManager = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 136, 7);
		String addtional = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 136, 8);
		String policyStatusOpn = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 136, 9);
		String premiumValue = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 136, 10);
		String expectedText  = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 136, 11);
		
		String uploadfilepath1 = DIR_PATH + "/TestDataFiles/5-Policy.pdf";

		LoginPage loginPage = new LoginPage(driver, webActionUtils);
		loginPage.enterUserName(USER_NAME);
		loginPage.enterPassword(PASSWORD);
		loginPage.clickOnSignin();
		UploadFilePage uploadFile = new UploadFilePage(driver, webActionUtils);
		uploadFile.clickOnUploadFile();
		uploadFile.clickOnPolicyInsightsExpressCheckRadiobutton();
		uploadFile.enterRequiredAccount(accountName);
		uploadFile.enterRequiredDepartment(departmentName);
		uploadFile.enterRequiredNamedInsured(namedInsured);
		uploadFile.enterRequiredLOB(lob);
		uploadFile.enterRequiredPolicyNumber(policyNumber);
		uploadFile.enterRequiredEffectiveDate(effectiveDate);
		uploadFile.enterHighlightedDateFromClient();
		uploadFile.enterRequiredAccountManager(accountManager);
		uploadFile.enterRequiredAdditionalField(addtional);
		uploadFile.enterRequiredPolicyStatus(policyStatusOpn);
		
		uploadFile.enterRequiredPremiumRange(premiumValue);
		uploadFile.uploadRequiredFiles(uploadfilepath1);
		uploadFile.clickonSubmitButton();
		uploadFile.getMessageandVerify(expectedText);

//		enterFieldvaluesFOrPILiteAndCarrierfieldmandatory(accountName, departmentName, lob,
//				policyNumber, effDate, accountManager, policyStatusOpn, uploadfilepath1, uploadfilepath2,
//				expectedText2);

	}
}
