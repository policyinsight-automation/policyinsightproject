package com.automation.pi.test002.uploadfilepage;

import java.lang.reflect.Method;

import org.testng.annotations.Test;

import com.automation.pi.library.BaseTest;
import com.automation.pi.pages.LoginPage;
import com.automation.pi.pages.UploadFilePage;

public class UploadFile12 extends BaseTest {

	@Test
	public void PIDC_TC_1075(Method m) {

		webActionUtils.extentTestName(m.getName());
		webActionUtils.TestCaseinfo("================================");
		webActionUtils.TestCaseinfo(
				" Test Case Name : Verify that Premium Range Accepts combination of given special Charecters and Numbers");
		webActionUtils.TestCaseinfo("================================");

		String premiunRangeText = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 34, 1);
		
		LoginPage loginPage = new LoginPage(driver, webActionUtils);
		loginPage.enterUserName(USER_NAME);
		loginPage.enterPassword(PASSWORD);
		loginPage.clickOnSignin();

		UploadFilePage uploadFile = new UploadFilePage(driver, webActionUtils);
		uploadFile.clickOnUploadFile();
		uploadFile.verifyPremiumRangeInputs(premiunRangeText);
	}
}