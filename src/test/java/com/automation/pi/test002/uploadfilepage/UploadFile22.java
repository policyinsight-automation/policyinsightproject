package com.automation.pi.test002.uploadfilepage;

import java.lang.reflect.Method;

import org.apache.commons.lang3.RandomStringUtils;
import org.testng.annotations.Test;

import com.automation.pi.library.BaseTest;
import com.automation.pi.pages.LoginPage;
import com.automation.pi.pages.UploadFilePage;

public class UploadFile22 extends BaseTest {
	
	@Test
	public void PIDC_TC_182(Method m) {
		// Verify user is able to view the popup window when clicked on the submit
		// button in upload page whenever the internal due date is greater than client
		// due date in the upload file page.
		webActionUtils.extentTestName(m.getName());
		webActionUtils.TestCaseinfo("================================");
		webActionUtils.TestCaseinfo(
				"Test Case Name :Internal Pop Up - Action when internal due date is greater than client due date.");
		webActionUtils.TestCaseinfo("================================");

		String accountName = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 64, 1);
		String departmentName = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 64, 2);
		String namedInsured = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 64, 3);
		String lob = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 64, 4);
		String policyNumber = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 64, 5);
		String effectiveDate = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 64, 6);
		String accountManager = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 64, 7);
		String policyStatusOpn = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 64, 8);
		String clientDate = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 64, 9);
		String internalDate = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 64, 10);
		String internalPopUpText = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 64, 11);
		
		namedInsured = namedInsured + "test " + RandomStringUtils.randomAlphabetic(12);
		String uploadfilepath = DIR_PATH + "/TestDataFiles/5-Policy.pdf";

		LoginPage loginPage = new LoginPage(driver, webActionUtils);
		loginPage.enterUserName(USER_NAME);
		loginPage.enterPassword(PASSWORD);
		loginPage.clickOnSignin();

		UploadFilePage uploadFile = new UploadFilePage(driver, webActionUtils);
		uploadFile.clickOnUploadFile();
		uploadFile.enterRequiredAccount(accountName);
		uploadFile.enterRequiredDepartment(departmentName);
		uploadFile.enterRequiredNamedInsured(namedInsured);
		uploadFile.enterRequiredLOB(lob);
		uploadFile.enterRequiredPolicyNumber(policyNumber);
		uploadFile.enterRequiredEffectiveDate(effectiveDate);
		uploadFile.enterHighlightedDateFromClient();
		uploadFile.enterRequiredAccountManager(accountManager);
		uploadFile.enterRequiredPolicyStatus(policyStatusOpn);
		uploadFile.uploadRequiredFiles(uploadfilepath);
		uploadFile.enterInternalAndClientDueDate(internalDate, clientDate);
		uploadFile.compareInternalAndClientDueDate(internalDate, clientDate);
		uploadFile.clickonSubmitButton1();
		uploadFile.verifyInternalPopUpToastMessage(internalPopUpText);
	}
}