package com.automation.pi.test002.uploadfilepage;

import java.lang.reflect.Method;

import org.apache.commons.lang3.RandomStringUtils;
import org.testng.annotations.Test;

import com.automation.pi.library.BaseTest;
import com.automation.pi.pages.LoginPage;
import com.automation.pi.pages.UploadFilePage;

public class UploadFile75 extends BaseTest {

	@Test
	public void PIDC_TC_2880(Method m) {

		webActionUtils.extentTestName(m.getName());
		webActionUtils.TestCaseinfo("================================");
		webActionUtils.TestCaseinfo(
				" Test Case Name : Verify that user is able to view all the package recods in the grid for the default saved filters in the manage page");
		webActionUtils.TestCaseinfo("================================");

		String accountName = "CREST";
		String departmentName = "JRU-Location - RLY-Segmentation - TKT-Checking Type";
		String namedInsured = "CREST_";
		String lob = "PKG_lhakpaOK";
		String policyNumber = "1234" + RandomStringUtils.randomAlphanumeric(8);
		String effDate = "05/23/2024";
		String accountManager = "Other";
		String newAccountManager = "test123";
		String assistentAccountManager = "Other";
		String newAssistentAccountManager = "AAMtest123";
		String message = "Message: Please fill in Account Manager's Email.";

		LoginPage loginPage = new LoginPage(driver, webActionUtils);
		loginPage.enterUserName(USER_NAME);
		loginPage.enterPassword(PASSWORD);
		loginPage.clickOnSignin();
		UploadFilePage uploadFile = new UploadFilePage(driver, webActionUtils);
		uploadFile.clickOnUploadFile();
		uploadFile.clickOnPolicyInsightsAdvanceCheckbutton();
		uploadFile.enterRequiredAccount(accountName);
		uploadFile.enterRequiredDepartment(departmentName);
		uploadFile.enterRequiredNamedInsured(namedInsured);
		uploadFile.enterRequiredLOB(lob);
		uploadFile.enterRequiredPolicyNumber(policyNumber);
		uploadFile.enterRequiredEffectiveDate(effDate);
		uploadFile.enterHighlightedDateFromClient();
		uploadFile.enterRequiredAccountManager(accountManager);
		uploadFile.enterNewAccountManagerName(newAccountManager);
		uploadFile.checkNewAssitantAccountManagerCheckbox();
		uploadFile.enterRequiredAssitantAccountManager(assistentAccountManager);
		uploadFile.enterNewAssistentAccountManagerName(newAssistentAccountManager);
		uploadFile.clickonSubmitButton();
		uploadFile.verifyingToast(message);
	}
}