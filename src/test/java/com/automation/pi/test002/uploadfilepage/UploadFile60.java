package com.automation.pi.test002.uploadfilepage;

import java.lang.reflect.Method;

import org.testng.annotations.Test;

import com.automation.pi.library.BaseTest;
import com.automation.pi.pages.LoginPage;
import com.automation.pi.pages.UploadFilePage;

public class UploadFile60 extends BaseTest {

	@Test
	public void PIDC_TC_668(Method m) {
		
		webActionUtils.extentTestName(m.getName());
		webActionUtils.TestCaseinfo("================================");
		webActionUtils.TestCaseinfo(" Test Case Name : Verify error ' Message: Policy should be less than 100 characters.is displayed");
		webActionUtils.TestCaseinfo("================================");
		
		String accountName = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 166, 1);;
		String departmentName = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 166, 2);
		String namedInsured = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 166, 3);
		String lob = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 166, 4);
		String policyNumber = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 166, 5);
		String expectedText=excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 166, 6);

		LoginPage loginPage = new LoginPage(driver, webActionUtils);
		loginPage.enterUserName(USER_NAME);
		loginPage.enterPassword(PASSWORD);
		loginPage.clickOnSignin();
		UploadFilePage uploadFile = new UploadFilePage(driver, webActionUtils);
		uploadFile.clickOnUploadFile();
		uploadFile.clickOnPolicyInsightsAdvanceCheckbutton();
		uploadFile.enterRequiredAccount(accountName);
		uploadFile.enterRequiredDepartment(departmentName);
		uploadFile.enterRequiredNamedInsured(namedInsured);
		uploadFile.enterRequiredLOB(lob);
		uploadFile.enterRequiredPolicyNumber(policyNumber);
		uploadFile.clickonSubmitButton();
		uploadFile.verifyToastMessage(expectedText);
		
		}
}
