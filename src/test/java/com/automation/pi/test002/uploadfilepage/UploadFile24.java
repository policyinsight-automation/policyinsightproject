package com.automation.pi.test002.uploadfilepage;

import java.lang.reflect.Method;

import org.apache.commons.lang3.RandomStringUtils;
import org.testng.annotations.Test;

import com.automation.pi.library.BaseTest;
import com.automation.pi.pages.LoginPage;
import com.automation.pi.pages.UploadFilePage;

public class UploadFile24 extends BaseTest {

	@Test
	public void PIDC_TC_137(Method m) {

		// Verify that Package is not created, On click of No button on pop up window in
		// the upload page .
		webActionUtils.extentTestName(m.getName());
		webActionUtils.TestCaseinfo("================================");
		webActionUtils.TestCaseinfo(" Test Case Name : click on No button on internal pop up");
		webActionUtils.TestCaseinfo("================================");

		String accountName = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 70, 1);
		String departmentName = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 70, 2);
		String namedInsured = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 70, 3);
		String lob = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 70, 4);
		String policyNumber = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 70, 5);
		String effectiveDate = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 70, 6);
		String accountManager = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 70, 7);
		String policyStatusOpn = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 70, 8);
		String clientDate = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 70, 9);
		String internalDate = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 70, 10);
		String internalPopUpText = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 70, 11);

		namedInsured = namedInsured + "test " + RandomStringUtils.randomAlphabetic(12);
		String uploadfilepath = DIR_PATH + "/TestDataFiles/5-Policy.pdf";

		LoginPage loginPage = new LoginPage(driver, webActionUtils);
		loginPage.enterUserName(USER_NAME);
		loginPage.enterPassword(PASSWORD);
		loginPage.clickOnSignin();

		UploadFilePage uploadFile = new UploadFilePage(driver, webActionUtils);
		uploadFile.clickOnUploadFile();
		uploadFile.enterRequiredAccount(accountName);
		uploadFile.enterRequiredDepartment(departmentName);
		uploadFile.enterRequiredNamedInsured(namedInsured);
		uploadFile.enterRequiredLOB(lob);
		uploadFile.enterRequiredPolicyNumber(policyNumber);
		uploadFile.enterRequiredEffectiveDate(effectiveDate);
		uploadFile.enterHighlightedDateFromClient();
		uploadFile.enterRequiredAccountManager(accountManager);
		uploadFile.enterRequiredPolicyStatus(policyStatusOpn);
		uploadFile.uploadRequiredFiles(uploadfilepath);
		uploadFile.enterInternalAndClientDueDate(internalDate, clientDate);
		uploadFile.compareInternalAndClientDueDate(internalDate, clientDate);
		uploadFile.clickonSubmitButton1();
		uploadFile.verifyInternalPopUpToastMessage(internalPopUpText);
		uploadFile.clickNoForInternalDueDatePopup();
	}
}