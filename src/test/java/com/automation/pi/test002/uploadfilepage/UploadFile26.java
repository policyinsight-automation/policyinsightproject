package com.automation.pi.test002.uploadfilepage;

import java.lang.reflect.Method;

import org.testng.annotations.Test;

import com.automation.pi.library.BaseTest;
import com.automation.pi.pages.LoginPage;
import com.automation.pi.pages.UploadFilePage;

public class UploadFile26 extends BaseTest
{
	@Test
	public void PIDC_TC_961(Method m){

		//Verify that user is able to submit the package by selecting 'Renewal' policy status, 'This is a renewal policy, but no Expiring Policy uploaded. Do you want to continue?'  popup is displayed .
		webActionUtils.extentTestName(m.getName());
		webActionUtils.TestCaseinfo("================================");
		webActionUtils.TestCaseinfo(" Test Case Name : Renewal pop up- action confirm yes ");
		webActionUtils.TestCaseinfo("================================");
		
		String accountName = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 76, 1);
		String departmentName = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 76, 2);
		String nameInsured = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 76, 3);
		String lob = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 76, 4);
		String policyNumber = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 76, 5);
		String effectiveDate = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 76, 6);
		String accountManager = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 76, 7);
		String policyStatusOpn = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 76, 8);
		String renewalPopUpText = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 76, 9);
		String expectedText = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 76, 10);
		String uploadfilepath = DIR_PATH + "/TestDataFiles/5-Policy.pdf";
		
		LoginPage loginPage = new LoginPage(driver, webActionUtils);
		loginPage.enterUserName(USER_NAME);
		loginPage.enterPassword(PASSWORD);
		loginPage.clickOnSignin();
		UploadFilePage uploadFile = new UploadFilePage(driver, webActionUtils);
		uploadFile.clickOnUploadFile();
		uploadFile.clickOnUploadFile();
		uploadFile.enterRequiredAccount(accountName);
		uploadFile.enterRequiredDepartment(departmentName);
		uploadFile.enterRequiredNamedInsured(nameInsured);
		uploadFile.enterRequiredLOB(lob);
		uploadFile.enterRequiredPolicyNumber(policyNumber);
		uploadFile.enterRequiredEffectiveDate(effectiveDate);
		uploadFile.enterHighlightedDateFromClient();
		uploadFile.enterRequiredAccountManager(accountManager);
		uploadFile.enterRequiredPolicyStatus(policyStatusOpn);
		uploadFile.uploadRequiredFiles(uploadfilepath);
		uploadFile.clickonSubmitButton1();
		uploadFile.verifyRenewalPopUpToastMessage(renewalPopUpText);
		uploadFile.confirmYesOnRenewalPopUp();
		uploadFile.verifyToastMessage(expectedText);
	}
}