package com.automation.pi.test002.uploadfilepage;

import java.lang.reflect.Method;

import org.testng.annotations.Test;

import com.automation.pi.library.BaseTest;
import com.automation.pi.pages.LoginPage;
import com.automation.pi.pages.UploadFilePage;

public class UploadFile45 extends BaseTest {

	@Test
	public void PIDC_TC_230(Method m) {
	
		// Verify that Office/Location field is cleared ,when user click in and out of
		// Department/Office Code field (for the radio button PI and PI Lite).
		webActionUtils.extentTestName(m.getName());
		webActionUtils.TestCaseinfo("================================");
		webActionUtils.TestCaseinfo(" Test Case Name : Office/Location field is cleared ,when user click in and out of Department/Office Code field ");
		webActionUtils.TestCaseinfo("================================");

		String accountName = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 121, 1);
		String departmentName = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 121, 2);

		LoginPage loginPage = new LoginPage(driver, webActionUtils);
		loginPage.enterUserName(USER_NAME);
		loginPage.enterPassword(PASSWORD);
		loginPage.clickOnSignin();
		UploadFilePage uploadFile = new UploadFilePage(driver, webActionUtils);
		uploadFile.clickOnUploadFile();
		uploadFile.selectAccountDepartment(accountName, departmentName);
		uploadFile.clickOnDepartment_OfficeCode();
		uploadFile.verifyOfficeLocationCleared();
	}
}
