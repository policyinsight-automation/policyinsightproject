package com.automation.pi.test002.uploadfilepage;

import java.lang.reflect.Method;

import org.apache.commons.lang3.RandomStringUtils;
import org.testng.annotations.Test;

import com.automation.pi.library.BaseTest;
import com.automation.pi.pages.LoginPage;
import com.automation.pi.pages.UploadFilePage;

public class UploadFile23 extends BaseTest 
{//02

	@Test
	public void PIDC_TC_136(Method m) 
	{
		//Verify that upload page is displayed on click of yes button on Internal pop-up window and the package is created with success message in the upload file page.
		webActionUtils.extentTestName(m.getName());
		webActionUtils.TestCaseinfo("================================");
		webActionUtils.TestCaseinfo(" Test Case Name : Package Creation - Action - On Click of 'YES on Internal POP UP' ");
		webActionUtils.TestCaseinfo("================================");

		String accountName = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 67, 1);
		String departmentName = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 67, 2);
		String namedInsured = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 67, 3);
		String lob = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 67, 4);
		String policyNumber = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 67, 5);
		String effectiveDate = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 67, 6);
		String accountManager = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 67, 7);
		String policyStatusOpn = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 67, 8);
		String clientDate = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 67, 9);
		String internalDate = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 67, 10);
		String internalPopUpText = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 67, 11);
		String expectedText = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 67, 12);

		namedInsured = namedInsured + "test " + RandomStringUtils.randomAlphabetic(12);
		String uploadfilepath = DIR_PATH + "/TestDataFiles/5-Policy.pdf";

		LoginPage loginPage = new LoginPage(driver, webActionUtils);
		loginPage.enterUserName(USER_NAME);
		loginPage.enterPassword(PASSWORD);
		loginPage.clickOnSignin();

		UploadFilePage uploadFile = new UploadFilePage(driver, webActionUtils);
		uploadFile.clickOnUploadFile();
		uploadFile.enterRequiredAccount(accountName);
		uploadFile.enterRequiredDepartment(departmentName);
		uploadFile.enterRequiredNamedInsured(namedInsured);
		uploadFile.enterRequiredLOB(lob);
		uploadFile.enterRequiredPolicyNumber(policyNumber);
		uploadFile.enterRequiredEffectiveDate(effectiveDate);
		uploadFile.enterHighlightedDateFromClient();
		uploadFile.enterRequiredAccountManager(accountManager);
		uploadFile.enterRequiredPolicyStatus(policyStatusOpn);
		uploadFile.uploadRequiredFiles(uploadfilepath);
		uploadFile.enterInternalAndClientDueDate(internalDate, clientDate);
		uploadFile.compareInternalAndClientDueDate(internalDate, clientDate);
		uploadFile.clickonSubmitButton1();
		uploadFile.verifyInternalPopUpToastMessage(internalPopUpText);
		uploadFile.clickConfirmYesAndCheckToast(expectedText);

	}
}