package com.automation.pi.test002.uploadfilepage;

import java.lang.reflect.Method;

import org.testng.annotations.Test;

import com.automation.pi.library.BaseTest;
import com.automation.pi.pages.LoginPage;
import com.automation.pi.pages.UploadFilePage;

public class UploadFile28 extends BaseTest {

	@Test
	public void PIDC_TC_965(Method m) {
		// Verify that on click of 'No' button of the ''This is a renewal policy, but no
		// Expiring Policy uploaded. Do you want to continue?' popup, package is not
		// submitted in the upload file page
		webActionUtils.extentTestName(m.getName());
		webActionUtils.TestCaseinfo("================================");
		webActionUtils.TestCaseinfo(" Test Case Name : Renewal pop up - Action click on No button");
		webActionUtils.TestCaseinfo("================================");

		String accountName = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 82, 1);
		String departmentName = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 82, 2);
		String nameInsured = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 82, 3);
		String lob = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 82, 4);
		String policyNumber = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 82, 5);
		String effectiveDate = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 82, 6);
		String accountManager = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 82, 7);
		String policyStatusOpn = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 82, 8);
		String renewalPopUpText = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 82, 9);
		String uploadfilepath = DIR_PATH + "/TestDataFiles/5-Policy.pdf";

		LoginPage loginPage = new LoginPage(driver, webActionUtils);
		loginPage.enterUserName(USER_NAME);
		loginPage.enterPassword(PASSWORD);
		loginPage.clickOnSignin();
		UploadFilePage uploadFile = new UploadFilePage(driver, webActionUtils);
		uploadFile.clickOnUploadFile();
		uploadFile.enterRequiredAccount(accountName);
		uploadFile.enterRequiredDepartment(departmentName);
		uploadFile.enterRequiredNamedInsured(nameInsured);
		uploadFile.enterRequiredLOB(lob);
		uploadFile.enterRequiredPolicyNumber(policyNumber);
		uploadFile.enterRequiredEffectiveDate(effectiveDate);
		uploadFile.enterHighlightedDateFromClient();
		uploadFile.enterRequiredAccountManager(accountManager);
		uploadFile.enterRequiredPolicyStatus(policyStatusOpn);
		uploadFile.uploadRequiredFiles(uploadfilepath);
		uploadFile.clickonSubmitButton1();
		uploadFile.verifyRenewalPopUpToastMessage(renewalPopUpText);
		uploadFile.clickNoForRenewalPopup();
	}
}