package com.automation.pi.test002.uploadfilepage;

import java.lang.reflect.Method;

import org.apache.commons.lang3.RandomStringUtils;
import org.testng.annotations.Test;

import com.automation.pi.library.BaseTest;
import com.automation.pi.pages.LoginPage;
import com.automation.pi.pages.UploadFilePage;

public class UploadFile21 extends BaseTest {
	@Test
	public void PIDC_TC_181(Method m) {
		// Verify that pop up is displayed with form section details for the
		// mentioned�details ( Policy#, Effective date and LOB) for multiple times in
		// upload page
		webActionUtils.extentTestName(m.getName());
		webActionUtils.TestCaseinfo("================================");
		webActionUtils.TestCaseinfo(
				" Test Case Name :Duplicate Pop Up with Form Section- when same Policy#,Effective date and LOB");
		webActionUtils.TestCaseinfo("================================");
		
		
		String accountName = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 61, 1);
		String departmentName = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 61, 2);
		String namedInsured = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 61, 3);
		String lob = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 61, 4);
		String policyNumber = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 61, 5);
		String effectiveDate = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 61, 6);
		String accountManager = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 61, 7);
		String policyStatusOpn = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 61, 8);
		String expectedText = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 61, 9);
		String expectedPopUpText = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 61, 10);
		
		String uploadfilepath1 = DIR_PATH + "/TestDataFiles/5-Policy.pdf";
		String uploadfilepath2 = DIR_PATH + "/TestDataFiles/5-Proposal.pdf";
		String uploadfilepath3 = uploadfilepath1 + "\n" + uploadfilepath2;

		namedInsured = namedInsured + " Test" + RandomStringUtils.randomAlphanumeric(10);
		String nameInsured1 = namedInsured;
		policyNumber = policyNumber + " TestPOl" + RandomStringUtils.randomAlphanumeric(10);
		String policyNumber1 = policyNumber;		
		
		LoginPage loginPage = new LoginPage(driver, webActionUtils);
		loginPage.enterUserName(USER_NAME);
		loginPage.enterPassword(PASSWORD);
		loginPage.clickOnSignin();
		
		UploadFilePage uploadFile = new UploadFilePage(driver, webActionUtils);
		uploadFile.clickOnUploadFile();
		uploadFile.enterRequiredAccount(accountName);
		uploadFile.enterRequiredDepartment(departmentName);
		uploadFile.enterRequiredConstantNamedInsured(namedInsured);
		uploadFile.enterRequiredLOB(lob);
		uploadFile.enterConstantPolicyNumber(policyNumber);
		uploadFile.enterRequiredEffectiveDate(effectiveDate);
		uploadFile.enterHighlightedDateFromClient();
		uploadFile.enterRequiredAccountManager(accountManager);
		uploadFile.enterRequiredPolicyStatus(policyStatusOpn);
		uploadFile.uploadRequiredFiles(uploadfilepath3);
		uploadFile.clickonSubmitButton1();
		uploadFile.getMessageContainsVerify(expectedText);
		webActionUtils.refreshPage();
		webActionUtils.refreshPage();
		webActionUtils.waitForPageLoad();
		uploadFile.enterRequiredAccount(accountName);
		uploadFile.enterRequiredDepartment(departmentName);
		uploadFile.enterRequiredConstantNamedInsured(nameInsured1);
		uploadFile.enterRequiredLOB(lob);
		uploadFile.enterConstantPolicyNumber(policyNumber1);
		uploadFile.enterRequiredEffectiveDate(effectiveDate);
		uploadFile.enterHighlightedDateFromClient();
		uploadFile.enterRequiredAccountManager(accountManager);
		uploadFile.enterRequiredPolicyStatus(policyStatusOpn);
		uploadFile.uploadRequiredFiles(uploadfilepath3);
		uploadFile.verifySectionTable();
		uploadFile.clickonSubmitButton1();
		uploadFile.verifyDuplicatePopUpToastMessage(expectedPopUpText);
		uploadFile.clickConfirmYesAndCheckToast(expectedText);

	}
}