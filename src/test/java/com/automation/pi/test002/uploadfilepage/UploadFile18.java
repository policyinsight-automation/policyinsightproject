package com.automation.pi.test002.uploadfilepage;

import java.lang.reflect.Method;

import org.apache.commons.lang3.RandomStringUtils;
import org.testng.annotations.Test;

import com.automation.pi.library.BaseTest;
import com.automation.pi.pages.LoginPage;
import com.automation.pi.pages.UploadFilePage;

public class UploadFile18 extends BaseTest {

	@Test
	public void PIDC_TC_174(Method m) {

		webActionUtils.extentTestName(m.getName());
		webActionUtils.TestCaseinfo("================================");
		webActionUtils.TestCaseinfo(
				" Test Case Name : Verify that when internal due date is not equal to client due date , the internal due date is not changed in the Upload file page ");
		webActionUtils.TestCaseinfo("================================");

		String accountName = "CBIZ";
		String departmentName = "Alpharetta, GA - Auto Renewal";
		String nameInsured = "Test" + RandomStringUtils.randomAlphanumeric(8);
		String lobValue = "PKG";
		String policyNumber = "1234" + RandomStringUtils.randomAlphanumeric(8);
		String dateformat = "04/20/2024";
		String dateFromClient = "10/10/2024"; // add 5days
		String internalDueDate = "10/15/2024";
		String policyStatusOpn = "New Business";

		LoginPage loginPage = new LoginPage(driver, webActionUtils);
		loginPage.enterUserName(USER_NAME);
		loginPage.enterPassword(PASSWORD);
		loginPage.clickOnSignin();

		UploadFilePage UploadFile = new UploadFilePage(driver, webActionUtils);
		UploadFile.clickOnUploadFile();
		UploadFile.uploadValuesForInternalDateCheck(accountName, departmentName, nameInsured, lobValue, policyNumber,
				dateformat, internalDueDate);
		UploadFile.checkInternalDateAndClientDueDate(internalDueDate, dateFromClient, policyStatusOpn);
	}
}