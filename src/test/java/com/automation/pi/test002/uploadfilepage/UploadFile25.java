package com.automation.pi.test002.uploadfilepage;

import java.lang.reflect.Method;

import org.apache.commons.lang3.RandomStringUtils;
import org.testng.annotations.Test;

import com.automation.pi.library.BaseTest;
import com.automation.pi.pages.LoginPage;
import com.automation.pi.pages.UploadFilePage;

public class UploadFile25 extends BaseTest{
	
	@Test
	public void PIDC_TC_138(Method m) {
		// Verify that 2 popup's are displayed in the upload page when internal due date
		// is greater than client due date in the upload file page.
		webActionUtils.extentTestName(m.getName());
		webActionUtils.TestCaseinfo("================================");
		webActionUtils.TestCaseinfo(
				" Test Case Name :2 popup's displayed -Action - 'internal due date is greater than client due date'.");
		webActionUtils.TestCaseinfo("================================");
		
		
		String accountName = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 73, 1);
		String departmentName = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 73, 2);
		String nameInsured = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 73, 3);
		String lob = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 73, 4);
		String policyNumber = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 73, 5);
		String effectiveDate = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 73, 6);
		String accountManager = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 73, 7);
		String policyStatusOpn = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 73, 8);
		String clientDate = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 73, 9);
		String internalDate = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 73, 10);
		String internalPopUpText = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 73, 11);
		String renewalPopUpText = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 73, 12);
		String DuplicatePopUpText = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 73, 12);
		String expectedText = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 73, 13);

		nameInsured = nameInsured +" Test" + RandomStringUtils.randomAlphabetic(5);
		String nameInsured1 = nameInsured;
		policyNumber = policyNumber + RandomStringUtils.randomAlphabetic(3);
		String policyNumber1 = policyNumber;
		
		String uploadfilepath = DIR_PATH + "/TestDataFiles/5-Policy.pdf";
		
		LoginPage loginPage = new LoginPage(driver, webActionUtils);
		loginPage.enterUserName(USER_NAME);
		loginPage.enterPassword(PASSWORD);
		loginPage.clickOnSignin();
		UploadFilePage uploadFile = new UploadFilePage(driver, webActionUtils);
		uploadFile.clickOnUploadFile();
		uploadFile.enterRequiredAccount(accountName);
		uploadFile.enterRequiredDepartment(departmentName);
		uploadFile.enterRequiredConstantNamedInsured(nameInsured);
		uploadFile.enterRequiredLOB(lob);
		uploadFile.enterConstantPolicyNumber(policyNumber);
		uploadFile.enterRequiredEffectiveDate(effectiveDate);
		uploadFile.enterHighlightedDateFromClient();
		uploadFile.enterRequiredAccountManager(accountManager);
		uploadFile.enterRequiredPolicyStatus(policyStatusOpn);
		uploadFile.uploadRequiredFiles(uploadfilepath);
		uploadFile.clickonSubmitButton1();
		uploadFile.confirmYesOnRenewalPopUp();
		uploadFile.verifyToastMessage(expectedText);
		webActionUtils.refreshPage();
		webActionUtils.waitForPageLoad();
		uploadFile.enterRequiredAccount(accountName);
		uploadFile.enterRequiredDepartment(departmentName);
		uploadFile.enterRequiredConstantNamedInsured(nameInsured1);
		uploadFile.enterRequiredLOB(lob);
		uploadFile.enterConstantPolicyNumber(policyNumber1);
		uploadFile.enterRequiredEffectiveDate(effectiveDate);
		uploadFile.enterHighlightedDateFromClient();
		uploadFile.internalLaterThanClientDueDate(internalDate, clientDate);
		uploadFile.enterRequiredAccountManager(accountManager);
		uploadFile.enterRequiredPolicyStatus(policyStatusOpn);
		uploadFile.uploadRequiredFiles(uploadfilepath);
		uploadFile.clickonSubmitButton1();
		uploadFile.verifyRenewalPopUpToastMessage(renewalPopUpText);
		uploadFile.confirmYesOnRenewalPopUp();
		uploadFile.verifyInternalPopUpToastMessage(internalPopUpText);
		uploadFile.confirmYesOninternalPopUp(expectedText);
		uploadFile.verifyDuplicatePopUpToastMessage(DuplicatePopUpText);	
		uploadFile.clickOnYesbutton();
		uploadFile.verifyToastMessage(expectedText);

		
	}
}