package com.automation.pi.test002.uploadfilepage;

import java.lang.reflect.Method;

import org.apache.commons.lang3.RandomStringUtils;
import org.testng.annotations.Test;

import com.automation.pi.library.BaseTest;
import com.automation.pi.pages.LoginPage;
import com.automation.pi.pages.UploadFilePage;

public class UploadFile35 extends BaseTest {

	@Test
	public void PIDC_TC_2831(Method m) {
		// Verfiy that the records created from the 'Rush settings page are displayed as
		// an options in the ''Rush Fixed Wordings' dropdown upload file page
		webActionUtils.extentTestName(m.getName());
		webActionUtils.TestCaseinfo("================================");
		webActionUtils
				.TestCaseinfo(" Test Case Name :Verfiy 'Rush Fixed Wordings' dropdown value on the 'Upload file' page");
		webActionUtils.TestCaseinfo("================================");

		String accountName = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 97, 1);
		String departmentName = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 97, 2);
		String namedInsured = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 97, 3);
		String lob = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 97, 4);
		String policyNumber = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 97, 5);
		String effectiveDate = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 97, 6);
		String accountManager = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 97, 7);
		String policyStatusOpn = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 97, 8);
		String rushDropdown = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 97, 9);
		String rushOption = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 97, 10);

		namedInsured = namedInsured + "test " + RandomStringUtils.randomAlphabetic(12);
		String uploadfilepath2 = DIR_PATH + "/TestDataFiles/5-Policy.pdf";
		String uploadfilepath1 = DIR_PATH + "/TestDataFiles/Exp Pol.pdf";
		String uploadfilepath3 = uploadfilepath1 + "\n" + uploadfilepath2;

		LoginPage loginPage = new LoginPage(driver, webActionUtils);
		loginPage.enterUserName(USER_NAME);
		loginPage.enterPassword(PASSWORD);
		loginPage.clickOnSignin();

		UploadFilePage uploadFile = new UploadFilePage(driver, webActionUtils);
		uploadFile.clickOnUploadFile();
		uploadFile.enterRequiredAccount(accountName);
		uploadFile.enterRequiredDepartment(departmentName);
		uploadFile.enterRequiredNamedInsured(namedInsured);
		uploadFile.enterRequiredLOB(lob);
		uploadFile.enterRequiredPolicyNumber(policyNumber);
		uploadFile.enterRequiredEffectiveDate(effectiveDate);
		uploadFile.enterHighlightedDateFromClient();
		uploadFile.enterRequiredAccountManager(accountManager);
		uploadFile.enterRequiredPolicyStatus(policyStatusOpn);
		uploadFile.uploadRequiredFiles(uploadfilepath3);
		uploadFile.clickOnRushCheckBox();
		uploadFile.verifyRushFixedWordingsDrodownPresent();
		uploadFile.selectRushFixedWordings(rushDropdown);
		uploadFile.verifyRushTextAreaDropdownvalue(rushOption);

	}
}