package com.automation.pi.test002.uploadfilepage;

import java.lang.reflect.Method;

import org.testng.annotations.Test;

import com.automation.pi.library.BaseTest;
import com.automation.pi.pages.LoginPage;
import com.automation.pi.pages.UploadFilePage;

public class UploadFile27 extends BaseTest {
	
	@Test
	public void PIDC_TC_962(Method m) throws Exception {

		// Verify that 'The Internal Due Date is later than the Client Due Date, are you
		// sure you want to submit ?' pop up is displayed on click of yes button of
		// 'This is a renewal policy, but no Expiring Policy uploaded. Do you want to
		// continue' pop up .
		webActionUtils.extentTestName(m.getName());
		webActionUtils.TestCaseinfo("================================");
		webActionUtils.TestCaseinfo("Test Case Name :Internal Pop up display check - action-Click on confirm Yes on renewal pop up");
		webActionUtils.TestCaseinfo("================================");

		String accountName = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 79, 1);
		String departmentName = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 79, 2);
		String nameInsured = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 79, 3);
		String lob = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 79, 4);
		String policyNumber = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 79, 5);
		String effectiveDate = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 79, 6);
		String accountManager = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 79, 7);
		String policyStatusOpn = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 79, 8);
		String clientDate = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 79, 9);
		String internalDate = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 79, 10);
		String internalPopUpText = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 79, 11);
		String renewalPopUpText = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 79, 12);
		String uploadfilepath = DIR_PATH + "/TestDataFiles/5-Policy.pdf";

		LoginPage loginPage = new LoginPage(driver, webActionUtils);
		loginPage.enterUserName(USER_NAME);
		loginPage.enterPassword(PASSWORD);
		loginPage.clickOnSignin();
		UploadFilePage uploadFile = new UploadFilePage(driver, webActionUtils);
		uploadFile.clickOnUploadFile();
		uploadFile.enterRequiredAccount(accountName);
		uploadFile.enterRequiredDepartment(departmentName);
		uploadFile.enterRequiredNamedInsured(nameInsured);
		uploadFile.enterRequiredLOB(lob);
		uploadFile.enterRequiredPolicyNumber(policyNumber);
		uploadFile.enterRequiredEffectiveDate(effectiveDate);
		uploadFile.enterHighlightedDateFromClient();
		uploadFile.enterRequiredAccountManager(accountManager);
		uploadFile.enterRequiredPolicyStatus(policyStatusOpn);
		uploadFile.uploadRequiredFiles(uploadfilepath);
		uploadFile.enterInternalAndClientDueDate(internalDate, clientDate);
		uploadFile.clickonSubmitButton1();
		uploadFile.verifyRenewalPopUpToastMessage(renewalPopUpText);
		uploadFile.confirmYesOnRenewalPopUp();
		uploadFile.verifyInternalPopUpToastMessage(internalPopUpText);
		uploadFile.clickOnYesbutton();
	}
}