package com.automation.pi.test002.uploadfilepage;

import java.lang.reflect.Method;

import org.testng.annotations.Test;

import com.automation.pi.library.BaseTest;
import com.automation.pi.pages.LoginPage;
import com.automation.pi.pages.UploadFilePage;

public class UploadFile67 extends BaseTest {

	//PIDC-TC-1110: Verify that based on the selected Office, Segmentation and Checking Type, Department is loaded in the Department/Program field.
	@Test
	public void PIDC_TC_1110(Method m) {

		webActionUtils.extentTestName(m.getName());
		webActionUtils.TestCaseinfo("================================");
		webActionUtils.TestCaseinfo(
				" Test Case Name : Verify that based on the selected Office, Segmentation and Checking Type, Department is loaded in the Department/Program field.");
		webActionUtils.TestCaseinfo("================================");

		String accountName = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 184, 1);
		String officeLocationName = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 184, 2);
		String segmentationName = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 184, 3);
		String checkingTypeName = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 184, 4);
		String departmentName = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 184, 5);

		LoginPage loginPage = new LoginPage(driver, webActionUtils);
		loginPage.enterUserName(USER_NAME);
		loginPage.enterPassword(PASSWORD);
		loginPage.clickOnSignin();
		
		UploadFilePage uploadFile = new UploadFilePage(driver, webActionUtils);
		uploadFile.clickOnUploadFile();
		uploadFile.clickOnPolicyInsightsAdvanceCheckbutton();
		uploadFile.enterRequiredAccount(accountName);
		uploadFile.selectRequiredOfficeLocationAvailable(officeLocationName);
		uploadFile.selectRequiredSegmentationAvailable(segmentationName);
		uploadFile.selectRequiredCheckingTypeAvailable(checkingTypeName);
		uploadFile.enterRequiredDepartment(departmentName);
		//message

	}
}