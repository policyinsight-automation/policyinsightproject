package com.automation.pi.test002.uploadfilepage;

import java.lang.reflect.Method;

import org.testng.annotations.Test;

import com.automation.pi.library.BaseTest;
import com.automation.pi.pages.LoginPage;
import com.automation.pi.pages.UploadFilePage;

public class UploadFile31 extends BaseTest {

	@Test
	public void PIDC_TC_1017(Method m){
		//PIDC-TC-1017: Verify that 'effective date ' not accepts invalid date and error toast message is displayed as
		//'Message: Please fill in Effective Date with right format m/d/yyyy.'

		webActionUtils.extentTestName(m.getName());
		webActionUtils.TestCaseinfo("================================");
		webActionUtils.TestCaseinfo(" Test Case Name : Verify Message: Please fill in Effective Date with right format m/d/yyyy");
		webActionUtils.TestCaseinfo("================================");
		
		String accountName = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 91, 1);
		String departmentName = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 91, 2);
		String namedInsured = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 91, 3);
		String lob = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 91, 4);
		String policyNumber = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 91, 5);
		String effDate = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 91, 6);
		String accountManager = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 91, 7);
		String addtional = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 91, 8);
		String premiumValue = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 91, 9);
		String policyStatusOpn = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 91, 10);
		String message = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 91, 11);
		String carrier= excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 91, 12);
		String uploadfilepath1 = DIR_PATH + "/TestDataFiles/5-Policy.pdf";
		String uploadfilepath2 = DIR_PATH + "/TestDataFiles/5-Proposal.pdf";
		String uploadfilepath3 = uploadfilepath1+"\n"+uploadfilepath2;
	
		LoginPage loginPage = new LoginPage(driver, webActionUtils);
		loginPage.enterUserName(USER_NAME);
		loginPage.enterPassword(PASSWORD);
		loginPage.clickOnSignin();
		UploadFilePage uploadFile = new UploadFilePage(driver, webActionUtils);
		uploadFile.clickOnUploadFile();
		uploadFile.clickOnPolicyInsightsExpressCheckRadiobutton();
		uploadFile.enterRequiredAccount(accountName);
		uploadFile.enterRequiredDepartment(departmentName);
		uploadFile.enterRequiredNamedInsured(namedInsured);
		uploadFile.enterRequiredLOB(lob);
		uploadFile.enterRequiredPolicyNumber(policyNumber);
		uploadFile.enterRequiredEffectiveDate(effDate);
		uploadFile.enterHighlightedDateFromClient();
		uploadFile.enterRequiredAccountManager(accountManager);
		uploadFile.enterRequiredAdditionalField(addtional);
		uploadFile.enterRequiredPolicyStatus(policyStatusOpn);
		uploadFile.enterRequiredPremiumRange(premiumValue);
		uploadFile.uploadRequiredFiles(uploadfilepath3);
		uploadFile.enterRequiredCarrier(carrier);
		uploadFile.clickonSubmitButton1();	
		uploadFile.verifyToastMessage(message);
	}
}