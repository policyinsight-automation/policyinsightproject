package com.automation.pi.test002.uploadfilepage;

import java.lang.reflect.Method;

import org.apache.commons.lang3.RandomStringUtils;
import org.testng.annotations.Test;

import com.automation.pi.library.BaseTest;
import com.automation.pi.pages.LoginPage;
import com.automation.pi.pages.UploadFilePage;

public class UploadFile42 extends BaseTest {// 23

	@Test
	public void PIDC_TC_1136(Method m) 
	{
		webActionUtils.extentTestName(m.getName());
		webActionUtils.TestCaseinfo("================================");
		webActionUtils.TestCaseinfo("Test Case Name : Verify that The Carrier field is a mandatory field for PI ExpressCheck task");
		webActionUtils.TestCaseinfo("================================");

		String accountName = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 112, 1);
		String departmentName = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 112, 2);
		String namedInsured = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 112, 3);
		String lob = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 112, 4);
		String policyNumber = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 112, 5);
		String effectiveDate = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 112, 6);
		String accountManager = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 112, 7);
		String policyStatusOpn = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 112, 8);
		String carrierText = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 112, 9);

		namedInsured = namedInsured + "test " + RandomStringUtils.randomAlphabetic(12);
		String uploadfilepath1 = DIR_PATH + "/TestDataFiles/5-Policy.pdf";
		String uploadfilepath2 = DIR_PATH + "/TestDataFiles/5-Proposal.pdf";
		String uploadfilepath3 = uploadfilepath1 + "\n" + uploadfilepath2;
		
		LoginPage loginPage = new LoginPage(driver, webActionUtils);
		loginPage.enterUserName(USER_NAME);
		loginPage.enterPassword(PASSWORD);
		loginPage.clickOnSignin();
		
		UploadFilePage uploadFile = new UploadFilePage(driver, webActionUtils);
		uploadFile.clickOnUploadFile();
		uploadFile.clickOnPolicyInsightsExpressCheckRadiobutton();
		uploadFile.enterRequiredAccount(accountName);
		uploadFile.enterRequiredDepartment(departmentName);
		uploadFile.enterRequiredNamedInsured(namedInsured);
		uploadFile.enterRequiredLOB(lob);
		uploadFile.enterRequiredPolicyNumber(policyNumber);
		uploadFile.enterRequiredEffectiveDate(effectiveDate);
		uploadFile.enterHighlightedDateFromClient();
		uploadFile.enterRequiredAccountManager(accountManager);
		uploadFile.enterRequiredPolicyStatus(policyStatusOpn);
		uploadFile.uploadRequiredFiles(uploadfilepath3);
		uploadFile.verifyCarriertextbox();
		uploadFile.clickonSubmitButton1();
		uploadFile.verifyToastMessage(carrierText);
	}
}
