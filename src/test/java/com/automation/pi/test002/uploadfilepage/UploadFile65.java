package com.automation.pi.test002.uploadfilepage;

import java.lang.reflect.Method;

import org.testng.annotations.Test;

import com.automation.pi.library.BaseTest;
import com.automation.pi.pages.LoginPage;
import com.automation.pi.pages.UploadFilePage;

public class UploadFile65 extends BaseTest {

	
	//PIDC-TC-1108: Verify that based on user selection of Segmentation, dropdown values of Office/Location, Checking Type and Department/Program fields will be narrowed down filtered based on Existing Department/Program.
	@Test
	public void PIDC_TC_1108(Method m) {

		webActionUtils.extentTestName(m.getName());
		webActionUtils.TestCaseinfo("================================");
		webActionUtils.TestCaseinfo(
				" Test Case Name : Verify that based on user selection of Segmentation, dropdown values of Office/Location, Checking Type and Department/Program fields will be narrowed down filtered based on Existing Department/Program.");
		webActionUtils.TestCaseinfo("================================");

		String accountName = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 181, 1);
		String officeLocationName = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 181, 2);
		String segmentationName = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 181, 3);
		String checkingTypeName = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 181, 4);
		String departmentName = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 181, 5);

		LoginPage loginPage = new LoginPage(driver, webActionUtils);
		loginPage.enterUserName(USER_NAME);
		loginPage.enterPassword(PASSWORD);
		loginPage.clickOnSignin();
		
		UploadFilePage uploadFile = new UploadFilePage(driver, webActionUtils);
		uploadFile.clickOnUploadFile();
		uploadFile.clickOnPolicyInsightsAdvanceCheckbutton();
		uploadFile.enterRequiredAccount(accountName);
		uploadFile.selectRequiredSegmentationAvailable(segmentationName);
		uploadFile.selectRequiredOfficeLocationAvailable(officeLocationName);
		uploadFile.selectRequiredCheckingTypeAvailable(checkingTypeName);
		uploadFile.enterRequiredDepartment(departmentName);
		//message

	}
}