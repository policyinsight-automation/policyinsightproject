package com.automation.pi.test002.uploadfilepage;

import java.lang.reflect.Method;

import org.apache.commons.lang3.RandomStringUtils;
import org.testng.annotations.Test;

import com.automation.pi.library.BaseTest;
import com.automation.pi.pages.LoginPage;
import com.automation.pi.pages.UploadFilePage;

public class UploadFile16 extends BaseTest {
	
	@Test
	public void PIDC_TC_172(Method m) {
		// Verify that when internal due date = client due date and internal due date >
		// current date, the New Internal Due Date will be equal to (Original Internal
		// Due Date - 1 day) in the Upload file pag
		webActionUtils.extentTestName(m.getName());
		webActionUtils.TestCaseinfo("================================");
		webActionUtils.TestCaseinfo(" Test Case Name : Verification of Internal Due Date "
				+ "when Internal Due Date and Client Due Date are Equal "
				+ "and Internal Due Date is Greater than Current Date ");
		webActionUtils.TestCaseinfo("================================");

		String accountName = "CBIZ";
		String departmentName = "Alpharetta, GA - Auto Renewal";
		String nameInsured = "Test" + RandomStringUtils.randomAlphanumeric(8);
		String lobValue = "PKG";
		String policyNumber = "1234" + RandomStringUtils.randomAlphanumeric(8);
		String dateformat = "09/11/2024";
		String dateFromClient = "12/31/2024";
		String internalDueDate = "11/17/2024";
		String policyStatusOpn = "New Business";

		LoginPage loginPage = new LoginPage(driver, webActionUtils);
		loginPage.enterUserName(USER_NAME);
		loginPage.enterPassword(PASSWORD);
		loginPage.clickOnSignin();

		UploadFilePage UploadFile = new UploadFilePage(driver, webActionUtils);
		UploadFile.clickOnUploadFile();
		UploadFile.uploadValuesForInternalDateCheck(accountName, departmentName, nameInsured, lobValue, policyNumber,
				dateformat, internalDueDate);
		UploadFile.checkInternalDateAndClientDueDate(internalDueDate, dateFromClient, policyStatusOpn);
	}
}