package com.automation.pi.test002.uploadfilepage;

import java.lang.reflect.Method;

import org.testng.annotations.Test;

import com.automation.pi.library.BaseTest;
import com.automation.pi.pages.LoginPage;
import com.automation.pi.pages.UploadFilePage;

public class UploadFile07 extends BaseTest 
{
	@Test
	public void PIDC_TC_954(Method m) 
	{
		webActionUtils.extentTestName(m.getName());
		webActionUtils.TestCaseinfo("================================");
		webActionUtils.TestCaseinfo(" Test Case Name : Verify that 'Type in new assistant account manager' and 'Type in assistant account manager email'  text field is populated  when user selects 'Other' option.");
		webActionUtils.TestCaseinfo("================================");


		String accountName = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 19, 1);
		String departmentName = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 19, 2);
		String namedInsured = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 19, 3);
		String lob = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 19, 4);
		String policyNumber = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 19, 5);
		String effectiveDate = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 19, 6);
		String accountManager = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 19, 7);
		String assitantAccountManager = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 19, 8);


		LoginPage loginPage = new LoginPage(driver, webActionUtils);
		loginPage.enterUserName(USER_NAME);
		loginPage.enterPassword(PASSWORD);
		loginPage.clickOnSignin();

		UploadFilePage uploadFile = new UploadFilePage(driver, webActionUtils);
		uploadFile.clickOnUploadFile();
		uploadFile.enterRequiredAccount(accountName);
		uploadFile.enterRequiredDepartment(departmentName);
		uploadFile.enterRequiredNamedInsured(namedInsured);
		uploadFile.enterRequiredLOB(lob);
		uploadFile.enterRequiredPolicyNumber(policyNumber);
		uploadFile.enterRequiredEffectiveDate(effectiveDate);
		uploadFile.enterHighlightedDateFromClient();
		uploadFile.enterRequiredAccountManager(accountManager);
		Boolean bool1 = uploadFile.verifyAAmCheckboxischeckedbyDeafultOrNot();
		uploadFile.checkNewAssitantAccountManagerCheckboxandClearDeafults(bool1);
		uploadFile.enterRequiredAssitantAccountManager(assitantAccountManager);
		uploadFile.verifyNewAssitantAccountManagerFieldSIsDisplayed();
	}
}