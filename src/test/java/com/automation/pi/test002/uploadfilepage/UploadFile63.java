package com.automation.pi.test002.uploadfilepage;

import java.lang.reflect.Method;

import org.testng.annotations.Test;

import com.automation.pi.library.BaseTest;
import com.automation.pi.pages.LoginPage;
import com.automation.pi.pages.UploadFilePage;

public class UploadFile63 extends BaseTest {

	//PIDC-TC-1106: Verify that when the user selects an account, - all the values Office, Segmentation and Checking Type associated are loaded in the dropdowns
	
	@Test
	public void PIDC_TC_1106(Method m) {

		webActionUtils.extentTestName(m.getName());
		webActionUtils.TestCaseinfo("================================");
		webActionUtils.TestCaseinfo(
				" Verify that when the user selects an account, - all the values Office, Segmentation and Checking Type associated are loaded in the dropdowns");
		webActionUtils.TestCaseinfo("================================");
	
		String accountName = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 175, 1);

		LoginPage loginPage = new LoginPage(driver, webActionUtils);
		loginPage.enterUserName(USER_NAME);
		loginPage.enterPassword(PASSWORD);
		loginPage.clickOnSignin();
		
		UploadFilePage uploadFile = new UploadFilePage(driver, webActionUtils);
		uploadFile.clickOnUploadFile();
		uploadFile.clickOnPolicyInsightsAdvanceCheckbutton();
		uploadFile.enterRequiredAccount(accountName);
		uploadFile.displayAllOfficeLocationAvailable();
		uploadFile.displayAllSegmentationAvailable();
		uploadFile.displayAllCheckingTypeAvailable();
	
	}
}