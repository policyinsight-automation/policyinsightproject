package com.automation.pi.test002.uploadfilepage;

import java.lang.reflect.Method;

import org.testng.annotations.Test;

import com.automation.pi.library.BaseTest;
import com.automation.pi.pages.LoginPage;
import com.automation.pi.pages.UploadFilePage;

public class UploadFile57 extends BaseTest {

	@Test
	public void PIDC_TC_665(Method m) {

		webActionUtils.extentTestName(m.getName());
		webActionUtils.TestCaseinfo("================================");
		webActionUtils.TestCaseinfo(
				" Test Case Name : Verify error message 'Message: Please enter valid email address for Assistant Account Manager' is displayed");
		webActionUtils.TestCaseinfo("================================");
		
		String accountName = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 157, 1);
		String departmentName = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 157, 2);
		String namedInsured = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 157, 3);
		String lob = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 157, 4);
		String policyNumber = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 157, 5);
		String effectiveDate = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 157, 6);
		String accountManager = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 157, 7);
		String assistentAccountManager = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 157, 8);
		String newAssistentAccountManager = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 157, 9);
		String newAssistentAccountManagerEmail  = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 157, 10);	
		String expectedText
  = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 157, 11);	

		LoginPage loginPage = new LoginPage(driver, webActionUtils);
		loginPage.enterUserName(USER_NAME);
		loginPage.enterPassword(PASSWORD);
		loginPage.clickOnSignin();
		
		UploadFilePage uploadFile = new UploadFilePage(driver, webActionUtils);
		uploadFile.clickOnUploadFile();
		uploadFile.clickOnPolicyInsightsAdvanceCheckbutton();
		uploadFile.enterRequiredAccount(accountName);
		uploadFile.enterRequiredDepartment(departmentName);
		uploadFile.enterRequiredNamedInsured(namedInsured);
		uploadFile.enterRequiredLOB(lob);
		uploadFile.enterRequiredPolicyNumber(policyNumber);
		uploadFile.enterRequiredEffectiveDate(effectiveDate);
		uploadFile.enterHighlightedDateFromClient();
		uploadFile.enterRequiredAccountManager(accountManager);
		uploadFile.checkNewAssitantAccountManagerCheckbox();
		uploadFile.enterRequiredAssitantAccountManager(assistentAccountManager);
		uploadFile.enterNewAssistentAccountManagerName(newAssistentAccountManager);
		uploadFile.enterNewAssistentAccountManagerEmail(newAssistentAccountManagerEmail);
		uploadFile.clickonSubmitButton();
		uploadFile.verifyToastMessage1(expectedText
);
	}
}
