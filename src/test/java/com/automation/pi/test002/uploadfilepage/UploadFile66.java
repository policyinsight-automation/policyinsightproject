package com.automation.pi.test002.uploadfilepage;

import java.lang.reflect.Method;

import org.testng.annotations.Test;

import com.automation.pi.library.BaseTest;
import com.automation.pi.pages.LoginPage;
import com.automation.pi.pages.UploadFilePage;

public class UploadFile66 extends BaseTest {

	//PIDC-TC-1109: Verify that based on user selection of Checking Type, dropdown values of Office/Location, Segmentation and Department/Program fields will be narrowed down filtered based on Existing Department/Program.
	
	
	
	@Test
	public void PIDC_TC_1109(Method m) {

		webActionUtils.extentTestName(m.getName());
		webActionUtils.TestCaseinfo("================================");
		webActionUtils.TestCaseinfo(
				" Test Case Name : Verify that based on user selection of Checking Type, dropdown values of Office/Location, Segmentation and Department/Program fields will be narrowed down filtered based on Existing Department/Program.");
		webActionUtils.TestCaseinfo("================================");

		String accountName = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 184, 1);
		String officeLocationName = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 184, 2);
		String segmentationName = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 184, 3);
		String checkingTypeName = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 184, 4);
		String departmentName = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 184, 5);

		LoginPage loginPage = new LoginPage(driver, webActionUtils);
		loginPage.enterUserName(USER_NAME);
		loginPage.enterPassword(PASSWORD);
		loginPage.clickOnSignin();
		
		UploadFilePage uploadFile = new UploadFilePage(driver, webActionUtils);
		uploadFile.clickOnUploadFile();
		uploadFile.clickOnPolicyInsightsAdvanceCheckbutton();
		uploadFile.enterRequiredAccount(accountName);
		uploadFile.selectRequiredCheckingTypeAvailable(checkingTypeName);
		uploadFile.selectRequiredOfficeLocationAvailable(officeLocationName);
		uploadFile.selectRequiredSegmentationAvailable(segmentationName);
		uploadFile.enterRequiredDepartment(departmentName);
		//message

	}
}