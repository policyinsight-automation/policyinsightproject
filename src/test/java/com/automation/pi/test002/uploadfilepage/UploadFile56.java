package com.automation.pi.test002.uploadfilepage;

import java.lang.reflect.Method;

import org.testng.annotations.Test;

import com.automation.pi.library.BaseTest;
import com.automation.pi.pages.LoginPage;
import com.automation.pi.pages.UploadFilePage;

public class UploadFile56 extends BaseTest {

	@Test
	public void PIDC_TC_664(Method m) {

		webActionUtils.extentTestName(m.getName());
		webActionUtils.TestCaseinfo("================================");
		webActionUtils.TestCaseinfo(
				" Test Case Name : Verify error ' Message: Please enter valid email address for Account Manager is displayed");
		webActionUtils.TestCaseinfo("================================");
	
		String accountName = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 154, 1);
		String departmentName = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 154, 2);
		String namedInsured = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 154, 3);
		String lob = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 154, 4);
		String policyNumber = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 154, 5);
		String effectiveDate = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 154, 6);
		String accountManager = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 154, 7);
		String newAccountManager = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 154, 8);
		String newAccountManagerEmail = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 154, 9);
		String expectedText  = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 154, 10);	
		
		LoginPage loginPage = new LoginPage(driver, webActionUtils);
		loginPage.enterUserName(USER_NAME);
		loginPage.enterPassword(PASSWORD);
		loginPage.clickOnSignin();
		UploadFilePage uploadFile = new UploadFilePage(driver, webActionUtils);
		uploadFile.clickOnUploadFile();
		uploadFile.clickOnPolicyInsightsAdvanceCheckbutton();
		uploadFile.enterRequiredAccount(accountName);
		uploadFile.enterRequiredDepartment(departmentName);
		uploadFile.enterRequiredNamedInsured(namedInsured);
		uploadFile.enterRequiredLOB(lob);
		uploadFile.enterRequiredPolicyNumber(policyNumber);
		uploadFile.enterRequiredEffectiveDate(effectiveDate);
		uploadFile.enterHighlightedDateFromClient();
		uploadFile.enterRequiredAccountManager(accountManager);
		uploadFile.verifyNewAccountManagerFieldIsDisplayed();
		uploadFile.enterNewAccountManagerName(newAccountManager);
		uploadFile.enterNewAccountManagerEmail(newAccountManagerEmail);
		uploadFile.clickonSubmitButton();
		uploadFile.verifyToastMessage1(expectedText);
	}
}
