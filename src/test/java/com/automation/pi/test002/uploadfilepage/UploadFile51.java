package com.automation.pi.test002.uploadfilepage;

import java.lang.reflect.Method;

import org.testng.annotations.Test;

import com.automation.pi.library.BaseTest;
import com.automation.pi.pages.LoginPage;
import com.automation.pi.pages.UploadFilePage;

public class UploadFile51 extends BaseTest {

	@Test
	public void PIDC_TC_245(Method m) {
		// Verify that error message displayed as "There must be a document called
		// 'Policy' . Please check" when user selects a Expiring Policy source type.
		webActionUtils.extentTestName(m.getName());
		webActionUtils.TestCaseinfo("================================");
		webActionUtils.TestCaseinfo(
				" Test Case Name : Error message displayed as 'There must be a document called 'Policy' . Please check'");
		webActionUtils.TestCaseinfo("================================");

		String accountName = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 139, 1);
		String departmentName = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 139, 2);
		String namedInsured = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 139, 3);
		String lob = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 139, 4);
		String policyNumber = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 139, 5);
		String effectiveDate = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 139, 6);
		String accountManager = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 139, 7);
		String addtional = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 139, 8);
		String policyStatusOpn = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 139, 9);
		String premiumValue = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 139, 10);
		String carrier  = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 139, 11);
		String expectedText  = excelLibrary.getExcelData(XL_TESTPATH, "UploadFileTC", 139, 12);
			
		String uploadfilepath1 = DIR_PATH + "/TestDataFiles/5-Proposal.pdf";
		String uploadfilepath2 = DIR_PATH + "/TestDataFiles/Binder.pdf";
		String uploadfilepath3 = uploadfilepath1 + "\n" + uploadfilepath2;		
		
		LoginPage loginPage = new LoginPage(driver, webActionUtils);
		loginPage.enterUserName(USER_NAME);
		loginPage.enterPassword(PASSWORD);
		loginPage.clickOnSignin();
		UploadFilePage uploadFile = new UploadFilePage(driver, webActionUtils);
		uploadFile.clickOnUploadFile();
		uploadFile.clickOnPolicyInsightsExpressCheckRadiobutton();
		uploadFile.enterRequiredAccount(accountName);
		uploadFile.enterRequiredDepartment(departmentName);
		uploadFile.enterRequiredNamedInsured(namedInsured);
		uploadFile.enterRequiredLOB(lob);
		uploadFile.enterRequiredPolicyNumber(policyNumber);
		uploadFile.enterRequiredEffectiveDate(effectiveDate);
		uploadFile.enterHighlightedDateFromClient();
		uploadFile.enterRequiredAccountManager(accountManager);
		uploadFile.enterRequiredAdditionalField(addtional);
		uploadFile.enterRequiredPolicyStatus(policyStatusOpn);
		uploadFile.enterRequiredPremiumRange(premiumValue);
		uploadFile.uploadRequiredFiles(uploadfilepath3);
		uploadFile.enterRequiredCarrier(carrier);
		uploadFile.clickonSubmitButton();
		uploadFile.getMessageandVerify(expectedText);
	}
}
