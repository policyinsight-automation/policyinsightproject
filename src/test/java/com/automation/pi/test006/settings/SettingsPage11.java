package com.automation.pi.test006.settings;

import java.lang.reflect.Method;

import org.testng.annotations.Test;

import com.automation.pi.library.BaseTest;
import com.automation.pi.pages.LoginPage;
import com.automation.pi.pages.Settings;

public class SettingsPage11 extends BaseTest
{//785
	@Test
	public void PPR_7327_Test(Method m) 
	{

		webActionUtils.extentTestName(m.getName());
		webActionUtils.TestCaseinfo("================================");
		webActionUtils.TestCaseinfo(" Test Case Name : Department Settings- View sub settings pages");
		webActionUtils.TestCaseinfo("================================");

		String DepartmentSetting = "ManageDepartment - Policy Checking Platform";
		String DepartmentMapping = "Manage Department Mapping - Policy Checking Platform";
		String ParameterSetting = "Parameter Setting - Policy Checking Platform";
		String DepartmentSegmentation = "Department Segmentation - Policy Checking Platform";	

		
		LoginPage loginPage = new LoginPage(driver, webActionUtils);
		loginPage.enterUserName(USER_NAME);
		loginPage.enterPassword(PASSWORD);
		loginPage.clickOnSignin();

		Settings settings = new Settings(driver, webActionUtils);

		// DepartmentSetting
		settings.clickSettingOption();
		settings.hoverOnDepartmentSettingsSubMenu();
		settings.clickonDepartmentSettingsOption();
		settings.verifyDepartmentSettingPage(DepartmentSetting);
		settings.verifyDepartmentSettingPageElement();
	//	webActionUtils.refreshPage();

		// DepartmentMapping
		settings.clickSettingOption();
		settings.hoverOnDepartmentSettingsSubMenu();
		settings.clickonDepartmentMappingSettingsOption();
		settings.verifyDepartmentMappingSettingPage(DepartmentMapping);
		settings.verifyDepartmentMappingSettingPageElement();
		//webActionUtils.refreshPage();

		// Parameter Setting
		settings.clickSettingOption();
		settings.hoverOnDepartmentSettingsSubMenu();
		settings.clickonDepartmentParameterSettingOption();
		settings.verifyParameterSettingPage(ParameterSetting);
		settings.verifyParameterSettingPageElement();
		//webActionUtils.refreshPage();
		
		
		// Department Segmentation 
		settings.clickSettingOption();
		settings.hoverOnDepartmentSettingsSubMenu();
		settings.clickonDepartmentDepartmentSegmentationOption();
		settings.verifyDepartmentSegmentationPage(DepartmentSegmentation);
		settings.verifyDepartmentSegmentationSettingPageElement();
		//webActionUtils.refreshPage();
		
	}
}