
package com.automation.pi.test006.settings;

import java.lang.reflect.Method;

import org.testng.annotations.Test;

import com.automation.pi.library.BaseTest;
import com.automation.pi.pages.LoginPage;
import com.automation.pi.pages.Settings;

public class SettingsPage22 extends BaseTest
{//28
	@Test
	public void PPR_8250_Test(Method m)
	{
		webActionUtils.extentTestName(m.getName());
		webActionUtils.TestCaseinfo("================================");
		webActionUtils.TestCaseinfo(" Test Case Name : Advanced Search Communication Setting - Add new record ");
		webActionUtils.TestCaseinfo("================================");
		String CommunicationSetting = "ManageCommunicationSettings - Policy Checking Platform";

		LoginPage loginPage = new LoginPage(driver, webActionUtils);
		loginPage.enterUserName(USER_NAME);
		loginPage.enterPassword(PASSWORD);
		loginPage.clickOnSignin();

		Settings settings = new Settings(driver, webActionUtils);
		// Communication Setting
		settings.clickSettingOption();
		settings.hoverOnAdvancedSettingsSubMenu();
		settings.clickCommunicationSettingOption();
		settings.verifyCommunicationSettingPage(CommunicationSetting);
		settings.verifyCommunicationSettingPageElement();
		settings.clickOnAddbutton();
		settings.addNewRecordINCommunicationSetting();
	}
}