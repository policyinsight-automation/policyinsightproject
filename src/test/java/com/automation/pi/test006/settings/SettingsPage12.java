package com.automation.pi.test006.settings;

import java.lang.reflect.Method;

import org.testng.annotations.Test;

import com.automation.pi.library.BaseTest;
import com.automation.pi.pages.LoginPage;
import com.automation.pi.pages.Settings;

public class SettingsPage12 extends BaseTest {// 05
	@Test
	public void PPR_7336_Test(Method m) {
		webActionUtils.extentTestName(m.getName());
		webActionUtils.TestCaseinfo("================================");
		webActionUtils.TestCaseinfo(" Test Case Name : Checklist Template Settings -View sub settings pages");
		webActionUtils.TestCaseinfo("================================");

		String LOBSetting = "LOBSetting - Policy Checking Platform";
		String CheckpointSetting = "Manage Checkpoint Details - Policy Checking Platform";
		String AMCustomiasedCheckpointSetting = "AM Customize Checkpoint - Policy Checking Platform";
		String SectionLOBSetting = "ManageSectionLOB - Policy Checking Platform";
		String SourceSetting = "Manage Source Details - Policy Checking Platform";
		String SectionSetting = "Manage Section - Policy Checking Platform";
		String TemplateManager = "Template Master - Policy Checking Platform";
		String ConfigureAMSection = "Configure AM Section - Policy Checking Platform";
		String FormSectionSetting = "Manage Form Section - Policy Checking Platform";
		String FormCheckpointSetting = "Manage Form Checkpoint Details - Policy Checking Platform";
		String FormSettingonUploadFilePage = "Manage Team Account - Policy Checking Platform";
		String SourceAlternativeNameSetting = "Manage Source AlternativeName - Policy Checking Platform";
		String DefaultTemplateSections = "ManageDefaultTemplateSections - Policy Checking Platform";
		String TemplateDefaultValues = "Manage Default Values - Policy Checking Platform";
		String AMAlternativeNameSetting = "Manage Alternative Name for AM on Checklist - Policy Checking Platform";
		String ChecklistHeaderSetting = "Manage Checklist Header Details - Policy Checking Platform";

		LoginPage loginPage = new LoginPage(driver, webActionUtils);
		loginPage.enterUserName(USER_NAME);
		loginPage.enterPassword(PASSWORD);
		loginPage.clickOnSignin();

		Settings settings = new Settings(driver, webActionUtils);

		// ChecklistTemplateSettings
		settings.clickSettingOption();
		settings.hoverOnChecklistTemplateSettingsSubMenu();
		settings.verifyChecklistTemplateSettingsOptionsList();
		
		// Lob
//		settings.clickSettingOption();
//		settings.hoverOnChecklistTemplateSettingsSubMenu();
		settings.clickLOBSettingOption();
		settings.verifyLOBSettingPage(LOBSetting);
		settings.verifyLOBSettingPageElement();
		settings.verifyStandardLOBPageElement();
		settings.verifyAccountSpecifiedLOBPageElement();
		settings.verifyPolicyStatusTablePageElement();
		webActionUtils.refreshPage();

		// Checkpoint Setting
		settings.clickSettingOption();
		settings.hoverOnChecklistTemplateSettingsSubMenu();
		settings.clickCheckpointSettingOption();
		settings.verifyCheckpointSettingPage(CheckpointSetting);
		settings.verifyCheckpointSettingPageElement();
		webActionUtils.refreshPage();

		// AM Customized Checkpoint
		settings.clickSettingOption();
		settings.hoverOnChecklistTemplateSettingsSubMenu();
		settings.clickAMCustomizedCheckpointOption();
		settings.verifyAMCustomizedCheckpointSettingPage(AMCustomiasedCheckpointSetting);
		settings.verifyAMCustomizedCheckpointSettingPageElement();
		webActionUtils.refreshPage();

		// Section LOB Setting
		settings.clickSettingOption();
		settings.hoverOnChecklistTemplateSettingsSubMenu();
		settings.clickSectionLOBSettingOption();
		settings.verifySectionLOBSettingPage(SectionLOBSetting);
		settings.verifySectionLOBSettingPageElement();
		webActionUtils.refreshPage();

		// Source Setting
		settings.clickSettingOption();
		settings.hoverOnChecklistTemplateSettingsSubMenu();
		settings.clickSourceSettingOption();
		settings.verifySourceSettingPage(SourceSetting);
		settings.verifySourceSettingPageElement();
		webActionUtils.refreshPage();

		// Section Setting
		settings.clickSettingOption();
		settings.hoverOnChecklistTemplateSettingsSubMenu();
		settings.clickSectionSettingOption();
		settings.verifySectionSettingPage(SectionSetting);
		settings.verifySectionSettingPageElement();
		webActionUtils.refreshPage();

		// Template Manager Setting
		settings.clickSettingOption();
		settings.hoverOnChecklistTemplateSettingsSubMenu();
		settings.clickTemplateManagerOption();
		settings.verifyTemplateManagerSettingPage(TemplateManager);
		settings.verifyTemplateManagerSettingPageElement();
		webActionUtils.refreshPage();

		// Configure AM Section
		settings.clickSettingOption();
		settings.hoverOnChecklistTemplateSettingsSubMenu();
		settings.clickConfigureAMSectionOption();
		settings.verifyConfigureAMSectionSettingPage(ConfigureAMSection);
		settings.verifyConfigureAMSectionSettingPageElement();
		webActionUtils.refreshPage();

		// Form Section Setting
		settings.clickSettingOption();
		settings.hoverOnChecklistTemplateSettingsSubMenu();
		settings.clickFormSectionSettingOption();
		settings.verifyFormSectionSettingPage(FormSectionSetting);
		settings.verifyFormSectionSettingPageElement();
		webActionUtils.refreshPage();

		// Form Checkpoint Setting
		settings.clickSettingOption();
		settings.hoverOnChecklistTemplateSettingsSubMenu();
		settings.clickFormCheckpointSettingOption();
		settings.verifyFormCheckpointSettingPage(FormCheckpointSetting);
		settings.verifyFormCheckpointSettingPageElement();
		webActionUtils.refreshPage();

		// Form Setting on Upload File Page
		settings.clickSettingOption();
		settings.hoverOnChecklistTemplateSettingsSubMenu();
		settings.clickFormSettingonUploadFilePageOption();
		settings.verifyFormSettingUploadFileSettingPage(FormSettingonUploadFilePage);
		settings.verifyFormSettingUploadFileSettingPageElement();
		webActionUtils.refreshPage();

		// Source Alternative Name Setting
		settings.clickSettingOption();
		settings.hoverOnChecklistTemplateSettingsSubMenu();
		settings.clickSourceAlternativeNameSettingOption();
		settings.verifySourceAlternativeNameSettingPage(SourceAlternativeNameSetting);
		settings.verifySourceAlternativeNameSettingPageElement();
		webActionUtils.refreshPage();

		// Default Template Section Setting
		settings.clickSettingOption();
		settings.hoverOnChecklistTemplateSettingsSubMenu();
		settings.clickDefaultTemplateSectionsOption();
		settings.verifyDefaultTemplateSectionsSettingPage(DefaultTemplateSections);
		settings.verifyDefaultTemplateSectionSettingPageElement();
		webActionUtils.refreshPage();

		// Template Default Values Setting
		settings.clickSettingOption();
		settings.hoverOnChecklistTemplateSettingsSubMenu();
		settings.clickTemplateDefaultValuesOption();
		settings.verifyTemplateDefaultValuesSettingPage(TemplateDefaultValues);
		settings.verifyTemplateDefaultValuesSettingPageElement();

		// AM Alternative Name Setting
		settings.clickSettingOption();
		settings.hoverOnChecklistTemplateSettingsSubMenu();
		settings.clickAMAlternativeNameSettingOption();
		settings.verifyAMAlternativeNameSettingPage(AMAlternativeNameSetting);
		settings.verifyAMAlternativeNameSettingPageElement();
		webActionUtils.refreshPage();

		// Checklist Header Setting
		settings.clickSettingOption();
		settings.hoverOnChecklistTemplateSettingsSubMenu();
		settings.clickChecklistHeaderSettingOption();
		settings.verifyChecklistHeaderSettingPage(ChecklistHeaderSetting);
		settings.verifyChecklistHeaderSettingPageElement();
		webActionUtils.refreshPage();

	}
}
