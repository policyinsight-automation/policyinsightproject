
package com.automation.pi.test006.settings;

import java.lang.reflect.Method;

import org.testng.annotations.Test;

import com.automation.pi.library.BaseTest;
import com.automation.pi.pages.LoginPage;
import com.automation.pi.pages.Settings;

public class SettingsPage21 extends BaseTest 
{//27
	@Test
	public void PPR_8249_Test(Method m)
	{

		webActionUtils.extentTestName(m.getName());
		webActionUtils.TestCaseinfo("================================");
		webActionUtils.TestCaseinfo(" Test Case Name : Advanced Search Checking Point Setting - Add new record ");
		webActionUtils.TestCaseinfo("================================");
		String AdvancedSearch = "Advanced Search - Policy Checking Platform";
		
		LoginPage loginPage = new LoginPage(driver, webActionUtils);
		loginPage.enterUserName(USER_NAME);
		loginPage.enterPassword(PASSWORD);
		loginPage.clickOnSignin();

		Settings settings = new Settings(driver, webActionUtils);

		// Advanced Search Checking Point Setting
		settings.clickSettingOption();
		settings.hoverOnAdvancedSettingsSubMenu();
		settings.clickAdvancedSearchOption();
		settings.verifyAdvancedSearchSettingPage(AdvancedSearch);
		settings.verifyAdvancedSearchCheckingPointSettingPageElement();
		settings.clickOnAddbutton();
		settings.addNewRecordINAdvancedSearchCheckingPointSetting();
	}
}