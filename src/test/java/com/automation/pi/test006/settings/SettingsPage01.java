package com.automation.pi.test006.settings;

import java.lang.reflect.Method;

import org.testng.annotations.Test;

import com.automation.pi.library.BaseTest;
import com.automation.pi.pages.LoginPage;
import com.automation.pi.pages.Settings;

public class SettingsPage01  extends BaseTest
{//13
		@Test
		public void PPR_7345_Test(Method m)
		{
			webActionUtils.extentTestName(m.getName());
			webActionUtils.TestCaseinfo("================================");
			webActionUtils.TestCaseinfo(" Test Case Name : Settings - Order of Settings page ");
			webActionUtils.TestCaseinfo("================================");

			String DepartmentSettings = "Department Settings";
			String ChecklistTemplateSettings ="Checklist Template Settings";
			String AdvancedSettings= "Advanced Settings";
			String AccountManagerSetting = "Account Manager Setting";
			String AdhocPIRequestSetting = "Ad-hoc PI Request Setting";
			String DAISySettings = "DAISy Settings";
			String AMPSettings = "AMP Settings";
			String Refresh = "Refresh";
			String IRPackageStatus ="IR Package Status";
						
			LoginPage loginPage = new LoginPage(driver, webActionUtils);
			loginPage.enterUserName(USER_NAME);
			loginPage.enterPassword(PASSWORD);
			loginPage.clickOnSignin();

			Settings settings = new Settings(driver, webActionUtils);
			settings.clickSettingOption();
			settings.verifySettingPageListinGivenOrder(DepartmentSettings,ChecklistTemplateSettings,AdvancedSettings,AccountManagerSetting,AdhocPIRequestSetting,DAISySettings,AMPSettings,Refresh,IRPackageStatus);
		}
	}