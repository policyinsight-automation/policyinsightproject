
package com.automation.pi.test006.settings;

import java.lang.reflect.Method;

import org.testng.annotations.Test;

import com.automation.pi.library.BaseTest;
import com.automation.pi.pages.LoginPage;
import com.automation.pi.pages.Settings;

public class SettingsPage25 extends BaseTest 
{//31
	@Test
	public void PPR_8253_Test(Method m)
	{
		webActionUtils.extentTestName(m.getName());
		webActionUtils.TestCaseinfo("================================");
		webActionUtils.TestCaseinfo(" Test Case Name : DAISy Settings -Add new record");
		webActionUtils.TestCaseinfo("================================");

		String ConfigureAccountForDAISySetting = "Configure Account For DAISy - Policy Checking Platform";

		LoginPage loginPage = new LoginPage(driver, webActionUtils);
		loginPage.enterUserName(USER_NAME);
		loginPage.enterPassword(PASSWORD);
		loginPage.clickOnSignin();

		Settings settings = new Settings(driver, webActionUtils);

		// DAISy Settings
		settings.clickSettingOption();
		settings.hoverOnDAISySettingSubMenu();
		settings.verifyDAISySettingsOptionsList();
		settings.clickConfigureAccountForDAISySettingOption();
		settings.verifyConfigureAccountForDAISySettingPage(ConfigureAccountForDAISySetting);
		settings.clickOnAddbutton();
		settings.addNewRecordINDaisy();
	}
}