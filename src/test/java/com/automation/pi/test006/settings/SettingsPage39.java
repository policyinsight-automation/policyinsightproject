package com.automation.pi.test006.settings;

import java.lang.reflect.Method;

import org.testng.annotations.Test;

import com.automation.pi.library.BaseTest;
import com.automation.pi.pages.LoginPage;
import com.automation.pi.pages.Settings;

public class SettingsPage39 extends BaseTest {
	@Test
	public void PIDC_TC_2074_Test(Method m) {

		webActionUtils.extentTestName(m.getName());
		webActionUtils.TestCaseinfo("================================");
		webActionUtils.TestCaseinfo(
				" Test Case Name : Verify that  Policy Insights - AdvanceCheck and Policy Insights - ExpressCheck options are created in the 'Service type' dropdown.");
		webActionUtils.TestCaseinfo("================================");

		String DepartmentSetting = "ManageDepartment - Policy Checking Platform";
		String PIExpressCheck = "Policy Insights - ExpressCheck";
		String PIAdvanceCheck= "Policy Insights - AdvanceCheck";

		LoginPage loginPage = new LoginPage(driver, webActionUtils);
		loginPage.enterUserName(USER_NAME);
		loginPage.enterPassword(PASSWORD);
		loginPage.clickOnSignin();

		Settings settings = new Settings(driver, webActionUtils);

		// DepartmentSetting
		settings.clickSettingOption();
		settings.hoverOnDepartmentSettingsSubMenu();
		settings.clickonDepartmentSettingsOption();
		settings.verifyDepartmentSettingPage(DepartmentSetting);
		settings.verifyDepartmentSettingPageElement();
		settings.clickOnAddbuttonDepartmentSettingsPage();
		settings.verifyServicetypeOptionsAvailable(PIAdvanceCheck,PIExpressCheck);

	}
}