package com.automation.pi.test006.settings;

import java.lang.reflect.Method;

import org.testng.annotations.Test;

import com.automation.pi.library.BaseTest;
import com.automation.pi.pages.LoginPage;
import com.automation.pi.pages.Settings;

public class SettingsPage17 extends BaseTest 
{//10
	@Test
	public void PPR_7342_Test(Method m)
	{
		webActionUtils.extentTestName(m.getName());
		webActionUtils.TestCaseinfo("================================");
		webActionUtils.TestCaseinfo(" Test Case Name :AMP Setting - View sub settings pages");
		webActionUtils.TestCaseinfo("================================");
	
		String ConfigureNewClientActionsForAMP = "Configure New Client Actions for AMP Setting - Policy Checking Platform";
		String AMPSeverityIndicator	= "AMP Severity Indicator - Policy Checking Platform";
		
		LoginPage loginPage = new LoginPage(driver, webActionUtils);
		loginPage.enterUserName(USER_NAME);
		loginPage.enterPassword(PASSWORD);
		loginPage.clickOnSignin();

		Settings settings = new Settings(driver, webActionUtils);
	
		// ConfigureNewClientActionsForAMP
		settings.clickSettingOption();
		settings.hoverOnAMPSettingsSubMenu();
		settings.verifyAMPSettingsOptionsList();	
		settings.clickConfigureNewClientActionsForAMPOption();
		settings.verifyConfigureNewClientActionsForAMPSettingPage(ConfigureNewClientActionsForAMP);
		settings.verifyConfigureNewClientActionsforAMPSettingPageElement();		
		webActionUtils.refreshPage();
			
		settings.clickSettingOption();
		settings.hoverOnAMPSettingsSubMenu();
		settings.verifyAMPSettingsOptionsList();	
		settings.clickAMPSeverityIndicatorOption();
		settings.verifyAMPSeverityIndicatorPage(AMPSeverityIndicator);
		settings.verifyAMPSeverityIndicatorPageElement();		
		webActionUtils.refreshPage();
	}
}