
package com.automation.pi.test006.settings;

import java.lang.reflect.Method;

import org.testng.annotations.Test;

import com.automation.pi.library.BaseTest;
import com.automation.pi.pages.LoginPage;
import com.automation.pi.pages.Settings;

public class SettingsPage24 extends BaseTest 
{//30
	@Test
	public void PPR_8252_Test(Method m) 
	{
		webActionUtils.extentTestName(m.getName());
		webActionUtils.TestCaseinfo("================================");
		webActionUtils.TestCaseinfo(" Test Case Name : Add new record to the Manage Department/Office Code Setting page  ");
		webActionUtils.TestCaseinfo("================================");
				
		LoginPage loginPage = new LoginPage(driver, webActionUtils);
		loginPage.enterUserName(USER_NAME);
		loginPage.enterPassword(PASSWORD);
		loginPage.clickOnSignin();

		Settings settings = new Settings(driver, webActionUtils);
		String DepartmentOfficeCodeSetting = "Manage Department/Office Code Setting - Policy Checking Platform";
		// DepartmentOfficeCodeSetting
		settings.clickSettingOption();
		settings.hoverOnAdvancedSettingsSubMenu();
		settings.clickDepartmentOfficeCodeSettingOption();
		settings.verifyDepartmentOfficeCodeSettingPage(DepartmentOfficeCodeSetting);
		settings.clickOnAddbutton();
		settings.addNewRecordINManageDepartment_OfficeCodeSetting();
	}
}