package com.automation.pi.test006.settings;

import java.lang.reflect.Method;

import org.testng.annotations.Test;

import com.automation.pi.library.BaseTest;
import com.automation.pi.pages.LoginPage;
import com.automation.pi.pages.Settings;

public class SettingsPage18 extends BaseTest 
{//11
	@Test
	public void PPR_7343_Test(Method m) 
	{
		webActionUtils.extentTestName(m.getName());
		webActionUtils.TestCaseinfo("================================");
		webActionUtils.TestCaseinfo(" Test Case Name : Refresh Settings -View sub settings pages");
		webActionUtils.TestCaseinfo("================================");
		String Refresh = "Refresh - Policy Checking Platform";
		
		LoginPage loginPage = new LoginPage(driver, webActionUtils);
		loginPage.enterUserName(USER_NAME);
		loginPage.enterPassword(PASSWORD);
		loginPage.clickOnSignin();

		Settings settings = new Settings(driver, webActionUtils);

		//  Refresh Settings
		settings.clickSettingOption();
		settings.hoverOnRefreshSubMenu();
		settings.verifyRefreshSettingsOptionsList();
		settings.clickRefreshOption();
		settings.verifyRefreshSettingPage(Refresh);
		settings.verifyRefreshSettingPageElement();
		webActionUtils.refreshPage();
	}
}