
package com.automation.pi.test006.settings;

import java.lang.reflect.Method;

import org.testng.annotations.Test;

import com.automation.pi.library.BaseTest;
import com.automation.pi.pages.LoginPage;
import com.automation.pi.pages.Settings;

public class SettingsPage27 extends BaseTest 
{//33
	@Test
	public void PPR_8255_Test(Method m)
	{
		webActionUtils.extentTestName(m.getName());
		webActionUtils.TestCaseinfo("================================");
		webActionUtils.TestCaseinfo(" Test Case Name : Refresh Settings -Error Message Check");
		webActionUtils.TestCaseinfo("================================");
		
		String Refresh = "Refresh - Policy Checking Platform";
		String text11="Message: AMP Refresh is initiated. Please check the refreshed data in sometime.";
		String text12 = "Message: AMP Refreshed Successfully";
		String text13= "Message: User cannot Refresh AMP Cache during US Business Hours, which is at 6:30 PM to 7 AM CST or 4 PM to 4:30 AM IST";

		String text2= "Message: AMP Demo Refreshed Successfully";
		String text3 = "Message: PI Platform Refreshed Successfully";
		
		LoginPage loginPage = new LoginPage(driver, webActionUtils);
		loginPage.enterUserName(USER_NAME);
		loginPage.enterPassword(PASSWORD);
		loginPage.clickOnSignin();

		Settings settings = new Settings(driver, webActionUtils);

		//  Refresh Settings
		settings.clickSettingOption();
		settings.hoverOnRefreshSubMenu();
		settings.verifyRefreshSettingsOptionsList();
		
		settings.clickRefreshOption();
		settings.verifyRefreshSettingPage(Refresh);
		
		settings.clickonAMPRefreshButton();
		settings.verifyAMPRefreshErrorMessage(text11,text12,text13);
		
		settings.clickonPIPlatformRefreshButton();
		settings.verifyPIPlatformRefreshErrorMessage(text3);	
		
		settings.clickonAMPDemoRefreshButton();
		settings.verifyAMPDemoRefreshErrorMessage(text2);

	}
}