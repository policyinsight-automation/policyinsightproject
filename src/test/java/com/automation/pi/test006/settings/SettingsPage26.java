
package com.automation.pi.test006.settings;

import java.lang.reflect.Method;

import org.testng.annotations.Test;

import com.automation.pi.library.BaseTest;
import com.automation.pi.pages.LoginPage;
import com.automation.pi.pages.Settings;

public class SettingsPage26 extends BaseTest 
{//32
	@Test
	public void PPR_8254_Test(Method m)
	{
		webActionUtils.extentTestName(m.getName());
		webActionUtils.TestCaseinfo("================================");
		webActionUtils.TestCaseinfo(" Test Case Name : Upload file alternative Settings.");
		webActionUtils.TestCaseinfo("================================");
			
		String UploadFileAlternativeSetting = "Manage Upload File Alternative Setting - Policy Checking Platform";
		
		LoginPage loginPage = new LoginPage(driver, webActionUtils);
		loginPage.enterUserName(USER_NAME);
		loginPage.enterPassword(PASSWORD);
		loginPage.clickOnSignin();

		Settings settings = new Settings(driver, webActionUtils);
	
		// UploadFileAlternativeSetting
		settings.clickSettingOption();
		settings.hoverOnAdvancedSettingsSubMenu();
		settings.clickUploadFileAlternativeSettingOption();
		settings.verifyUploadFileAlternativeSettingPage(UploadFileAlternativeSetting);
		settings.clickOnAddbutton();
		settings.addNewRecordInUploadFileAlternativeSetting();
	}
}