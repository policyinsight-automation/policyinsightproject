package com.automation.pi.test006.settings;

import java.lang.reflect.Method;

import org.testng.annotations.Test;

import com.automation.pi.library.BaseTest;
import com.automation.pi.pages.LoginPage;
import com.automation.pi.pages.Settings;

public class SettingsPage37 extends BaseTest {// 785
	@Test
	public void PIDC_TC_2072_Test(Method m) {

		webActionUtils.extentTestName(m.getName());
		webActionUtils.TestCaseinfo("================================");
		webActionUtils.TestCaseinfo(" Test Case Name : Department Settings- View sub settings pages");
		webActionUtils.TestCaseinfo("================================");

		String DepartmentSegmentation = "Department Segmentation - Policy Checking Platform";

		LoginPage loginPage = new LoginPage(driver, webActionUtils);
		loginPage.enterUserName(USER_NAME);
		loginPage.enterPassword(PASSWORD);
		loginPage.clickOnSignin();

		Settings settings = new Settings(driver, webActionUtils);

		// Department Segmentation
		settings.clickSettingOption();
		settings.hoverOnDepartmentSettingsSubMenu();
		settings.clickonDepartmentDepartmentSegmentationOption();
		settings.verifyDepartmentSegmentationPage(DepartmentSegmentation);
		settings.verifyServiceTypeFieldIsAvailbleinDepartmentSettingsPage();

	}
}