
package com.automation.pi.test006.settings;

import java.lang.reflect.Method;

import org.testng.annotations.Test;

import com.automation.pi.library.BaseTest;
import com.automation.pi.pages.LoginPage;
import com.automation.pi.pages.Settings;

public class SettingsPage20 extends BaseTest 
{//26
	@Test
	public void PPR_8248_Test(Method m) 
	{
		webActionUtils.extentTestName(m.getName());
		webActionUtils.TestCaseinfo("================================");
		webActionUtils.TestCaseinfo(" Test Case Name : Configure Discrepancy Available : Add new record ");
		webActionUtils.TestCaseinfo("================================");
		
		String ConfigureDiscrepancyAvailable = "Configure Discrepancy Available - Policy Checking Platform";		
		LoginPage loginPage = new LoginPage(driver, webActionUtils);
		loginPage.enterUserName(USER_NAME);
		loginPage.enterPassword(PASSWORD);
		loginPage.clickOnSignin();

		Settings settings = new Settings(driver, webActionUtils);
		
		// Advanced Settings
		settings.clickSettingOption();
		settings.hoverOnAdvancedSettingsSubMenu();
		settings.verifyAdvancedSettingsOptionsList();

		// Configure Discrepancy Available
		settings.clickConfigureDiscrepancyAvailableOption();
		settings.verifyConfigureDiscrepancyAvailableSettingPage(ConfigureDiscrepancyAvailable);
		settings.verifyConfigureDiscrepancyAvailablePageElement();
		settings.clickOnAddbutton();
		settings.addNewRecordinDiscrepancyAvailable();
	}
}