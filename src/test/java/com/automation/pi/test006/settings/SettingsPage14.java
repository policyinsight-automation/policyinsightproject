package com.automation.pi.test006.settings;

import java.lang.reflect.Method;

import org.testng.annotations.Test;

import com.automation.pi.library.BaseTest;
import com.automation.pi.pages.LoginPage;
import com.automation.pi.pages.Settings;

public class SettingsPage14 extends BaseTest
{//07
	@Test
	public void PPR_7338_Test(Method m)
	{
		webActionUtils.extentTestName(m.getName());
		webActionUtils.TestCaseinfo("================================");
		webActionUtils.TestCaseinfo(" Test Case Name :Account Manager Setting - View sub settings pages");
		webActionUtils.TestCaseinfo("================================");
		String AccountManagerSetting = "ManageAccountManager - Policy Checking Platform";

		LoginPage loginPage = new LoginPage(driver, webActionUtils);
		loginPage.enterUserName(USER_NAME);
		loginPage.enterPassword(PASSWORD);
		loginPage.clickOnSignin();

		Settings settings = new Settings(driver, webActionUtils);

		// Account Manager Setting
		settings.clickSettingOption();
		settings.hoverOnAccountManagerSettingSubMenu();
		settings.verifyAccountManagerSettingsOptionsList();
		settings.clickAccountManagerSettingOption();
		settings.verifyAccountManagerSettingPage(AccountManagerSetting);
		settings.verifyAccountManagerSettingPageElement();
		webActionUtils.refreshPage();
	}
}