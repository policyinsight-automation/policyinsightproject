
package com.automation.pi.test006.settings;

import java.lang.reflect.Method;

import org.testng.annotations.Test;

import com.automation.pi.library.BaseTest;
import com.automation.pi.pages.LoginPage;
import com.automation.pi.pages.Settings;

public class SettingsPage34 extends BaseTest {
	@Test
	public void PPR_TC_665_Test(Method m) {
		webActionUtils.extentTestName(m.getName());
		webActionUtils.TestCaseinfo("================================");
		webActionUtils.TestCaseinfo(" Test Case Name :  Records dashboard  is created  below the 'Save' and 'Clear' button of Update PIS owner");
		webActionUtils.TestCaseinfo("================================");

		String title="UpdatePisOwners - Policy Checking Platform";
		
		LoginPage loginPage = new LoginPage(driver, webActionUtils);
		loginPage.enterUserName(USER_NAME);
		loginPage.enterPassword(PASSWORD);
		loginPage.clickOnSignin();

		Settings settings = new Settings(driver, webActionUtils);
		settings.clickSettingOption();
		settings.hoverOnAdhocPIRequestSettingSubMenu();
		settings.clickOnUpdatePISOwnersOption();
		settings.verifyUpdatePISOwners(title);
		settings.CheckUpdatePISOwnersRecordDisplayed();

	}
}