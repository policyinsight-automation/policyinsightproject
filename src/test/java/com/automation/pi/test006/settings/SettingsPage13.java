package com.automation.pi.test006.settings;

import java.lang.reflect.Method;

import org.testng.annotations.Test;

import com.automation.pi.library.BaseTest;
import com.automation.pi.pages.LoginPage;
import com.automation.pi.pages.Settings;

public class SettingsPage13 extends BaseTest
{//06
	@Test
	public void PPR_7337_Test(Method m) 
	{
		webActionUtils.extentTestName(m.getName());
		webActionUtils.TestCaseinfo("================================");
		webActionUtils.TestCaseinfo(" Test Case Name : Advanced Settings -View sub settings pages");
		webActionUtils.TestCaseinfo("================================");

		String ConfigureDiscrepancyAvailable = "Configure Discrepancy Available - Policy Checking Platform";
		String AdvancedSearch = "Advanced Search - Policy Checking Platform";
		String ClientEstimatedTime = "ManageTaskAssign - Policy Checking Platform";
		String CommunicationSetting = "ManageCommunicationSettings - Policy Checking Platform";
		String CoverageSectionSettings = "CoverageSectionSettings - Policy Checking Platform";
		String DepartmentOfficeCodeSetting = "Manage Department/Office Code Setting - Policy Checking Platform";
		String UploadFileAlternativeSetting = "Manage Upload File Alternative Setting - Policy Checking Platform";
		String FormLibrarySetting = "ManageFormLibrary - Policy Checking Platform";
		String  RushSetting = "Rush Setting - Policy Checking Platform";
		String  NoteAndMissingInfo = "Notes and Missing Info - Policy Checking Platform";

		
		LoginPage loginPage = new LoginPage(driver, webActionUtils);
		loginPage.enterUserName(USER_NAME);
		loginPage.enterPassword(PASSWORD);
		loginPage.clickOnSignin();

		Settings settings = new Settings(driver, webActionUtils);

		// Advanced Settings
		settings.clickSettingOption();
		settings.hoverOnAdvancedSettingsSubMenu();
		settings.verifyAdvancedSettingsOptionsList();

		// Configure Discrepancy Available
		settings.clickConfigureDiscrepancyAvailableOption();
		settings.verifyConfigureDiscrepancyAvailableSettingPage(ConfigureDiscrepancyAvailable);
		settings.verifyConfigureDiscrepancyAvailablePageElement();
		webActionUtils.refreshPage();

		// Advanced Search Checking Point Setting
		settings.clickSettingOption();
		settings.hoverOnAdvancedSettingsSubMenu();
		settings.clickAdvancedSearchOption();
		settings.verifyAdvancedSearchSettingPage(AdvancedSearch);
		settings.verifyAdvancedSearchCheckingPointSettingPageElement();
		webActionUtils.refreshPage();

		// Task Assignment Setting
		settings.clickSettingOption();
		settings.hoverOnAdvancedSettingsSubMenu();
		settings.clickClientEstimatedTimeOption();
		settings.verifyClientEstimatedTimeSettingPage(ClientEstimatedTime);
		settings.verifyClientEstimatedTime_TaskAssignmentSettingPageElement();
		webActionUtils.refreshPage();

		// Communication Setting
		settings.clickSettingOption();
		settings.hoverOnAdvancedSettingsSubMenu();
		settings.clickCommunicationSettingOption();
		settings.verifyCommunicationSettingPage(CommunicationSetting);
		settings.verifyCommunicationSettingPageElement();

		// Coverage Section Settings
		settings.clickSettingOption();
		settings.hoverOnAdvancedSettingsSubMenu();
		settings.clickCoverageSectionSettingsOption();
		settings.verifyCoverageSectionSettingPage(CoverageSectionSettings);
		settings.verifyCoverageSectionSettingsPageElement();
		settings.verifyCoverageSectionSettings1PageElement();
		settings.verifyCoverageSectionSettings2PageElement();
		settings.verifyCoverageSectionSettings3PageElement();
		webActionUtils.refreshPage();

		// DepartmentOfficeCodeSetting
		settings.clickSettingOption();
		settings.hoverOnAdvancedSettingsSubMenu();
		settings.clickDepartmentOfficeCodeSettingOption();
		settings.verifyDepartmentOfficeCodeSettingPage(DepartmentOfficeCodeSetting);
		settings.verifyDepartmentOfficeCodeSettingPageElement();
		webActionUtils.refreshPage();

		// UploadFileAlternativeSetting
		settings.clickSettingOption();
		settings.hoverOnAdvancedSettingsSubMenu();
		settings.clickUploadFileAlternativeSettingOption();
		settings.verifyUploadFileAlternativeSettingPage(UploadFileAlternativeSetting);
		settings.verifyUploadFileAlternativeSettingPageElement();
		webActionUtils.refreshPage();
		

		// FormLibrarySetting
		settings.clickSettingOption();
		settings.hoverOnAdvancedSettingsSubMenu();
		settings.clickFormLibrarySettingOption();
		settings.verifyFormLibrarySettingPage(FormLibrarySetting);
		settings.verifyFormLibrarySettingPageElement();
		webActionUtils.refreshPage();
		

		//  Rush Setting

		settings.clickSettingOption();
		settings.hoverOnAdvancedSettingsSubMenu();
		settings.clickRushSettingOption();
		settings.verifyRushSettingPage(RushSetting);
		settings.verifyRushSettingPageElement();
		webActionUtils.refreshPage();
			

		//  Note and Missing Info
		settings.clickSettingOption();
		settings.hoverOnAdvancedSettingsSubMenu();
		settings.clickNoteAndMissingInfoOption();
		settings.verifyNoteAndMissingInfoPage(NoteAndMissingInfo);
		settings.verifyNoteAndMissingInfoPageElement();
		webActionUtils.refreshPage();
		
		
	}
}
