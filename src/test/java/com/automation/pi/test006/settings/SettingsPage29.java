
package com.automation.pi.test006.settings;

import java.lang.reflect.Method;

import org.testng.annotations.Test;

import com.automation.pi.library.BaseTest;
import com.automation.pi.pages.LoginPage;
import com.automation.pi.pages.Settings;

public class SettingsPage29 extends BaseTest {
	@Test
	public void PPR_TC_1900_Test(Method m) {
		webActionUtils.extentTestName(m.getName());
		webActionUtils.TestCaseinfo("================================");
		webActionUtils.TestCaseinfo(
				" Test Case Name : Verify that user is able add new parameter/option to  'Checking Type' list box using 'Add to Checking Type' button ");
		webActionUtils.TestCaseinfo("================================");

		LoginPage loginPage = new LoginPage(driver, webActionUtils);
		loginPage.enterUserName(USER_NAME);
		loginPage.enterPassword(PASSWORD);
		loginPage.clickOnSignin();

		Settings settings = new Settings(driver, webActionUtils);
		settings.clickSettingOption();
		settings.hoverOnDepartmentSettingsSubMenu();
		settings.clickOnParameterSettingOption();
		settings.selectrequiredAccount("WA");
		settings.enterParameter();
		settings.clickOnAddtoCheckingTypeButton()	;
		settings.clickOnParameterSaveButton();
		settings.verifyParameterMessage();
	
	}
}