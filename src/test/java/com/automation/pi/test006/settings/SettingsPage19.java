
package com.automation.pi.test006.settings;

import java.lang.reflect.Method;

import org.testng.annotations.Test;

import com.automation.pi.library.BaseTest;
import com.automation.pi.pages.LoginPage;
import com.automation.pi.pages.Settings;

public class SettingsPage19 extends BaseTest 
{//12
	@Test
	public void PPR_7344_Test(Method m)
	{
		webActionUtils.extentTestName(m.getName());
		webActionUtils.TestCaseinfo("================================");
		webActionUtils.TestCaseinfo(" Test Case Name : IR Package Status - View sub settings pages");
		webActionUtils.TestCaseinfo("================================");
		
		String IRPackageStatus = "Package Status - Policy Checking Platform";

		LoginPage loginPage = new LoginPage(driver, webActionUtils);
		loginPage.enterUserName(USER_NAME);
		loginPage.enterPassword(PASSWORD);
		loginPage.clickOnSignin();

		Settings settings = new Settings(driver, webActionUtils);
		
		// IRPackageStatus
		settings.clickSettingOption();
		settings.hoverOnIRPackageStatusSubMenu();
		settings.verifyIRPackageStatusSettingsOptionsList();
		settings.clickIRPackageStatusOption();
		settings.verifyIRPackageStatusSettingPage(IRPackageStatus);
		settings.verifyIRPackageStatusSettingPageElement();
		webActionUtils.refreshPage();
	}
}