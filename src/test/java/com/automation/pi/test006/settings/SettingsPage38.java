package com.automation.pi.test006.settings;

import java.lang.reflect.Method;

import org.testng.annotations.Test;

import com.automation.pi.library.BaseTest;
import com.automation.pi.pages.LoginPage;
import com.automation.pi.pages.Settings;

public class SettingsPage38 extends BaseTest {
	@Test
	public void PIDC_TC_2073_Test(Method m) {

		webActionUtils.extentTestName(m.getName());
		webActionUtils.TestCaseinfo("================================");
		webActionUtils.TestCaseinfo(
				" Test Case Name : Verify tha 'Service Type' field is created in the Add page in Department Settings page.");
		webActionUtils.TestCaseinfo("================================");

		String DepartmentSetting = "ManageDepartment - Policy Checking Platform";

		LoginPage loginPage = new LoginPage(driver, webActionUtils);
		loginPage.enterUserName(USER_NAME);
		loginPage.enterPassword(PASSWORD);
		loginPage.clickOnSignin();

		Settings settings = new Settings(driver, webActionUtils);

		// DepartmentSetting
		settings.clickSettingOption();
		settings.hoverOnDepartmentSettingsSubMenu();
		settings.clickonDepartmentSettingsOption();
		settings.verifyDepartmentSettingPage(DepartmentSetting);
		settings.verifyDepartmentSettingPageElement();
		settings.clickOnAddbuttonDepartmentSettingsPage();
		settings.verifyServiceTypeFieldIsAvailbleinDepartmentSettingsPage();

	}
}