package com.automation.pi.test006.settings;

import java.lang.reflect.Method;

import org.testng.annotations.Test;

import com.automation.pi.library.BaseTest;
import com.automation.pi.pages.LoginPage;
import com.automation.pi.pages.Settings;

public class SettingsPage15 extends BaseTest
{//08
	@Test
	public void PPR_7339_Test(Method m) {

		
		webActionUtils.extentTestName(m.getName());
		webActionUtils.TestCaseinfo("================================");
		webActionUtils.TestCaseinfo(" Test Case Name : Ad-hoc PI Request Setting - View sub settings pages");
		webActionUtils.TestCaseinfo("================================");

		String ChangeDatetoClientPolicy = "Change Date to Client & Policy Status - Policy Checking Platform";
		String AddFBEEmployee = "Add FBE Employee - Policy Checking Platform";
		String ChangeChecklistTitle = "Change CheckList title - Policy Checking Platform";
		String AMPReports="AMP Reports - Policy Checking Platform";
		String PolicyList="Policy List - Policy Checking Platform";
		String AssignEmployeeLocation="Assign Employee Location - Policy Checking Platform";
		String FormDiscrepancyReport="Form Discrepancy Report - Policy Checking Platform";
		String CommunicationReport="CommunicationReport - Policy Checking Platform";
		String RushReport="Rush Report - Policy Checking Platform";
		String UpdatePISOwners ="UpdatePisOwners - Policy Checking Platform";


		LoginPage loginPage = new LoginPage(driver, webActionUtils);
		loginPage.enterUserName(USER_NAME);
		loginPage.enterPassword(PASSWORD);
		loginPage.clickOnSignin();

		Settings settings = new Settings(driver, webActionUtils);

		// Ad-hoc PI Request Setting
		settings.clickSettingOption();
		settings.hoverOnAdhocPIRequestSettingSubMenu();
		settings.verifyAdhocPIRequestSettingsOptionsList();

		// ChangeDatetoClientPolicy
		settings.clickChangeDatetoClientPolicyStatusOption();
		settings.verifyChangeDatetoClientPolicyStatusSettingPage(ChangeDatetoClientPolicy);
		settings.verifyChangeDatetoClientPolicyStatusPageElement();
		webActionUtils.refreshPage();

		// AddFBEEmployee
		settings.clickSettingOption();
		settings.hoverOnAdhocPIRequestSettingSubMenu();
		settings.clickAddFBEEmployeeOption();
		settings.verifyAddFBEEmployeeSettingPage(AddFBEEmployee);
		settings.verifyAddFBEEmployeePageElement();
		webActionUtils.refreshPage();

		// ChangeChecklistTitle
		settings.clickSettingOption();
		settings.hoverOnAdhocPIRequestSettingSubMenu();
		settings.clickChangeChecklistTitleOption();
		settings.verifyChangeChecklistTitleSettingPage(ChangeChecklistTitle);
		settings.verifyChangeChecklistTitleSettingPageElement();
		webActionUtils.refreshPage();

		//AMP Reports
		settings.clickSettingOption();
		settings.hoverOnAdhocPIRequestSettingSubMenu();
		settings.clickAMPReportsOption();
		settings.verifyAMPReportsSettingPage(AMPReports);
		settings.verifyAMPReportsSettingPageElement();
		webActionUtils.refreshPage();

		// PolicyListTitle
		settings.clickSettingOption();
		settings.hoverOnAdhocPIRequestSettingSubMenu();
		settings.clickPolicyListOption();
		settings.verifyPolicyListSettingPage(PolicyList);
		settings.verifyPolicyListSettingPageElement();
		webActionUtils.refreshPage();

		// Assign Employee Location
		settings.clickSettingOption();
		settings.hoverOnAdhocPIRequestSettingSubMenu();
		settings.AssignEmployeeLocationOption();
		settings.verifyAssignEmployeeLocationSettingPage(AssignEmployeeLocation);
		settings.verifyAssignEmployeeLocationSettingPageElement();
		webActionUtils.refreshPage();

		// Form Discrepancy Report
		settings.clickSettingOption();
		settings.hoverOnAdhocPIRequestSettingSubMenu();
		settings.clickFormDiscrepancyReportOption();
		settings.verifyFormDiscrepancyReportSettingPage(FormDiscrepancyReport);
		settings.verifyFormDiscrepancyReportSettingPageElement();
		webActionUtils.refreshPage();
		
		
		// Communication Report
		settings.clickSettingOption();
		settings.hoverOnAdhocPIRequestSettingSubMenu();
		settings.clickCommunicationReportOption();
		settings.verifyCommunicationReportPage(CommunicationReport);
		settings.verifyCommunicationReportPageElement();
		webActionUtils.refreshPage();
		
		
		//Rush Report
		settings.clickSettingOption();
		settings.hoverOnAdhocPIRequestSettingSubMenu();
		settings.clickRushReportOption();
		settings.verifyRushReportgPage(RushReport);
		settings.verifyRushReportPageElement();
		webActionUtils.refreshPage();
		
		//Update PIS Owners
		settings.clickSettingOption();
		settings.hoverOnAdhocPIRequestSettingSubMenu();
		settings.clickUpdatePISOwnersOption();
		settings.verifyUpdatePISOwnersPage(UpdatePISOwners);
		settings.verifyUpdatePISOwnersPageElement();
		webActionUtils.refreshPage();
	}
}