
package com.automation.pi.test006.settings;

import java.lang.reflect.Method;

import org.testng.annotations.Test;

import com.automation.pi.library.BaseTest;
import com.automation.pi.pages.LoginPage;
import com.automation.pi.pages.Settings;

public class SettingsPage35 extends BaseTest {


		@Test
		public void PIDC_TC_2070_Test(Method m) 
	{
	//Verify that all special characters are allowed except ";" in parameter settings page
		webActionUtils.extentTestName(m.getName());
		webActionUtils.TestCaseinfo("================================");
		webActionUtils.TestCaseinfo(" Test Case Name :Verify that all special characters are allowed except \";\" in parameter settings page");		webActionUtils.TestCaseinfo("================================");
		webActionUtils.TestCaseinfo("================================");

		LoginPage loginPage = new LoginPage(driver, webActionUtils);
		loginPage.enterUserName(USER_NAME);
		loginPage.enterPassword(PASSWORD);
		loginPage.clickOnSignin();

		Settings settings = new Settings(driver, webActionUtils);
		settings.clickSettingOption();
		settings.hoverOnDepartmentSettingsSubMenu();
		settings.clickOnParameterSettingOption();
		settings.selectrequiredAccount("WA");
		settings.checkSemicolonisNotAllowedParameter();
		settings.checkAllowedSplCharParameter();

	}
}