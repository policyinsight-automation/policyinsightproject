package com.automation.pi.test005.reports;

import java.lang.reflect.Method;

import org.testng.annotations.Test;

import com.automation.pi.library.BaseTest;
import com.automation.pi.pages.LoginPage;
import com.automation.pi.pages.ReportsPage;

public class ReportsPage03 extends BaseTest
{//807
	@Test
	public void PPR_8323_Test(Method m)
	{
		webActionUtils.extentTestName(m.getName());
		webActionUtils.TestCaseinfo("================================");
		webActionUtils.TestCaseinfo(" Test Case Name : Reports - Navigate to 'Policy Insights Work Summary'");
		webActionUtils.TestCaseinfo("================================");

		String PolicyInsightWorkSummaryTitle = excelLibrary.getExcelData(XL_TESTPATH, "SmokeTestSheet", 24, 1);
		
		LoginPage loginPage = new LoginPage(driver, webActionUtils);
		loginPage.enterUserName(USER_NAME);
		loginPage.enterPassword(PASSWORD);
		loginPage.clickOnSignin();

		ReportsPage reportsPage = new ReportsPage(driver, webActionUtils);
		reportsPage.clickReportsTab();
		reportsPage.clickPolicyInsightWorkSummaryOption();
		reportsPage.verifyPolicyInsightWorkSummaryPage(PolicyInsightWorkSummaryTitle);
	}
}