package com.automation.pi.test005.reports;

import java.lang.reflect.Method;

import org.testng.annotations.Test;

import com.automation.pi.library.BaseTest;
import com.automation.pi.pages.LoginPage;
import com.automation.pi.pages.ReportsPage;

public class ReportsPage01 extends BaseTest
{//805
	@Test
	public void PPR_8321_Test(Method m)
	{
		webActionUtils.extentTestName(m.getName());
		webActionUtils.TestCaseinfo("================================");
		webActionUtils.TestCaseinfo(" Test Case Name : Reports - Options Order Verification  ");
		webActionUtils.TestCaseinfo("================================");

		String PISTimeSheet = "PIS Time Sheet";
		String PolicyInsightsWorkSummaryReport = "Policy Insights Work Summary";
		String PolicyInsightsReport = "Policy Insights Reports";
		String PolicyInsightsGetCovergeSectionsReport = "Policy Insights Coverage sections Reports";
		String PolicyInsightsDataCaptureBLRTeamReport = "Policy Insights Data Capture BLR Team Reports";
		String PolicyStatusReport = "Policy Status Report";
		String DiscrepancyReport = "Discrepancy Report";
		String ExceptionReport = "Exception Report";

		LoginPage loginPage = new LoginPage(driver, webActionUtils);
		loginPage.enterUserName(USER_NAME);
		loginPage.enterPassword(PASSWORD);
		loginPage.clickOnSignin();

		ReportsPage reportsPage = new ReportsPage(driver, webActionUtils);
		reportsPage.clickReportsTab();
		reportsPage.verifyReportsPageListinGivenOrder(PISTimeSheet, PolicyInsightsWorkSummaryReport,
													  PolicyInsightsReport, PolicyInsightsGetCovergeSectionsReport, 
													  PolicyInsightsDataCaptureBLRTeamReport,PolicyStatusReport, 
													  DiscrepancyReport, ExceptionReport);
	}
}