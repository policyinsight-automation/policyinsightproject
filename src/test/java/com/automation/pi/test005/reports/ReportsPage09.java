package com.automation.pi.test005.reports;

import java.lang.reflect.Method;

import org.testng.annotations.Test;

import com.automation.pi.library.BaseTest;
import com.automation.pi.pages.LoginPage;
import com.automation.pi.pages.ReportsPage;

public class ReportsPage09 extends BaseTest
{//813
	@Test
	public void PPR_8329_Test(Method m)
	{
		webActionUtils.extentTestName(m.getName());
		webActionUtils.TestCaseinfo("================================");
		webActionUtils.TestCaseinfo(" Test Case Name : Reports - Navigate to 'Exception Report'");
		webActionUtils.TestCaseinfo("================================");

		String ExceptionReportTitle = excelLibrary.getExcelData(XL_TESTPATH, "SmokeTestSheet", 30, 1);

	
		LoginPage loginPage = new LoginPage(driver, webActionUtils);
		loginPage.enterUserName(USER_NAME);
		loginPage.enterPassword(PASSWORD);
		loginPage.clickOnSignin();

		ReportsPage reportsPage = new ReportsPage(driver, webActionUtils);
		reportsPage.clickReportsTab();
		reportsPage.clickExceptionReportOption();
		reportsPage.verifyExceptionReportPage(ExceptionReportTitle);
	}
}