package com.automation.pi.test005.reports;

import java.lang.reflect.Method;

import org.testng.annotations.Test;

import com.automation.pi.library.BaseTest;
import com.automation.pi.pages.LoginPage;
import com.automation.pi.pages.ReportsPage;

public class ReportsPage11 extends BaseTest 
{//767
	@Test
	public void PPR_4439_Test(Method m)
	{
		webActionUtils.extentTestName(m.getName());
		webActionUtils.TestCaseinfo("================================");
		webActionUtils.TestCaseinfo(" Test Case Name :Reports - Policy Insight Work Summary - Generate function'");
		webActionUtils.TestCaseinfo("================================");

		String PolicyInsightWorkSummaryTitle = excelLibrary.getExcelData(XL_TESTPATH, "SmokeTestSheet", 24, 1);
		String PolicyPlusWorkSummaryTXT="Policy Plus Work Summary";
		
		LoginPage loginPage = new LoginPage(driver, webActionUtils);
		loginPage.enterUserName(USER_NAME);
		loginPage.enterPassword(PASSWORD);
		loginPage.clickOnSignin();

		ReportsPage reportsPage = new ReportsPage(driver, webActionUtils);
		reportsPage.clickReportsTab();
		reportsPage.clickPolicyInsightWorkSummaryOption();
		reportsPage.verifyPolicyInsightWorkSummaryPage(PolicyInsightWorkSummaryTitle);
		reportsPage.selectRequiredAccount();
		reportsPage.clickOnGenerateButton();
		reportsPage.verifyPolicyWorkSummaryTabel(PolicyPlusWorkSummaryTXT);
	}
}