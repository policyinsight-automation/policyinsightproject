package com.automation.pi.test005.reports;

import java.lang.reflect.Method;

import org.testng.annotations.Test;

import com.automation.pi.library.BaseTest;
import com.automation.pi.pages.LoginPage;
import com.automation.pi.pages.ReportsPage;

public class ReportsPage10 extends BaseTest 
{//766
	@Test
	public void PPR_4437_Test(Method m)
	{
		webActionUtils.extentTestName(m.getName());
		webActionUtils.TestCaseinfo("================================");
		webActionUtils.TestCaseinfo(" Test Case Name : Reports - PIS Time Sheet - Generate function'");
		webActionUtils.TestCaseinfo("================================");

		String PISTimeSheetTitle = excelLibrary.getExcelData(XL_TESTPATH, "SmokeTestSheet", 23, 1);
		String PoicyCheckingTimesheetTXT = "Policy Checking Timesheet";

		LoginPage loginPage = new LoginPage(driver, webActionUtils);
		loginPage.enterUserName(USER_NAME);
		loginPage.enterPassword(PASSWORD);
		loginPage.clickOnSignin();

		ReportsPage reportsPage = new ReportsPage(driver, webActionUtils);
		reportsPage.clickReportsTab();
		reportsPage.clickPISTimeSheetOptions();
		reportsPage.verifyPISTimeSheetPage(PISTimeSheetTitle);
		reportsPage.clickOnGenerateButton();
		reportsPage.verifyPolicyCheckingTimesheetTabel(PoicyCheckingTimesheetTXT);
	}
}