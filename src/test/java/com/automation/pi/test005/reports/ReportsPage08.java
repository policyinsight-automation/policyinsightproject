package com.automation.pi.test005.reports;

import java.lang.reflect.Method;

import org.testng.annotations.Test;

import com.automation.pi.library.BaseTest;
import com.automation.pi.pages.LoginPage;
import com.automation.pi.pages.ReportsPage;

public class ReportsPage08 extends BaseTest
{//812
	@Test
	public void PPR_8328_Test(Method m) 
	{
		webActionUtils.extentTestName(m.getName());
		webActionUtils.TestCaseinfo("================================");
		webActionUtils.TestCaseinfo(" Test Case Name : Reports - Navigate to 'Discrepancy Report'");
		webActionUtils.TestCaseinfo("===============================");

		String DiscrepancyReportTitle = excelLibrary.getExcelData(XL_TESTPATH, "SmokeTestSheet", 29, 1);
		
		LoginPage loginPage = new LoginPage(driver, webActionUtils);
		loginPage.enterUserName(USER_NAME);
		loginPage.enterPassword(PASSWORD);
		loginPage.clickOnSignin();

		ReportsPage reportsPage = new ReportsPage(driver, webActionUtils);
		reportsPage.clickReportsTab();
		reportsPage.clickDiscrepancyReportOption();
		reportsPage.verifyDiscrepancyReportPage(DiscrepancyReportTitle);
	}
}